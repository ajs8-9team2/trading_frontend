import {TStatusAccRequest, TStatusTransferDeposit,TStatusStock} from "../types/reestrOfAlias";

// "Pending" | "WorkInProgress" | "Approved" | "Declined"
// "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
// "Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted"
export const getStatus = (status: TStatusTransferDeposit | TStatusAccRequest | TStatusStock): string=>{
    let innerStr = ""
    switch(status){
        case "Pending": innerStr = "поступило новое заявление";
            break;
        case "FrontWIP":
        case "WorkInProgress":
            innerStr = "на рассмотрении у сотрудника фронт-офиса";
            break;
        case "AccountantWIP": innerStr = "на рассмотрении у бухгалтера";
            break;
        case "TraderWIP": innerStr = "на рассмотрении у трейдера";
            break;
        case "Approved": innerStr = "заявление одобрено сотрудником фронт-офиса";
            break;
        case "Completed":innerStr = "заявление одобрено бухгалтером";
            break;
        case "PartiallyCompleted":innerStr = "заявление одобрено трейдером";
            break;
        case "Declined": innerStr = "заявление отклонено";
            break;
        default : innerStr = ""
    }
    return innerStr
}
