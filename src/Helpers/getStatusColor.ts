export const getStatusColor = (status:string): string=>{
    let color = "default";
    if (status === "Approved") {
        color = "gold";
        return color;
    } else {
        color = "processing";
        return color;      
    }
}