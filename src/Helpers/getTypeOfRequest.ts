// import {TNoticeObj} from "../types/typesNotification";
// export const getTypeOfRequest = (typeOfRequest: Pick<TNoticeObj,"typeOfRequest">): string=>{
export const getTypeOfRequest = (typeOfRequest: "accRequest" | "depositRequest" | "transferRequest" | "stockRequest" ): string=>{
    // const ss:Pick<TNoticeObj,"typeOfRequest"> = {typeOfRequest:"accRequest"}
    // const ss:keyof TNoticeObj = "typeOfRequest"
    let innerStr = ""
    switch(typeOfRequest){
        case "accRequest":
            innerStr = "ID клиентского заявления";
            break;
        case "depositRequest":
            innerStr = "ID заявления на пополнение счета";
            break;
        case "transferRequest":
            innerStr = "ID заявления на перевод денежных средств";
            break;
            // "accRequest" | "depositRequest" | "transferRequest"| "stockRequest",
        case "stockRequest":
            innerStr = "ID заявления по операциям с акциями";
            break;
        default :
            innerStr = ""
    }
    return innerStr
}

