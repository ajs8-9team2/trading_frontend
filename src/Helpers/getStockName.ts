export const stocksList = ["Alibaba ADR", "Amazon", "AMD", "Apple", "Coca-Cola", "Halyk Bank AO", "Intel", "Kaspi.kz AO", "Kcell AO", "Microsoft", "Netflix", "NK KazMunayGaz AO", "NVIDIA", "Procter&Gamble", "Tesla" ];

export const getStockName = (compareStr: string, getStockOrTicker: string): string => {
    const arrayTicker = [
        ["BABA", "Alibaba ADR"],
        ["AMZN", "Amazon"],
        ["AMD", "AMD"],
        ["AAPL", "Apple"],
        ["KO", "Coca-Cola"],
        ["HSBK", "Halyk Bank AO"],
        ["INTC", "Intel"],
        ["KSPI", "Kaspi.kz AO"],
        ["KCEL", "Kcell AO"],
        ["MSFT", "Microsoft"],
        ["NFLX", "Netflix"],
        ["KMGZ", "NK KazMunayGaz AO"],
        ["NVDA", "NVIDIA"],
        ["PG", "Procter&Gamble"],
        ["TSLA", "Tesla"]
    ]

    for (const [Ticker, Company] of arrayTicker) {
        if (getStockOrTicker === "COMPANY") {
            if (Ticker === compareStr) {
                return Company;
            }
        } else {
            if (Company === compareStr) {
                return Ticker;
            }
        }
    }
    return "";
};