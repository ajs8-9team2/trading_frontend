// const rowsArrayTableStr = [
//     ["_id", "ID заявления"],
//     ["date", "дата созд."],
//     ["assignedTo", "ID менеджера"],
//     ["status", "Статус"],
//     ["firstName", "Имя"],
//     ["lastName", "Фамилия"],
//     ["patronym", "Отчество"],
//     ["dateOfBirth", "Д/Р"],
//     ["identificationNumber", "ИИН"]
// ];
import {getDate} from "./getDate";
import {getStatus} from "./getStatus";
import {cutLength} from "./cutLength";

export const getValueOfKey = (key: string, itemObj: any): string | undefined=>{

    switch(key){
        case undefined:
            return undefined;
        case "date":
            return getDate(itemObj[key]);
        case "amount":
            return ""+itemObj[key].toLocaleString('ru');
        case "status":
            return getStatus(itemObj[key]);
        case "rejectionReason":
            return cutLength(itemObj[key],10);
        case "_id":
        case "assignedTo":
        case "frontManager":
            const innerStr = (itemObj[key]) ? itemObj[key].toUpperCase() : "none"
            return innerStr;
        default :
            return itemObj[key]
    }
}

