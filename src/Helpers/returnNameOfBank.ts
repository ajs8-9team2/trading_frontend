// EXAMPLE
// console.log( returnNameOfBank("HCSKKZKA","BANK") )
// console.log( returnNameOfBank("ДБ АО 'Home Credit Bank'","BIC") )

export const  banks = ["АО «Жилищный строительный сберегательный банк «Отбасы банк»", "АО «Дочерний банк «Казахстан - Зираат интернешнл банк»", "АО «Altyn Bank» (ДБ China Citic Bank Corporation Ltd)", "АО «Торгово-промышленный Банк Китая в Алматы»", "АО «Народный сберегательный банк Казахстана»", "АО ДБ «Национальный Банк Пакистана»", "АО «Банк Фридом Финанс Казахстан»", "АО «First Heartland Jýsan Bank»", "АО ДБ «Банк Китая в Казахстане»", "АО «Исламский банк «Заман-Банк»", "АО «Исламский Банк «Al-Hilal»", "АО «Шинхан Банк Казахстан»", "ДО АО Банк ВТБ (Казахстан)", "ДБ АО 'Home Credit Bank'", "АО «Ситибанк Казахстан»", "АО ДБ «Есо Center Bank»", "АО «Банк ЦентрКредит»", "АО «Евразийский Банк»", "АО «Банк «Bank RBK»", "АО «Bereke Bank»", "АО «Kaspi Bank»", "АО «ForteBank»", "АО «Нурбанк»"]


export const returnNameOfBank = (compareStr: string, getBIKOrBANK:string): string=>{
    const arrayBIC = [
        ["HCSKKZKA", "АО «Жилищный строительный сберегательный банк «Отбасы банк»"],
        ["KZIBKZKA", "АО «Дочерний банк «Казахстан - Зираат интернешнл банк»"],
        ["ATYNKZKA", "АО «Altyn Bank» (ДБ China Citic Bank Corporation Ltd)"],
        ["ICBKKZKX", "АО «Торгово-промышленный Банк Китая в Алматы»"],
        ["HSBKKZKX", "АО «Народный сберегательный банк Казахстана»"],
        ["NBPAKZKA", "АО ДБ «Национальный Банк Пакистана»"],
        ["KSNVKZKA", "АО «Банк Фридом Финанс Казахстан»"],
        ["TSESKZKA", "АО «First Heartland Jýsan Bank»"],
        ["BKCHKZKA", "АО ДБ «Банк Китая в Казахстане»"],
        ["ZAJSKZ22", "АО «Исламский банк «Заман-Банк»"],
        ["HLALKZKZ", "АО «Исламский Банк «Al-Hilal»"],
        ["SHBKKZKA", "АО «Шинхан Банк Казахстан»"],
        ["VTBAKZKZ", "ДО АО Банк ВТБ (Казахстан)"],
        ["INLMKZKA", "ДБ АО 'Home Credit Bank'"],
        ["CITIKZKA", "АО «Ситибанк Казахстан»"],
        ["ALFAKZKA", "АО ДБ «Есо Center Bank»"],
        ["KCJBKZKX", "АО «Банк ЦентрКредит»"],
        ["EURIKZKA", "АО «Евразийский Банк»"],
        ["KINCKZKA", "АО «Банк «Bank RBK»"],
        ["BRKEKZKA", "АО «Bereke Bank»"],
        ["CASPKZKA", "АО «Kaspi Bank»"],
        ["IRTYKZKA", "АО «ForteBank»"],
        ["NURSKZKX", "АО «Нурбанк»"],
    ]

    for(const [BIK, Bank] of arrayBIC){
        if(getBIKOrBANK === "BANK"){
            if(BIK === compareStr)
                return Bank
        }else{
            if(Bank === compareStr)
                return BIK
        }
    }
    return " "
}
