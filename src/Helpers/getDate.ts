export const getDate = (strDate:string): string=>{
    const innerRegexTest = new RegExp(/^\d{4}(-\d{2}){2}\w(\d{2}:){2}\d{2}\.\d{3}\w$/)

    if(innerRegexTest.test(strDate)){
        const myDate = new Date(strDate)
        const innerStrDate = ("0" + myDate.getDate());
        const innerStrMonth = ("0" + (myDate.getMonth() + 1))

        return innerStrDate.slice(innerStrDate.length - 2, innerStrDate.length) + "." +
            innerStrMonth.slice(innerStrMonth.length - 2, innerStrMonth.length) + "." +
            myDate.getFullYear();
    }else
        return "dd.mm.yyyy";

    // if(myDate){
    //     const strDate = ("0" + myDate.getDate());
    //     const strMonth = ("0" + (myDate.getMonth() + 1))
    //
    //     return strDate.slice(strDate.length - 2, strDate.length) + "." +
    //         strMonth.slice(strMonth.length - 2, strMonth.length) + "." +
    //         myDate.getFullYear();
    // }else{
    //     return "dd.mm.yyyy";
    // }
}