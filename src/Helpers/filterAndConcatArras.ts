import {TDepositRequest, TGetPostDepositRequestAT} from "../types/typesDepositRequest";
import {TGetAccRequestsAT} from "../types/typesAccReq";
import {TTotalPickTable} from "../containers/FrontOfficeModern/DepositRequest/FrontOfficeDepositListReq";
import {TIdentificationNumber, TSenderFullName} from "../types/reestrOfAlias";
// console.clear()
// console.log("----------------------")
// export async function filterAndConcatArras (
export const filterAndConcatArras = (
    allDepositsRequest: TGetPostDepositRequestAT[],
    allAccRequests: TGetAccRequestsAT[],
    keyArrayDepReq: string[],
    keyArrayAccReq: string[],
    statusFilter:string[],
    // ): Promise<TTotalPickTable[]>{
): TTotalPickTable[]=>{
    // let innerArray: any = [];
    let concatArray: TTotalPickTable[] = [];
    let iterObj: { [key: string]: any } = {};
    let myObj: any = {};

    // console.log(allDepositsRequest.length)
    for(let i1 = 0; i1 < allDepositsRequest.length; i1++){
        const item1 = allDepositsRequest[i1];
        if(statusFilter.includes(item1?.status!))continue;
        // console.log("------------------------------------------------")
        iterObj = {};
        myObj = {};
        iterObj = {...item1}
        // сбор ключей первого массива
        if(1)
            for(const [key, value] of Object.entries(iterObj)){
                if(!keyArrayDepReq.includes(key)) continue // <<<

                if(typeof value === "string" || typeof value === "number"){
                    // console.log(key,"\t",value)
                    myObj[key] = value;
                }else if(
                    Object.prototype.toString.call(value).includes("Null") === false &&
                    typeof value === "object"){
                    // console.log(key,"\t",Object.values(value)[0])
                    myObj[key] = Object.values(value)[0];
                }
            }

        // сбор ключей второго массива
        if(1)
            for(let i2 = 0; i2 < allAccRequests.length; i2++){
                let isFirstName = false;
                let islastname = false;
                let isPatronym = false;
                const item2 = allAccRequests[i2];
                // console.log(item2)
                let innerStr = ""

                if(item1?.user?._id !== item2?.user?._id) continue
                iterObj = {};
                iterObj = {...item2}

                for(const [key, value] of Object.entries(iterObj)){
                    if(key === "firstName")
                        isFirstName = true
                    if(key === "lastName")
                        islastname = true;
                    if(key === "patronym")
                        isPatronym = true
                }
                // console.log(isFirstName + " " + islastname + " " + isPatronym)

                for(const [key, value] of Object.entries(iterObj)){
                    if(!keyArrayAccReq.includes(key)) continue // <<<

                    if(typeof value === "string" || typeof value === "number"){

                        if(key !== "firstName" && key !== "lastName" && key !== "patronym"){
                            // console.log(key)
                            myObj[key] = value;
                        }else if(isFirstName && islastname && isPatronym){
                            innerStr = innerStr + (" " + value);
                            // console.log(key, "\t", value)
                            // console.log(myObj.getterFullName)
                            if(key === "patronym"){
                                myObj.getterFullName = innerStr.trim();
                            }
                        }else if(isFirstName && islastname && !isPatronym){
                            innerStr = innerStr + (" " + value);
                            // console.log(key, "\t", value)
                            // console.log(myObj.getterFullName)
                            if(key === "lastName"){
                                myObj.getterFullName = innerStr.trim();
                            }
                        }

                    }else if(Object.prototype.toString.call(value).includes("Null") === false &&
                        typeof value === "object"){
                        // console.log(key,"\t",Object.values(value)[0])
                        myObj[key] = Object.values(value)[0];
                    }
                }
            }
        concatArray.push(myObj)
    }
    // console.log(concatArray)
    return concatArray;
}