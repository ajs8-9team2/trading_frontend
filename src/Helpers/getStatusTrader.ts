export const getStatusTrader = (status:string | undefined ): any =>{
    if (status === "Approved") {
        return "В ожидании"
    } else if (status === "TraderWIP" || status === "AccountantWIP") {
        return "В процессе"
    }
}