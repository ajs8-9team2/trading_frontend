import {TNoticeObj} from "../types/typesNotification";

export const notificationObject = (
    requestID: string,
    userID: string,
    status:"TraderWIP"|"PartiallyCompleted"|"Pending"|"FrontWIP"|"AccountantWIP"|"Approved"|"Declined"|"Completed",
    typeOfRequest: "accRequest" | "depositRequest" | "transferRequest"| "stockRequest",
    notice: string
)=>{
    // const innerObj: Pick<TNoticeObj,"requestID"|"type"|"userID"|"status"|"notice"|"typeOfRequest">  =  {
    const innerObj: Omit<TNoticeObj, "isWasRead"> = {
        requestID,
        userID,
        status,
        typeOfRequest,
        notice,
        type: "MESSAGE_CREATED"
        // isWasRead: false,
    }

    return {
        // "accRequest" | "depositRequest" | "transferRequest",
        type: `notification/patchNotification/${typeOfRequest}`,
        payload: innerObj
    }
}