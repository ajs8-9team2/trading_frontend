// EXAMPLE
// console.log(cutLength("aza_aza_aza_aza_aza_aza_aza_aza_aza_aza", 10))

export function cutLength(str: string, maxLen: number): string{
    if(str.length <= maxLen){
        return str
    }else{
        return str.substring(0, maxLen).concat('...');
    }
}

