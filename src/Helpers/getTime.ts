export const getTime = (strDate: string): string=>{
    const innerRegexTest = new RegExp(/^\d{4}(-\d{2}){2}\w(\d{2}:){2}\d{2}\.\d{3}\w$/)
    if(innerRegexTest.test(strDate)){
        const myDate = new Date(strDate)
        const strHour = ("0" + myDate.getHours());
        const strMin = ("0" + myDate.getMinutes());

        return strHour.slice(strHour.length - 2, strHour.length) + ":" +
            strMin.slice(strMin.length - 2, strMin.length)
    }else{
        return "hh:mm";
    }
}