import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import axios from "../../axiosApi";
import {TAccountState, TGetPostAccountAT/*, TPatchAccountAT*/} from "../../types/typesAccount";
import {TGetAccRequestsAT, TPatchAccRequestsAT} from "../../types/typesAccReq";
import {patchAccRequestAT} from "./accRequestSlice";
// import {TNoticeObj} from "../../types/reestrOfAlias";


const initialState: TAccountState = {
    oneAccount: undefined,
    allAccount: [],
    accountStatus: null,
    error: null,
    // stateBell:0,
    // isRing:false,
}

// получить сразу все счета
export const fetchAccountAT = createAsyncThunk<TGetPostAccountAT[], undefined, { rejectValue: string | null }>(
    'account/fetchAccount',
    async function(_, {rejectWithValue}){
        // alert("11111111111111111")
        try{
            const response = await axios
                .get(`/accounts`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

export const getAccountAT = createAsyncThunk<TGetPostAccountAT, string, { rejectValue: string | null }>(
    'account/getAccount',
    async function(id, {rejectWithValue}){
        // alert(JSON.stringify(id, null, 2))
        try{
            const response = await axios
                .get(`/accounts/client?clientid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

// Получить один счет по id
// Добавлено 19.12.22  Виктория

export const fetchOneAccountAT = createAsyncThunk<TGetPostAccountAT, string | number, { rejectValue: string | null }>(
    'account/fetchOneAccount',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/accounts/${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

//http://localhost:8000/accounts?accreqid=637f3266fe62e19d447f07ea
export const createAccountAT = createAsyncThunk<TGetPostAccountAT, string, { rejectValue: string | null }>(
    'account/createAccount',
    async function(id, {rejectWithValue}){
        // alert(JSON.stringify(id, null, 2))
        try{
            const response = await axios
                .post(`/accounts?accreqid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

// Изменить account по id заявки
/** PATCH http://localhost:8000/accounts/63808b606dc61e6b36084d9d */
// export const patchAccountAT = createAsyncThunk<TGetPostAccountAT, TPatchAccountAT, {
//     rejectValue: string | null //, state: { reducerAccRequests: TAccRequestsState }
// }>(
//     'account/patchAccount',
//     async function(obj, {rejectWithValue, getState}){
//         // alert(JSON.stringify(obj, null, 2))
//         const innerObj: TPatchAccountAT = obj;
//
//         try{
//             const response = await axios
//                 .patch(`/accounts/${obj._idAccount}`, innerObj?.userNotifications)
//                 .then(res=>{
//                     return res.data
//                 });
//             return response;
//         }catch(error: any){
//             return rejectWithValue(error.message);
//         }
//     }
// );

const setPending = (state: TAccountState)=>{
    state.accountStatus = 'loading';
    state.error = null;
}

const setError = (state: TAccountState, action: any)=>{
    state.accountStatus = 'rejected';
    state.error = action.payload;
}

const accountSlice = createSlice({
    name: 'account',
    initialState,
    reducers: {
        // setNewObj:((state:TAccountState, action:PayloadAction<TNoticeObj>)=>{ //,action){
        //     state.oneAccount?.userNotifications?.push(action.payload)
        // }),
        // setFlagRing:((state:TAccountState, action:PayloadAction<boolean>)=>{ //,action){
        //     state.isRing = action.payload;
        // }),
        // setRingState:((state:TAccountState, action:PayloadAction<0|1|2>)=>{ //,action){
        //     state.stateBell = action.payload;
        // }),
        // countOfNotification (state:TAccountState,action:PayloadAction<number>){ //,action){
        //     // alert(JSON.stringify(action.payload, null, 2))
        //     state.countNotice = action.payload;
        //     // state.user = undefined;
        //     // state.userStatus = null;
        //     // state.error = null;
        // },
        // addDishes:((state:TAccountState,action:PayloadAction<number>)=>{
        //     // alert(JSON.stringify(action, null, 2))
        //     state.countNotice = action.payload;
        // }),
    },
    extraReducers: (builder)=>{
        //---------------------------------------------------
        builder.addCase(fetchAccountAT.pending, setPending);
        builder.addCase(fetchAccountAT.fulfilled, (state, action)=>{
            state.accountStatus = 'resolved';
            state.allAccount = action.payload;
        });
        builder.addCase(fetchAccountAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(getAccountAT.pending, setPending);
        builder.addCase(getAccountAT.fulfilled, (state, action)=>{
            state.accountStatus = 'resolved';
            state.oneAccount = action.payload;
        });
        builder.addCase(getAccountAT.rejected, setError);
        //---------------------------------------------------  Добавлено 19.12.22  Виктория 
        builder.addCase(fetchOneAccountAT.pending, setPending);
        builder.addCase(fetchOneAccountAT.fulfilled, (state, action)=>{
            state.accountStatus = 'resolved';
            state.oneAccount = action.payload;
        });
        builder.addCase(fetchOneAccountAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(createAccountAT.pending, setPending);
        builder.addCase(createAccountAT.fulfilled, (state, action)=>{
            state.accountStatus = 'resolved';
            state.allAccount.push(action.payload);
        });
        builder.addCase(createAccountAT.rejected, setError);
        //---------------------------------------------------
        // builder.addCase(patchAccountAT.pending, setPending);
        // builder.addCase(patchAccountAT.fulfilled, (state, action)=>{
        //     state.accountStatus = 'resolved';
        //
        //     // alert(JSON.stringify(action.payload, null, 2))
        //     if(action.payload){
        //         state.oneAccount = action.payload
        //         // alert(JSON.stringify(state.oneAccount, null, 2))
        //         // const allAccountObj = state.allAccount.find(item=>item?._id === action.payload?._id!)
        //         // let oneAccountObj = state.oneAccount;
        //         //
        //         // if(!!oneAccountObj){
        //         //     oneAccountObj = action.payload;
        //         // }
        //
        //
        //         // if(allAccReqObj){
        //         //     // let innerAssignedTo:any = action.payload.assignedTo;
        //         //     // if(allAccReqObj.assignedTo){
        //         //     //     // alert(JSON.stringify(innerAssignedTo, null, 2))
        //         //     //     allAccReqObj.assignedTo._id = innerAssignedTo as string;
        //         //     // }
        //         //     allAccReqObj.assignedTo = action.payload?.assignedTo!;
        //         //     allAccReqObj.status = action.payload?.status!;
        //         //     allAccReqObj.rejectionReason = action.payload?.rejectionReason!;
        //         // }
        //         // if(accountStatus){
        //         //     accountStatus.assignedTo = action.payload?.assignedTo!;
        //         //     accountStatus.status = action.payload?.status!;
        //         //     accountStatus.rejectionReason = action.payload?.rejectionReason!;
        //         // }
        //     }
        // });
        // builder.addCase(patchAccountAT.rejected, setError);
        //---------------------------------------------------
    }
});

// export const {setRingState,setNewObj,setFlagRing} = accountSlice.actions;
export default accountSlice.reducer;