import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from "../../axiosApi";
import {TUserState} from "../../types/typesUser";
import {TGetPostTransferReqAT, TPatchTransferReqAT, TTransferReqState} from "../../types/typesTransferRequest";
//https://gitlab.com/ajs8-9team2/trading-api/-/wikis/POST-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B.-PATCH-%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B

const initialState: TTransferReqState = {
    oneTransferReq: undefined,
    allTransferReq: [],
    transferReqStatus: null,
    error: null,
}

// все заявки на перевод денег
/** GET_FETCH http://localhost:8000/transferRequests */
export const fetchTransferReqAT = createAsyncThunk<TGetPostTransferReqAT[], undefined, { rejectValue: string | null }>(
    'transfer/fetchTransferReq',
    async function(_, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/transferRequests`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// все заявки на перевод денег для фронт сотрудников.
// имеют статусы pending, frontwip (работает фронт менеджер)
/** GET_FETCH http://localhost:8000/transferRequests/front */
export const fetchTransferReqFrontAT = createAsyncThunk<TGetPostTransferReqAT[], undefined, { rejectValue: string | null }>(
    'transfer/fetchTransferReqFront',
    async function(_, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/transferRequests/front`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// все заявки на перевод денег для бухгалтеров.
// имеют статусы approved и accountantwip (работает бухгалтер)
/** GET_FETCH http://localhost:8000/transferRequests/accountant */
export const fetchTransferReqAccountantAT = createAsyncThunk<TGetPostTransferReqAT[], undefined, { rejectValue: string | null }>(
    'transfer/fetchTransferReqAccountant',
    async function(_, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/transferRequests/accountant`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// заявки на перевод денег, над которыми работает фронт менеджер.
// все имеют статус workinprogress. где frontid это id фронт менеджера
/** GET_QUERY   http://localhost:8000/transferRequests/frontwip?frontid=${id} */
export const queryTransferReqFrontWIPAT = createAsyncThunk<TGetPostTransferReqAT[], string, { rejectValue: string | null }>(
    'transfer/queryTransferReqFrontWIP',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`transferRequests/frontwip?frontid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// заявки на перевод денег, над которыми работает бухгалтер.
// все имеют статус workinprogress. где frontid это id бухгалтера
/** GET_QUERY   http://localhost:8000/transferRequests/accountantwip?accountantid=${id} */

// Изменено. Роут в get запросе изменен на accountantwip?accountantid | 19.12.22 Виктория

export const queryTransferReqAccountantWIPAT = createAsyncThunk<TGetPostTransferReqAT[], string, { rejectValue: string | null }>(
    'transfer/queryTransferReqAccountantWIP',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`transferRequests/accountantwip?accountantid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

// export const queryTransferReqAccountantWIPAT = createAsyncThunk<TGetPostTransferReqAT[], string, { rejectValue: string | null }>(
//     'transfer/queryTransferReqAccountantWIP',
//     async function(id, {rejectWithValue}){
//         try{
//             const response = await axios
//                 .get(`transferRequests/frontwip?frontid=${id}`)
//                 .then(res=>{
//                     return res.data
//                 });
//             return response;
//         }catch(error: any){
//             return rejectWithValue(error.message);
//         }
//     }
// );


// заявки на перевод денег конкретного клиента.
// где clientid это id клиента
/** GET_QUERY http://localhost:8000/transferRequests/client?clientid=${id} */
export const queryTransferReqClientAT = createAsyncThunk<TGetPostTransferReqAT[], string, { rejectValue: string | null }>(
    'transfer/queryTransferReqClient',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`transferRequests/client?clientid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// Получить одну заявку конкретного клиента по params
/** GET_PARAMS http://localhost:8000/transferRequests/${id} */
export const paramsTransferReqAT = createAsyncThunk<TGetPostTransferReqAT, string | number, { rejectValue: string | null }>(
    'transfer/paramsTransferReq',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/transferRequests/${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// изменить заявку на перевод денег по id заявки
export const patchTransferReqAT = createAsyncThunk<TGetPostTransferReqAT, TPatchTransferReqAT, {
    rejectValue: string | null //, state: { reducerAccRequests: TAccRequestsState }
}>(
    'transfer/patchAccRequest',
    async function(obj, {rejectWithValue, getState}){
        const innerObj: TPatchTransferReqAT= (obj.rejectionReason)
            ?// отклонить
            {
                status: obj.status, // "Declined"
                rejectionReason: obj.rejectionReason,
            }
            :// закрепить за собой / одобрить
            {
                status: obj.status,// "FrontWIP" | "AccountantWIP" | "Approved" |  "Completed"
                frontManager: obj.frontManager,
                accountant: obj.accountant,
            }

        try{
            const response = await axios
                // .patch(`/accRequests?_id=${obj._id}`,innerObj)
                .patch(`/transferRequests/${obj._idTransferReq}`, innerObj)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

// создать заявку на перевод денег.
// Поля для создания заявления
/**
 senderFullName: string
 recipientFullName: string
 amount: number
 senderIIN: string   (12 цифр обязательно)
 recipientIIN: string   (12 цифр обязательно)
 senderIIK: string
 recipientIIK: string
 senderBIK: string
 recipientBIK: string
 senderBank: string
 reicpientBank: string
 senderKBe: string
 recipientKBe: string
 */

/** POST http://localhost:8000/transferRequests */
export const createTransferReqAT = createAsyncThunk<TGetPostTransferReqAT, TGetPostTransferReqAT, {
    rejectValue: string | null, state: { reducerUsers: TUserState }
}>(
    'transfer/createTransferReq',
    async function(transferReqData, {rejectWithValue, getState}){
        const token: string = getState().reducerUsers.user?.token!

        try{
            return await axios
                // отрпавка с токеном
                .post('/transferRequests', transferReqData, {
                    headers: {
                        'Authentication': token
                    }
                }).then(res=>res.data);
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


const setPending = (state: TTransferReqState)=>{
    state.transferReqStatus = 'loading';
    state.error = null;
}

const setError = (state: TTransferReqState, action: any)=>{
    state.transferReqStatus = 'rejected';
    state.error = action.payload;
}

const transferRequestSlice = createSlice({
    name: 'accRequests',
    initialState,
    reducers: {},
    extraReducers: (builder)=>{
        //---------------------------------------------------
        builder.addCase(fetchTransferReqAT.pending, setPending);
        builder.addCase(fetchTransferReqAT.fulfilled, (state, action)=>{
            state.transferReqStatus = 'resolved';
            // alert(JSON.stringify(action.payload, null, 2))
            state.allTransferReq = action.payload;
        });
        builder.addCase(fetchTransferReqAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(fetchTransferReqFrontAT.pending, setPending);
        builder.addCase(fetchTransferReqFrontAT.fulfilled, (state, action)=>{
            state.transferReqStatus = 'resolved';
            state.allTransferReq = action.payload;
        });
        builder.addCase(fetchTransferReqFrontAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(fetchTransferReqAccountantAT.pending, setPending);
        builder.addCase(fetchTransferReqAccountantAT.fulfilled, (state, action)=>{
            state.transferReqStatus = 'resolved';
            state.allTransferReq = action.payload;
        });
        builder.addCase(fetchTransferReqAccountantAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryTransferReqFrontWIPAT.pending, setPending);
        builder.addCase(queryTransferReqFrontWIPAT.fulfilled, (state, action)=>{
            state.transferReqStatus = 'resolved';
            state.allTransferReq = action.payload;
        });
        builder.addCase(queryTransferReqFrontWIPAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryTransferReqAccountantWIPAT.pending, setPending);
        builder.addCase(queryTransferReqAccountantWIPAT.fulfilled, (state, action)=>{
            state.transferReqStatus = 'resolved';
            state.allTransferReq = action.payload;
        });
        builder.addCase(queryTransferReqAccountantWIPAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryTransferReqClientAT.pending, setPending);
        builder.addCase(queryTransferReqClientAT.fulfilled, (state, action)=>{
            state.transferReqStatus = 'resolved';
            state.allTransferReq = action.payload;
        });
        builder.addCase(queryTransferReqClientAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(paramsTransferReqAT.pending, setPending);
        builder.addCase(paramsTransferReqAT.fulfilled, (state, action)=>{
            state.transferReqStatus = 'resolved';
            state.oneTransferReq = action.payload;
        });
        builder.addCase(paramsTransferReqAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(createTransferReqAT.pending, setPending);
        builder.addCase(createTransferReqAT.fulfilled, (state, action)=>{
            state.transferReqStatus = 'resolved';
            state.allTransferReq.push(action.payload);
        });
        builder.addCase(createTransferReqAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(patchTransferReqAT.pending, setPending);
        builder.addCase(patchTransferReqAT.fulfilled, (state, action)=>{
            state.transferReqStatus = 'resolved';
            // alert(JSON.stringify(action.payload, null, 2))
            if(action.payload){
                const allTransferReqData = state.allTransferReq.find(item=>item?._id === action.payload?._id!)
                const oneTransferReqData = state.oneTransferReq;
                if(allTransferReqData){
                    allTransferReqData.frontManager = action.payload?.frontManager!;
                    allTransferReqData.status = action.payload?.status!;
                    allTransferReqData.accountant = action.payload?.accountant!;
                    allTransferReqData.rejectionReason = action.payload?.rejectionReason!;

                }
                if(oneTransferReqData){
                    oneTransferReqData.frontManager = action.payload?.frontManager!;
                    oneTransferReqData.status = action.payload?.status!;
                    oneTransferReqData.accountant = action.payload?.accountant!;
                    oneTransferReqData.rejectionReason = action.payload?.rejectionReason!;
                }
            }
        });
        builder.addCase(patchTransferReqAT.rejected, setError);
        //---------------------------------------------------
    }
});

// export const {} = usersDataSlice.actions;
export default transferRequestSlice.reducer;


/*

!! Заявки на перевод денег / Transfer Requests

Все заявки
GET http://localhost:8000/transferRequests
router.get("/", transferReqController.getTransferReqs);

Все заявки для фронт сотрудников. Имеют статусы Pending, FrontWIP (работает фронт менеджер)
GET http://localhost:8000/transferRequests/front
router.get("/front", transferReqController.getTransferReqFront);

Заявки, над которыми работает фронт менеджер. Все имеют статус FrontWIP. Где frontid это id фронт менеджера
GET http://localhost:8000/transferRequests/frontwip?frontid=${id}
router.get("/frontwip", transferReqController.getTransferReqFrontWIP);

Все заявки для бухгалтеров. Имеют статусы Approved и AccountantWIP (работает бухгалтер)
GET http://localhost:8000/transferRequests/accountant
router.get("/accountant", transferReqController.getTransferReqAccountant);

Заявки, над которыми работает бухгалтер. Все имеют статус AccountantWIP. Где accountantid это id бухгалтера
GET http://localhost:8000/transferRequests/accountantwip?accountantid=${id}
router.get("/accountantwip", transferReqController.getTransferReqAccountantWIP);

Заявки конкретного клиента. Где clientid это id клиента
GET http://localhost:8000/transferRequests/client?clientid=${id}
router.get("/client", transferReqController.getTransferReqClient);


router.post("/", transferReqController.createTransferReq);
router.patch("/:id", transferReqController.changeTransferReq);
router.get("/:id", transferReqController.getTransferReq);




import mongoose, { Schema } from "mongoose";

export interface ITransferReq extends mongoose.Document {
    user: string;
    status: string;
    senderIIN: string;
    recipientIIN: string;
    senderIIK: string;
    recipientIIK: string;
    senderBIK: string;
    recipientBIK: string;
    senderBank: string;
    recipientBank: string;
    senderKBe: string;
    recipientKBe: string;
    senderFullName: string;
    recipientFullName: string;
    frontManager: string;
    accountant: string;
    rejectionReason: string;
    amount: number;
    date: string;
};

const TransferReqSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User's ID is required"]
    },
    status: {
        type: String,
        required: true,
        default: "Pending",
        enum: ["Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]
    },
    amount: {
        type: Number,
        required: [true, "Money amount is required"]
    },
    senderFullName: {
        type: String,
        required: [true, "Sender's full name is required"]
    },
    recipientFullName: {
        type: String,
        required: [true, "Recipient's full name is required"]
    },
    senderIIN: {
        type: String,
        required: [true, "Sender's IIN is required"],
        minlength: 12,
        maxlength: 12
    },
    recipientIIN: {
        type: String,
        required: [true, "Recipient's IIN is required"],
        minlength: 12,
        maxlength: 12
    },
    senderBank: {
        type: String,
        required: [true, "Sender's Bank is required"]
    },
    recipientBank: {
        type: String,
        required: [true, "Recipient's Bank is required"]
    },
    senderIIK: {
        type: String,
        required: [true, "Sender's IIK is required"]
    },
    recipientIIK: {
        type: String,
        required: [true, "Recipient's IIK is required"]
    },
    senderBIK: {
        type: String,
        required: [true, "Sender's BIK is required"]
    },
    recipientBIK: {
        type: String,
        required: [true, "Recipient's BIK is required"]
    },
    senderKBe: {
        type: String,
        required: [true, "Sender's KBe is required"]
    },
    recipientKBe: {
        type: String,
        required: [true, "Recipient's KBe is required"]
    },
    frontManager: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    accountant: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    rejectionReason: {
        type: String,
    },
    requestType: {
        type: String,
        required: true,
        default: "Transfer request"
    },
    date: {
        type: Date,
        default: new Date()
    }
});

const TransferRequest = mongoose.model<ITransferReq>("TransferRequest", TransferReqSchema);

export default TransferRequest;
 */
