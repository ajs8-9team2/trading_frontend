import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from "../../axiosApi";
import {TClientAccountState, TGetPostClientAccountAT} from "../../types/typesClentAccount";
import {TUserState} from "../../types/typesUser";

const initialState: TClientAccountState = {
    oneClientAccount: undefined,
    allClientAccount: [],
    clientAccountStatus: null,
    error: null,
}

/* GET /client-accounts   */
/* Полуаем все клиентские счета */
export const fetchClientAccountAT = createAsyncThunk<TGetPostClientAccountAT[], undefined, { rejectValue: string | null }>(
    'clientAccount/fetchClientAccount',
    async function(_, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/client-accounts`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

/* GET /client-accounts/client?clientid=${id}  */
/* Получаем все клиентские счета определенного клиента */
export const getClientAccountAT = createAsyncThunk<TGetPostClientAccountAT[], string, { rejectValue: string | null }>(
    'clientAccount/getClientAccount',
    async function(id, {rejectWithValue}){
        // alert(JSON.stringify(id, null, 2))
        try{
            const response = await axios
                .get(`/client-accounts/client?clientid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

/* POST /client-accounts   */
/* Создаем один клиентских счет */
export const createClientAccountAT = createAsyncThunk<TGetPostClientAccountAT, TGetPostClientAccountAT, {
    rejectValue: string | null, state: { reducerUsers: TUserState }
}>(
    'clientAccount/createClientAccount',
    async function(clientAccountData, {rejectWithValue, getState}){
        const token: string = getState().reducerUsers.user?.token!

        try{
            return await axios
                .post('/client-accounts', clientAccountData, {
                    headers: {
                        'Authentication': token
                    }
                }).then(res=>res.data);
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

/* GET /client-accounts/:id   */
/* Получаем один клиентский счет */
/** GET_PARAMS http://localhost:8000/client-accounts/:id */
export const paramsClientAccountAT = createAsyncThunk<TGetPostClientAccountAT, string | undefined, { rejectValue: string | null }>(
    'clientAccount/paramsClientAccount',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/client-accounts/${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

const setPending = (state: TClientAccountState)=>{
    state.clientAccountStatus = 'loading';
    state.error = null;
}

const setError = (state: TClientAccountState, action: any)=>{
    state.clientAccountStatus = 'rejected';
    state.error = action.payload;
}

const accountSlice = createSlice({
    name: 'clientAccount',
    initialState,
    reducers: {},
    extraReducers: (builder)=>{
        //---------------------------------------------------
        builder.addCase(fetchClientAccountAT.pending, setPending);
        builder.addCase(fetchClientAccountAT.fulfilled, (state, action)=>{
            state.clientAccountStatus = 'resolved';
            state.allClientAccount = action.payload;
        });
        builder.addCase(fetchClientAccountAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(getClientAccountAT.pending, setPending);
        builder.addCase(getClientAccountAT.fulfilled, (state, action)=>{
            state.clientAccountStatus = 'resolved';
            state.allClientAccount = action.payload;
        });
        builder.addCase(getClientAccountAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(createClientAccountAT.pending, setPending);
        builder.addCase(createClientAccountAT.fulfilled, (state, action)=>{
            state.clientAccountStatus = 'resolved';
            state.allClientAccount.push(action.payload);
        });
        builder.addCase(createClientAccountAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(paramsClientAccountAT.pending, setPending);
        builder.addCase(paramsClientAccountAT.fulfilled, (state, action)=>{
            state.clientAccountStatus = 'resolved';
            state.oneClientAccount = action.payload;
        });
        builder.addCase(paramsClientAccountAT.rejected, setError);
        //---------------------------------------------------
    }
});

export default accountSlice.reducer;