import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import axios from "../../axiosApi";

export type TUser = {
    _id?: string;
    username: string;
    IIN: string;
    [propName: string]: any;
}

type TUsersState = {
    oneUser: TUser;
    allUsers: TUser[];
    usersStatus: string | null,// три статуса: pending, resolved, rejected
    error: string | null,
}

const initialState: TUsersState = {
    oneUser: {
        _id: "",
        username: "",
        IIN: "",
    },
    allUsers: [],
    usersStatus: null,
    error: null,
}

export const fetchAllUsersAT = createAsyncThunk<TUser[], undefined, { rejectValue: string | null }>(
    'allUsers/fetch',
    async function(_, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/allUsers`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

export const getOneUserAT = createAsyncThunk<TUser, string, { rejectValue: string | null }>(
    'allUsers/getCocktails',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/users/${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

export const deleteUserAT = createAsyncThunk<string, string, {
    rejectValue: string,
    // reducerUsers:{reducerUsers:TUser}
}>(
    'allUsers/deleteCocktail',
    async function(id, {rejectWithValue, getState}){
        // const TUser = getState().reducerUsers
        // console.log(TUser)

        try{
            return await axios
                // .delete(`/allUsers/${id}`, {
                //     headers: {
                //         Authenticate: TUser.token,
                //         role: TUser.role,
                //         _id: id
                //     }
                // })
                // .then(res => res.data);
                .delete(`/allUsers/${id}`)
                .then(res => res.data);
        }catch(error:any){
            return rejectWithValue(error.message);
        }
    }
);

const setPending = (state: TUsersState)=>{
    state.usersStatus = 'loading';
    state.error = null;
}

const setError = (state: TUsersState, action: any)=>{
    state.usersStatus = 'rejected';
    state.error = action.payload;
}

const usersDataSlice = createSlice({
    name: 'allUsers',
    initialState,
    reducers: {},
    extraReducers: (builder)=>{
        //---------------------------------------------------
        builder.addCase(fetchAllUsersAT.pending,setPending);
        builder.addCase(fetchAllUsersAT.fulfilled, (state, action)=>{
            state.usersStatus = 'resolved';
            state.allUsers = action.payload;
        });
        builder.addCase(fetchAllUsersAT.rejected,setError);
        //---------------------------------------------------
        builder.addCase(getOneUserAT.pending,setPending);
        builder.addCase(getOneUserAT.fulfilled, (state, action)=>{
            state.usersStatus = 'resolved';
            state.oneUser = action.payload;
        });
        builder.addCase(getOneUserAT.rejected,setError);
        //---------------------------------------------------
        builder.addCase(deleteUserAT.pending,setPending);
        builder.addCase(deleteUserAT.fulfilled, (state, action)=>{
            state.usersStatus = 'resolved';
            state.allUsers = state.allUsers.filter(item => item._id !== action.payload)
        });
        builder.addCase(deleteUserAT.rejected,setError)
        //---------------------------------------------------
    }
});

// export const {} = usersDataSlice.actions;
export default usersDataSlice.reducer;