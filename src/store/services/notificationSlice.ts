import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import axios from "../../axiosApi";
import {TNoticeObj,TNotificationState, TGetPostNotificationAT, TPatchNotificationAT} from "../../types/typesNotification";
import {TGetAccRequestsAT, TPatchAccRequestsAT} from "../../types/typesAccReq";
import {patchAccRequestAT} from "./accRequestSlice";

const initialState: TNotificationState = {
    oneNotification: undefined,
    allNotification: [],
    notificationStatus: null,
    error: null,
    stateBell:0,
    isRing:false,
}

// получить сразу все счета
export const fetchNotificationAT = createAsyncThunk<TGetPostNotificationAT[], undefined, { rejectValue: string | null }>(
    'notification/fetchNotification',
    async function(_, {rejectWithValue}){
        // alert("11111111111111111")
        try{
            const response = await axios
                .get(`/notifications`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

export const getNotificationAT = createAsyncThunk<TGetPostNotificationAT, string, { rejectValue: string | null }>(
    'notification/getNotification',
    async function(id, {rejectWithValue}){
        // alert(JSON.stringify(id, null, 2))
        try{
            const response = await axios
                .get(`/notifications/client?clientid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

export const fetchOneNotificationAT = createAsyncThunk<TGetPostNotificationAT, string | number, { rejectValue: string | null }>(
    'notification/fetchOneNotification',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/notifications/${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

//http://localhost:8000/notifications/637f3266fe62e19d447f07ea
export const createNotificationAT = createAsyncThunk<TGetPostNotificationAT, string, { rejectValue: string | null }>(
    'notification/createNotification',
    async function(id, {rejectWithValue}){
        // alert(JSON.stringify(id, null, 2))
        try{
            const response = await axios
                .post(`/notifications/${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

// Изменить notification по id заявки
/** PATCH http://localhost:8000/notifications/63808b606dc61e6b36084d9d */
export const patchNotificationAT = createAsyncThunk<TGetPostNotificationAT, TPatchNotificationAT, {
    rejectValue: string | null //, state: { reducerAccRequests: TAccRequestsState }
}>(
    'notification/patchNotification',
    async function(obj, {rejectWithValue, getState}){
        // alert(JSON.stringify(obj, null, 2))
        const innerObj: TPatchNotificationAT = obj;

        try{
            const response = await axios
                .patch(`/notifications/${obj._idNotification}`, innerObj?.userNotifications)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

const setPending = (state: TNotificationState)=>{
    state.notificationStatus = 'loading';
    state.error = null;
}

const setError = (state: TNotificationState, action: any)=>{
    state.notificationStatus = 'rejected';
    state.error = action.payload;
}

const notificationSlice = createSlice({
    name: 'notification',
    initialState,
    reducers: {
        setNewObj:((state:TNotificationState, action:PayloadAction<TNoticeObj>)=>{ //,action){
            // alert(JSON.stringify(action.payload, null, 2))
            state.oneNotification?.userNotifications?.push(action.payload)
        }),
        setFlagRing:((state:TNotificationState, action:PayloadAction<boolean>)=>{ //,action){
            state.isRing = action.payload;
        }),
        setRingState:((state:TNotificationState, action:PayloadAction<0|1|2>)=>{ //,action){
            state.stateBell = action.payload;
        }),
        // countOfNotification (state:TNotificationState,action:PayloadAction<number>){ //,action){
        //     // alert(JSON.stringify(action.payload, null, 2))
        //     state.countNotice = action.payload;
        //     // state.user = undefined;
        //     // state.userStatus = null;
        //     // state.error = null;
        // },
        // addDishes:((state:TNotificationState,action:PayloadAction<number>)=>{
        //     // alert(JSON.stringify(action, null, 2))
        //     state.countNotice = action.payload;
        // }),
    },
    extraReducers: (builder)=>{
        //---------------------------------------------------
        builder.addCase(fetchNotificationAT.pending, setPending);
        builder.addCase(fetchNotificationAT.fulfilled, (state, action)=>{
            state.notificationStatus = 'resolved';
            state.allNotification = action.payload;
        });
        builder.addCase(fetchNotificationAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(getNotificationAT.pending, setPending);
        builder.addCase(getNotificationAT.fulfilled, (state, action)=>{
            state.notificationStatus = 'resolved';
            state.oneNotification = action.payload;
        });
        builder.addCase(getNotificationAT.rejected, setError);
        //---------------------------------------------------  Добавлено 19.12.22  Виктория 
        builder.addCase(fetchOneNotificationAT.pending, setPending);
        builder.addCase(fetchOneNotificationAT.fulfilled, (state, action)=>{
            state.notificationStatus = 'resolved';
            state.oneNotification = action.payload;
        });
        builder.addCase(fetchOneNotificationAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(createNotificationAT.pending, setPending);
        builder.addCase(createNotificationAT.fulfilled, (state, action)=>{
            state.notificationStatus = 'resolved';
            state.allNotification.push(action.payload);
        });
        builder.addCase(createNotificationAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(patchNotificationAT.pending, setPending);
        builder.addCase(patchNotificationAT.fulfilled, (state, action)=>{
            state.notificationStatus = 'resolved';

            // alert(JSON.stringify(action.payload, null, 2))
            if(action.payload){
                state.oneNotification = action.payload
                // alert(JSON.stringify(state.oneNotification, null, 2))
                // const allNotificationObj = state.allNotification.find(item=>item?._id === action.payload?._id!)
                // let oneNotificationObj = state.oneNotification;
                //
                // if(!!oneNotificationObj){
                //     oneNotificationObj = action.payload;
                // }


                // if(allAccReqObj){
                //     // let innerAssignedTo:any = action.payload.assignedTo;
                //     // if(allAccReqObj.assignedTo){
                //     //     // alert(JSON.stringify(innerAssignedTo, null, 2))
                //     //     allAccReqObj.assignedTo._id = innerAssignedTo as string;
                //     // }
                //     allAccReqObj.assignedTo = action.payload?.assignedTo!;
                //     allAccReqObj.status = action.payload?.status!;
                //     allAccReqObj.rejectionReason = action.payload?.rejectionReason!;
                // }
                // if(notificationStatus){
                //     notificationStatus.assignedTo = action.payload?.assignedTo!;
                //     notificationStatus.status = action.payload?.status!;
                //     notificationStatus.rejectionReason = action.payload?.rejectionReason!;
                // }
            }
        });
        builder.addCase(patchNotificationAT.rejected, setError);
        //---------------------------------------------------
    }
});

export const {setRingState,setNewObj,setFlagRing} = notificationSlice.actions;
export default notificationSlice.reducer;