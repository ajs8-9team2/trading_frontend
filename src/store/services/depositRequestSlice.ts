import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from "../../axiosApi";
import {TDepositRequestState, TGetPostDepositRequestAT, TPatchDepositRequestAT} from "../../types/typesDepositRequest";
import {TUserState} from "../../types/typesUser";
import {TGetPostTransferReqAT} from "../../types/typesTransferRequest";
import {TGetAccRequestsAT, TPatchAccRequestsAT} from "../../types/typesAccReq";

const initialState: TDepositRequestState = {
    oneDepositRequest: undefined,
    allDepositsRequest: [],
    depositRequestStatus: null,
    error: null,
}


// все заявки на открытие счета
/** GET_FETCH http://localhost:8000/accRequests */
export const fetchDepositRequestAT = createAsyncThunk<any, undefined, { rejectValue: string | null }>(
    'depositRequest/fetchDepositRequest',
    async function(_, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/depositRequests`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// все заявки на открытие счета для фронт сотрудников.
// имеют статусы pending, frontwip (работает фронт менеджер)
/** GET_FETCH http://localhost:8000/depositRequests/front */
export const fetchDepositReqFrontAT = createAsyncThunk<any, undefined, { rejectValue: string | null }>(
    'depositRequest/fetchDepositReqFront',
    async function(_, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/depositRequests/front`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


//все заявки на открытие счета для бухгалтеров. имеют статусы approved и
// accountantwip (работает бухгалтер)
/** GET_FETCH http://localhost:8000/depositRequests/accountant */

// Изменено. В createAsyncThunk string заменен на undefined

export const fetchDepositReqAccountantAT = createAsyncThunk<TGetPostTransferReqAT[], undefined, { rejectValue: string | null }>(
    'depositRequest/fetchDepositReqAccountant',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/depositRequests/accountant`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

// export const fetchDepositReqAccountantAT = createAsyncThunk<TGetPostTransferReqAT[], string, { rejectValue: string | null }>(
//     'depositRequest/fetchDepositReqAccountant',
//     async function(id, {rejectWithValue}){
//         try{
//             const response = await axios
//                 .get(`/depositRequests/accountant`)
//                 .then(res=>{
//                     return res.data
//                 });
//             return response;
//         }catch(error: any){
//             return rejectWithValue(error.message);
//         }
//     }
// );


// заявки на открытие счета, над которыми работает фронт менеджер.
// все имеют статус frontwip.
// где frontid это id фронт менеджера
/** GET_QUERY http://localhost:8000/depositRequests/frontwip?frontid=${id} */
export const queryDepositReqFrontWIPAT = createAsyncThunk<TGetPostTransferReqAT[], string, { rejectValue: string | null }>(
    'depositRequest/queryDepositReqFrontWIP',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/depositRequests/frontwip?frontid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


//заявки на открытие счета, над которыми работает бухгалтер.
// все имеют статус accountantwip.
// где accountantid это id бухгалтера
/** GET_QUERY http://localhost:8000/depositRequests/accountantwip?accountantid=${id} */
export const queryDepositReqAccountantWIPAT = createAsyncThunk<TGetPostTransferReqAT[], string, { rejectValue: string | null }>(
    'depositRequest/queryDepositReqAccountantWIP',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/depositRequests/accountantwip?accountantid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// заявки конкретного клиента. где clientid это id клиента
/** GET_QUERY http://localhost:8000/depositRequests/client?clientid=${id} */
export const queryDepositReqClientAT = createAsyncThunk<TGetPostTransferReqAT[], string, { rejectValue: string | null }>(
    'depositRequest/queryDepositReqClient',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/depositRequests/client?clientid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// Получить одну заявку на открытие счета конкретного клиента по params
/** GET_PARAMS http://localhost:8000/depositRequests/${id} */
export const paramsDepositReqAT = createAsyncThunk<TGetPostTransferReqAT, string | number, { rejectValue: string | null }>(
    'depositRequest/paramsDepositReq',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/depositRequests/${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

// Изменить заявку на пополнение счета по id заявки на открытие счета
/** PATCH http://localhost:8000/depositRequests/63808b606dc61e6b36084d9d */
export const patchDepositRequestAT = createAsyncThunk<TGetPostDepositRequestAT, TPatchDepositRequestAT, {
    rejectValue: string | null //, state: { reducerAccRequests: TAccRequestsState }
}>(
    'depositRequest/patchAccRequest',
    async function(obj, {rejectWithValue, getState}){

        // если в объекте имеется ключ rejectionReason
        // то на сервер улетает только статус "Declined" и причина отклонения
        // alert(JSON.stringify(obj, null, 2))
        const innerObj: TPatchDepositRequestAT = (obj.rejectionReason)
            ?// отклонить
            {
                status: obj.status, // "Declined"
                rejectionReason: obj.rejectionReason,
            }
            :// закрепить за собой / одобрить
            {
                status: obj.status,// "FrontWIP" | "AccountantWIP" | "Approved" |  "Completed"
                frontManager: obj.frontManager,
                accountant: obj.accountant,
            }

        try{
            const response = await axios
                .patch(`/depositRequests/${obj._idDepositRequest}`, innerObj)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// // Изменить заявку на создание счета по id заявки
// /** PATCH http://localhost:8000/accRequests/63808b606dc61e6b36084d9d */
// export const patchAccRequestAT = createAsyncThunk<TGetAccRequestsAT, TPatchAccRequestsAT, {
//     rejectValue: string | null //, state: { reducerAccRequests: TAccRequestsState }
// }>(
//     'accRequests/patchAccRequest',
//     async function(obj, {rejectWithValue, getState}){
//         const innerObj: TPatchAccRequestsAT = (obj.rejectionReason)
//             ?
//             {
//                 status: obj.status,
//                 rejectionReason: obj.rejectionReason,
//                 // assignedTo: obj.assignedTo,
//             }
//             :
//             {
//                 // rejectionReason: obj.rejectionReason,
//                 status: obj.status,
//                 assignedTo: obj.assignedTo,
//             }
//
//         try{
//             const response = await axios
//                 .patch(`/accRequests/${obj._idAccReq}`, innerObj)
//                 .then(res=>{
//                     return res.data
//                 });
//             return response;
//         }catch(error: any){
//             return rejectWithValue(error.message);
//         }
//     }
// );



// отправить заявку на пополнение счета
/** POST http://localhost:8000/depositRequests */
export const createDepositRequestAT = createAsyncThunk<TGetPostDepositRequestAT, TGetPostDepositRequestAT | undefined, {
    // rejectValue: string | null, state: { reducerUsers: TUser }
    rejectValue: string | null, state: { reducerUsers: TUserState }
}>(
    'depositRequest/createDepositRequest',
    async function(depositRequestData, {rejectWithValue, getState}){
        const token: string = getState().reducerUsers.user?.token!
        try{
            return await axios
                // отрпавка с токеном
                .post('/depositRequests', depositRequestData, {
                        headers: {
                            'Authentication': token
                        }
                    }
                ).then(res=>res.data);
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

const setPending = (state: TDepositRequestState)=>{
    state.depositRequestStatus = 'loading';
    state.error = null;
}

const setError = (state: TDepositRequestState, action: any)=>{
    state.depositRequestStatus = 'rejected';
    state.error = action.payload;
}


const depositRequestSlice = createSlice({
    name: 'depositRequest',
    initialState,
    reducers: {},
    extraReducers: (builder)=>{
        //---------------------------------------------------
        builder.addCase(fetchDepositRequestAT.pending, setPending);
        builder.addCase(fetchDepositRequestAT.fulfilled, (state, action)=>{
            state.depositRequestStatus = 'resolved';
            state.allDepositsRequest = action.payload;
        });
        builder.addCase(fetchDepositRequestAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(fetchDepositReqFrontAT.pending, setPending);
        builder.addCase(fetchDepositReqFrontAT.fulfilled, (state, action)=>{
            state.depositRequestStatus = 'resolved';
            state.allDepositsRequest = action.payload;
        });
        builder.addCase(fetchDepositReqFrontAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(fetchDepositReqAccountantAT.pending, setPending);
        builder.addCase(fetchDepositReqAccountantAT.fulfilled, (state, action)=>{
            state.depositRequestStatus = 'resolved';
            state.allDepositsRequest = action.payload;
        });
        builder.addCase(fetchDepositReqAccountantAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryDepositReqFrontWIPAT.pending, setPending);
        builder.addCase(queryDepositReqFrontWIPAT.fulfilled, (state, action)=>{
            state.depositRequestStatus = 'resolved';
            // alert(JSON.stringify(action.payload, null, 2))
            state.allDepositsRequest = action.payload;
        });
        builder.addCase(queryDepositReqFrontWIPAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryDepositReqAccountantWIPAT.pending, setPending);
        builder.addCase(queryDepositReqAccountantWIPAT.fulfilled, (state, action)=>{
            state.depositRequestStatus = 'resolved';
            // alert(JSON.stringify(action.payload, null, 2))
            state.allDepositsRequest = action.payload;
        });
        builder.addCase(queryDepositReqAccountantWIPAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryDepositReqClientAT.pending, setPending);
        builder.addCase(queryDepositReqClientAT.fulfilled, (state, action)=>{
            state.depositRequestStatus = 'resolved';
            state.allDepositsRequest = action.payload;
        });
        builder.addCase(queryDepositReqClientAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(paramsDepositReqAT.pending, setPending);
        builder.addCase(paramsDepositReqAT.fulfilled, (state, action)=>{
            state.depositRequestStatus = 'resolved';
            state.oneDepositRequest = action.payload;
        });
        builder.addCase(paramsDepositReqAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(createDepositRequestAT.pending, setPending);
        builder.addCase(createDepositRequestAT.fulfilled, (state, action)=>{
            state.depositRequestStatus = 'resolved';
            state.allDepositsRequest.push(action.payload);
        });
        builder.addCase(createDepositRequestAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(patchDepositRequestAT.pending, setPending);
        builder.addCase(patchDepositRequestAT.fulfilled, (state, action)=>{
            state.depositRequestStatus = 'resolved';
            if(action.payload){

                const allDepositsRequestData = state.allDepositsRequest.find(item=>item?._id === action.payload?._id!)
                state.oneDepositRequest = allDepositsRequestData;
                let oneDepositRequestData = state.oneDepositRequest;

                if(allDepositsRequestData){
                    allDepositsRequestData.frontManager = action.payload?.frontManager!;
                    allDepositsRequestData.accountant = action.payload?.accountant!;
                    allDepositsRequestData.status = action.payload?.status!;
                }

                if(oneDepositRequestData){
                    oneDepositRequestData.frontManager = action.payload?.frontManager!;
                    oneDepositRequestData.accountant = action.payload?.accountant!;
                    oneDepositRequestData.status = action.payload?.status!;
                    // alert(JSON.stringify(oneDepositRequestData, null, 2))
                }
            }
        });
        builder.addCase(patchDepositRequestAT.rejected, setError);
        //---------------------------------------------------
    }
});

export default depositRequestSlice.reducer;
/*
Заявки на пополнение счета / Deposit Requests

Все заявки на открытие счета
GET http://localhost:8000/depositRequests
// router.get("/", depositReqController.getDepositReqs);

Все заявки на открытие счета для фронт сотрудников. Имеют статусы Pending, FrontWIP (работает фронт менеджер)
GET http://localhost:8000/depositRequests/front
// router.get("/front", depositReqController.getDepositReqFront);

Заявки, над которыми работает фронт менеджер. Все имеют статус FrontWIP. Где frontid это id фронт менеджера
GET http://localhost:8000/depositRequests/frontwip?frontid=${id}
// router.get("/frontwip", depositReqController.getDepositReqFrontWIP);

Все заявки на открытие счета для бухгалтеров. Имеют статусы Approved и AccountantWIP (работает бухгалтер)
GET http://localhost:8000/depositRequests/accountant
// router.get("/accountant", depositReqController.getDepositReqAccountant);

Заявки, над которыми работает бухгалтер. Все имеют статус AccountantWIP. Где accountantid это id бухгалтера
GET http://localhost:8000/depositRequests/accountantwip?accountantid=${id}
// router.get("/accountantwip", depositReqController.getDepositReqAccountantWIP);

Заявки конкретного клиента. Где clientid это id клиента
GET http://localhost:8000/depositRequests/client?clientid=${id}
// router.get("/client", depositReqController.getDepositReqClient);

Изменить заявку на пополнение счета по id заявки
PATCH http://localhost:8000/depositRequests/63808b606dc61e6b36084d9d
// router.patch("/:id", depositReqController.changeDepositReq);

Отправить заявку на пополнение счета
POST http://localhost:8000/depositRequests
// router.post("/", depositReqController.createDepositReq);

// router.get("/:id", depositReqController.getDepositReq);
//------------------------------------------------------------------------
*/



