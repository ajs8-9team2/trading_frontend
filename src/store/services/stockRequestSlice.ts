import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from "../../axiosApi";
import { TStockRequestState, TGetPostStockRequestAT, TPatchStockRequestAT } from "../../types/typesStockRequest";
import { TUserState } from "../../types/typesUser";
import {TPatchDepositRequestAT} from "../../types/typesDepositRequest";

const initialState: TStockRequestState = {
    oneStockRequest: undefined,
    allStocksRequest: [],
    stockRequestStatus: null,
    error: null,
}

// все заявки на акции
/** GET_FETCH http://localhost:8000/stock-requests*/
export const fetchStockRequestAT = createAsyncThunk<TGetPostStockRequestAT[], undefined, { rejectValue: string | null }>(
    'stockRequest/fetchStockRequest',
    async function (_, { rejectWithValue }) {
        try {
            const response = await axios
                .get(`/stock-requests`)
                .then(res => {
                    return res.data
                });
            return response;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

// Все заявки для фронт сотрудников. Имеют статусы Pending, FrontWIP (работает фронт менеджер)
// GET http://localhost:8000/stock-requests/front
/** GET_FETCH http://localhost:8000/stock-requests/front */
export const fetchStockReqFrontAT = createAsyncThunk<TGetPostStockRequestAT[], undefined, { rejectValue: string | null }>(
    'stockRequest/fetchStockReqFront',
    async function (_, { rejectWithValue }) {
        try {
            const response = await axios
                .get(`/stock-requests/front`)
                .then(res => {
                    return res.data
                });
            return response;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

// Заявки одного фронт сотрудника
// Заявки, над которыми работает фронт менеджер. Все имеют статус FrontWIP. Где frontid это id фронт менеджера
/** GET_QUERY http://localhost:8000/stock-requests/frontwip?frontid=${id} */
export const queryStockReqFrontWIPAT = createAsyncThunk<TGetPostStockRequestAT[], string, { rejectValue: string | null }>(
    'stockRequest/queryStockReqFrontWIP',
    async function (id, { rejectWithValue }) {
        try {
            const response = await axios
                .get(`/stock-requests/frontwip?frontid=${id}`)
                .then(res => {
                    return res.data
                });
            return response;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

// Все заявки для трейдеров. Имеют статусы Approved и TraderWIP (работает trader)
/** GET_FETCH http://localhost:8000/stock-requests/trader */
export const queryStockReqTraderAT = createAsyncThunk<TGetPostStockRequestAT[], undefined, { rejectValue: string | null }>(
    'stockRequest/queryStockReqTrader',
    async function (_, { rejectWithValue }) {
        try {
            const response = await axios
                .get(`/stock-requests/trader`)
                .then(res => {
                    return res.data
                });
            return response;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

// Заявки, над которыми работает один трейдер. Все имеют статус TraderWIP.
// Где traderid это id трейдера
/** GET_QUERY http://localhost:8000/stock-requests/traderwip?traderid=${id} */
export const queryStockReqTraderWIPAT = createAsyncThunk<TGetPostStockRequestAT[], string, { rejectValue: string | null }>(
    'stockRequest/queryStockReqTraderWIP',
    async function (id, { rejectWithValue }) {
        try {
            const response = await axios
                .get(`stock-requests/traderwip?traderid=${id}`)
                .then(res => {
                    return res.data
                });
            return response;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

// Заявки конкретного клиента. Где clientid это id клиента
/** GET_QUERY http://localhost:8000/stock-requests/client?clientid=${id} */
export const queryStockReqClientAT = createAsyncThunk<TGetPostStockRequestAT[], string, { rejectValue: string | null }>(
    'stockRequest/queryStockReqClient',
    async function (id, { rejectWithValue }) {
        try {
            const response = await axios
                .get(`/stock-requests/client?clientid=${id}`)
                .then(res => {
                    return res.data
                });
            return response;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

//  Получить одну заявку по id заявки
/** GET_PARAMS http://localhost:8000/stock-requests/${id} */
export const paramsStockReqAT = createAsyncThunk<TGetPostStockRequestAT, string, { rejectValue: string | null }>(
    'stockRequest/paramsStockReq',
    async function (id, { rejectWithValue }) {
        try {
            const response = await axios
                .get(`/stock-requests/${id}`)
                .then(res => {
                    return res.data
                });
            return response;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

// отправить заявку на покупку акции
/** POST http://localhost:8000/stockRequests */
export const createStockRequestAT = createAsyncThunk<TGetPostStockRequestAT, TGetPostStockRequestAT | undefined, {
    // rejectValue: string | null, state: { reducerUsers: TUser }
    rejectValue: string | null, state: { reducerUsers: TUserState }
}>(
    'stockRequest/createStockRequest',
    async function (stockRequestData, { rejectWithValue, getState }) {
        const token: string = getState().reducerUsers.user?.token!
        try {
            return await axios
                // отрпавка с токеном
                .post('/stock-requests', stockRequestData, {
                    headers: {
                        'Authentication': token
                    }
                }
                ).then(res => res.data);
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

// Изменить заявку на пополнение счета по id заявки
//
/** PATCH http://localhost:8000/stock-requests/${id} */
export const patchStockRequestAT = createAsyncThunk<TGetPostStockRequestAT, TPatchStockRequestAT, {
    rejectValue: string | null //, state: { reducerAccRequests: TAccRequestsState }
}>(
    'stockRequest/patchAccRequest',
    async function(obj, {rejectWithValue, getState}){
        // если в объекте имеется ключ rejectionReason
        // то на сервер улетает только статус "Declined" и причина отклонения
        // alert(JSON.stringify(obj, null, 2))

        const innerObj: TPatchStockRequestAT = (obj.rejectionReason)
            ?// отклонить
            {
                status: obj.status, // "Declined"
                rejectionReason: obj.rejectionReason,
            }
            :// закрепить за собой / одобрить
            {
                status: obj.status,// "Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted"
                frontManager: obj.frontManager,
                trader: obj.trader,
            }

        try{
            const response = await axios
                .patch(`/stock-requests/${obj._idStockRequest}`, innerObj)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

const setPending = (state: TStockRequestState) => {
    state.stockRequestStatus = 'loading';
    state.error = null;
}

const setError = (state: TStockRequestState, action: any) => {
    state.stockRequestStatus = 'rejected';
    state.error = action.payload;
}

const stockRequestSlice = createSlice({
    name: 'stockRequest',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        //---------------------------------------------------
        builder.addCase(fetchStockRequestAT.pending, setPending);
        builder.addCase(fetchStockRequestAT.fulfilled, (state, action) => {
            state.stockRequestStatus = 'resolved';
            state.allStocksRequest = action.payload;
        });
        builder.addCase(fetchStockRequestAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(fetchStockReqFrontAT.pending, setPending);
        builder.addCase(fetchStockReqFrontAT.fulfilled, (state, action) => {
            state.stockRequestStatus = 'resolved';
            state.allStocksRequest = action.payload;
        });
        builder.addCase(fetchStockReqFrontAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryStockReqFrontWIPAT.pending, setPending);
        builder.addCase(queryStockReqFrontWIPAT.fulfilled, (state, action) => {
            state.stockRequestStatus = 'resolved';
            state.allStocksRequest = action.payload;
        });
        builder.addCase(queryStockReqFrontWIPAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryStockReqTraderAT.pending, setPending);
        builder.addCase(queryStockReqTraderAT.fulfilled, (state, action) => {
            state.stockRequestStatus = 'resolved';
            state.allStocksRequest = action.payload;
        });
        builder.addCase(queryStockReqTraderAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryStockReqTraderWIPAT.pending, setPending);
        builder.addCase(queryStockReqTraderWIPAT.fulfilled, (state, action) => {
            state.stockRequestStatus = 'resolved';
            state.allStocksRequest = action.payload;
        });
        builder.addCase(queryStockReqTraderWIPAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryStockReqClientAT.pending, setPending);
        builder.addCase(queryStockReqClientAT.fulfilled, (state, action) => {
            state.stockRequestStatus = 'resolved';
            state.allStocksRequest = action.payload;
        });
        builder.addCase(queryStockReqClientAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(paramsStockReqAT.pending, setPending);
        builder.addCase(paramsStockReqAT.fulfilled, (state, action) => {
            state.stockRequestStatus = 'resolved';
            state.oneStockRequest = action.payload;
        });
        builder.addCase(paramsStockReqAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(createStockRequestAT.pending, setPending);
        builder.addCase(createStockRequestAT.fulfilled, (state, action) => {
            state.stockRequestStatus = 'resolved';
            state.allStocksRequest.push(action.payload);
        });
        builder.addCase(createStockRequestAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(patchStockRequestAT.pending, setPending);
        builder.addCase(patchStockRequestAT.fulfilled, (state, action)=>{
            state.stockRequestStatus = 'resolved';
            if(action.payload){

                const allStocksRequestData = state.allStocksRequest.find(item=>item?._id === action.payload?._id!)
                state.oneStockRequest = allStocksRequestData;
                let oneStockRequestData = state.oneStockRequest;

                if(allStocksRequestData){
                    allStocksRequestData.frontManager = action.payload?.frontManager!;
                    allStocksRequestData.trader = action.payload?.trader!;
                    allStocksRequestData.status = action.payload?.status!;
                }

                if(oneStockRequestData){
                    oneStockRequestData.frontManager = action.payload?.frontManager!;
                    oneStockRequestData.trader = action.payload?.trader!;
                    oneStockRequestData.status = action.payload?.status!;
                    // alert(JSON.stringify(oneStockRequestData, null, 2))
                }
            }
        });
        builder.addCase(patchStockRequestAT.rejected, setError);
        //---------------------------------------------------
    }
})

export default stockRequestSlice.reducer;
