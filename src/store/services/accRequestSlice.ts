import {ActionReducerMapBuilder, createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from "../../axiosApi";
import {TAccRequestsState, TPostAccRequestAT, TGetAccRequestsAT, TPatchAccRequestsAT} from "../../types/typesAccReq";
import {TUserState} from "../../types/typesUser";

const initialState: TAccRequestsState = {
    oneAccRequest: undefined,
    // userAccRequest: undefined,
    allAccRequests: [],
    accRequestStatus: null,
    error: null,
}

// получить сразу все заявки
/** GET_FETCH http://localhost:8000/accRequests */
export const fetchAccRequestsAT = createAsyncThunk<TGetAccRequestsAT[], undefined, { rejectValue: string | null }>(
    'accRequests/fetchAccRequests',
    async function(_, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/accRequests`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


//Все заявки со статусами Pending и WorkInProgress
/** GET_FETCH http://localhost:8000/accRequests/front */
export const fetchAccReqFrontAT = createAsyncThunk<TGetAccRequestsAT[], string, { rejectValue: string | null }>(
    'accRequests/fetchAccReqFront',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/accRequests/front`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// Заявки, над которыми работает фронт менеджер.
// Все имеют статус WorkInProgress. Где frontid это id фронт менеджера
/** GET_QUERY http://localhost:8000/accRequests/frontwip?frontid=${id} */
export const queryAccReqFrontWIPAT = createAsyncThunk<TGetAccRequestsAT[], string, { rejectValue: string | null }>(
    'accRequests/queryAccReqFrontWIP',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/accRequests/frontwip?frontid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// Заявки конкретного клиента. Где clientid это id клиента
/** GET_QUERY http://localhost:8000/accRequests/client?clientid=${id} */
export const queryAccReqClientAT = createAsyncThunk<TGetAccRequestsAT[], string, { rejectValue: string | null }>(
    'accRequests/queryAccReqClient',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/accRequests/client?clientid=${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// .get(`/accRequests/client?clientid=${id}`)
// export const getUserAccRequestAT = createAsyncThunk<TGetAccRequestsAT, string | undefined, { rejectValue: string | null }>(
//     'accRequests/getUserAccRequest',
//     async function(id, {rejectWithValue}){
//         // alert(JSON.stringify(id, null, 2))
//         try{
//             const response = await axios
//                 .get(`/accRequests/client?clientid=${id}`)
//                 .then(res=>{
//                     return res.data
//                 });
//             return response;
//         }catch(error: any){
//             return rejectWithValue(error.message);
//         }
//     }
// );


// получить одну заявку по id заявки
/** GET_PARAMS http://localhost:8000/accRequests/63808b606dc61e6b36084d9d */
export const paramsAccRequestAT = createAsyncThunk<TGetAccRequestsAT, string, { rejectValue: string | null }>(
    'accRequests/paramsAccRequest',
    async function(id, {rejectWithValue}){
        try{
            const response = await axios
                .get(`/accRequests/${id}`)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// Изменить заявку на создание счета по id заявки
/** PATCH http://localhost:8000/accRequests/63808b606dc61e6b36084d9d */
export const patchAccRequestAT = createAsyncThunk<TGetAccRequestsAT, TPatchAccRequestsAT, {
    rejectValue: string | null //, state: { reducerAccRequests: TAccRequestsState }
}>(
    'accRequests/patchAccRequest',
    async function(obj, {rejectWithValue, getState}){
        const innerObj: TPatchAccRequestsAT = (obj.rejectionReason)
            ?
            {
                status: obj.status,
                rejectionReason: obj.rejectionReason,
                // assignedTo: obj.assignedTo,
            }
            :
            {
                // rejectionReason: obj.rejectionReason,
                status: obj.status,
                assignedTo: obj.assignedTo,
            }

        try{
            const response = await axios
                .patch(`/accRequests/${obj._idAccReq}`, innerObj)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);


// Отправить заявку на создание счета.Токен передается в заголовок "Authentication"
/** POST http://localhost:8000/accRequests */
export const createAccRequestAT = createAsyncThunk<TGetAccRequestsAT, TPostAccRequestAT, {
    rejectValue: string | null, state: { reducerUsers: TUserState }
}>(
    'accRequests/createAccRequest',
    async function(AccRequestData, {rejectWithValue, getState}){
        const token: string = getState().reducerUsers.user?.token!

        try{
            return await axios
                .post('/accRequests', AccRequestData, {
                    headers: {
                        'Authentication': token
                    }
                }).then(res=>res.data);
        }catch(error: any){
            return rejectWithValue(error.message);
        }
    }
);

// export const deleteAccRequestAT = createAsyncThunk<string, string, {
//     rejectValue: string,
//     // reducerUsers:{reducerUsers:TUser}
// }>(
//     'accRequests/deleteAccRequest',
//     async function(id, {rejectWithValue, getState}){
//         // const TUser = getState().reducerUsers
//         // console.log(TUser)
//
//         try{
//             return await axios
//                 // .delete(`/allUsers/${id}`, {
//                 //     headers: {
//                 //         Authenticate: TUser.token,
//                 //         role: TUser.role,
//                 //         _id: id
//                 //     }
//                 // })
//                 // .then(res => res.data);
//                 .delete(`/allUsers/${id}`)
//                 .then(res => res.data);
//         }catch(error:any){
//             return rejectWithValue(error.message);
//         }
//     }
// );

const setPending = (state: TAccRequestsState)=>{
    state.accRequestStatus = 'loading';
    state.error = null;
}

const setError = (state: TAccRequestsState, action: any)=>{
    state.accRequestStatus = 'rejected';
    state.error = action.payload;
}

// на потом
// const fetchBuilder = (builder: ActionReducerMapBuilder<TAccRequestsState>,
// fetch: typeof fetchAccRequestsAT) =>{
//     builder.addCase(fetch.pending, setPending);
//     builder.addCase(fetch.fulfilled, (state, action)=>{
//         state.accRequestStatus = 'resolved';
//         // alert(JSON.stringify(action.payload, null, 2))
//         state.allAccRequests = action.payload;
//     });
//     builder.addCase(fetch.rejected, setError);
// }

const accRequestsSlice = createSlice({
    name: 'accRequests',
    initialState,
    reducers: {},
    extraReducers: (builder)=>{
        builder.addCase(fetchAccRequestsAT.pending, setPending);
        builder.addCase(fetchAccRequestsAT.fulfilled, (state, action)=>{
            state.accRequestStatus = 'resolved';
            // alert(JSON.stringify(action.payload, null, 2))
            state.allAccRequests = action.payload;
        });
        builder.addCase(fetchAccRequestsAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(fetchAccReqFrontAT.pending, setPending);
        builder.addCase(fetchAccReqFrontAT.fulfilled, (state, action)=>{
            state.accRequestStatus = 'resolved';
            // alert(JSON.stringify(action.payload, null, 2))
            state.allAccRequests = action.payload;
        });
        builder.addCase(fetchAccReqFrontAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryAccReqFrontWIPAT.pending, setPending);
        builder.addCase(queryAccReqFrontWIPAT.fulfilled, (state, action)=>{
            state.accRequestStatus = 'resolved';
            state.allAccRequests = action.payload;
        });
        builder.addCase(queryAccReqFrontWIPAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(queryAccReqClientAT.pending, setPending);
        builder.addCase(queryAccReqClientAT.fulfilled, (state, action)=>{
            state.accRequestStatus = 'resolved';
            state.allAccRequests = action.payload;
        });
        builder.addCase(queryAccReqClientAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(paramsAccRequestAT.pending, setPending);
        builder.addCase(paramsAccRequestAT.fulfilled, (state, action)=>{
            state.accRequestStatus = 'resolved';
            state.oneAccRequest = action.payload;
        });
        builder.addCase(paramsAccRequestAT.rejected, setError);
        // ??????????????????????????????????????????????????
        // builder.addCase(getUserAccRequestAT.pending, setPending);
        // builder.addCase(getUserAccRequestAT.fulfilled, (state, action)=>{
        //     state.accRequestStatus = 'resolved';
        //     state.userAccRequest = action.payload;
        // });
        // builder.addCase(getUserAccRequestAT.rejected, setError);
        // ??????????????????????????????????????????????????
        builder.addCase(createAccRequestAT.pending, setPending);
        builder.addCase(createAccRequestAT.fulfilled, (state, action)=>{
            state.accRequestStatus = 'resolved';
            state.allAccRequests.push(action.payload);
        });
        builder.addCase(createAccRequestAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(patchAccRequestAT.pending, setPending);
        builder.addCase(patchAccRequestAT.fulfilled, (state, action)=>{
            state.accRequestStatus = 'resolved';

            // alert(JSON.stringify(action.payload, null, 2))
            if(action.payload){
                const allAccReqObj = state.allAccRequests.find(item=>item?._id === action.payload?._id!)

                const oneAccRequest = state.oneAccRequest;
                if(allAccReqObj){
                    // let innerAssignedTo:any = action.payload.assignedTo;
                    // if(allAccReqObj.assignedTo){
                    //     // alert(JSON.stringify(innerAssignedTo, null, 2))
                    //     allAccReqObj.assignedTo._id = innerAssignedTo as string;
                    // }
                    allAccReqObj.assignedTo = action.payload?.assignedTo!;
                    allAccReqObj.status = action.payload?.status!;
                    allAccReqObj.rejectionReason = action.payload?.rejectionReason!;
                }
                if(oneAccRequest){
                    oneAccRequest.assignedTo = action.payload?.assignedTo!;
                    oneAccRequest.status = action.payload?.status!;
                    oneAccRequest.rejectionReason = action.payload?.rejectionReason!;
                }
            }
        });
        builder.addCase(patchAccRequestAT.rejected, setError);
        //---------------------------------------------------
        // builder.addCase(deleteAccRequest.pending,setPending);
        // builder.addCase(deleteAccRequest.fulfilled, (state, action)=>{
        //     state.accRequestStatus = 'resolved';
        //     state.allAccRequests = state.allAccRequests.filter(item => item._id !== action.payload)
        // });
        // builder.addCase(deleteAccRequest.rejected,setError)
        //---------------------------------------------------
    }
});

// export const {} = usersDataSlice.actions;
export default accRequestsSlice.reducer;

/*


! Все заявки
GET http://localhost:8000/accRequests
router.get("/", accReqController.getAccReqs);


! Все заявки со статусами Pending и WorkInProgress
GET http://localhost:8000/accRequests/front
router.get("/front", accReqController.getAccReqFront);


! Заявки, над которыми работает фронт менеджер.
Все имеют статус WorkInProgress. Где frontid это id фронт менеджера
GET http://localhost:8000/accRequests/frontwip?frontid=${id}
router.get("/frontwip", accReqController.getAccReqFrontWIP);


! Заявки конкретного клиента. Где clientid это id клиента
GET http://localhost:8000/accRequests/client?clientid=${id}
router.get("/client", accReqController.getAccReqClient);


Отправить заявку на создание счета.Токен передается в заголовок "Authentication"
POST http://localhost:8000/accRequests


Изменить заявку на создание счета по id заявки
PATCH http://localhost:8000/accRequests/63808b606dc61e6b36084d9d
router.patch("/:id", accReqController.changeAccReqStatus);


router.get("/:id", accReqController.getAccReq);




import mongoose, { Schema } from "mongoose";

export interface IAccReq extends mongoose.Document {
    user: string;
    status: string;
    firstName: string;
    lastName: string;
    patronym: string,
    dateOfBirth: string;
    identificationNumber: string;
    email: string;
    phone: string;
    country: string;
    city: string;
    area: string;
    address: string;
    date: string;
    authority: string;
    dateOfIssue: string;
    expirationDate: string;
    documentFront: string;
    documentBack: string;
    rejectionReason: string;
    assignedTo: string;
    idCardNumber: string;
    requestType: string;
};

const AccReqSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "Укажите ID пользователя"]
    },
    status: {
        type: String,
        required: true,
        default: "Pending",
        enum: ["Pending", "WorkInProgress", "Approved", "Declined"]
    },
    firstName: {
        type: String,
        required: [true, "Необходимо имя клиента"]
    },
    lastName: {
        type: String,
        required: [true, "Необходима фамилия клиента"]
    },
    patronym: {
        type: String
    },
    dateOfBirth: {
        type: String,
        required: [true, "Необходима дата рождения клиента"]
    },
    identificationNumber: {
        type: String,
        required: [true, "Необходим ИИН клиента"],
        minlength: 12,
        maxlength: 12
    },
    email: {
        type: String,
        required: [true, "Необходима почта клиента"]
    },
    phone: {
        type: String
    },
    country: {
        type: String
    },
    area: {
        type: String
    },
    city: {
        type: String
    },
    address: {
        type: String
    },
    authority: {
        type: String,
        required: [true, "Необходимо поле кем выдано удостоверение"]
    },
    dateOfIssue: {
        type: String,
        required: [true, "Необходима дата выдачи удостоверения"]
    },
    expirationDate: {
        type: String,
        required: [true, "Необходима дата действия удостоверения"]
    },
    documentFront: {
        type: String,
        required: [true, "Необходимо загрузить первую страницу удостоверения"]
    },
    documentBack: {
        type: String,
        required: [true, "Необходимо загрузить первую страницу удостоверения"]
    },
    requestType: {
        type: String,
        required: true,
        default: "Account Request",
        enum: ["Account Request"]
    },
    rejectionReason: {
        type: String
    },
    assignedTo: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
    idCardNumber: {
        type: String,
        required: [true, "Необходим номер удостоверения"]
    },
    date: {
        type: Date,
        default: new Date()
    }
});


const AccRequest = mongoose.model<IAccReq>("AccRequest", AccReqSchema);

export default AccRequest;
*/