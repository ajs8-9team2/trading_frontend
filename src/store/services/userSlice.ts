import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import axios from "../../axiosApi";
import {TUserState} from "../../types/typesUser";
import {TUser} from "../../types/reestrOfAlias";


const initialState: TUserState = {
    user: undefined,
    userStatus: null,
    error: null,
}

// регистрация
export const createUsersAT = createAsyncThunk<TUser, TUser, { rejectValue: string | object }>(
    'user/postUsers',
    async function(userData, {rejectWithValue}){
        try{
            const response = await axios
                .post(`/users`, userData)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            console.dir(error.response.data)
            if(error?.response?.data){
                return rejectWithValue(error.response.data);
            }else{
                return rejectWithValue({global: "Потеряно соединение!"});
            }
        }
    }
);

// авторизация
export const createUsersSessionAT = createAsyncThunk<TUser, TUser, { rejectValue: string | object }>(
    'user/postSessionUsers',
    async function(userData, {rejectWithValue}){
        try{
            const response = await axios
                .post(`/users/sessions`, userData)
                .then(res=>{
                    return res.data
                });
            return response;
        }catch(error: any){
            console.dir(error.response.data)
            if(error?.response?.data){
                return rejectWithValue(error.response.data);
            }else{
                return rejectWithValue({global: "Потеряно соединение!"});
            }
        }
    }
);

// прерывание сессии
export const deleteUsersSessionAT = createAsyncThunk<undefined, undefined, {
    rejectValue: string | object, state: { reducerUsers: TUserState }
}>(
    'user/deleteUsersSession',
    async function(_, {rejectWithValue, getState}){
        const token:string = getState().reducerUsers.user?.token!

        try{
            await axios
                .delete("/users/sessions", {
                    headers: {
                        'Authentication': token
                    }
                })

        }catch(error: any){
            if(error?.response?.data){
                return rejectWithValue(error.response.data);
            }else{
                return rejectWithValue({global: "Потеряно соединение!"});
            }
        }
    }
);

const setPending = (state: TUserState)=>{
    state.userStatus = 'loading';
    state.error = null;
}

const setError = (state: TUserState, action: any)=>{
    state.userStatus = 'rejected';
    state.error = action.payload;
}

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        restartAllStates(state){
            state.user = undefined;
            state.userStatus = null;
            state.error = null;
        },
    },
    extraReducers: (builder)=>{
        //---------------------------------------------------
        builder.addCase(createUsersAT.pending, setPending);
        builder.addCase(createUsersAT.fulfilled, (state, action)=>{
            state.userStatus = 'resolved';
            state.user = action.payload;
        });
        builder.addCase(createUsersAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(createUsersSessionAT.pending, setPending);
        builder.addCase(createUsersSessionAT.fulfilled, (state, action)=>{
            state.userStatus = 'resolved';
            state.user = action.payload;
        });
        builder.addCase(createUsersSessionAT.rejected, setError);
        //---------------------------------------------------
        builder.addCase(deleteUsersSessionAT.pending, setPending);
        builder.addCase(deleteUsersSessionAT.fulfilled, (state, action)=>{
            state.userStatus = null;
            state.user = undefined;
        });
        builder.addCase(deleteUsersSessionAT.rejected, setError);
        //---------------------------------------------------
    }
});

export const {restartAllStates} = userSlice.actions;
export default userSlice.reducer;