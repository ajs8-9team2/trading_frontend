import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from "../../axiosApi";
import { TClientStockState, TGetPostClientStockAT,  } from "../../types/typesClientStock";
import { TUserState } from "../../types/typesUser";

const initialState: TClientStockState = {
    allClientStocks: [],
    clientStockStatus: null,
    error: null,
}

// Акции конкретного клиента. Где clientid это id клиента
/** GET_QUERY http://localhost:8000/client-stocks/client?clientid=${id} */
export const getClientStocksAT = createAsyncThunk<TGetPostClientStockAT[], string, { rejectValue: string | null }>(
    'clientStocks/getClientStocks',
    async function (id, { rejectWithValue }) {
        try {
            const response = await axios
                .get(`/client-stocks/client?clientid=${id}`)
                .then(res => {
                    return res.data
                });
            return response;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);


/** POST http://localhost:8000/client-stocks */
export const createClientStockAT = createAsyncThunk<TGetPostClientStockAT, TGetPostClientStockAT | undefined, {
    rejectValue: string | null, state: { reducerUsers: TUserState }
}>(
    'clientStocks/createClientStock',
    async function (clientStockData, { rejectWithValue, getState }) {
        const token: string = getState().reducerUsers.user?.token!
        try {
            return await axios
                .post('/client-stocks', clientStockData, {
                    headers: {
                        'Authentication': token
                    }
                }
                ).then(res => res.data);
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

const setPending = (state: TClientStockState) => {
    state.clientStockStatus = 'loading';
    state.error = null;
};

const setError = (state: TClientStockState, action: any) => {
    state.clientStockStatus = 'rejected';
    state.error = action.payload;
};

const clientStockSlice: any = createSlice({
    name: 'clientStock',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        //---------------------------------------------------
        builder.addCase(getClientStocksAT.pending, setPending);
        builder.addCase(getClientStocksAT.fulfilled, (state, action) => {
            state.clientStockStatus = 'resolved';
            state.allClientStocks = action.payload;
        });
        builder.addCase(getClientStocksAT.rejected, setError);
        
        //---------------------------------------------------
        builder.addCase(createClientStockAT.pending, setPending);
        builder.addCase(createClientStockAT.fulfilled, (state, action) => {
            state.clientStockStatus = 'resolved';
            state.allClientStocks.push(action.payload);
        });
        builder.addCase(createClientStockAT.rejected, setError);
        //---------------------------------------------------
    }
})

export default clientStockSlice.reducer;