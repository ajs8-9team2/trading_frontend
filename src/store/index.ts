import {configureStore, MiddlewareAPI, Dispatch, AnyAction} from '@reduxjs/toolkit';
import usersSlice from './services/userSlice';
import accRequestsSlice from './services/accRequestSlice';
import depositRequestSlice from './services/depositRequestSlice'
import transferRequestSlice from './services/transferRequestSlice'
import clientAccountSlice from './services/clientAccountSlice'
import notificationSlice, {setNewObj, setRingState} from './services/notificationSlice'
import accountSlice from './services/accountSlice'
import stockRequestSlice from './services/stockRequestSlice'
import clientStockSlice from "./services/clientStockSlice";
import {TUserState} from "../types/typesUser";
import {TAccRequestsState} from "../types/typesAccReq";
import {CurriedGetDefaultMiddleware} from "@reduxjs/toolkit/dist/getDefaultMiddleware";
// import {TAccount, TAccountState} from "../types/typesNotification";
import {combineReducers, applyMiddleware, createStore} from 'redux'
import {Middleware} from 'redux'
import {T_id, TUser} from "../types/reestrOfAlias";
import {useAppDispatch} from "../hook";
import {TNoticeObj} from "../types/typesNotification";


// import {SubMiddlewareApi} from "@reduxjs/toolkit/dist/query/core/buildMiddleware/types";
// import {Dispatch} from "react";
// все состояния store

/*
    https://redux.js.org/usage/usage-with-typescript // типизация мидлваров 06/01/23

    //https://ru.hexlet.io/courses/js-redux/lessons/middlewares/theory_unit
    *** Внешняя функция
    Именно она является мидлварой и передается в метод applyMiddleware().
    Функция получает на вход объект store, который содержит методы dispatch() и
    getState() для работы с флоу Redux.

    *** Первая вложенная функция
    Её аргументом будет особая функция next(). Вызов этой функции в теле мидлвары с
     действием в качестве аргумента может прокидывать действие дальше по цепочке мидлвар.

    Но если next() вызван в последней мидлваре в цепочке (цепочка может состоять и из одной мидлвары),
    она диспатчит действие, отправляя его в редьюсер и вызывая обновление стейта.

    *** Вторая вложенная функция
    Это функция, замыкающая в себе действие action при его диспатчинге.
    Всякое действие в приложении, отправляемое в редьюсер, будет перехватываться мидлварой.
*/

// type allStatesStore = TUserState & TAccRequestsState
const logger = ({getState}: MiddlewareAPI)=>
    (next: Dispatch)=>
        (action: AnyAction)=>{
            // console.clear()
            // console.group(action.type);
            console.log("##################################################")
            // console.log(action.type);
            // console.info('dispatching', action);
            // console.log(JSON.stringify(action, null, 2));
            // const result = next(action);
            // console.log(getState().reducerAccount?.oneAccount)
            // // console.log(JSON.stringify(result, null, 2))
            // // console.log('next state', store.getState());
            // // console.log(JSON.stringify(store.getState(), null, 2));
            // // console.groupEnd();
            // return result;
        };

// const localStorageMiddleware = ({getState}: TUser)=>(next: any)=>(action: any)=>{
const localStorageMiddleware = ({getState}: MiddlewareAPI)=>
    (next: Dispatch)=>
        (action: AnyAction)=>{
            const result = next(action);
            const innerObj: string = JSON.stringify(getState().reducerUsers?.user!)
            localStorage.setItem('userStorage', innerObj);
            // console.log("localStorageMiddleware")
            return result;
        };

//https://wanago.io/2021/12/20/redux-middleware-websockets/
// const crashMiddleware = ({getState}: MiddlewareAPI)=>
//     (next: Dispatch)=>
//         (action: AnyAction)=>{
//             console.log('crashMiddleware');
//             try{
//                 return next(action)
//             }catch(error){
//                 console.error('Caught an exception!', error)
//                 throw error;
//             }
//         }

// import { io } from 'socket.io-client';
// const chatMiddleware: Middleware = store => next => action => {
// const chatMiddleware = ( {getState}: MiddlewareAPI)=>
//     (next: Dispatch)=>
//         (action: AnyAction)=>{
//             const state = getState()
//
//             if(!chatActions.startConnecting.match(action)){
//                 return next(action);
//             }
//
//             // const socket = io(process.env.REACT_APP_API_URL, {
//             //     withCredentials: true,
//             // });
//
//             // socket.on('connect', ()=>{
//             //     // store.dispatch(chatActions.connectionEstablished());
//             //     state.dispatch(chatActions.connectionEstablished())
//             // })
//
//             next(action);
//         }

// enum ChatEvent {
//     SendMessage = 'send_message',
//         RequestAllMessages = 'request_all_messages',
//         SendAllMessages = 'send_all_messages',
//         ReceiveMessage = 'receive_message'
// }
//
// interface ChatMessage {
//     id: number;
//     content: string;
//     author: {
//         email: string;
//     }
// }

const chatMiddleware: Middleware = storeApi=>{
    // на тот случай если страница перезагрузится
    const token = storeApi.getState()?.reducerUsers?.user?.token!
    let socket:any;
    // если известен токен, то только тогда устанавливаем соедение
    // данное соединение устанавливается, если страница была перезагружена

    if(token){
        const role = storeApi.getState()?.reducerUsers?.user?.role!
        socket = new WebSocket(`ws://localhost:8000/chat?token=${token}`);

        socket.onopen = (event: any)=>{
            console.log("Соединение установлено после перезагрузки страницы");
            // alert("Отправляем данные на сервер");
            // socket.send("Меня зовут Джон");
        };

        socket.onmessage = function(event: any){
            // alert(`[message] Данные получены с сервера: ${event.data}`);
            const decodedMessage = JSON.parse(event.data);
            // alert(JSON.stringify(decodedMessage, null, 2))
            if(role === "client")
                storeApi.dispatch(setNewObj(decodedMessage))
            else
                console.log(decodedMessage)
        };

        // socket.addEventListener("message", (msg: any)=>{
        //     const decodedMessage = JSON.parse(msg.data);
        //     switch(decodedMessage.type){
        //         case "NEW_MESSAGE":
        //             storeApi.dispatch(setNewObj(decodedMessage))
        //             break;
        //     }
        // })

        socket.onclose = (event: any)=>{
            if(event.wasClean){
                console.log(`Соединение закрыто чисто, код=${event.code} причина=${event.reason}`);
            }else{
                console.log('Соединение прервано');
            }
        }
    }
    // socket.close();

//-----------------------------------------------------------------------------------
//****************
    return next=>{
//**********************
        return action=>{
//****************************
            const result = next(action);

            // console.log(  JSON.stringify(action, null, 2))
            // console.log(action)
            //account/getAccount/fulfilled
            //user/postSessionUsers/fulfilled

            if(action.type==="user/postSessionUsers/fulfilled"){
                // alert("user/postSessionUsers/fulfilled")
                const token = storeApi.getState()?.reducerUsers?.user?.token!
                const role = storeApi.getState()?.reducerUsers?.user?.role!
                // alert(token)
                socket = new WebSocket(`ws://localhost:8000/chat?token=${token}`);

                socket.onopen = (event: any)=>{
                    console.log("Соединение установлено при входе пользователя");
                    // alert("Отправляем данные на сервер");
                    // socket.send("Меня зовут Джон");
                };

                socket.onmessage = function(event:any) {
                    // alert(`[message] Данные получены с сервера: ${event.data}`);
                    const decodedMessage = JSON.parse(event.data);
                    // alert(JSON.stringify(decodedMessage, null, 2))
                    if(role === "client")
                        storeApi.dispatch(setNewObj(decodedMessage))
                    else
                        console.log(decodedMessage)
                };
            }

            if(action.type==="user/deleteUsersSession/fulfilled"){
                console.log("user/deleteUsersSession/fulfilled")
                storeApi.dispatch(setRingState(2));
                socket.close();
                socket.onclose = function(event:any) {
                    if (event.wasClean) {
                        console.log(`[close] Соединение закрыто чисто, код=${event.code} причина=${event.reason}`);
                    } else {
                        // например, сервер убил процесс или сеть недоступна
                        // обычно в этом случае event.code 1006
                        console.log('[close] Соединение прервано');
                    }
                };
            }
            /** ACCREQUEST */
            if(action.type==="notification/patchNotification/accRequest"){
                const innerObj:TNoticeObj = action.payload;
                // alert(JSON.stringify(innerObj, null, 2))
                socket.send(JSON.stringify(innerObj));
            }

            /** DEPOSITREQUEST */
            if(action.type==="notification/patchNotification/depositRequest"){
                const innerObj:TNoticeObj = action.payload;
                // alert(JSON.stringify(innerObj, null, 2))
                socket.send(JSON.stringify(innerObj));
            }

            /** TRANSFERREQUEST */
            if(action.type==="notification/patchNotification/transferRequest"){
                const innerObj:TNoticeObj = action.payload;
                // alert(JSON.stringify(innerObj, null, 2))
                socket.send(JSON.stringify(innerObj));
            }

            /** STOCKREQUEST */
            if(action.type==="notification/patchNotification/stockRequest"){
                const innerObj:TNoticeObj = action.payload;
                // alert(JSON.stringify(innerObj, null, 2))
                socket.send(JSON.stringify(innerObj));
            }


            return result
        }
    }
}
// export const exampleMiddleware: Middleware<{}, TAccountState> = storeApi=>next=>action=>{
//     const state = storeApi.getState() // correctly typed as RootState
//     console.clear()
//     console.log("##################################################")
//     // console.log(state.oneAccount);
//     // next(action);
//
//     console.log('next state', store.getState())
//     console.log(action.type)
//     console.info('dispatching', action)
//
//     let result = next(action)
//     console.log('next state', store.getState())
//     console.log(action.type)
//
//     return result
//     //
//     // const result = next(action);
//     // console.log(result)
// }
//--------------------------------------------------------------------------
const loadFromLocalStorage = (): any=>{
    const strJSONUser: string | null = localStorage.getItem('userStorage');

    // Object.prototype.valueOf()

    if(strJSONUser !== "undefined"){
        // if(!Object.prototype.toString.call(strJSONUser).includes("Undefined")){
        // alert(JSON.stringify(strJSONUser, null, 2));
        const innerParse = JSON.parse(strJSONUser as string);
        return {
            reducerUsers: {
                user: innerParse
                // user: JSON.parse(<string>localStorage.getItem('userStorage'))
            }
        };
    }else
        return undefined;
};


// Reducer<{reducerUsers: TUserState, reducerAccRequests: TAccRequestsState}, AnyAction> | ReducersMapObject<{reducerUsers: TUserState, reducerAccRequests: TAccRequestsState}, AnyAction>
//TUserState & TAccRequestsState
// const store = configureStore<any,any,any>({
// const store = configureStore<{reducer:{reducerUsers: TUserState, reducerAccRequests: TAccRequestsState}}>({
// const store = configureStore<{ reducerUsers: TUserState, reducerAccRequests: TAccRequestsState }>({
    // const allAccRequests: TGetAccRequestsAT[] = useAppSelector(state=>state.reducerAccRequests.allAccRequests);
const rootReducer = combineReducers({
    // reducerChat: chatSlice,
    reducerUsers: usersSlice,
    reducerAccRequests: accRequestsSlice,
    reducerAccount: accountSlice,
    reducerDepositRequest: depositRequestSlice,
    reducerTransferRequest: transferRequestSlice,
    reducerClientAccount: clientAccountSlice,
    reducerNotification:notificationSlice,
    reducerStock: stockRequestSlice,
    reducerClientStock: clientStockSlice
})

const store = configureStore({
    reducer: rootReducer,
    preloadedState: loadFromLocalStorage(),
    // middleware: (getDefaultMiddleware: ()=>any[])=>
    middleware: (getDefaultMiddleware: CurriedGetDefaultMiddleware)=>{
        // return getDefaultMiddleware().concat([localStorageMiddleware, logger, crashMiddleware])
        return getDefaultMiddleware().concat([localStorageMiddleware, chatMiddleware])
    }
    // .concat(logger)
});

// export default function configureStore(preloadedState: any){
// const store = function configureStore(preloadedState: any){
// // const store = configureStore(preloadedState: any){
//     // const middlewares = [loggerMiddleware, thunkMiddleware]
//     // const middlewareEnhancer = applyMiddleware(...middlewares)
//
//     // const enhancers = [middlewareEnhancer, monitorReducersEnhancer]
//     // const composedEnhancers = composeWithDevTools(...enhancers)
//
//     // const store = createStore(rootReducer, preloadedState, composedEnhancers)
//     const rootReducer = combineReducers({
//         // Define a top-level state field named `todos`, handled by `todosReducer`
//         // todos: todosReducer,
//         // filters: filtersReducer
//         reducerUsers: usersSlice,
//         reducerAccRequests: accRequestsSlice,
//     })
//
//     const store = createStore(rootReducer, preloadedState)
//     return store
// }

export default store;
// type RootState = ReturnType<typeof rootReducer>
export type RootState = ReturnType<typeof store.getState>; // в будущем закоментить
// export type RootState = ReturnType<typeof rootReducer>; // <<< это раскоментить  aza 09/01/23
export type AppDispatch = typeof store.dispatch;
// export type AppDispatch = typeof rootReducer;