// const apiURL = 'http://localhost:8000'
// export default  apiURL;

import axios from "axios";
// import apiURL from "./config";


let apiHost = "http://localhost";
let apiPort = 8000;

if(process.env.REACT_APP_NODE_ENV === "test") {
    apiPort = 8010;
}

if(process.env.REACT_APP_CI) {
    apiHost = "http://trading-broker.ddns.net";
}

if(process.env.REACT_APP_NODE_ENV === "prod") {
    apiHost = "https://trading-broker.ddns.net";
    apiPort = 81;
}

let apiURL = `${apiHost}:${apiPort}`;
export const uploadsUrl = `${apiURL}/uploads`;

export default axios.create({
    // baseURL: "http://localhost:8000"
    baseURL: apiURL
});