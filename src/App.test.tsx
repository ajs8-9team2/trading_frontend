import React from 'react';
import {render, screen} from '@testing-library/react';
import App from './App/App';
import {checkIIN} from './Helpers/checkIIN';
import {cutLength} from './Helpers/cutLength';
import {getDate} from './Helpers/getDate';
import {getTime} from "./Helpers/getTime";
import {getStatus} from "./Helpers/getStatus";
import {getStatusColor} from "./Helpers/getStatusColor";
import {getStatusTrader} from "./Helpers/getStatusTrader";
import {getStockName} from "./Helpers/getStockName";
import {getStockReqType} from "./Helpers/getStockReqType";
import {getTypeOfRequest} from "./Helpers/getTypeOfRequest";
import {getValueOfKey} from "./Helpers/getValueOfKey";
import {notificationObject} from "./Helpers/notificationObject";
import {returnNameOfBank} from "./Helpers/returnNameOfBank";
// import {validateValue} from './Helpers/validateValue'

// test('renders learn react link', () => {
//   render(<App/>);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

describe("функция checkIIN - " +
    "валидация ИИН на соотвествие алгоритму", ()=>{
    test("проверка корректного ИИН", ()=>{
        expect(checkIIN("880922302893"))
            .toBe(true);
    });
    test("проверка НЕ корректного ИИН", ()=>{
        expect(checkIIN("880922302891"))
            .toBe(false);
    });
});

describe("функция cutLength - " +
    "обрезает лишнюю длину строки", ()=>{
    test("проверка на обрезку строки", ()=>{
        expect(cutLength("почти длинная строка", 10))
            .toBe("почти длин...");
    });
    test("проверка без обрезки строки", ()=>{
        expect(cutLength("строка", 10))
            .toBe("строка");
    });
});

describe("функция getDate - " +
    "функция возвращающая дату в формате dd.mm.yyyy", ()=>{
    test("проверка на возврат даты", ()=>{
        expect(getDate("2023-01-05T13:27:56.750Z"))
            .toBe("05.01.2023");
    });
    test("проверка на возврат дефолтного значения строки dd.mm.yyyy при" +
        " передаче некорректной даты", ()=>{
        expect(getDate("01-05T13:27:56.750Z"))
            .toBe("dd.mm.yyyy");
    });
});

describe("функция getTime - " +
    "функция возвращающая время в формате hh:mm", ()=>{
    test("проверка на возврат времени в формате hh:mm", ()=>{
        expect(getTime("2023-01-29T09:46:53.906Z"))
            .toBe("15:46");
    });
    test("проверка на возврат дефолтного значения строки hh:mm при" +
        " передаче некорректной даты", ()=>{
        expect(getTime("01-05T13:27:56.750Z"))
            .toBe("hh:mm");
    });
});

describe("функция getStatus - " +
    "функция возвращающая текущий статус заявления", ()=>{
    test("функция возвращает: поступило новое заявление", ()=>{
        expect(getStatus("Pending"))
            .toBe("поступило новое заявление");
    });
    test("функция возвращает: на рассмотрении у сотрудника фронт-офиса", ()=>{
        expect(getStatus("FrontWIP"))
            .toBe("на рассмотрении у сотрудника фронт-офиса");
    });
    test("функция возвращает: на рассмотрении у сотрудника фронт-офиса", ()=>{
        expect(getStatus("WorkInProgress"))
            .toBe("на рассмотрении у сотрудника фронт-офиса");
    });
    test("функция возвращает: на рассмотрении у бухгалтера", ()=>{
        expect(getStatus("AccountantWIP"))
            .toBe("на рассмотрении у бухгалтера");
    });
    test("функция возвращает: на рассмотрении у трейдера", ()=>{
        expect(getStatus("TraderWIP"))
            .toBe("на рассмотрении у трейдера");
    });
    test("функция возвращает: заявление одобрено сотрудником фронт-офиса", ()=>{
        expect(getStatus("Approved"))
            .toBe("заявление одобрено сотрудником фронт-офиса");
    });
    test("функция возвращает: заявление одобрено бухгалтером", ()=>{
        expect(getStatus("Completed"))
            .toBe("заявление одобрено бухгалтером");
    });
    test("функция возвращает: заявление одобрено трейдером", ()=>{
        expect(getStatus("PartiallyCompleted"))
            .toBe("заявление одобрено трейдером");
    });
    test("функция возвращает: заявление отклонено", ()=>{
        expect(getStatus("Declined"))
            .toBe("заявление отклонено");
    });
});

describe("функция getStatusColor - " +
    "функция возвращающая цвет в зависимости от статуса", ()=>{
    test("проверка на возврат цвета gold", ()=>{
        expect(getStatusColor("Approved"))
            .toBe("gold");
    });
    test("проверка на возврат дефолтного  при" +
        " передаче иного статуса", ()=>{
        expect(getStatusColor("some_status"))
            .toBe("processing");
    });
});

describe("функция getStatusTrader - " +
    "функция возвращающая цвет в зависимости от статуса", ()=>{
    test("проверка на возврат цвета gold", ()=>{
        expect(getStatusTrader("Approved"))
            .toBe("В ожидании");
    });
    test("проверка на возврат дефолтного значения при" +
        " передаче статуса TraderWIP", ()=>{
        expect(getStatusTrader("TraderWIP"))
            .toBe("В процессе");
    });
});

describe("функция getStockName - " +
    "функция возвращающая тиккер или название компании", ()=>{
    test("функция возвращающая название компании при передаче тиккера", ()=>{
        expect(getStockName("TSLA", "COMPANY"))
            .toBe("Tesla");
    });
    test("функция возвращающая название тиккера при передаче компании", ()=>{
        expect(getStockName("NVIDIA", "COMP"))
            .toBe("NVDA");
    });
});

describe("функция returnNameOfBank - " +
    "функция возвращающая БИК или название Банка", ()=>{
    test("функция возвращающая название банка при передаче в параметры БИК", ()=>{
        expect(returnNameOfBank("HCSKKZKA","BANK"))
            .toBe("АО «Жилищный строительный сберегательный банк «Отбасы банк»");
    });
    test("функция возвращающая название БИК при передаче названия банка", ()=>{
        expect(returnNameOfBank("АО «Жилищный строительный сберегательный банк «Отбасы банк»", "БИК"))
            .toBe("HCSKKZKA");
    });
});

describe("функция getStockReqType - " +
    "возвращает вид действия по трейдерским операциям", ()=>{
    test("функция возвращающая строку Покупка при передаче Buy", ()=>{
        expect(getStockReqType("Buy"))
            .toBe("Покупка");
    });
    test("функция возвращающая строку Продажа  при передаче Sell", ()=>{
        expect(getStockReqType("Sell"))
            .toBe("Продажа");
    });
});

describe("функция getTypeOfRequest для уведомлений описывает вид заявления", ()=>{
    test("функция возвращающая строку Покупка при передаче Buy", ()=>{
        expect(getTypeOfRequest("accRequest"))
            .toBe("ID клиентского заявления");
    });
    test("функция возвращающая строку: 'ID заявления на пополнение счета' при передаче 'depositRequest'", ()=>{
        expect(getTypeOfRequest("depositRequest"))
            .toBe("ID заявления на пополнение счета");
    });
    test("функция возвращающая строку: 'ID заявления на перевод денежных средств' при передаче 'transferRequest'", ()=>{
        expect(getTypeOfRequest("transferRequest"))
            .toBe("ID заявления на перевод денежных средств");
    });
    test("функция возвращающая строку: 'ID заявления по операциям с акциями' при передаче 'stockRequest'", ()=>{
        expect(getTypeOfRequest("stockRequest"))
            .toBe("ID заявления по операциям с акциями");
    });
});

describe("функция getValueOfKey используется для таблиц, возвращает соотвествующее значение в зависимости от ключа", ()=>{
    test("функция возвращающая дате в формате dd.mm.yyyy", ()=>{
        expect(getValueOfKey("date", {date: "2023-01-05T13:27:56.750Z"}))
            .toBe("05.01.2023");
    });
    test("функция возвращающая строку числа разбитую на разряды", ()=>{
        expect(getValueOfKey("amount", {amount: "100000"}))
            .toBe("100000");
    });
    test("функция возвращающая статус заявления", ()=>{
        expect(getValueOfKey("status", {status: "Pending"}))
            .toBe("поступило новое заявление");
    });
    test("функция возвращающая обрезанную строку", ()=>{
        expect(getValueOfKey("rejectionReason", {rejectionReason: "самая длинная строка"}))
            .toBe("самая длин...");
    });
    test("функция возвращающая обрезанную строку", ()=>{
        expect(getValueOfKey("rejectionReason", {rejectionReason: "самая длинная строка"}))
            .toBe("самая длин...");
    });
    test("функция возвращающая строку в верхнем регистре", ()=>{
        expect(getValueOfKey("_id", {_id: "as12sf125662"}))
            .toBe("AS12SF125662");
    });
    test("функция возвращающая строку в верхнем регистре", ()=>{
        expect(getValueOfKey("assignedTo", {assignedTo: "as12sf125662"}))
            .toBe("AS12SF125662");
    });
    test("функция возвращающая строку в верхнем регистре", ()=>{
        expect(getValueOfKey("frontManager", {frontManager: "as12sf125662"}))
            .toBe("AS12SF125662");
    });
});

describe("функция notificationObject используется для диспатча обрабатываемая " +
    "в мидлвэйрах редакса", ()=>{
    test("функция возвращающая объектбв параметры переданы: FrontWIP, accRequest,", ()=>{
        expect(notificationObject(
            "as12sf125662",
            "as12sf125663",
            "FrontWIP",
            "accRequest",
            "какое-то сообщение"
        )).toEqual({
            type: 'notification/patchNotification/accRequest',
            payload: {
                requestID: 'as12sf125662',
                userID: 'as12sf125663',
                status: 'FrontWIP',
                typeOfRequest: 'accRequest',
                notice: 'какое-то сообщение',
                type: 'MESSAGE_CREATED'
            }
        })
    })
    test("функция возвращающая объектбв параметры переданы: Approved, depositRequest", ()=>{
        expect(notificationObject(
            "as12sf125662",
            "as12sf125663",
            "Approved",
            "depositRequest",
            "какое-то сообщение"
        )).toEqual({
            type: 'notification/patchNotification/depositRequest',
            payload: {
                requestID: 'as12sf125662',
                userID: 'as12sf125663',
                status: 'Approved',
                typeOfRequest: 'depositRequest',
                notice: 'какое-то сообщение',
                type: 'MESSAGE_CREATED'
            }
        })
    })
    test("функция возвращающая объектбв параметры переданы: AccountantWIP, transferRequest", ()=>{
        expect(notificationObject(
            "as12sf125662",
            "as12sf125663",
            "AccountantWIP",
            "transferRequest",
            "какое-то сообщение"
        )).toEqual({
            type: 'notification/patchNotification/transferRequest',
            payload: {
                requestID: 'as12sf125662',
                userID: 'as12sf125663',
                status: 'AccountantWIP',
                typeOfRequest: 'transferRequest',
                notice: 'какое-то сообщение',
                type: 'MESSAGE_CREATED'
            }
        })
    })
});
