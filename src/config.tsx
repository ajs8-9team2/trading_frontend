import * as Yup from 'yup';
import { checkIIN } from './Helpers/checkIIN';

export const validationFirstStage = Yup.object().shape({
    firstName: Yup.string()
        .required('Заполните поле'),
    lastName: Yup.string()
        .required('Заполните поле'),
    dateOfBirth: Yup.string()
        .required('Заполните поле'),
    identificationNumber: Yup.string().test("identificationNumber", "ИНН не валидный", checkIIN)
        .required('Заполните поле')
        .min(12, 'ИИН состоит из 12 символов')
        .max(12),
});

export const validationSecondStage = Yup.object().shape({
    documentFront: Yup.mixed().test("file", "Выберите файл", (value) => {
        if (value.length > 0) {
            return true;
        }
        return false;
    }),
    documentBack: Yup.mixed().test("file", "Выберите файл", (value) => {
        if (value.length > 0) {
            return true;
        }
        return false;
    }),
    citizenship: Yup.string()
        .required('Выберите поле'),
    idCardNumber: Yup.number()
        .required('Заполните поле')
        .typeError('Введите цифры')
        .min(9, 'Номер состоит из 9 символов'),
    authority: Yup.string()
        .required('Выберите поле'),
    dateOfIssue: Yup.string()
        .required('Заполните поле'),
    expirationDate: Yup.string()
        .required('Заполните поле')
});


export const validationTransfer = Yup.object().shape({
    senderFullName: Yup.string()
        .required('Заполните поле'),
    senderIIN: Yup.string()
        .required('Заполните поле')
        .min(12, 'ИИН состоит из 12 символов!')
        .max(12),
    senderIIK: Yup.string()
        .required('Заполните поле'),
    senderBIK: Yup.string()
        .required('Заполните SWIFT-код банка или БИК'),
    senderBank: Yup.string() || Yup.array()
        .required('Заполните наименование банка бенефициара'),
    senderKBe: Yup.number()
        .required('Заполните поле')
        .typeError('Код Бенефициара состоит из 2 символов!')
        .min(2, 'Код Бенефициара состоит из 2 символов'),
    amount: Yup.number()
        .required('Сумма перевода не может быть нулем')
});


export const validationWithdraw = Yup.object().shape({
    senderFullName: Yup.string()
        .required('Заполните поле'),
    recipientFullName: Yup.string()
        .required('Заполните поле'),
    senderIIN: Yup.string()
        .required('Заполните поле')
        .min(12, 'ИИН состоит из 12 символов!')
        .max(12),
    recipientIIN: Yup.string()
        .required('Заполните поле')
        .min(12, 'ИИН состоит из 12 символов!')
        .max(12),
    senderIIK: Yup.string()
        .required('Заполните поле'),
    recipientIIK: Yup.string()
        .required('Заполните поле'),
    senderBIK: Yup.string()
        .required('Заполните SWIFT-код банка или БИК'),
    recipientBIK: Yup.string()
        .required('Заполните SWIFT-код банка или БИК'),
    senderBank: Yup.string() || Yup.array()
        .required('Заполните наименование банка бенефициара'),
    recipientBank: Yup.string() || Yup.array()
        .required('Заполните наименование банка бенефициара'),
    senderKBe: Yup.number()
        .required('Заполните поле')
        .typeError('Код Бенефициара состоит из 2 символов!')
        .min(2, 'Код Бенефициара состоит из 2 символов'),
    recipientKBe: Yup.number()
        .required('Заполните поле')
        .typeError('Код Бенефициара состоит из 2 символов!')
        .min(2, 'Код Бенефициара состоит из 2 символов'),
    amount: Yup.number()
        .required('Сумма перевода не может быть нулем')
});
// const apiURL = 'http://localhost:8000'
// export default apiURL;