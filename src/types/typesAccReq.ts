// import {TFirstUser, TUser} from "./typesUser";
import {
    TUser,
    TAddress, TArea, TAuthority, TCitizenship,
    TCity,
    TCountry,
    TDateOfBirth, TDateOfIssue, TDocumentBack, TDocumentFront, TEmail, TExpirationDate,
    TFirstName, TIdCardNumber,
    TIdentificationNumber,
    TLastname,
    TPatronym, TPhone, TStatusAccRequest, TRejectionReason, T_id, TDate, T__v, TRequestType, TAssignedTo, TStateStatus
} from "./reestrOfAlias";

/*** типы для полей объектов, стейтов ***/
export type TFirstAccReq = {
    firstName: TFirstName,
    lastName: TLastname,
    patronym: TPatronym,
    dateOfBirth: TDateOfBirth,
    identificationNumber: TIdentificationNumber,
}

export type TSecondAccReq = {
    country: TCountry,
    area: TArea,
    city: TCity,
    address: TAddress,
}

export type TThirdAccReq = {
    email: TEmail,
    phone: TPhone,
}

export type TFourthAccReq = {
    documentFront?: TDocumentFront,
    documentBack?: TDocumentBack,
    idCardNumber: TIdCardNumber,
    citizenship: TCitizenship,
    authority: TAuthority,
    dateOfIssue: TDateOfIssue,
    expirationDate: TExpirationDate,
}

// поля формируемые на стороне сервера
export type TAccReqServerTypes = {
    user: TUser,
    status: TStatusAccRequest,
    __v?: T__v,
    _id?: T_id,
    date: TDate,
    rejectionReason: TRejectionReason,
    assignedTo: TAssignedTo,
    requestType: TRequestType,// "Account Request" ??????????? нужен ключ ??????????
}


//#############################################################################################
/*** типы для параметров запросов Async Thunk в redux-slice***/
export type TPatchAccRequestsAT = {
    // workStatus?: "Pending" | "WorkInProgress" | "Completed",
    // rejectionReason: string,
    status: TStatusAccRequest,
    // _idAccReq: string,
    // _idUser:string,
    _idAccReq?: string,// ключ на сервер не отправляется
    assignedTo?: TUser | string,
    rejectionReason?: string, //| number,
}

// тип для запроса GET: fetchAccRequestsAT и getAccRequestAT
export type TGetAccRequestsAT = undefined | (TAccReqServerTypes & TFirstAccReq & TSecondAccReq & TThirdAccReq & TFourthAccReq);

// тип для запроса POST: createAccRequestAT
export type TPostAccRequestAT = TFirstAccReq & TSecondAccReq & TThirdAccReq & TFourthAccReq;

//#############################################################################################
/*** типы для слайсов ***/
// тип для стейта слайса
export type TAccRequestsState = {
    oneAccRequest: TGetAccRequestsAT;
    // userAccRequest: any,
    allAccRequests: TGetAccRequestsAT[];
    accRequestStatus: TStateStatus;//"loading" | "pending" | "resolved" | "rejected" | null,
    error: string | null;
}

//#############################################################################################
/*** PROPS STATES ***/

export type TRegisterPropsFirstStage = {
    state: TFirstAccReq,
    onChange: React.ChangeEventHandler<HTMLInputElement>,
    ObjFirstName: any,
    ObjLastName: any,
    ObjBirthday: any,
    ObjIdentificationNumber: any,
    errors: any
}

export type TRegisterPropsSecondStage = {
    state: TSecondAccReq,
    onChange: React.ChangeEventHandler<HTMLInputElement>,
}

export type TRegisterPropsThirdStage = {
    state: TThirdAccReq,
    onChange: React.ChangeEventHandler<HTMLInputElement>,
}

export type TRegisterPropsFourthStage = {
    state: TFourthAccReq,
    onChange: React.ChangeEventHandler<HTMLInputElement>,
    fileChangeHandler: any,
    selectFourthChangeHandler: any,
    ObjDocumentFront: any,
    ObjDocumentBack: any,
    ObjDateOfIssue: any,
    ObjExpirationDate: any,
    ObjIdCardNumber: any,
    ObjCitizenship: any,
    ObjAuthority: any,
    errorsSecond: any
}

/*import mongoose, { Schema } from "mongoose";

export interface IAccReq extends mongoose.Document {
    user: string;
    status: string;
    firstName: string;
    lastName: string;
    patronym: string,
    dateOfBirth: string;
    identificationNumber: string;
    email: string;
    phone: string;
    country: string;
    city: string;
    area: string;
    address: string;
    date: string;
    authority: string;
    dateOfIssue: string;
    expirationDate: string;
    documentFront: string;
    documentBack: string;
    rejectionReason: string;
    assignedTo: string;
    idCardNumber: string;
    requestType: string;
};

const AccReqSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "Укажите ID пользователя"]
    },
    status: {
        type: String,
        required: true,
        default: "Pending",
        enum: ["Pending", "WorkInProgress", "Approved", "Declined"]
    },
    firstName: {
        type: String,
        required: [true, "Необходимо имя клиента"]
    },
    lastName: {
        type: String,
        required: [true, "Необходима фамилия клиента"]
    },
    patronym: {
        type: String
    },
    dateOfBirth: {
        type: String,
        required: [true, "Необходима дата рождения клиента"]
    },
    identificationNumber: {
        type: String,
        required: [true, "Необходим ИИН клиента"],
        minlength: 12,
        maxlength: 12
    },
    email: {
        type: String,
        required: [true, "Необходима почта клиента"]
    },
    phone: {
        type: String
    },
    country: {
        type: String
    },
    area: {
        type: String
    },
    city: {
        type: String
    },
    address: {
        type: String
    },
    authority: {
        type: String,
        required: [true, "Необходимо поле кем выдано удостоверение"]
    },
    dateOfIssue: {
        type: String,
        required: [true, "Необходима дата выдачи удостоверения"]
    },
    expirationDate: {
        type: String,
        required: [true, "Необходима дата действия удостоверения"]
    },
    documentFront: {
        type: String,
        required: [true, "Необходимо загрузить первую страницу удостоверения"]
    },
    documentBack: {
        type: String,
        required: [true, "Необходимо загрузить первую страницу удостоверения"]
    },
    requestType: {
        type: String,
        required: true,
        default: "Account Request",
        enum: ["Account Request"]
    },
    rejectionReason: {
        type: String
    },
    assignedTo: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
    idCardNumber: {
        type: String,
        required: [true, "Необходим номер удостоверения"]
    },
    date: {
        type: Date,
        default: new Date()
    }
});


const AccRequest = mongoose.model<IAccReq>("AccRequest", AccReqSchema);

export default AccRequest;*/