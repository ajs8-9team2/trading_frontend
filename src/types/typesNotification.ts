import {
    T__v,
    T_id,
    TUser,
    TStateStatus
} from "./reestrOfAlias";


// export type TNoticeObj = {
//     date?: SchemaDefinitionProperty<string> | undefined;
//     notice: string;
//     status: "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed";// статус заявки
//     typeOfRequest: "accRequest" | "depositRequest" | "transferRequest";//вид заявки
//     isWasRead: boolean;
//     RequestID:string,//просто строка, объект заявок не приклеиваю
// }


export type TNoticeObj = {
    _id?: T_id;
    type?: "NEW_MESSAGE" | "MESSAGE_CREATED";
    date?: string;
    userID?: string;
    requestID: string;
    notice: string;
    isWasRead: boolean;
    status: "Pending" | "FrontWIP" | "AccountantWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted";
    typeOfRequest: "accRequest" | "depositRequest" | "transferRequest"| "stockRequest";
}

export type TNotification = {
    _id?: T_id;
    __v?: T__v;
    user: TUser;                                  // required
    userNotifications?: TNoticeObj[];
}
//#############################################################################################
/*** типы для параметров запросов Async Thunk в redux-slice***/
export type TGetPostNotificationAT = undefined | TNotification

//#############################################################################################
/*** типы для параметров запросов Async Thunk в redux-slice***/
export type TPatchNotificationAT = {
    _idNotification?: string;// ключ на сервер не отправляется
    userNotifications: TNoticeObj[];
}

//#############################################################################################
/*** типы для слайсов ***/
export type TNotificationState = {
    oneNotification: TGetPostNotificationAT,
    allNotification: TGetPostNotificationAT[],
    notificationStatus: TStateStatus,
    error: null,
    stateBell?: 0 | 1 | 2,
    isRing?: boolean,
}