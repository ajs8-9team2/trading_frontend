import {
    T__v, T_id,
    TAccountant,
    TAmount,
    TDate,
    TFrontManager,
    TRejectionReason,
    TSenderBank, TSenderBIK,
    TSenderFullName,
    TSenderIIK,
    TSenderIIN,
    TSenderKBe, TStateStatus,
    TStatusTransferDeposit,
    TUser
} from "./reestrOfAlias";

export type TDepositRequest = {
    _id?: T_id;
    __v?: T__v;
    user?: TUser;
    status?: TStatusTransferDeposit;
    senderIIN: TSenderIIN;              // required
    senderIIK: TSenderIIK;              // required
    senderBIK: TSenderBIK;              // required
    senderBank: TSenderBank;            // required
    senderKBe: TSenderKBe;              // required
    senderFullName: TSenderFullName;    // required
    amount: TAmount;                    // required
    frontManager?: TFrontManager;
    accountant?: TAccountant;
    rejectionReason?: TRejectionReason;
    date?: TDate;
}


//#############################################################################################
/*** типы для параметров запросов Async Thunk в redux-slice***/
export type TGetPostDepositRequestAT = TDepositRequest | undefined;

export type TPatchDepositRequestAT = {
    _idDepositRequest?: string,// ключ на сервер не отправляется
    rejectionReason?: TRejectionReason;
    status: TStatusTransferDeposit;//"Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed",
    frontManager?: TUser | string;
    accountant?: TUser | string;
}


//#############################################################################################
/*** типы для слайсов ***/
// тип для стейта слайса
export type TDepositRequestState = {
    oneDepositRequest: TGetPostDepositRequestAT,
    allDepositsRequest: TGetPostDepositRequestAT[],
    depositRequestStatus: TStateStatus,
    error: null,
}

/*
import mongoose, { Schema } from "mongoose";

export interface IDepositReq extends mongoose.Document {
    user: string;
    status: string;
    senderIIN: string;
    senderIIK: string;
    senderBIK: string;
    senderBank: string;
    senderKBe: string;
    senderFullName: string;
    frontManager: string;
    accountant: string;
    rejectionReason: string;
    amount: number;
    date: string;
};

const DepositReqSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User's ID is required"]
    },
    status: {
        type: String,
        required: true,
        default: "Pending",
        enum: ["Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]
    },
    amount: {
        type: Number,
        required: [true, "Money amount is required"]
    },
    senderFullName: {
        type: String,
        required: [true, "Sender's full name is required"]
    },
    senderIIN: {
        type: String,
        required: [true, "Sender's IIN is required"],
        minlength: 12,
        maxlength: 12
    },
    senderBank: {
        type: String,
        required: [true, "Sender's Bank is required"]
    },
    senderIIK: {
        type: String,
        required: [true, "Sender's IIK is required"]
    },
    senderBIK: {
        type: String,
        required: [true, "Sender's BIK is required"]
    },
    senderKBe: {
        type: String,
        required: [true, "Sender's KBe is required"]
    },
    frontManager: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    accountant: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    rejectionReason: {
        type: String,
    },
    requestType: {
        type: String,
        required: true,
        default: "Deposit request"
    },
    date: {
        type: Date,
        default: new Date()
    }
});

const DepositRequest = mongoose.model<IDepositReq>("DepositRequest", DepositReqSchema);

export default DepositRequest;
*/