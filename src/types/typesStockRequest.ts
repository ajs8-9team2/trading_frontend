import {
    T__v, T_id,
    TTrader,
    TStatusStock,
    TDate,
    TFrontManager,
    TRejectionReason,
    TUserFullName,
    TCompany,
    TTicker,
    TStockAmount,
    TPrice,
    TStockReqType,
    TStockResult,
    TStockFail,
    TPriceResult,
    TStateStatus,
    TUser
} from "./reestrOfAlias";

export type TStockRequest = {
    _id?: T_id;
    user?: TUser;
    status?: TStatusStock;
    frontManager?: TFrontManager;
    rejectionReason?: TRejectionReason;
    date?: TDate;
    userFullName: TUserFullName;        // required
    company: TCompany;                  // required
    ticker: TTicker;                    // required
    stockAmount: TStockAmount;          // required
    price: TPrice;                      // required
    stockReqType: TStockReqType;        // required
    stockResult?: TStockResult;
    stockFail?: TStockFail;
    priceResult?: TPriceResult;

    __v?: T__v;
    trader?: TTrader;
}


//#############################################################################################
/*** типы для параметров запросов Async Thunk в redux-slice***/
export type TGetPostStockRequestAT = TStockRequest | undefined;

export type TPatchStockRequestAT = {
    _idStockRequest?: string,// ключ на сервер не отправляется
    rejectionReason?: TRejectionReason;
    // statusStock: TStatusStock;//"Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted",
    status: TStatusStock;//"Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted",
    frontManager?: TUser | string;
    trader?: TUser | string;
    stockResult?: TStockResult;
    stockFail?: TStockFail;
    priceResult?: TPriceResult;
}


//#############################################################################################
/*** типы для слайсов ***/
// тип для стейта слайса
export type TStockRequestState = {
    oneStockRequest: TGetPostStockRequestAT,
    allStocksRequest: TGetPostStockRequestAT[],
    stockRequestStatus: TStateStatus,
    error: null,
}
