//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/** общие стейты */
export type T_id = string;
export type T__v = number | string;
export type TDate = string;
export type TRejectionReason = string;
// статус для стейта
export type TStateStatus = "loading" | "pending" | "resolved" | "rejected" | null;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/** User */
export type TPassword = string;
export type TPassword1 = string;
export type TPassword2 = string;
export type TRole = "client"| "frontOffice"| "backOffice"| "trader"| "accountant"| "admin";
export type TToken = string;
// export type TAccount = object
export type TEmail = string;
export type TUserPath = "account-opening" | "acc-pending" | "client-office";//| string;
export type TIsHasAccount = boolean;
export type TUser = undefined | {
    _id?: T_id;
    __v?: T__v;
    email: TEmail;          // required
    path?: TUserPath;
    password?: TPassword;
    password1?: TPassword1;
    password2?: TPassword2;
    role?: TRole;
    token?: TToken;
    // account?: TAccount;
    isHasAccount?: TIsHasAccount;
    fullName?: string;
};
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/** AccRequest */
export type TStatusAccRequest = "Pending" | "WorkInProgress" | "Approved" | "Declined";
export type TFirstName = string;
export type TLastname = string;
export type TPatronym = string;
export type TDateOfBirth = string;
export type TIdentificationNumber = string;

export type TCountry = string;
export type TArea = string;
export type TCity = string;
export type TAddress = string;

export type TPhone = string;

export type TDocumentFront = string;
export type TDocumentBack = string;
export type TIdCardNumber = string;
export type TCitizenship = string;
export type TAuthority = string;
export type TDateOfIssue = string;
export type TExpirationDate = string;

export type TRequestType = string;// "Account Request"

export type TAssignedTo = TUser | undefined;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/** TransferReq */
export type TFrontManager = TUser;
export type TAccountant = TUser;
export type TStatusTransferDeposit = "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
export type TSenderIIN = string;
export type TRecipientIIN = string;
export type TSenderIIK = string;
export type TRecipientIIK = string | undefined;
export type TSenderBIK = string;
export type TRecipientBIK = string | undefined;
export type TSenderBank = string;
export type TRecipientBank = string | undefined;
export type TSenderKBe = number | string;
export type TRecipientKBe = number | string;
export type TSenderFullName = string ;
export type TRecipientFullName = string | undefined;
export type TAmount = number | string ;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/** DepositReq */
// export type TUser= string;
// export type TStatusTransferDeposit = "Pending"| "FrontWIP"| "AccountantWIP"| "Approved"| "Declined"| "Completed"
// export type TSenderIIN= string;
// export type TSenderIIK= string;
// export type TSenderBIK= string;
// export type TSenderBank= string;
// export type TSenderKBe= string;
// export type TSenderFullName= string;
// export type TFrontManager= string;
// export type TAccountant= string;
// export type TRejectionReason= string;
// export type TAmount= number;
// export type TDate= string;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/** Account */
// export type TUser = string;
// export type TFirstName = string;
// export type TDateOfBirth = string;
// export type TIdentificationNumber = string;
// export type TEmail = string;
// export type TPhone = string;
// export type TAuthority = string;
// export type TDateOfIssue = string;
// export type TExpirationDate = string;
// export type TIdCardNumber = string;
// export type TAmount = string;
// export type TPatronym = string;
// export type TCountry = string;
// export type TCity = string;
// export type TArea = string;
// export type TAddress = string;
export type TAccountNumber = string;
export type TLastName = string;
export type TAccountType = string;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/** AccountClient */
// user: string;
export type TIBAN = string;
export type TRecipientName = string | undefined;
export type TMyIban = boolean;
// export type TBIK = string;
// export type TIIK = string;
// export type Tbank = string;
// export type TKBe = number;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/** StockRequest */
export type TStatusStock = "Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted";
export type TUserFullName = string;
export type TCompany = string;  // Название компании, например Apple
export type TTicker = string;  // Тикер - код, например AAPL
export type TStockAmount = number | string;  //  Количество акций, запрашиваемое клиентом
export type TPrice = number | string;  //  Цена, предлагаемая клиентом
export type TTrader = TUser;
export type TStockReqType = "Buy" | "Sell";  // Тип заявки - покупка или продажа
export type TStockResult = number | string;   //  Для Тейдера. Кол-во акций, которое получилось купить/продать
export type TStockFail = number | string;    //  Для Трейдера. Кол-во акций, которое НЕ получилось купить-продать
export type TPriceResult = number | string;    //  Для Трейдера. Цена исполнения