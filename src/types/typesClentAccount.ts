import {T__v, T_id, TIBAN, TSenderBank, TSenderBIK, TSenderIIK, TSenderKBe, TStateStatus, TUser, TRecipientName, TMyIban} from "./reestrOfAlias";

export type TClientAccount = {
    _id?: T_id;
    __v?: T__v;
    user?: TUser;
    BIK: TSenderBIK;
    IIK: TSenderIIK;
    bank: TSenderBank;
    /*** Добавила новые поля Nastya 14/12 **/
    recipientName: TRecipientName,
    isMyIIK : TMyIban,

}
//#############################################################################################
/*** типы для параметров запросов Async Thunk в redux-slice***/
export type TGetPostClientAccountAT = undefined | TClientAccount
//#############################################################################################
/*** типы для слайсов ***/
export type TClientAccountState = {
    oneClientAccount: TGetPostClientAccountAT,
    allClientAccount: TGetPostClientAccountAT[],
    clientAccountStatus: TStateStatus,
    error: null,
}

/*
import mongoose, { Schema } from "mongoose";

export interface IСlientAccount extends mongoose.Document {
    user: string;
    IBAN: string;
    BIK: string;
    IIK: string ;
    bank: string ;
    KBe: number ;

};

const СlientAccountSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User's ID is required"]
    },
    IBAN: {
        type: String,
        required : [true, "IBAN is required"]
    },
    BIK: {
        type: String,
        required: [true, "BIK is required"]
    },
    IIK: {
        type: String,
        required: [true, "IIK is required"]
    },
    bank: {
        type: String,
        required: [true, "Bank is required"]
    },
    KBe: {
        type: Number,
        required: [true, "KBe is required"]
    }

});


const СlientAccount = mongoose.model<IСlientAccount>("СlientAccount", СlientAccountSchema);

export default СlientAccount;
*/