import { T_id, TUser, TCompany, TTicker, TStockAmount, TStateStatus} from "./reestrOfAlias";

export type TClientStock = {
    _id?: T_id;
    user?: TUser;
    company: TCompany;
    ticker: TTicker;
    amount: TStockAmount;
};

export type TGetPostClientStockAT = TClientStock | undefined;

export type TClientStockState = {
    allClientStocks: TGetPostClientStockAT[],
    clientStockStatus: TStateStatus;
    error: null;
}