import React from "react";
import {TStateStatus, TUser} from "./reestrOfAlias";

export type TFirstUser = {
    email: string,
    password?: string,
    password1?: string,
    password2?: string,
    // [propName: string]: any,
}
//**************************
// тип для стейта в слайсах
export type TUserState = {
    user: TUser,
    userStatus: TStateStatus,
    error: string | null | { "message": string, [key: string]: any },
}
//**************************
/*** PROPS STATES ***/
export type TRegisterLoginProps = {
    errors: any;
    dataUser: any;// ошибка из сервера: юзер уже зарегест. / ошибка в пароле или логине
    buttonText: string;
    ObjPassword1: any;
    ObjPassword2?: any;
    ObjEmail: any;
    onSubmit: React.FormEventHandler<HTMLFormElement>;
    handleSubmit: any;
    passwordError?: string;
}

// export type TmenusProps = TFirstUser & {
//     logout: React.MouseEventHandler<HTMLLIElement>,
//     handleClose: React.MouseEventHandler<HTMLLIElement>,
//     handleClick:  React.MouseEventHandler<HTMLButtonElement> | undefined,
//     open: boolean,
//     anchorEl:  Element | ((element: Element) => Element) | null | undefined
// }

// export type TmenusPropsForClient = TFirstUser & {
//     logout: React.MouseEventHandler<HTMLLIElement>,
//     handleClose: React.MouseEventHandler<HTMLLIElement>,
//     handleClick:  React.MouseEventHandler<HTMLButtonElement> | undefined,
//     handleRequest: React.MouseEventHandler<HTMLLIElement>,
//     handleMyProfile: React.MouseEventHandler<HTMLLIElement>,
//     handleMyIbans: React.MouseEventHandler<HTMLLIElement>,
//     open: boolean,
//     anchorEl:  Element | ((element: Element) => Element) | null | undefined
// }



/*import mongoose, {Schema} from "mongoose";
import bcrypt from "bcrypt";
import { nanoid } from "nanoid";

const saltWorkFactor = 10;

export interface IUser extends mongoose.Document {
    email: string;
    password: string;
    role: string;
    token: string;
    account?: string;
    path: string;
    generateToken(): Promise<string>;
    checkPassword(password: string): Promise<boolean>;
};

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, "Необходима почта"]
    },
    password: {
        type: String,
        required: [true, "Необходим пароль"]
    },
    role: {
        type: String,
        required: true,
        default: "client",
        enum: ["client", "frontOffice", "backOffice", "trader", "accountant", "admin"]
    },
    token: {
        type: String,
        required: true
    },
    account: {
        type: Schema.Types.ObjectId,
        ref: "Account",
    },
    path: {
        type: String,
        default: "account-opening",
        enum: ["account-opening", "acc-pending", "client-office"]
    },
});

UserSchema.pre("save", async function (next: (err?: Error) => void) {
    let user = this as IUser;

    if (!user.isModified("password")) return next();

    const salt = await bcrypt.genSalt(saltWorkFactor);

    const hash = await bcrypt.hashSync(user.password, salt);

    user.password = hash;

    return next();
});

UserSchema.set("toJSON", {
    transform: (doc: any, ret: any) => {
        delete ret.password;
        return ret;
    }
});

UserSchema.methods.checkPassword = async function (password: string) {
    return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function () {
    this.token = nanoid();
};

const User = mongoose.model<IUser>("User", UserSchema);

export default User;*/