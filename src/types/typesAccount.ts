import {
    T__v, T_id,
    TAccountNumber,
    TAccountType, TAddress,
    TAmount, TArea, TAuthority,
    TCity, TCountry,
    TDateOfBirth,
    TDateOfIssue, TEmail, TExpirationDate, TFirstName, TIdCardNumber,
    TIdentificationNumber, TLastName,
    TPatronym, TPhone, TStateStatus,
    TUser,
    TDocumentFront,
    TDocumentBack, TStatusAccRequest, TCitizenship
} from "./reestrOfAlias";

export type TAccount = {
    _id?: T_id;
    __v?: T__v;
    /*01*/    user: TUser;                                  // required
    /*02*/    accountNumber: TAccountNumber;                // required
    /*03*/    firstName: TFirstName;                       // required
    /*04*/    lastName: TLastName;                         // required
    /*05*/    dateOfBirth: TDateOfBirth;                   // required
    /*06*/    identificationNumber: TIdentificationNumber;  // required
    /*07*/    email: TEmail;                                // required
    /*08*/    phone: TPhone;                                // required
    /*09*/    authority: TAuthority;                        // required
    /*10*/    dateOfIssue: TDateOfIssue;                    // required
    /*11*/    expirationDate: TExpirationDate;              // required
    /*12*/    idCardNumber: TIdCardNumber;                  // required
    /*13*/    amount: TAmount                               // required
    /*14*/    accountType?: TAccountType;
    /*15*/    patronym?: TPatronym,
    /*16*/    country?: TCountry;
    /*17*/    city?: TCity;
    /*18*/    area?: TArea;
    /*19*/    address?: TAddress;
    citizenship?: TCitizenship;
    documentFront?: TDocumentFront;
    documentBack?: TDocumentBack;
    // userNotifications?: TNoticeObj[];
}
//#############################################################################################
/*** типы для параметров запросов Async Thunk в redux-slice***/
export type TGetPostAccountAT = undefined | TAccount

//#############################################################################################
/*** типы для параметров запросов Async Thunk в redux-slice***/
// export type TPatchAccountAT = {
//     _idAccount?: string;// ключ на сервер не отправляется
//     userNotifications: TNoticeObj[];
// }

//#############################################################################################
/*** типы для слайсов ***/
export type TAccountState = {
    oneAccount: TGetPostAccountAT,
    allAccount: TGetPostAccountAT[],
    accountStatus: TStateStatus,
    error: null,
    // stateBell?:0|1|2,
    // isRing?:boolean,
}


/*
import mongoose, { Schema } from "mongoose";

export interface IAccount extends mongoose.Document {
01    user: string;
02    accountNumber: string;
03    accountType: string;
04    firstName: string ;
05    lastName: string ;
06    dateOfBirth: string ;
07    identificationNumber: string;
08    email: string;
09    phone: string;
10    patronym: string,
11    country: string;
12    city: string;
13    area: string;
14    address: string;
15    authority: string;
16    dateOfIssue: string;
17    expirationDate: string;
18    idCardNumber: string;
19    amount: string
};

const AccountSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    accountNumber: {
        type: String,
        required : true,
    },
    accountType: {
        type: String,
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    dateOfBirth: {
        type: String,
        required: true
    },
    identificationNumber: {
        type: String,
        required: true,
        minlength: 12,
        maxlength: 12
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    patronym: {
        type: String
    },
    country: {
        type: String
    },
    area: {
        type: String
    },
    city: {
        type: String
    },
    address: {
        type: String
    },
    authority: {
        type: String,
        required: true
    },
    dateOfIssue: {
        type: String,
        required: true
    },
    expirationDate: {
        type: String,
        required: true
    },
    idCardNumber: {
        type: String,
        required: true
    },
    amount:{
        type: String,
        required: true
    }
});


const Account = mongoose.model<IAccount>("Account", AccountSchema);

export default Account;


type
accountNumber
firstName
lastName
dateOfBirth
identificationNumber
email
phone
authority
dateOfIssue
expirationDate
idCardNumber
amount
*/