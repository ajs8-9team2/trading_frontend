import {
    T__v,
    T_id,
    TAccountant,
    TAmount,
    TDate,
    TFrontManager,
    TRecipientBank,
    TRecipientBIK,
    TRecipientFullName,
    TRecipientIIK,
    TRecipientIIN,
    TRecipientKBe,
    TRejectionReason,
    TSenderBank,
    TSenderBIK,
    TSenderFullName,
    TSenderIIK,
    TSenderIIN,
    TSenderKBe, TStateStatus,
    TStatusTransferDeposit,
    TUser,
} from "./reestrOfAlias";

export type TTransferReq = {
    _id?: T_id;
    __v?: T__v;
    user?: TUser;
    frontManager?: TFrontManager;
    accountant?: TAccountant;
    status?: TStatusTransferDeposit; //"Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed",

    senderIIN: TSenderIIN;                    // required
    recipientIIN: TRecipientIIN;              // required
    senderIIK: TSenderIIK;                    // required
    recipientIIK: TRecipientIIK;              // required
    senderBIK: TSenderBIK;                    // required
    recipientBIK: TRecipientBIK;              // required
    senderBank: TSenderBank;                  // required
    recipientBank: TRecipientBank;            // required
    senderKBe: TSenderKBe;                    // required
    recipientKBe: TRecipientKBe;              // required
    senderFullName: TSenderFullName;          // required
    recipientFullName: TRecipientFullName;    // required
    amount: TAmount;                          // required
    rejectionReason?: TRejectionReason;
    date?: TDate;

    // [key:string]:any,// потом закоментить
}
//#############################################################################################
/*** типы для параметров запросов Async Thunk в redux-slice***/
export type TGetPostTransferReqAT = undefined | TTransferReq

export type TPatchTransferReqAT = {
    _idTransferReq?: string,// ключ на сервер не отправляется
    rejectionReason?: TRejectionReason;
    status: TStatusTransferDeposit, //"Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed",
    frontManager?: TUser | string,
    accountant?: TUser | string,
}

//#############################################################################################
/*** типы для слайсов ***/
// тип для стейта слайса
export type TTransferReqState = {
    oneTransferReq: TGetPostTransferReqAT,
    allTransferReq: TGetPostTransferReqAT[],
    transferReqStatus: TStateStatus,
    error: null,
}


/*
import mongoose, { Schema } from "mongoose";

export interface ITransferReq extends mongoose.Document {
    user: string;
    status: string;
    senderIIN: string;
    recipientIIN: string;
    senderIIK: string;
    recipientIIK: string;
    senderBIK: string;
    recipientBIK: string;
    senderBank: string;
    recipientBank: string;
    senderKBe: string;
    recipientKBe: string;
    senderFullName: string;
    recipientFullName: string;
    frontManager: string;
    accountant: string;
    rejectionReason: string;
    amount: number;
    date: string;
};

const TransferReqSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User's ID is required"]
    },
    status: {
        type: String,
        required: true,
        default: "Pending",
        enum: ["Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]
    },
    amount: {
        type: Number,
        required: [true, "Money amount is required"]
    },
    senderFullName: {
        type: String,
        required: [true, "Sender's full name is required"]
    },
    recipientFullName: {
        type: String,
        required: [true, "Recipient's full name is required"]
    },
    senderIIN: {
        type: String,
        required: [true, "Sender's IIN is required"],
        minlength: 12,
        maxlength: 12
    },
    recipientIIN: {
        type: String,
        required: [true, "Recipient's IIN is required"],
        minlength: 12,
        maxlength: 12
    },
    senderBank: {
        type: String,
        required: [true, "Sender's Bank is required"]
    },
    recipientBank: {
        type: String,
        required: [true, "Recipient's Bank is required"]
    },
    senderIIK: {
        type: String,
        required: [true, "Sender's IIK is required"]
    },
    recipientIIK: {
        type: String,
        required: [true, "Recipient's IIK is required"]
    },
    senderBIK: {
        type: String,
        required: [true, "Sender's BIK is required"]
    },
    recipientBIK: {
        type: String,
        required: [true, "Recipient's BIK is required"]
    },
    senderKBe: {
        type: String,
        required: [true, "Sender's KBe is required"]
    },
    recipientKBe: {
        type: String,
        required: [true, "Recipient's KBe is required"]
    },
    frontManager: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    accountant: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    rejectionReason: {
        type: String,
    },
    requestType: {
        type: String,
        required: true,
        default: "Transfer request"
    },
    date: {
        type: Date,
        default: new Date()
    }
});

const TransferRequest = mongoose.model<ITransferReq>("TransferRequest", TransferReqSchema);

export default TransferRequest;
 */
