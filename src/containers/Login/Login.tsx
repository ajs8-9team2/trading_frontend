import React, {useEffect} from 'react';
import { Container, Typography} from "@mui/material";
import styled from "@emotion/styled";
import LoginForm from '../../components/Forms/UserForm/LoginForm';
import {useAppDispatch, useAppSelector} from "../../hook";
import {useNavigate} from "react-router-dom";
import {createUsersSessionAT} from '../../store/services/userSlice';
import {useForm} from "react-hook-form";
import {TFirstUser, TUserState} from "../../types/typesUser";
import HttpsIcon from '@mui/icons-material/Https';

const StyledContainer = styled(Container)`
    padding-top: 30px;
    padding-bottom: 30px;
    box-shadow: 0 18px 30px 0 rgba(0, 0, 0, 0.6);
`;

const StyledTitle = styled(Typography)`
    text-align: center;
    font-size: 30px;
    margin-bottom: 30px;
`

const Login: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const dataUser: TUserState = useAppSelector(state=>state.reducerUsers);
    const navigate = useNavigate();

    useEffect(()=>{
        if(dataUser.userStatus === "resolved"){
            if(dataUser.user?.role === "client"){
                if(dataUser.user?.isHasAccount){
                    navigate("/client/");}
                    console.log(dataUser.user.path)
                if(dataUser.user?.path === "acc-pending"){
                        navigate("/client/acc-pending")

                }
                if(dataUser.user.path == "account-opening"){
                    navigate("/client/account-opening");
                }
            }else if(dataUser.user?.role === "frontOffice"){
                // navigate("/front-office/my-requests");
                navigate("/front-office/list-requests");
            }else if(dataUser.user?.role === "backOffice"){
                navigate("/back-office");
            }else if(dataUser.user?.role === "trader"){
                navigate("/trader");
            }else if(dataUser.user?.role === "accountant"){
                navigate("/accountant");
            }else if(dataUser.user?.role === "admin"){
                navigate("/admin");
            }

        }
    }, [dataUser.userStatus]); //, userAccReq.userAccRequest]);

    const {
        register,
        formState: {errors, isValid},
        handleSubmit,
        reset
    } = useForm<TFirstUser>({mode: "onBlur"});

    const ObjEmail = {
        ...register("email", {
            required: "поле обязательно для заполения",
            pattern: {
                value: /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu,
                message: "введите ваш корректный email"
            }
        })
    }

    const ObjPassword1 = {
        ...register("password1", {
            required: "поле обязательно для заполнения",
            minLength: {
                value: 8,
                message: "пароль должен состоять минимум из 8 символов "
            }
            // required: true
        })
    }

    const onSubmit = async(data: any)=>{
        // alert(JSON.stringify(data));
        const innerObj: { email: string, password: string } = {
            email: data.email,
            password: data.password1
        }
        await dispatch(createUsersSessionAT(innerObj));
        // reset();
    }
   
    return (
        <>
            <StyledContainer maxWidth="xs" style={{borderRadius: "5%", position: "absolute", left: 0, right:  0, margin: "0 auto", top: "30%", background: "white"}}>
                    <HttpsIcon fontSize="large" sx={{ marginLeft: "45%" }}/>
                <StyledTitle variant="h5">
                    Войти
                </StyledTitle>

                <LoginForm
                    errors={errors}
                    handleSubmit={handleSubmit}
                    onSubmit={onSubmit}
                    ObjEmail={ObjEmail}
                    ObjPassword1={ObjPassword1}
                    buttonText={"Войти"}
                    dataUser={dataUser}
                />
            </StyledContainer>
            {/*<pre>{JSON.stringify(dataUser, null, 2)}</pre>*/}
        </>
    );
}

export default Login;