import React, {useState} from "react";
import {Tabs,Typography } from 'antd';
import "./Accountant.css"
import type { SizeType } from 'antd/es/config-provider/SizeContext';
import AccountantDepReq from "../../components/CompAccountant/AccountantDepReq";
import AccountantTransReq from "../../components/CompAccountant/AccountantTransReq";
import AccountsListed from "../../components/CompAccountant/AccountsListed";
import { createTheme } from "@material-ui/core/styles";



const AccountantList: React.FC = ()=>{

    const { Title } = Typography;
  
    return (<>
        <div className="TraderList">
            <Title level={3}>Текущие заявки</Title>
            <Tabs defaultActiveKey="1" type="card" size="large">
                <Tabs.TabPane tab="Пополнение счета" key="depReq" style={{ paddingLeft: 20, paddingRight: 20 }}>
                    <AccountantDepReq />
                </Tabs.TabPane>

                <Tabs.TabPane tab="Перевод денежных средств" key="transReq" style={{ paddingLeft: 20, paddingRight: 20 }}>
                    <AccountantTransReq />
                </Tabs.TabPane>

                <Tabs.TabPane tab="Счета клиентов" key="accounts" style={{ paddingLeft: 20, paddingRight: 20 }}>
                    <AccountsListed />
                </Tabs.TabPane>
            </Tabs>
        </div>
        </>
    )
};

export default AccountantList;