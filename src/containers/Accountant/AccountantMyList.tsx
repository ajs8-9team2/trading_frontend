import React, {useState} from "react";
import {Tabs, Typography } from 'antd';
import type { SizeType } from 'antd/es/config-provider/SizeContext';
import AccountantMyDep from "../../components/CompAccountant/AccountantMyDep";
import AccountantMyTrans from "../../components/CompAccountant/AccountantMyTrans";

const AccountantMyList: React.FC = ()=>{
    const { Title } = Typography;

    return (
        <>
            <Title level={3}>Мои заявки в процессе работы</Title>
            <div className="TraderList">
                <Tabs defaultActiveKey="1" type="card" size="large" >
                    <Tabs.TabPane tab="Пополнение счета" key="depReq" style={{ paddingLeft: 20, paddingRight: 20 }}>
                        <AccountantMyDep />
                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Перевод денежных средств" key="transReq" style={{ paddingLeft: 20, paddingRight: 20 }}>
                        <AccountantMyTrans />
                    </Tabs.TabPane>
                </Tabs>
            </div>
        </>
    )
};

export default AccountantMyList;