import React, {useEffect} from "react";
import List from '@mui/material/List';
import {useAppDispatch, useAppSelector} from "../../hook";
import {fetchAccRequestsAT} from "../../store/services/accRequestSlice";
import FrontListItem from "./FrontListItem";
import {TGetAccRequestsAT} from "../../types/typesAccReq";
import FullInfo from "./FullInfo";
import './FrontOffice.css';

import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import {createTheme} from "@material-ui/core/styles";
import {ThemeProvider} from "@mui/material";

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const FrontList: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allAccRequests: TGetAccRequestsAT[] = useAppSelector(state=>state.reducerAccRequests.allAccRequests);
    const oneAccRequest: TGetAccRequestsAT = useAppSelector(state=>state.reducerAccRequests.oneAccRequest);

    useEffect(()=>{
        dispatch(fetchAccRequestsAT());
    }, []);
    const [value, setValue] = React.useState('1');

    const handleChange = (event: React.SyntheticEvent, newValue: string)=>{
        setValue(newValue);
    }

    return (
        <>
            <ThemeProvider theme={theme}>
            <div style={{margin: "30px"}}>
                <Box sx={{width: '100%', typography: 'body1', marginTop: '20px'}}>
                    <TabContext value={value}>
                        <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                            <TabList onChange={handleChange} aria-label="lab API tabs example">
                                <Tab label="Все заявки" value="1"/>
                                <Tab label="Открытие счета" value="2"/>
                                <Tab label="Операции с деньгами" value="3"/>
                                <Tab label="Покупка/продажа" value="4"/>
                            </TabList>
                        </Box>

                        <TabPanel value="1" className="container">
                            <List sx={{
                                width: '100%',
                                maxWidth: 360,
                                // bgcolor: 'background.paper',
                                backgroundColor: "red",
                                cursor: "pointer",
                                overflowY: "scroll"
                            }}>

                                {allAccRequests.length && allAccRequests.map(request=>{
                                    return (
                                        <FrontListItem
                                            {...request!}
                                            key={request?._id!}
                                        />
                                    )
                                })}
                            </List>
                            {allAccRequests && <FullInfo {...oneAccRequest!}/>}
                        </TabPanel>

                        <TabPanel value="2">Открытие счета</TabPanel>
                        <TabPanel value="3">Операции с деньгами</TabPanel>
                        <TabPanel value="4">Покупка/продажа</TabPanel>
                    </TabContext>
                </Box>
            </div>
            </ThemeProvider>
        </>
    )
}

export default FrontList;

