import React, {useEffect} from "react";
import List from '@mui/material/List';
import {useAppDispatch, useAppSelector} from "../../hook";
import {queryAccReqFrontWIPAT} from "../../store/services/accRequestSlice";
import FrontListItem from "./FrontListItem";
import {TGetAccRequestsAT} from "../../types/typesAccReq";
import './FrontOffice.css';
import {TUser} from "../../types/reestrOfAlias";
import MyFullInfo from "./MyFullInfo";
import {createTheme} from "@material-ui/core/styles";
import {ThemeProvider} from "@mui/material";

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const MyFrontList: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allAccRequests:TGetAccRequestsAT[] = useAppSelector(state=>state.reducerAccRequests.allAccRequests);
    const oneAccRequest:TGetAccRequestsAT = useAppSelector(state=>state.reducerAccRequests.oneAccRequest);
    const dataUser:TUser = useAppSelector(state=>state.reducerUsers.user);

    // const params = useParams();
    useEffect(() => {
        dispatch(queryAccReqFrontWIPAT(dataUser?._id!));
    }, []);

    return (<>
        <ThemeProvider theme={theme}>

        <div className="container">
            {/*<pre>{JSON.stringify(oneAccRequest, null, 2)}</pre>*/}
            {/*<pre>{JSON.stringify(Object.values(allAccRequests)[0] , null, 2)}</pre>*/}
            <List sx={{
                width: '100%',
                maxWidth: 360,
                bgcolor: 'background.paper',
                cursor: "pointer",
                overflowY: "scroll",
                color: 'primary.main'
            }}>

                {allAccRequests.map(request=>{
                    return (
                        <FrontListItem
                            {...request!}
                            key={request?._id!}
                        />
                    )
                })}
            </List>
            {allAccRequests && <MyFullInfo {...oneAccRequest!}/>}
        </div>
        </ThemeProvider>
        </>
    )
}

export default MyFrontList;

