import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import {TGetAccRequestsAT} from "../../types/typesAccReq";
import { Button } from "@mui/material";
import React from "react";
import {paramsAccRequestAT} from "../../store/services/accRequestSlice";
import {useAppDispatch} from "../../hook";

// const FrontListItem = ({_id, date, firstName, lastName, workStatus, status, onClick}:any )=>{
// const FrontListItem = ({dataUser, onClick}:any )=>{
const FrontListItem = (props:TGetAccRequestsAT)=>{
    const dispatch = useAppDispatch();
    const {_id, date, firstName, lastName, /*workStatus,*/ status} = props!;

    return (
        <div style={{width: "70%", display: "block" }}>
            {/* <pre>{JSON.stringify(firstName, null, 2)}</pre> */}


            <Typography
                sx={{ display: 'block' }}
            >
               ID {_id}
            </Typography>
            <Typography
                sx={{ display: 'block' }}
            >
             {date}
            </Typography>
            <Typography
                sx={{ display: 'block' }}
            >
              {firstName} {lastName}
            </Typography>
            <Typography
                sx={{ display: 'block' }}
            >
               {status}
            </Typography>
            {/*<Typography*/}
            {/*    sx={{ display: 'block' }}*/}
            {/*>*/}
            {/*   статус: {workStatus}*/}
            {/*</Typography>*/}
            <Button variant="contained" onClick={()=>{dispatch(paramsAccRequestAT(_id!))}}> Получить информацию</Button>
            <Divider style={{height: "15px"}}/>
        </div>
    )
}

export default FrontListItem;
