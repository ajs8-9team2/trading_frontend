import {Button} from "@mui/material";
import {TGetAccRequestsAT} from "../../types/typesAccReq";


const MyFullInfo = (propsOneAccRequest: TGetAccRequestsAT)=>{
    const {
        _id, date, lastName, firstName, patronym, dateOfBirth,
        identificationNumber, idCardNumber, authority, dateOfIssue,
        expirationDate, email, phone, country, area, city, address,
        status, /*workStatus,*/ assignedTo
    } = propsOneAccRequest!


    if(_id)
        return (
            <div style={{width: "70%", display: "block", marginLeft: "30px"}}>
           
                <>
                    <h1>Подробная информация о заявке</h1>
                    <p>ID: {_id}</p>
                    <p>Дата заявки: {date}</p>
                    <p>ФИО: {lastName} {firstName} {patronym}</p>
                    <p>Дата рождения : {dateOfBirth}</p>
                    <p>ИИН: {identificationNumber}</p>
                    <p>№ удостоверения : {idCardNumber}</p>
                    <p>Орган выдачи : {authority}</p>
                    <p>Дата выдачи - Срок
                        действия: {dateOfIssue} - {expirationDate} </p>
                    <p>e-mail: {email}</p>
                    <p>Номер телефона: {phone}</p>
                    <p>Адрес: {country} {area} {city} {address}</p>
                    <p>Тип заявки : {status}</p>
                    {/*<p>Статус заявки : {workStatus}</p>*/}

                    {!!assignedTo ? (
                        <p>Закрепелена за : {assignedTo?._id}</p>

                    ) : <p></p>}
                    <div >
                    <Button style={{marginRight:"20px"}} variant="contained"> Одобрить </Button>
                    <Button variant="outlined" color="error"> Отклонить </Button>
                    </div>
                    
                </>
            </div>
        )
    else
        return (
            <h1
                style={{textAlign: "center", verticalAlign: "middle"}}
            >
                {"Кликните по заявке из списка, чтобы начать работу."}
            </h1>
        )
}

export default MyFullInfo;
