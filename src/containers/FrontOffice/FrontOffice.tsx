import React, {useEffect} from "react";
import FrontList from "./FrontList";
import {useAppDispatch, useAppSelector} from "../../hook";
import {TGetAccRequestsAT, TPatchAccRequestsAT} from "../../types/typesAccReq";
import {fetchAccRequestsAT, patchAccRequestAT, queryAccReqFrontWIPAT} from "../../store/services/accRequestSlice";
import {Button, Container, Grid, ThemeProvider} from "@mui/material";
import styled from "@emotion/styled";
import {createTheme} from "@material-ui/core/styles";


const StyledContainer = styled(Container)`
    padding-top: 30px;
    padding-bottom: 30px;
    box-shadow: 0 18px 30px 0 rgba(0, 0, 0, 0.6);
`;

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});


const FrontOffice: React.FC = ()=>{

    return (
        <div>
            <ThemeProvider theme={theme}>
                <FrontList/>
            </ThemeProvider>
        </div>
    )

    }

export default FrontOffice;



