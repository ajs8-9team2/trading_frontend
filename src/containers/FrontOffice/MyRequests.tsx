import * as React from 'react';

import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import MyFrontList from './MyFrontList';

const MyRequests= ()=>{
    const [value, setValue] = React.useState('1');

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
      setValue(newValue);
    }
    return (
        <>
                    <div>
                <Box sx={{ width: '100%', typography: 'body1', marginTop: '20px' }}>
                    <TabContext value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList onChange={handleChange} aria-label="lab API tabs example">
                            <Tab label="Все заявки" value="1" />
                            <Tab label="Открытие счета" value="2" />
                            <Tab label="Операции с деньгами" value="3" />
                            <Tab label="Покупка/продажа" value="4" />
                        </TabList>
                        </Box>
                        <TabPanel value="1">Все заявки<MyFrontList/></TabPanel>
                        <TabPanel value="2">Открытие счета<MyFrontList/></TabPanel>
                        <TabPanel value="3">Операции с деньгами</TabPanel>
                        <TabPanel value="4">Покупка/продажа</TabPanel>
                    </TabContext>
                </Box>
            </div>
            
        </>
    )
}

export default MyRequests;


