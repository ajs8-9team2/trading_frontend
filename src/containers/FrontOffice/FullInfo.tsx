import {Button, ThemeProvider} from "@mui/material";
import {TGetAccRequestsAT} from "../../types/typesAccReq";
import {patchAccRequestAT} from "../../store/services/accRequestSlice";
import {useAppDispatch, useAppSelector} from "../../hook";
import {TUser} from "../../types/reestrOfAlias";
import {createTheme} from "@material-ui/core/styles";

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});


const FullInfo = (propsOneAccRequest: TGetAccRequestsAT)=>{
    const {
        _id, date, lastName, firstName, patronym, dateOfBirth,
        identificationNumber, idCardNumber, authority, dateOfIssue,
        expirationDate, email, phone, country, area, city, address,
        status, /*workStatus,*/ assignedTo
    } = propsOneAccRequest!

    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);


    // return (
    //     <pre>{JSON.stringify(propsOneAccRequest, null, 2)}</pre>
    // )

    if(_id)
        return (<>
            <ThemeProvider theme={theme}>

            <div style={{width: "70%", display: "block", marginLeft: "30px"}}>
                {/* <pre>{JSON.stringify( oneAccRequest, null, 2)}</pre> */}
                {/*{!!oneAccRequest*/}
                {/*    ? (*/}
                <>
                    <h1>Подробная информация о заявке</h1>
                    <p>ID: {_id}</p>
                    <p>Дата заявки: {date}</p>
                    <p>ФИО: {lastName} {firstName} {patronym}</p>
                    <p>Дата рождения : {dateOfBirth}</p>
                    <p>ИИН: {identificationNumber}</p>
                    <p>№ удостоверения : {idCardNumber}</p>
                    <p>Орган выдачи : {authority}</p>
                    <p>Дата выдачи - Срок
                        действия: {dateOfIssue} - {expirationDate} </p>
                    <p>e-mail: {email}</p>
                    <p>Номер телефона: {phone}</p>
                    <p>Адрес: {country} {area} {city} {address}</p>
                    <p>Тип заявки : {status}</p>
                    {/*<p>Статус заявки : {workStatus}</p>*/}

                    {!!assignedTo ? (
                        <p>Закрепелена за : {assignedTo?._id}</p>

                    ) : <p></p>}
                    <Button variant="contained"  onClick={()=>{
                        dispatch(patchAccRequestAT({
                            status: "WorkInProgress",
                            _idAccReq: _id,
                            assignedTo: dataUser?._id!// _id пользователя
                        }))
                    }}> Начать работу над заявкой</Button>
                </>
            </div>
            </ThemeProvider>
            </>
        )
    else
        return (
            <h1
                style={{textAlign: "center", verticalAlign: "middle"}}
            >
                {"Кликните по заявке из списка, чтобы начать работу."}
            </h1>
        )
}

export default FullInfo;
