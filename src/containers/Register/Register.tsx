import React, {useEffect, useState} from 'react';
import {Avatar, Container, Typography} from "@mui/material";
import styled from "@emotion/styled";
import LockIcon from '@mui/icons-material/Lock';
import {useAppDispatch, useAppSelector} from "../../hook";
import {useNavigate} from "react-router-dom";
import {createUsersAT} from '../../store/services/userSlice';
import RegisterForm from "../../components/Forms/UserForm/RegisterForm";
import {useForm} from "react-hook-form";
import {TFirstUser, TUserState} from "../../types/typesUser";
import HowToRegIcon from '@mui/icons-material/HowToReg';
// margin-top: 30px;
const StyledContainer = styled(Container)`
    padding-top: 30px;
    padding-bottom: 30px;
    box-shadow: 0 18px 30px 0 rgba(0, 0, 0, 0.6);
`;

const StyledTitle = styled(Typography)`
    text-align: center;
    font-size: 30px;
    margin-bottom: 30px;
`

const Register: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const dataUser: TUserState = useAppSelector(state=>state.reducerUsers);
    const navigate = useNavigate();
    const [passwordError, setPasswordError] = useState<string>("");

    useEffect(()=>{
        if(dataUser.userStatus === "resolved"){
            navigate("/client/account-opening");
        }
    }, [dataUser.userStatus]);

    const {
        register,
        formState: {errors, isValid},
        handleSubmit,
        reset
    } = useForm<TFirstUser>({
        mode: "onBlur"
    });

    const ObjEmail = {
        ...register("email", {
            required: "поле обязательно для заполения",
            pattern: {
                value: /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu,
                message: "введите ваш корректный email"
            }
        })
    }

    const ObjPassword1 = {
        ...register("password1", {
            required: "поле обязательно для заполения",
            minLength: {
                value: 8,
                message: "пароль должен состоять минимум из 8 символов "
            }
            // required: true
        })
    }

    const ObjPassword2 = {
        ...register("password2", {
            required: "поле обязательно для заполения",
            minLength: {
                value: 8,
                message: "пароль должен состоять минимум из 8 символов "
            }
            // required: true
        })
    }

    const onSubmit = async(data: any)=>{
        if(data.password1 === data.password2){
            // const innerObj: {email:string,password:string} = {
            const innerObj: TFirstUser = {
                email: data.email,
                password: data.password1
            }
            await dispatch(createUsersAT(innerObj));
            setPasswordError("");
            // reset();
        }else{
            setPasswordError("* Пароли не совпадают");
        }
    }

    return (
        <>
            <StyledContainer maxWidth="xs" style={{marginTop: "10%", borderRadius: "5%",position: "absolute",left: 0, right:  0, margin: "0 auto", top: "20%",background: "white"}}>
            <HowToRegIcon fontSize="large" sx={{ marginLeft: "45%" }}/>
                <StyledTitle variant="h5">
                    Регистрация
                </StyledTitle>

                <RegisterForm
                    errors={errors}
                    handleSubmit={handleSubmit}
                    onSubmit={onSubmit}
                    ObjEmail={ObjEmail}
                    ObjPassword1={ObjPassword1}
                    ObjPassword2={ObjPassword2}
                    buttonText={"Зарегистрироваться"}
                    dataUser={dataUser}
                    passwordError={passwordError}
                />
            </StyledContainer>
           
        </>
    );
}

export default Register;