import React, {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../../../hook";
import {TGetAccRequestsAT} from "../../../types/typesAccReq";
import {fetchAccRequestsAT} from "../../../store/services/accRequestSlice";
import {styled, ThemeProvider} from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import EnhancedTable from "../../../components/CompFrontOffice/CompAccRequest/ListAccReq/EnhancedTable/EnhancedTable";
import DiscreteSlider
    from "../../../components/CompFrontOffice/CompAccRequest/ListAccReq/DiscreteSlider/DiscreteSlider";
import InsetDividers from "../../../components/CompFrontOffice/CompAccRequest/ListAccReq/InsetDividers/InsetDividers";
import {TStateStatus, TUser} from "../../../types/reestrOfAlias";
import {Divider, Switch, Typography} from "@mui/material";
import Button from "@mui/material/Button";
import HomeIcon from '@mui/icons-material/Home';
import SettingsIcon from '@mui/icons-material/Settings';
import {Context} from "../../../context";
import TransferList from "../../../components/CompFrontOffice/CompAccRequest/TransferList/TransferList";
import TransferList2 from "../../../components/CompFrontOffice/CompAccRequest/TransferList2/TransferList2";
import TransferList3 from "../../../components/CompFrontOffice/CompAccRequest/TransferList3/TransferList3";
import {createTheme} from "@material-ui/core/styles";
import {fetchDepositRequestAT} from "../../../store/services/depositRequestSlice";

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const theme = createTheme({
    palette: {
        primary: {main: "#263238"},
        secondary: {main: '#11cb5f'},
    },
});


//###########################################################################################
//###########################################################################################
const FrontOfficeListReq: React.FC = ()=>{

    const dispatch = useAppDispatch();
    // const [getID, setGetID] = React.useState("");
    const allAccRequests: TGetAccRequestsAT[] = useAppSelector(state=>state.reducerAccRequests.allAccRequests);
    const oneAccRequest: TGetAccRequestsAT = useAppSelector(state=>state.reducerAccRequests.oneAccRequest);
    const accRequestStatus: TStateStatus = useAppSelector(state=>state.reducerAccRequests.accRequestStatus);
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [gridSize, setGridSize] = React.useState<number>(8);
    const [isToggle,setIsToggle]= React.useState<boolean>(false);
    const [isToggle2,setIsToggle2]= React.useState<boolean>(true);

    useEffect(()=>{
        const fetchData = async()=>{
            setIsToggle(true);
            await dispatch(fetchAccRequestsAT());
        }
        fetchData();
    }, []);
    // свитчер -----------------------------------------------------------------
    const [toggle, setToggle] = React.useState<boolean>(true);
    const [checked, setChecked] = React.useState(false);
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>)=>{
        // setToggle(true)
        setChecked(event.target.checked);
    };
    //--------------------------------------------------------------------------
    // фильтр для сотрудников
    //***************************
    const empArrayFun = ()=>{
        let count = 0;
        const innerObj = allAccRequests.reduce((totalObj: { [key: string]: number }, item)=>{
            const innerKey = item?.assignedTo?._id;
            if(innerKey){
                totalObj[innerKey] = (totalObj[innerKey] ?? 0) + 1;
            }else{
                count++;
                totalObj["not_assigned"] = count;
            }
            return totalObj;
        }, {});
        return Object.entries(innerObj)
    }
    const [employeesArray, setEmployeesArray] = React.useState<string[]>([]);
    const [employees, setEmployees] = React.useState<string[]>([]);
    const [left3, setLeft3] = React.useState<readonly number[]>([]);
    const [right3, setRight3] = React.useState<readonly number[]>([]);



    useEffect(()=>{
        if(empArrayFun().length && accRequestStatus === "resolved"){
            const innerArray = empArrayFun()?.map(item=>item[0] + " / " + item[1] + " дел.");
            setEmployeesArray(innerArray)
            if(isToggle || isToggle2){
                const right3Array = [...innerArray.map((item,index)=>index)];//[0,1,2,3,4,5,6]
                setRight3(right3Array);
                //--------------------------------------
                setEmployees(right3Array.map(item=>innerArray[item]));
                //--------------------------------------
                setIsToggle(false);
                setIsToggle2(false);
            }
        }
    }, [accRequestStatus]);

    //***************************
    //***************************
    // таблица1
    const rowsArrayTableStr = [
        ["_id", "ID заявления"],
        ["date", "дата созд."],
        ["assignedTo", "ID менеджера"],
        ["status", "Статус рассмотрения"],
        ["firstName", "Имя"],
        ["lastName", "Фамилия"],
        ["patronym", "Отчество"],
        ["dateOfBirth", "Д/Р"],
        ["identificationNumber", "ИИН"]
    ];
    const [tableRowsName, setTableRowsName] = React.useState<string[][]>(rowsArrayTableStr);
    const [right1, setRight1] = React.useState<readonly number[]>([...(()=>{
        const lenArray: number[] = [];
        for(let i1 = 0; i1 < rowsArrayTableStr.length; i1++){
            lenArray.push(i1);
        }
        return lenArray;
    })()]);
    const [left1, setLeft1] = React.useState<readonly number[]>([]);
    //***************************
    //***************************
// "Pending" , "WorkInProgress" , "Approved" , "Declined"
    const arrayStatusesRequest = ["Pending", "WorkInProgress", "Approved", "Declined"]
    const [statusReq, setStatusReq] = React.useState<string[]>(arrayStatusesRequest);
    const [left2, setLeft2] = React.useState<readonly number[]>([]);
    const [right2, setRight2] = React.useState<readonly number[]>([...(()=>{
        const lenArray: number[] = [];
        for(let i1 = 0; i1 < arrayStatusesRequest.length; i1++){
            lenArray.push(i1);
        }
        return lenArray;
    })()]);
// таблица1
// Context --------------------------------------------------------------------------------
    const value = {
        accRequestStatus,//*

        //TransferList
        rowsArrayTableStr,
        tableRowsName,
        setTableRowsName,
        right1, setRight1,
        left1, setLeft1,

        //TransferList2
        arrayStatusesRequest,
        setStatusReq,
        left2, setLeft2,
        right2, setRight2,

        //TransferList3
        employeesArray,
        employees,
        setEmployees,
        left3, setLeft3,
        right3, setRight3,

        statusReq
    }
//-----------------------------------------------------------------------------------------
    if(0)
        return (
            <>
                {/*<pre>{JSON.stringify(allAccRequests[allAccRequests.length - 1], null, 2)}</pre>*/}
                {/*<pre>{JSON.stringify(employees, null, 2)}</pre>*/}
                {/*<pre>{JSON.stringify(employeesArray, null, 2)}</pre>*/}
                <pre>{JSON.stringify(allAccRequests, null, 2)}</pre>
            </>
        )
// alert(getID)

    return (<>
            <ThemeProvider theme={theme}>
                {/*<pre>{JSON.stringify(employees)}</pre>*/}
                {/*<pre>{JSON.stringify(employeesArray)}</pre>*/}
                {/*<pre>{JSON.stringify(right3)}</pre>*/}

                <Context.Provider value={value}>
                    {!!allAccRequests.length ?
                        <Grid
                            container spacing={1}
                            // sx={{ height: "80vh"}}
                            sx={{
                                mt: 0,
                                // height: "100%",
                                backgroundColor: 'lightgrey',
                                // height: '50vh'
                            }}
                        >
                            {toggle &&
                            <Grid
                                item xs={12} md={12}
                            >
                                {
                                    // !Object.prototype.toString.call(oneAccRequest).includes("Undefined") &&
                                    <Item>
                                        <Grid
                                            container
                                            direction="row"
                                            justifyContent="space-between"
                                            alignItems="center"
                                        >
                                            <Button
                                                // variant="outlined"
                                                size="small"
                                                onClick={()=>{
                                                    setToggle(!toggle);
                                                }}
                                            >
                                                <SettingsIcon/>
                                            </Button>
                                            {/*<Divider orientation="vertical" flexItem/>*/}

                                            <Divider orientation="vertical" flexItem/>
                                            <Typography
                                                variant="h4"
                                                gutterBottom
                                                sx={{
                                                    m: 0,
                                                    p: 0,
                                                    textAlign: "center",
                                                    color: "grey"
                                                }}
                                            >
                                                {"заявление на открытие счета"}
                                            </Typography>
                                            <Divider orientation="vertical" flexItem/>

                                            <Typography
                                                // variant="h6"
                                                gutterBottom
                                                sx={{
                                                    m: 0,
                                                    p: 0,
                                                    textAlign: "center",
                                                    color: "grey"
                                                }}
                                            >
                                                ваш id: {dataUser?._id?.toUpperCase()}
                                            </Typography>

                                        </Grid>
                                    </Item>
                                }
                            </Grid>}


                            {!toggle &&
                            <Grid
                                item xs={12} md={12}
                            >
                                {
                                    // !Object.prototype.toString.call(oneAccRequest).includes("Undefined") &&
                                    <Item>
                                        <Grid
                                            container
                                            direction="row"
                                            justifyContent="space-between"
                                            alignItems="center"
                                        >
                                            <Button
                                                // variant="outlined"
                                                size="small"
                                                onClick={()=>{
                                                    setToggle(!toggle);
                                                }}
                                                disabled={checked}
                                            >
                                                {/*ваш ID*/}
                                                <HomeIcon/>
                                            </Button>

                                            <DiscreteSlider
                                                setGridSize={setGridSize}
                                            />
                                            {/*<Divider orientation="vertical" flexItem/>*/}

                                            <Grid>
                                                <Grid
                                                    container
                                                    direction="row"
                                                    justifyContent="space-between"
                                                    alignItems="center"
                                                >
                                                    <Typography sx={{
                                                        fontSize: 16,
                                                        mr: 2
                                                    }} color="text.secondary" gutterBottom>
                                                        {"фильтр"}
                                                    </Typography>

                                                    <Switch
                                                        checked={checked}
                                                        onChange={
                                                            handleChange
                                                        }
                                                        inputProps={{'aria-label': 'controlled'}}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        {/*<pre>{JSON.stringify(oneAccRequest, null, 2)}</pre>*/}
                                        {/*<pre>{*/}
                                        {/*    JSON.stringify(*/}
                                        {/*        Object.prototype.toString.call(oneAccRequest).includes("Undefined"), null, 2)*/}
                                        {/*}</pre>*/}
                                    </Item>
                                }
                            </Grid>}


                            {checked && allAccRequests.length &&
                            <Grid
                                item xs={12} md={12}
                            >
                                {
                                    <Item>
                                        {/*// xs, extra-small: 0px*/}
                                        {/*// sm, small: 600px*/}
                                        {/*// md, medium: 900px*/}
                                        {/*// lg, large: 1200px*/}
                                        {/*// xl, extra-large: 1536px*/}

                                        <Grid container spacing={2}>
                                            <Grid item lg={6} md={12}>
                                                <Item>
                                                    <Typography variant="overline" display="block" gutterBottom>
                                                        фильтрация по столбцам таблицы
                                                    </Typography>
                                                    <TransferList/>
                                                </Item>
                                            </Grid>
                                            <Grid item lg={6} md={12}>
                                                <Item>
                                                    <Typography variant="overline" display="block" gutterBottom>
                                                        фильтрация по статусу заявки
                                                    </Typography>
                                                    <TransferList2/>
                                                </Item>
                                            </Grid>
                                            {employeesArray?.length &&
                                            <Grid item lg={12} md={12}>
                                                <Item>
                                                    <Typography variant="overline" display="block" gutterBottom>
                                                        фильтрация по сотрудникам
                                                    </Typography>
                                                    <TransferList3/>
                                                </Item>
                                            </Grid>
                                            }
                                        </Grid>
                                    </Item>
                                }
                                {/*<pre>{JSON.stringify(employees)}</pre>*/}
                                {/*<pre>{JSON.stringify(employeesArray)}</pre>*/}
                                {/*<pre>{JSON.stringify(right3)}</pre>*/}

                                {/*<pre>{JSON.stringify(statusReq)}</pre>*/}
                                {/*<pre>{JSON.stringify(tableRowsName)}</pre>*/}
                                {/*<pre>{JSON.stringify(accRequestStatus)}</pre>*/}
                            </Grid>}

                            {!!(gridSize) &&
                            <Grid item xs={gridSize} md={gridSize}
                                  sx={{height: "100%"}}
                            >
                                <Item>
                                    {!!allAccRequests.length
                                        ?
                                        <EnhancedTable
                                            allAccRequests={allAccRequests}
                                        />
                                        :
                                        <>{"В базе данных нет никаких заявок!"}</>
                                    }
                                </Item>
                                {/*<pre>{JSON.stringify(allAccRequests,null,2)}</pre>*/}
                            </Grid>
                            }

                            {!!(12 - gridSize) &&
                            <Grid item xs={12 - gridSize} md={12 - gridSize}>
                                <Item>
                                    {
                                        // (Object.prototype.toString.call(oneAccRequest).includes("Undefined") ||
                                        //     Object.prototype.toString.call(oneAccRequest).includes("Null"))
                                        //     ?
                                        //     <>{"???"}</>
                                        //     :
                                        <InsetDividers
                                            {...oneAccRequest!}
                                        />
                                    }
                                </Item>
                            </Grid>
                            }
                        </Grid>
                        :
                        <Grid
                            container spacing={1}
                            // sx={{ height: "80vh"}}
                            sx={{
                                mt: 0,
                                // height: "100%",
                                backgroundColor: 'lightgrey',
                                // height: '50vh'
                            }}
                        >
                            <Grid
                                item xs={12} md={12}
                            >
                                <Typography variant="overline" display="block" gutterBottom>
                                    В базе нет данных для отображения
                                </Typography>
                            </Grid>
                        </Grid>
                    }
                </Context.Provider>
            </ThemeProvider>
        </>
    )
}

export default FrontOfficeListReq;