import React, {useEffect, useState} from 'react';
import {useAppDispatch, useAppSelector} from "../../../hook";
import {TGetAccRequestsAT} from "../../../types/typesAccReq";
import {queryAccReqFrontWIPAT} from "../../../store/services/accRequestSlice";
import Grid from "@mui/material/Grid";
import PinnedSubheaderList
    from "../../../components/CompFrontOffice/CompAccRequest/MyAccReq/PinnedSubheaderList/PinnedSubheaderList";
import RightSideForm from "../../../components/CompFrontOffice/CompAccRequest/MyAccReq/RightSideForm/RightSideForm";
import {TStateStatus, TUser} from "../../../types/reestrOfAlias";
import {Divider, ThemeProvider, Typography} from "@mui/material";
import Button from "@mui/material/Button";
import SettingsIcon from "@mui/icons-material/Settings";
import {styled} from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import {Context} from "../../../context";
import {createTheme} from "@material-ui/core/styles";

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const theme = createTheme({
    palette: {
        primary: {main: "#263238"},
        secondary: {main: '#11cb5f'},
    },
});


const FrontOfficeMyReq = ()=>{
    const dispatch = useAppDispatch();
    // const oneAccRequest : TGetAccRequestsAT =  useAppSelector(state=>state.reducerAccRequests.oneAccRequest);
    const allAccRequests: TGetAccRequestsAT[] = useAppSelector(state=>state.reducerAccRequests.allAccRequests);
    const accRequestStatus: TStateStatus = useAppSelector(state=>state.reducerAccRequests.accRequestStatus);

    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    //---------------------------------------------------------------------------------------------------
    const [_idAccReq, set_idAccReq] = useState<string>("");
    // стейты для таймера
    const [seconds, setSeconds] = useState<number>(20);
    const [timerActive, setTimerActive] = useState<boolean>(false);
    // стейты для хранения стейта редакса
    const [myArray, setMyArray] = useState<TGetAccRequestsAT[]>();


    useEffect(()=>{
        const fetchData = async()=>{
            await dispatch(queryAccReqFrontWIPAT(dataUser?._id!));
        }
        fetchData();
    }, []);

    useEffect(()=>{
        // "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
        setMyArray(allAccRequests.filter(
            item=>item?.status !== "Approved"
                && item?.status !== "Declined"))

        const innerIndex = allAccRequests.findIndex(item=>item?._id === _idAccReq)
        if(innerIndex !== -1 && ["Approved", "Declined"].includes(allAccRequests[innerIndex]?.status!))
            // запускаем таймер, если сотрудник принял или отклонил заявку
            setTimerActive(true)

    }, [accRequestStatus]);
    //-----------------------------------------------------------------------------------------

    useEffect(()=>{
        setSeconds(20)
    }, [_idAccReq]);


    React.useEffect(()=>{
        const fetchData = ()=>{
            if(seconds > 0 && timerActive){
                setTimeout(setSeconds, 100, seconds - 1);
            }else{
                setTimerActive(false);
            }
        }
        fetchData();
    }, [seconds, timerActive]);
    // Context --------------------------------------------------------------------------------
    const set_idAccReqContext = (_idAccReq: string)=>{
        set_idAccReq(_idAccReq)
    }
    const value = {set_idAccReqContext, _idAccReq, timerActive}
    //-----------------------------------------------------------------------------------------
    const MyReq = ()=>{
        return (<>
                <ThemeProvider theme={theme}>
                    <Grid
                        container spacing={1}
                        // sx={{ height: "80vh"}}
                        sx={{
                            mt: 0,// чтоб не приклеивалось к toolbar
                            // backgroundColor: '#d6f0ff',
                            // height: '50vh'
                        }}
                    >
                    </Grid>
                    <Grid
                        container spacing={1}
                        // sx={{ height: "80vh"}}
                        sx={{
                            mt: 0,// чтоб не приклеивалось к toolbar
                            // backgroundColor: '#d6f0ff',
                            // height: '50vh'
                        }}
                    >
                        {!!myArray?.length ?
                            <>
                                {/*    // xs, extra-small: 0px*/}
                                {/*    // sm, small: 600px*/}
                                {/*    // md, medium: 900px*/}
                                {/*    // lg, large: 1200px*/}
                                {/*    // xl, extra-large: 1536px*/}
                                <Grid
                                    item
                                    xs={4}
                                    md={4}
                                    xl={6}
                                    // sx={{height: "100%"}}
                                    sx={{bgcolor: 'background.paper'}}
                                >
                                    <PinnedSubheaderList
                                        // set_idAccReq={set_idAccReq}
                                        // timerActive={timerActive}
                                        myArray={
                                            (()=>{
                                                if(seconds !== 0){
                                                    let innerArray: TGetAccRequestsAT[] = [];
                                                    for(let allAccRequest of allAccRequests){
                                                        // если нашли заявление со статусом approved = true
                                                        // если нашли совпадение по id = false
                                                        // false + true
                                                        // if(allAccRequest?.status === "Approved" && allAccRequest?._id !== getID) continue;
                                                        if(
                                                            (allAccRequest?.status === "Approved" && allAccRequest?._id !== _idAccReq) ||
                                                            (allAccRequest?.status === "Declined" && allAccRequest?._id !== _idAccReq)
                                                        ) continue;
                                                        innerArray.push(allAccRequest)
                                                    }
                                                    return innerArray;
                                                }else{
                                                    return myArray;
                                                }
                                            })()
                                        }
                                    />
                                </Grid>
                                <Grid item
                                      xs={8}
                                      md={8}
                                      xl={6}
                                >
                                    {/*{myArray.some(item=>item?._id === getID) &&*/}
                                    {(_idAccReq && !!seconds) &&
                                    <RightSideForm
                                        seconds={seconds}
                                        dataUser={dataUser}
                                        userID={(()=>{
                                            // находим клиента по заявке
                                            const innerIndex: number = allAccRequests.findIndex(item=>item?._id === _idAccReq)
                                            if(innerIndex !== -1)
                                                return allAccRequests[innerIndex]?.user?._id;
                                            else
                                                return "noUserID"
                                        })()}
                                    />}
                                </Grid>
                                {/*<pre>{JSON.stringify(myArray, null, 2)}</pre>*/}
                            </>
                            :
                            <Grid item xs={12} md={12}>
                                <Typography
                                    variant="h4"
                                    gutterBottom
                                    sx={{
                                        mt: 10,
                                        textAlign: "center",
                                        color: "grey"
                                    }}
                                >
                                    Нет заявлений закрепленных за вами!
                                </Typography>
                            </Grid>
                        }

                    </Grid>


                </ThemeProvider>
            </>
        )
    }

    return (
        <Context.Provider value={value}>
            {MyReq()}
        </Context.Provider>
    )
}

export default FrontOfficeMyReq;