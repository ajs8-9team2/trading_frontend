import React, {useEffect} from "react";
import {styled, ThemeProvider} from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import {Container, Divider, Switch, Typography} from "@mui/material";
import {useAppDispatch, useAppSelector} from "../../../hook";
import {
    T_id,
    TIdentificationNumber,
    TRejectionReason,
    TSenderFullName,
    TStateStatus, TStatusTransferDeposit,
    TUser
} from "../../../types/reestrOfAlias";
import Button from "@mui/material/Button";
import DiscreteSlider
    from "../../../components/CompFrontOffice/CompStockReq/ListStockReq/DiscreteSlider/DiscreteSlider";
import InsetDividers from "../../../components/CompFrontOffice/CompStockReq/ListStockReq/InsetDividers/InsetDividers";
import EnhancedTable from "../../../components/CompFrontOffice/CompStockReq/ListStockReq/EnhancedTable/EnhancedTable";
import HomeIcon from '@mui/icons-material/Home';
import SettingsIcon from '@mui/icons-material/Settings';
// import {TGetPostTransferReqAT, TTransferReq} from "../../../types/typesTransferRequest";
import {fetchTransferReqAT} from "../../../store/services/transferRequestSlice";
import {Context} from "../../../context";
import TransferList from "../../../components/CompFrontOffice/CompStockReq/TransferList/TransferList";
import TransferList2 from "../../../components/CompFrontOffice/CompStockReq/TransferList2/TransferList2";
import TransferList3 from "../../../components/CompFrontOffice/CompStockReq/TransferList3/TransferList3";
import {TGetPostStockRequestAT} from "../../../types/typesStockRequest";
import stockRequestSlice, {fetchStockRequestAT/*, setChangeToggle*/} from "../../../store/services/stockRequestSlice";
import {createTheme} from "@material-ui/core/styles";

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const theme = createTheme({
    palette: {
        primary: {main: "#263238"},
        secondary: {main: '#11cb5f'},
    },
});

const FrontOfficeStockListReq: React.FC = ()=>{

    // reducerStock: stockRequestSlice
    const dispatch = useAppDispatch();
    const allStocksRequest: TGetPostStockRequestAT[] = useAppSelector(state=>state.reducerStock.allStocksRequest);
    const oneStockRequest: TGetPostStockRequestAT = useAppSelector(state=>state.reducerStock.oneStockRequest);
    const stockRequestStatus: TStateStatus = useAppSelector(state=>state.reducerStock.stockRequestStatus);
    // const isToggleState: boolean = useAppSelector(state=>state.reducerStock.isToggleState);

    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [gridSize, setGridSize] = React.useState<number>(8);
    const [isToggle, setIsToggle] = React.useState<boolean>(false);
    const [isToggle2,setIsToggle2]= React.useState<boolean>(true);


    useEffect(()=>{
        // dispatch(fetchTransferReqAT());
        const fetchData = async()=>{
            setIsToggle(true);
            await dispatch(fetchStockRequestAT());
        }
        fetchData()
    }, []);
    // свитчер -----------------------------------------------------------------

    //***************************
    //***************************
    const [toggle, setToggle] = React.useState<boolean>(true);
    const [checked, setChecked] = React.useState(false);
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>)=>{
        setChecked(event.target.checked);
    };
    //--------------------------------------------------------------------------
    // фильтр для сотрудников
    //***************************
    const empArrayFun = ()=>{
        let count = 0;
        const innerObj = allStocksRequest.reduce((totalObj: { [key: string]: number }, item)=>{
            const innerKey = item?.frontManager?._id;
            if(innerKey){
                totalObj[innerKey] = (totalObj[innerKey] ?? 0) + 1;
            }else{
                count++;
                totalObj["not_assigned"] = count;
            }
            return totalObj;
        }, {});
        return Object.entries(innerObj)
    }
    const [employeesArray, setEmployeesArray] = React.useState<string[]>([]);
    const [employees, setEmployees] = React.useState<string[]>([]);
    const [left3, setLeft3] = React.useState<readonly number[]>([]);
    const [right3, setRight3] = React.useState<readonly number[]>([]);
    // const isToggleRef = React.useRef<boolean>(true);

    useEffect(()=>{
        // if(empArrayFun().length && stockRequestStatus === "resolved"){
        if(empArrayFun().length && stockRequestStatus === "resolved"){
            const innerArray = empArrayFun()?.map(item=>item[0] + " / " + item[1] + " дел.");
            setEmployeesArray(innerArray)
            if(isToggle || isToggle2){
                const right3Array = [...innerArray.map((item, index)=>index)];//[0,1,2,3,4,5,6]
                setRight3(right3Array);
                //--------------------------------------
                setEmployees(right3Array.map(item=>innerArray[item]));
                //--------------------------------------
                setIsToggle(false);
                setIsToggle2(false);
            }
        }
    }, [stockRequestStatus]);

    //***************************
    //***************************
    //"Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted"
    const arrayStatusesRequest = ["Pending", "FrontWIP", "TraderWIP", "Approved", "Declined", "Completed", "PartiallyCompleted"]
    const [statusReq, setStatusReq] = React.useState<string[]>(arrayStatusesRequest);
    const [left2, setLeft2] = React.useState<readonly number[]>([]);
    const [right2, setRight2] = React.useState<readonly number[]>([...(()=>{
        const lenArray: number[] = [];
        for(let i1 = 0; i1 < arrayStatusesRequest.length; i1++){
            lenArray.push(i1);
        }
        return lenArray;
    })()]);

    //***************************
    //***************************
    // таблица3
    const rowsArrayTableStr = [
        ["_id", "ID заявления"],
        ["status", "Статус рассмотрения"],
        ["frontManager", "ID сотрудника"],
        ["rejectionReason", "Причина откл."],
        ["date", "дата созд."],

        ["userFullName", "ФИО клиента"],
        ["company", "компания"],
        ["ticker", "тиккер"],
        ["stockAmount", "Количество акций"],
        ["price", "Цена, предлагаемая клиентом"],
        ["stockReqType", "Buy / Sell"],
    ];
    const [tableRowsName, setTableRowsName] = React.useState<string[][]>(rowsArrayTableStr);
    const [left1, setLeft1] = React.useState<readonly number[]>([]);
    const [right1, setRight1] = React.useState<readonly number[]>([...(()=>{
        const lenArray: number[] = [];
        for(let i1 = 0; i1 < rowsArrayTableStr.length; i1++){
            lenArray.push(i1);
        }
        return lenArray;
    })()]);
    //***************************
    //***************************
    // Context --------------------------------------------------------------------------------
    const value = {
        stockRequestStatus,

        //TransferList
        rowsArrayTableStr,
        tableRowsName,
        setTableRowsName,
        left1, setLeft1,
        right1, setRight1,

        //TransferList2
        arrayStatusesRequest,
        setStatusReq,
        statusReq,
        left2, setLeft2,
        right2, setRight2,

        //TransferList3
        employeesArray,
        employees,
        setEmployees,
        left3, setLeft3,
        right3, setRight3,
    }
    //-----------------------------------------------------------------------------------------
    if(0)
        return (
            // <pre>{JSON.stringify(allAccRequests[allAccRequests.length - 1], null, 2)}</pre>
            <pre>{JSON.stringify(allStocksRequest, null, 2)}</pre>
        )
    return (<>
            <ThemeProvider theme={theme}>
                <Context.Provider value={value}>
                    {/*<pre>{JSON.stringify(stockRequestStatus)}</pre>*/}
                    {/*<pre>{JSON.stringify("-----------------------------------")}</pre>*/}
                    {/*/!*<pre>{JSON.stringify(isToggleState)}</pre>*!/*/}
                    {/*<pre>{JSON.stringify(employees)}</pre>*/}
                    {/*<pre>{JSON.stringify(employeesArray)}</pre>*/}
                    {/*<pre>{JSON.stringify(right3)}</pre>*/}
                    <Grid
                        container spacing={1}
                        // sx={{ height: "80vh"}}
                        sx={{
                            mt: 0,
                            // height: "100%",
                            backgroundColor: 'lightgrey',
                            // height: '50vh'
                        }}
                    >
                        {toggle &&
                        <Grid
                            item xs={12} md={12}
                        >
                            {
                                // !Object.prototype.toString.call(oneAccRequest).includes("Undefined") &&
                                <Item>
                                    <Grid
                                        container
                                        direction="row"
                                        justifyContent="space-between"
                                        alignItems="center"
                                    >
                                        {/*<pre>{JSON.stringify(concatOneAccOneDepos, null, 2)}</pre>*/}
                                        {/*<pre>{JSON.stringify(oneDepositRequest, null, 2)}</pre>*/}
                                        <Button
                                            // variant="outlined"
                                            size="small"
                                            onClick={()=>{
                                                setToggle(!toggle);
                                            }}
                                        >
                                            <SettingsIcon/>
                                        </Button>
                                        <Divider orientation="vertical" flexItem/>
                                        <Typography
                                            variant="h4"
                                            gutterBottom
                                            sx={{
                                                m: 0,
                                                p: 0,
                                                textAlign: "center",
                                                color: "grey"
                                            }}
                                        >
                                            {"заявления по акциям"}
                                        </Typography>
                                        <Divider orientation="vertical" flexItem/>
                                        <Typography
                                            // variant="h6"
                                            gutterBottom
                                            sx={{
                                                m: 0,
                                                p: 0,
                                                textAlign: "center",
                                                color: "grey"
                                            }}
                                        >
                                            ваш id: {dataUser?._id?.toUpperCase()}
                                        </Typography>
                                    </Grid>
                                </Item>
                            }
                        </Grid>}

                        {!toggle &&
                        <Grid
                            item xs={12} md={12}
                        >
                            {
                                // !Object.prototype.toString.call(oneAccRequest).includes("Undefined") &&
                                <Item>
                                    <Grid
                                        container
                                        direction="row"
                                        justifyContent="space-between"
                                        alignItems="center"
                                    >
                                        <Button
                                            // variant="outlined"
                                            size="small"
                                            onClick={()=>{
                                                setToggle(!toggle);
                                            }}
                                            disabled={checked}
                                        >
                                            <HomeIcon/>
                                        </Button>
                                        {/*<Divider orientation="vertical" flexItem/>*/}
                                        <DiscreteSlider
                                            setGridSize={setGridSize}
                                        />
                                        {/*<Divider orientation="vertical" flexItem/>*/}
                                        <Grid>
                                            <Grid
                                                container
                                                direction="row"
                                                justifyContent="space-between"
                                                alignItems="center"
                                            >
                                                <Typography sx={{
                                                    fontSize: 16,
                                                    mr: 2
                                                }} color="text.secondary" gutterBottom>
                                                    {"фильтр"}
                                                </Typography>

                                                <Switch
                                                    checked={checked}
                                                    onChange={
                                                        handleChange
                                                    }
                                                    inputProps={{'aria-label': 'controlled'}}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    {/*<pre>{JSON.stringify(oneAccRequest, null, 2)}</pre>*/}
                                    {/*<pre>{*/}
                                    {/*    JSON.stringify(*/}
                                    {/*        Object.prototype.toString.call(oneAccRequest).includes("Undefined"), null, 2)*/}
                                    {/*}</pre>*/}
                                </Item>
                            }
                        </Grid>}

                        {checked &&
                        <Grid
                            item xs={12} md={12}
                        >
                            {
                                <Item>
                                    {/*// xs, extra-small: 0px*/}
                                    {/*// sm, small: 600px*/}
                                    {/*// md, medium: 900px*/}
                                    {/*// lg, large: 1200px*/}
                                    {/*// xl, extra-large: 1536px*/}

                                    <Grid container spacing={2}>
                                        <Grid item lg={6} md={12}>
                                            <Item>
                                                <Typography variant="overline" display="block" gutterBottom>
                                                    фильтрация по столбцам таблицы
                                                </Typography>
                                                <TransferList/>
                                            </Item>
                                        </Grid>
                                        <Grid item lg={6} md={12}>
                                            <Item>
                                                <Typography variant="overline" display="block" gutterBottom>
                                                    фильтрация по статусу заявки
                                                </Typography>
                                                <TransferList2/>
                                            </Item>
                                        </Grid>
                                        {employeesArray?.length &&
                                        <Grid item lg={12} md={12}>
                                            <Item>
                                                <Typography variant="overline" display="block" gutterBottom>
                                                    фильтрация по сотрудникам
                                                </Typography>
                                                <TransferList3/>
                                            </Item>
                                        </Grid>
                                        }
                                    </Grid>
                                </Item>
                            }
                            {/*<pre>{JSON.stringify("isToggle")}</pre>*/}
                            {/*<pre>{JSON.stringify(employees)}</pre>*/}
                            {/*<pre>{JSON.stringify(employeesArray)}</pre>*/}
                            {/*<pre>{JSON.stringify(right3)}</pre>*/}

                            {/*<pre>{JSON.stringify(transferReqStatus)}</pre>*/}
                            {/*<pre>{JSON.stringify(statusReq)}</pre>*/}

                            {/*<pre>{JSON.stringify(arrayStatusesRequest)}</pre>*/}
                            {/*<pre>{JSON.stringify(statusReq)}</pre>*/}
                            {/*<pre>{JSON.stringify(left2)}</pre>*/}
                            {/*<pre>{JSON.stringify(right2)}</pre>*/}

                        </Grid>}

                        {!!(gridSize) &&
                        <Grid item xs={gridSize} md={gridSize}
                              sx={{height: "100%"}}
                        >
                            <Item>
                                {!!allStocksRequest.length
                                    ?
                                    <EnhancedTable
                                        // right={right}
                                        allStocksRequest={allStocksRequest}
                                    />
                                    :
                                    <>{"В базе данных нет никаких заявок!"}</>
                                }
                            </Item>
                        </Grid>
                        }

                        {!!(12 - gridSize) &&
                        <Grid item xs={12 - gridSize} md={12 - gridSize}>
                            <Item>
                                {
                                    // (Object.prototype.toString.call(oneAccRequest).includes("Undefined") ||
                                    //     Object.prototype.toString.call(oneAccRequest).includes("Null"))
                                    //     ?
                                    //     <>{"???"}</>
                                    //     :

                                    <InsetDividers
                                        {...oneStockRequest!}
                                    />
                                }

                                {/*{allAccRequests && <FullInfo {...oneAccRequest!}/>}*/}
                            </Item>
                        </Grid>
                        }
                    </Grid>
                </Context.Provider>
            </ThemeProvider>
        </>
    )

}

export default FrontOfficeStockListReq;

/* {
    "_id": "63d0d8367d235411bb37de2a",
    "user": {
      "_id": "63d0d82f7d235411bb37dca6",
      "email": "client4@mail.com",
      "role": "client",
      "token": "CTWwLpx7P995XGcPMRhgO",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Корбиниан Адалриков",
    "status": "Pending",
    "stockAmount": 31,
    "company": "Arthur J Gallagher",
    "ticker": "AJG-RM",
    "price": 33630,
    "rejectionReason": "none",
    "stockReqType": "Buy",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 17,
    "stockFail": 21,
    "priceResult": 2,
    "__v": 0
  },
  {
    "_id": "63d0d8367d235411bb37de2b",
    "user": {
      "_id": "63d0d82f7d235411bb37dca8",
      "email": "client6@mail.com",
      "role": "client",
      "token": "23aTxf0oGtx-1m37jay0W",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Ксейвр Бенедиктов",
    "status": "PartiallyCompleted",
    "stockAmount": 8,
    "company": "IDEXX Labs",
    "ticker": "IDXX-RM",
    "price": 18864,
    "rejectionReason": "none",
    "stockReqType": "Sell",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 9,
    "stockFail": 21,
    "priceResult": 21,
    "__v": 0
  },
  {
    "_id": "63d0d8367d235411bb37de2c",
    "user": {
      "_id": "63d0d82f7d235411bb37dca9",
      "email": "client7@mail.com",
      "role": "client",
      "token": "kIOeKqPPZtzhqKFgc-PbA",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Виланд Гоззоов",
    "status": "PartiallyCompleted",
    "stockAmount": 30,
    "company": "O’Reilly Automotive",
    "ticker": "ORLY-RM",
    "price": 1489,
    "rejectionReason": "none",
    "stockReqType": "Sell",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 40,
    "stockFail": 39,
    "priceResult": 1,
    "__v": 0
  },
  {
    "_id": "63d0d8367d235411bb37de2d",
    "user": {
      "_id": "63d0d82f7d235411bb37dcaa",
      "email": "client8@mail.com",
      "role": "client",
      "token": "xisQvGxIxjhvldWS9JvQ4",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Вольфрам Арменов",
    "status": "Approved",
    "stockAmount": 15,
    "company": "Constellation Brands A",
    "ticker": "STZ-RM",
    "price": 6970,
    "frontManager": {
      "_id": "63d0d82f7d235411bb37dcd4",
      "email": "front0@mail.com",
      "fullName": "Волфганг Волфов",
      "role": "frontOffice",
      "token": "x3AcVzfKmgfbdhLfOCRY6",
      "path": "account-opening",
      "isHasAccount": false,
      "__v": 0
    },
    "rejectionReason": "none",
    "stockReqType": "Sell",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 23,
    "stockFail": 1,
    "priceResult": 26,
    "__v": 0
  },
  {
    "_id": "63d0d8367d235411bb37de2e",
    "user": {
      "_id": "63d0d82f7d235411bb37dcac",
      "email": "client10@mail.com",
      "role": "client",
      "token": "L44BEk85rvjaK5fFlPWp7",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Адалрикус Ансгаров",
    "status": "PartiallyCompleted",
    "stockAmount": 23,
    "company": "PG E",
    "ticker": "PCG-RM",
    "price": 17048,
    "rejectionReason": "none",
    "stockReqType": "Buy",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 42,
    "stockFail": 17,
    "priceResult": 28,
    "__v": 0
  },
  {
    "_id": "63d0d8367d235411bb37de2f",
    "user": {
      "_id": "63d0d82f7d235411bb37dcad",
      "email": "client11@mail.com",
      "role": "client",
      "token": "b5MByMc_pg3aSpZNUy6K9",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Вольфрам Гоцов",
    "status": "PartiallyCompleted",
    "stockAmount": 23,
    "company": "Laboratory America",
    "ticker": "LH-RM",
    "price": 11934,
    "rejectionReason": "none",
    "stockReqType": "Buy",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 3,
    "stockFail": 47,
    "priceResult": 9,
    "__v": 0
  },
  {
    "_id": "63d0d8367d235411bb37de30",
    "user": {
      "_id": "63d0d82f7d235411bb37dcaf",
      "email": "client13@mail.com",
      "role": "client",
      "token": "qTe6fWz5XyU5DfmNUajBI",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Готтфрид Адальбертов",
    "status": "TraderWIP",
    "stockAmount": 2,
    "company": "PG E",
    "ticker": "PCG-RM",
    "price": 36396,
    "frontManager": {
      "_id": "63d0d82f7d235411bb37dcd5",
      "email": "front1@mail.com",
      "fullName": "Аделмар Дирков",
      "role": "frontOffice",
      "token": "US6chFBk5qcQFZI-K0hrR",
      "path": "account-opening",
      "isHasAccount": false,
      "__v": 0
    },
    "trader": {
      "_id": "63d0d82f7d235411bb37dcd8",
      "email": "trader0@mail.com",
      "fullName": "Воинот Брунсов",
      "role": "trader",
      "token": "nzopbjdYXvd5aakzXs1Kr",
      "path": "account-opening",
      "isHasAccount": false,
      "__v": 0
    },
    "rejectionReason": "none",
    "stockReqType": "Buy",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 7,
    "stockFail": 35,
    "priceResult": 25,
    "__v": 0
  },
  {
    "_id": "63d0d8367d235411bb37de31",
    "user": {
      "_id": "63d0d82f7d235411bb37dcb6",
      "email": "client20@mail.com",
      "role": "client",
      "token": "9cDUq5DSK6kXutQG9v3qq",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Бонифац Абелардов",
    "status": "PartiallyCompleted",
    "stockAmount": 22,
    "company": "American Water Works",
    "ticker": "AWK-RM",
    "price": 42323,
    "rejectionReason": "none",
    "stockReqType": "Buy",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 33,
    "stockFail": 47,
    "priceResult": 35,
    "__v": 0
  },
  {
    "_id": "63d0d8367d235411bb37de32",
    "user": {
      "_id": "63d0d82f7d235411bb37dcb7",
      "email": "client21@mail.com",
      "role": "client",
      "token": "Vr9cVApKxqQdtDaxAMBE0",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Вилберт Ингвазов",
    "status": "PartiallyCompleted",
    "stockAmount": 29,
    "company": "O’Reilly Automotive",
    "ticker": "ORLY-RM",
    "price": 11297,
    "rejectionReason": "none",
    "stockReqType": "Sell",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 5,
    "stockFail": 6,
    "priceResult": 27,
    "__v": 0
  },
  {
    "_id": "63d0d8367d235411bb37de33",
    "user": {
      "_id": "63d0d82f7d235411bb37dcb8",
      "email": "client22@mail.com",
      "role": "client",
      "token": "P54xiSCeps67_GBY-ht--",
      "path": "account-opening",
      "isHasAccount": true,
      "__v": 0
    },
    "userFullName": "Зэодор Бонифацов",
    "status": "Approved",
    "stockAmount": 44,
    "company": "Keurig Dr Pepper",
    "ticker": "KDP-RM",
    "price": 33565,
    "frontManager": {
      "_id": "63d0d82f7d235411bb37dcd4",
      "email": "front0@mail.com",
      "fullName": "Волфганг Волфов",
      "role": "frontOffice",
      "token": "x3AcVzfKmgfbdhLfOCRY6",
      "path": "account-opening",
      "isHasAccount": false,
      "__v": 0
    },
    "rejectionReason": "none",
    "stockReqType": "Sell",
    "date": "2023-01-25T07:20:15.511Z",
    "stockResult": 14,
    "stockFail": 22,
    "priceResult": 29,
    "__v": 0
  },
  */