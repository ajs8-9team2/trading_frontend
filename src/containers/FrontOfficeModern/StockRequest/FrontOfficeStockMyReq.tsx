import React, {useEffect, useState} from "react";
import {styled, ThemeProvider} from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import {Typography} from "@mui/material";
import {useAppDispatch, useAppSelector} from "../../../hook";
import {TStateStatus, TUser} from "../../../types/reestrOfAlias";
import PinnedSubheaderList
    from "../../../components/CompFrontOffice/CompStockReq/MyStockReq/PinnedSubheaderList/PinnedSubheaderList";
import RightSideForm from "../../../components/CompFrontOffice/CompStockReq/MyStockReq/RightSideForm/RightSideForm";
// import {TGetPostTransferReqAT} from "../../../types/typesTransferRequest";
import {queryTransferReqFrontWIPAT} from "../../../store/services/transferRequestSlice";
import {Context} from "../../../context";
import {createTheme} from "@material-ui/core/styles";
import {TGetPostStockRequestAT} from "../../../types/typesStockRequest";
import {queryStockReqFrontWIPAT/*, setChangeToggle*/} from "../../../store/services/stockRequestSlice";

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const FrontOfficeStockMyReq: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allStocksRequest: TGetPostStockRequestAT[] = useAppSelector(state=>state.reducerStock.allStocksRequest);
    // const oneStockRequest: TGetPostStockRequestAT = useAppSelector(state=>state.reducerStock.oneStockRequest);
    const stockRequestStatus: TStateStatus = useAppSelector(state=>state.reducerStock.stockRequestStatus);
    // const isToggleState: boolean = useAppSelector(state=>state.reducerStock.isToggleState);
    // const allTransferReq: TGetPostTransferReqAT[] = useAppSelector(state=>state.reducerTransferRequest.allTransferReq);
    // const oneTransferReq: TGetPostTransferReqAT = useAppSelector(state=>state.reducerTransferRequest.oneTransferReq);
    // const transferReqStatus:TStateStatus = useAppSelector(state=>state.reducerTransferRequest.transferReqStatus);
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    //---------------------------------------------------------------------------------------------------
    const [_idStockReq, set_idStockReq] = useState<string>("");
    const [seconds, setSeconds] = useState<number>(20);
    const [timerActive, setTimerActive] = useState<boolean>(false);
    const [myArray, setMyArray] = useState<TGetPostStockRequestAT[]>();

    useEffect(()=>{
        // dispatch(setChangeToggle(true));
        const fetchData = async()=>{
            await dispatch(queryStockReqFrontWIPAT(dataUser?._id!));//FrontWIP
        }
        fetchData();
    }, []);

    useEffect(()=>{
        // "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
        setMyArray(allStocksRequest.filter(
            item=>item?.status !== "Approved" &&
                    item?.status !== "Declined"
        ))

        const innerIndex = allStocksRequest.findIndex(item=>item?._id === _idStockReq)
        if(innerIndex !== -1 && ["Approved", "Declined"].includes(allStocksRequest[innerIndex]?.status!))
            // запускаем таймер, если сотрудник принял или отклонил заявку
            setTimerActive(true)
    }, [stockRequestStatus]);
    //-----------------------------------------------------------------------------------------
    useEffect(()=>{
        setSeconds(20)
    }, [_idStockReq]);


    React.useEffect(()=>{
        const fetchData = ()=>{
            if(seconds > 0 && timerActive){
                setTimeout(setSeconds, 100, seconds - 1);
            }else{
                setTimerActive(false);
            }
        }
        fetchData();
    }, [seconds, timerActive]);
// Context --------------------------------------------------------------------------------
    const set_idStockReqContext = (_idAccReq: string)=>{
        set_idStockReq(_idAccReq)
    }
    const value = {set_idStockReqContext, _idStockReq, timerActive}
    //-----------------------------------------------------------------------------------------
    const MyReq = ()=>{
        return (<>
            <ThemeProvider theme={theme}>
            <Grid
                container spacing={1}
                // sx={{ height: "80vh"}}
                sx={{
                    mt: 0,// чтоб не приклеивалось к toolbar
                    // backgroundColor: '#d6f0ff',
                    // height: '50vh'
                }}
            >
                {!!myArray?.length ?
                    <>
                        <Grid item xs={4} md={3}
                            // sx={{height: "100%"}}
                              sx={{bgcolor: 'background.paper'}}
                        >
                            {!!myArray?.length &&
                            <PinnedSubheaderList
                                // setGetID={set_idStockReq}
                                // timerActive={timerActive}
                                myArray={
                                    // myArray
                                    (()=>{
                                        if(seconds !== 0){
                                            let innerArray: TGetPostStockRequestAT[] = [];
                                            for(let allTransfer of allStocksRequest){
                                                // если нашли заявление со статусом approved = true
                                                // если нашли совпадение по id = false
                                                // false + true
                                                // if(allTransfer?.status === "Approved" && allTransfer?._id !== _idStockReq) continue;
                                                if(
                                                    (allTransfer?.status === "Approved" && allTransfer?._id !== _idStockReq) ||
                                                    (allTransfer?.status === "Declined" && allTransfer?._id !== _idStockReq)
                                                ) continue;
                                                innerArray.push(allTransfer)
                                            }
                                            return innerArray;
                                        }else{
                                            return myArray;
                                        }

                                    })()
                                }
                            />}

                        </Grid>
                        <Grid item xs={8} md={9}>
                            {/*{myArray.some(item=>item?._id === _idStockReq) &&*/}
                            {(_idStockReq && !!seconds) &&
                            <RightSideForm
                                seconds={seconds}
                                dataUser={dataUser}
                                userID={(()=>{
                                    // находим клиента по заявке
                                    const innerIndex: number = allStocksRequest.findIndex(item=>item?._id === _idStockReq)
                                    if(innerIndex !== -1)
                                        return allStocksRequest[innerIndex]?.user?._id;
                                    else
                                        return "noUserID"
                                })()}
                            />}
                        </Grid>
                    </>
                    :
                    <Grid item xs={12} md={12}>
                        <Typography
                            variant="h4"
                            gutterBottom
                            sx={{
                                mt: 10,
                                textAlign: "center",
                                color: "grey"
                            }}
                        >
                            Нет заявлений закрепленных за вами!
                        </Typography>
                    </Grid>
                }

            </Grid>
            </ThemeProvider>
            </>
        )
    }

    return (
        <Context.Provider value={value}>
            {/*<pre>{JSON.stringify(isToggleState, null, 2)}</pre>*/}
            {/*<pre>{JSON.stringify(_idStockReq, null, 2)}</pre>*/}
            {/*<pre>{JSON.stringify(allStocksRequest, null, 2)}</pre>*/}
            {MyReq()}
        </Context.Provider>
    )
}

export default FrontOfficeStockMyReq;

        // {/*<div>*/}
        // {/*    {seconds*/}
        // {/*        ?*/}
        // {/*        <button onClick={()=>setTimerActive(true)}>*/}
        // {/*            {timerActive ? 'stop' : 'start'}*/}
        // {/*        </button>*/}
        // {/*        :*/}
        // {/*        <button onClick={*/}
        // {/*            ()=>{*/}
        // {/*                setSeconds(20)*/}
        // {/*                // setTimerActive(true)*/}
        // {/*            }*/}
        // {/*        }>ещё раз</button>*/}
        // {/*    }*/}
        // {/*</div>*/}
        // {/*<pre>{JSON.stringify(seconds, null, 2)}</pre>*/}
        // {/*<pre>{JSON.stringify(!!seconds, null, 2)}</pre>*/}
        // {/*<pre>{JSON.stringify(timerActive, null, 2)}</pre>*/}
        // {/*<pre>{JSON.stringify(accRequestStatus, null, 2)}</pre>*/}
        //
        // {MyReq()}
        // {/*<>*/}
        // {/*    <pre>{JSON.stringify(myArray?.length, null, 2)}</pre>*/}
        // {/*    <pre>{JSON.stringify(allAccRequests.length, null, 2)}</pre>*/}
        // {/*</>*/}
        // {/*<pre>{JSON.stringify(allAccRequests, null, 2)}</pre>*/}
