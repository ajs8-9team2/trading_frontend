import React, {useEffect} from "react";
import {styled, ThemeProvider} from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import {Container, Divider, Switch, Typography} from "@mui/material";
import {useAppDispatch, useAppSelector} from "../../../hook";
import {
    TIdentificationNumber,
    TRejectionReason,
    TSenderFullName,
    TStateStatus, TStatusTransferDeposit,
    TUser
} from "../../../types/reestrOfAlias";
import Button from "@mui/material/Button";
import DiscreteSlider
    from "../../../components/CompFrontOffice/CompTransferReq/ListAccReq/DiscreteSlider/DiscreteSlider";
import TransferList from "../../../components/CompFrontOffice/CompTransferReq/TransferList/TransferList";
import InsetDividers from "../../../components/CompFrontOffice/CompTransferReq/ListAccReq/InsetDividers/InsetDividers";
import EnhancedTable from "../../../components/CompFrontOffice/CompTransferReq/ListAccReq/EnhancedTable/EnhancedTable";
import HomeIcon from '@mui/icons-material/Home';
import SettingsIcon from '@mui/icons-material/Settings';
import {TGetPostTransferReqAT, TTransferReq} from "../../../types/typesTransferRequest";
import {fetchTransferReqAT} from "../../../store/services/transferRequestSlice";
import {Context} from "../../../context";
import TransferList2 from "../../../components/CompFrontOffice/CompTransferReq/TransferList2/TransferList2";
import {createTheme} from "@material-ui/core/styles";
import TransferList3 from "../../../components/CompFrontOffice/CompDepositReq/TransferList3/TransferList3";

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const theme = createTheme({
    palette: {
        primary: {main: "#263238"},
        secondary: {main: '#11cb5f'},
    },
});

const FrontOfficeTransferListReq: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allTransferReq: TGetPostTransferReqAT[] = useAppSelector(state=>state.reducerTransferRequest.allTransferReq);
    const oneTransferReq: TGetPostTransferReqAT = useAppSelector(state=>state.reducerTransferRequest.oneTransferReq);
    const transferReqStatus: TStateStatus = useAppSelector(state=>state.reducerTransferRequest.transferReqStatus);
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [gridSize, setGridSize] = React.useState<number>(8);
    const [isToggle, setIsToggle] = React.useState<boolean>(false);
    const [isToggle2,setIsToggle2]= React.useState<boolean>(true);

    useEffect(()=>{
        const fetchData = async()=>{
            setIsToggle(true);
            await dispatch(fetchTransferReqAT());
        }
        fetchData()
    }, []);
    // свитчер -----------------------------------------------------------------


    //***************************
    //***************************
    const [toggle, setToggle] = React.useState<boolean>(true);
    const [checked, setChecked] = React.useState(false);
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>)=>{
        setChecked(event.target.checked);
    };
    //--------------------------------------------------------------------------
    // фильтр для сотрудников
    //***************************
    const empArrayFun = ()=>{
        let count = 0;
        const innerObj = allTransferReq.reduce((totalObj: { [key: string]: number }, item)=>{
            const innerKey = item?.frontManager?._id;
            if(innerKey){
                totalObj[innerKey] = (totalObj[innerKey] ?? 0) + 1;
            }else{
                count++;
                totalObj["not_assigned"] = count;
            }
            return totalObj;
        }, {});
        return Object.entries(innerObj)
    }
    const [employeesArray, setEmployeesArray] = React.useState<string[]>([]);
    const [employees, setEmployees] = React.useState<string[]>([]);
    const [left3, setLeft3] = React.useState<readonly number[]>([]);
    const [right3, setRight3] = React.useState<readonly number[]>([]);


    useEffect(()=>{
        if(empArrayFun().length && transferReqStatus === "resolved"){
            const innerArray = empArrayFun()?.map(item=>item[0] + " / " + item[1] + " дел.");
            setEmployeesArray(innerArray)

            if(isToggle || isToggle2){
                const right3Array = [...innerArray.map((item, index)=>index)];//[0,1,2,3,4,5,6]
                setRight3(right3Array);
                //--------------------------------------
                setEmployees(right3Array.map(item=>innerArray[item]));
                //--------------------------------------
                setIsToggle(false);
                setIsToggle2(false);
            }
        }
    }, [transferReqStatus]);

    //***************************
    //***************************
    const arrayStatusesRequest = ["Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]
    const [statusReq, setStatusReq] = React.useState<string[]>(arrayStatusesRequest);
    const [left2, setLeft2] = React.useState<readonly number[]>([]);
    const [right2, setRight2] = React.useState<readonly number[]>([...(()=>{
        const lenArray: number[] = [];
        for(let i1 = 0; i1 < arrayStatusesRequest.length; i1++){
            lenArray.push(i1);
        }
        return lenArray;
    })()]);

    //***************************
    //***************************
    // таблица3
    const rowsArrayTableStr = [
        ["_id", "ID заявления"],
        ["date", "дата созд."],
        ["frontManager", "ID сотрудника"],
        ["status", "Статус рассмотрения"],
        ["rejectionReason", "Причина откл."],
        ["amount", "сумма перевода"],
        ["senderIIN", "ИИН отправителя"],
        ["recipientIIN", "ИИН получателя"],
        ["senderIIK", "ИИК(IBAN) отправителя"],
        ["recipientIIK", "ИИК(IBAN) получателя"],
        ["senderBIK", "БИК отправителя"],
        ["recipientBIK", "БИК получателя"],
        ["senderBank", "Банк отправителя"],
        ["recipientBank", "Банк получателя"],
        ["senderKBe", "КБе отправителя"],
        ["recipientKBe", "КБе получателя"],
        ["senderFullName", "ФИО отправителя"],
        ["recipientFullName", "ФИО получателя"],
    ];
    const [tableRowsName, setTableRowsName] = React.useState<string[][]>(rowsArrayTableStr);
    const [left1, setLeft1] = React.useState<readonly number[]>([]);
    const [right1, setRight1] = React.useState<readonly number[]>([...(()=>{
        const lenArray: number[] = [];
        for(let i1 = 0; i1 < rowsArrayTableStr.length; i1++){
            lenArray.push(i1);
        }
        return lenArray;
    })()]);
    //***************************
    //***************************
    // Context --------------------------------------------------------------------------------
    const value = {
        transferReqStatus,

        //TransferList
        rowsArrayTableStr,
        tableRowsName,
        setTableRowsName,
        left1, setLeft1,
        right1, setRight1,

        //TransferList2
        arrayStatusesRequest,
        setStatusReq,
        statusReq,
        left2, setLeft2,
        right2, setRight2,

        //TransferList3
        employeesArray,
        employees,
        setEmployees,
        left3, setLeft3,
        right3, setRight3,
    }
    //-----------------------------------------------------------------------------------------
    if(0)
        return (
            // <pre>{JSON.stringify(allAccRequests[allAccRequests.length - 1], null, 2)}</pre>
            <pre>{JSON.stringify(statusReq, null, 2)}</pre>
        )

    return (<>
            <ThemeProvider theme={theme}>
                <Context.Provider value={value}>
                    <Grid
                        container spacing={1}
                        // sx={{ height: "80vh"}}
                        sx={{
                            mt: 0,
                            // height: "100%",
                            backgroundColor: 'lightgrey',
                            // height: '50vh'
                        }}
                    >
                        {toggle &&
                        <Grid
                            item xs={12} md={12}
                        >
                            {
                                // !Object.prototype.toString.call(oneAccRequest).includes("Undefined") &&
                                <Item>
                                    <Grid
                                        container
                                        direction="row"
                                        justifyContent="space-between"
                                        alignItems="center"
                                    >
                                        {/*<pre>{JSON.stringify(concatOneAccOneDepos, null, 2)}</pre>*/}
                                        {/*<pre>{JSON.stringify(oneDepositRequest, null, 2)}</pre>*/}
                                        <Button
                                            // variant="outlined"
                                            size="small"
                                            onClick={()=>{
                                                setToggle(!toggle);
                                            }}
                                        >
                                            <SettingsIcon/>
                                        </Button>
                                        <Divider orientation="vertical" flexItem/>
                                        <Typography
                                            variant="h4"
                                            gutterBottom
                                            sx={{
                                                m: 0,
                                                p: 0,
                                                textAlign: "center",
                                                color: "grey"
                                            }}
                                        >
                                            {"заявление на перевод денег"}
                                        </Typography>
                                        <Divider orientation="vertical" flexItem/>
                                        <Typography
                                            // variant="h6"
                                            gutterBottom
                                            sx={{
                                                m: 0,
                                                p: 0,
                                                textAlign: "center",
                                                color: "grey"
                                            }}
                                        >
                                            ваш id: {dataUser?._id?.toUpperCase()}
                                        </Typography>
                                    </Grid>
                                </Item>
                            }
                        </Grid>}

                        {!toggle &&
                        <Grid
                            item xs={12} md={12}
                        >
                            {
                                // !Object.prototype.toString.call(oneAccRequest).includes("Undefined") &&
                                <Item>
                                    <Grid
                                        container
                                        direction="row"
                                        justifyContent="space-between"
                                        alignItems="center"
                                    >
                                        <Button
                                            // variant="outlined"
                                            size="small"
                                            onClick={()=>{
                                                setToggle(!toggle);
                                            }}
                                            disabled={checked}
                                        >
                                            <HomeIcon/>
                                        </Button>
                                        {/*<Divider orientation="vertical" flexItem/>*/}
                                        <DiscreteSlider
                                            setGridSize={setGridSize}
                                        />
                                        {/*<Divider orientation="vertical" flexItem/>*/}
                                        <Grid>
                                            <Grid
                                                container
                                                direction="row"
                                                justifyContent="space-between"
                                                alignItems="center"
                                            >
                                                <Typography sx={{
                                                    fontSize: 16,
                                                    mr: 2
                                                }} color="text.secondary" gutterBottom>
                                                    {"фильтр"}
                                                </Typography>

                                                <Switch
                                                    checked={checked}
                                                    onChange={
                                                        handleChange
                                                    }
                                                    inputProps={{'aria-label': 'controlled'}}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    {/*<pre>{JSON.stringify(oneAccRequest, null, 2)}</pre>*/}
                                    {/*<pre>{*/}
                                    {/*    JSON.stringify(*/}
                                    {/*        Object.prototype.toString.call(oneAccRequest).includes("Undefined"), null, 2)*/}
                                    {/*}</pre>*/}
                                </Item>
                            }
                        </Grid>}

                        {checked &&
                        <Grid
                            item xs={12} md={12}
                        >
                            {
                                <Item>
                                    {/*// xs, extra-small: 0px*/}
                                    {/*// sm, small: 600px*/}
                                    {/*// md, medium: 900px*/}
                                    {/*// lg, large: 1200px*/}
                                    {/*// xl, extra-large: 1536px*/}

                                    <Grid container spacing={2}>
                                        <Grid item lg={6} md={12}>
                                            <Item>
                                                <Typography variant="overline" display="block" gutterBottom>
                                                    фильтрация по столбцам таблицы
                                                </Typography>
                                                <TransferList/>
                                            </Item>
                                        </Grid>
                                        <Grid item lg={6} md={12}>
                                            <Item>
                                                <Typography variant="overline" display="block" gutterBottom>
                                                    фильтрация по статусу заявки
                                                </Typography>
                                                <TransferList2/>
                                            </Item>
                                        </Grid>
                                        {employeesArray?.length &&
                                        <Grid item lg={12} md={12}>
                                            <Item>
                                                <Typography variant="overline" display="block" gutterBottom>
                                                    фильтрация по сотрудникам
                                                </Typography>
                                                <TransferList3/>
                                            </Item>
                                        </Grid>
                                        }
                                    </Grid>
                                </Item>
                            }
                            {/*<pre>{JSON.stringify(employees)}</pre>*/}
                            {/*<pre>{JSON.stringify(employeesArray)}</pre>*/}
                            {/*<pre>{JSON.stringify(right3)}</pre>*/}

                            {/*<pre>{JSON.stringify(transferReqStatus)}</pre>*/}
                            {/*<pre>{JSON.stringify(statusReq)}</pre>*/}
                        </Grid>}

                        {!!(gridSize) &&
                        <Grid item xs={gridSize} md={gridSize}
                              sx={{height: "100%"}}
                        >
                            <Item>
                                {!!allTransferReq.length
                                    ?
                                    <EnhancedTable
                                        // right={right}
                                        allTransferReq={allTransferReq}
                                    />
                                    :
                                    <>{"В базе данных нет никаких заявок!"}</>
                                }
                            </Item>
                        </Grid>
                        }

                        {!!(12 - gridSize) &&
                        <Grid item xs={12 - gridSize} md={12 - gridSize}>
                            <Item>
                                {
                                    // (Object.prototype.toString.call(oneAccRequest).includes("Undefined") ||
                                    //     Object.prototype.toString.call(oneAccRequest).includes("Null"))
                                    //     ?
                                    //     <>{"???"}</>
                                    //     :
                                    <InsetDividers
                                        {...oneTransferReq!}
                                    />
                                }

                                {/*{allAccRequests && <FullInfo {...oneAccRequest!}/>}*/}
                            </Item>
                        </Grid>
                        }
                    </Grid>
                </Context.Provider>
            </ThemeProvider>
        </>
    )
}

export default FrontOfficeTransferListReq;

