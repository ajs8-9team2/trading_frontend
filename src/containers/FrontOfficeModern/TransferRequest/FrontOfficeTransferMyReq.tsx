import React, {useEffect, useState} from "react";
import {styled, ThemeProvider} from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import {Typography} from "@mui/material";
import {useAppDispatch, useAppSelector} from "../../../hook";
import {TStateStatus, TUser} from "../../../types/reestrOfAlias";
import PinnedSubheaderList
    from "../../../components/CompFrontOffice/CompTransferReq/MyAccReq/PinnedSubheaderList/PinnedSubheaderList";
import RightSideForm from "../../../components/CompFrontOffice/CompTransferReq/MyAccReq/RightSideForm/RightSideForm";
import {TGetPostTransferReqAT} from "../../../types/typesTransferRequest";
import {queryTransferReqFrontWIPAT} from "../../../store/services/transferRequestSlice";
import {Context} from "../../../context";
import {createTheme} from "@material-ui/core/styles";

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const FrontOfficeTransferMyReq: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allTransferReq: TGetPostTransferReqAT[] = useAppSelector(state=>state.reducerTransferRequest.allTransferReq);
    // const oneTransferReq: TGetPostTransferReqAT = useAppSelector(state=>state.reducerTransferRequest.oneTransferReq);
    const transferReqStatus:TStateStatus = useAppSelector(state=>state.reducerTransferRequest.transferReqStatus);
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    //---------------------------------------------------------------------------------------------------
    const [_idTransferReq, set_idTransferReq] = useState<string>("");
    const [seconds, setSeconds] = useState<number>(20);
    const [timerActive, setTimerActive] = useState<boolean>(false);
    const [myArray, setMyArray] = useState<TGetPostTransferReqAT[]>();


    useEffect(()=>{
        const fetchData = async()=>{
            await dispatch(queryTransferReqFrontWIPAT(dataUser?._id!));//FrontWIP
        }
        fetchData()
    }, []);

    useEffect(()=>{
        // "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
        setMyArray(allTransferReq.filter(
            item=>item?.status !== "Approved" &&
                    item?.status !== "Declined"
        ))

        const innerIndex = allTransferReq.findIndex(item=>item?._id === _idTransferReq)
        if(innerIndex !== -1 && ["Approved", "Declined"].includes(allTransferReq[innerIndex]?.status!))
            // запускаем таймер, если сотрудник принял или отклонил заявку
            setTimerActive(true)
    }, [transferReqStatus]);
    //-----------------------------------------------------------------------------------------
    useEffect(()=>{
        setSeconds(20)
    }, [_idTransferReq]);


    React.useEffect(()=>{
        const fetchData = ()=>{
            if(seconds > 0 && timerActive){
                setTimeout(setSeconds, 100, seconds - 1);
            }else{
                setTimerActive(false);
            }
        }
        fetchData();
    }, [seconds, timerActive]);
// Context --------------------------------------------------------------------------------
    const set_idTransferReqContext = (_idAccReq: string)=>{
        set_idTransferReq(_idAccReq)
    }
    const value = {set_idTransferReqContext, _idTransferReq, timerActive}
    //-----------------------------------------------------------------------------------------
    const MyReq = ()=>{
        return (<>
            <ThemeProvider theme={theme}>
            <Grid
                container spacing={1}
                // sx={{ height: "80vh"}}
                sx={{
                    mt: 0,// чтоб не приклеивалось к toolbar
                    // backgroundColor: '#d6f0ff',
                    // height: '50vh'
                }}
            >
                {!!myArray?.length ?
                    <>
                        <Grid item xs={4} md={3}
                            // sx={{height: "100%"}}
                              sx={{bgcolor: 'background.paper'}}
                        >
                            {!!myArray?.length &&
                            <PinnedSubheaderList
                                // setGetID={set_idTransferReq}
                                // timerActive={timerActive}
                                myArray={
                                    // myArray
                                    (()=>{
                                        if(seconds !== 0){
                                            let innerArray: TGetPostTransferReqAT[] = [];
                                            for(let allTransfer of allTransferReq){
                                                // если нашли заявление со статусом approved = true
                                                // если нашли совпадение по id = false
                                                // false + true
                                                // if(allTransfer?.status === "Approved" && allTransfer?._id !== _idTransferReq) continue;
                                                if(
                                                    (allTransfer?.status === "Approved" && allTransfer?._id !== _idTransferReq) ||
                                                    (allTransfer?.status === "Declined" && allTransfer?._id !== _idTransferReq)
                                                ) continue;
                                                innerArray.push(allTransfer)
                                            }
                                            return innerArray;
                                        }else{
                                            return myArray;
                                        }

                                    })()
                                }
                            />}

                        </Grid>
                        <Grid item xs={8} md={9}>
                            {/*{myArray.some(item=>item?._id === _idTransferReq) &&*/}
                            {(_idTransferReq && !!seconds) &&
                            <RightSideForm
                                seconds={seconds}
                                dataUser={dataUser}
                                userID={(()=>{
                                    // находим клиента по заявке
                                    const innerIndex: number = allTransferReq.findIndex(item=>item?._id === _idTransferReq)
                                    if(innerIndex !== -1)
                                        return allTransferReq[innerIndex]?.user?._id;
                                    else
                                        return "noUsertID"
                                })()}
                            />}
                        </Grid>
                    </>
                    :
                    <Grid item xs={12} md={12}>
                        <Typography
                            variant="h4"
                            gutterBottom
                            sx={{
                                mt: 10,
                                textAlign: "center",
                                color: "grey"
                            }}
                        >
                            Нет заявлений закрепленных за вами!
                        </Typography>
                    </Grid>
                }

            </Grid>
            </ThemeProvider>
            </>
        )
    }

    return (
        <Context.Provider value={value}>
            {/*<pre>{JSON.stringify(_idTransferReq, null, 2)}</pre>*/}
            {/*<pre>{JSON.stringify(myArray, null, 2)}</pre>*/}
            {MyReq()}
        </Context.Provider>
    )
}

export default FrontOfficeTransferMyReq;

        // {/*<div>*/}
        // {/*    {seconds*/}
        // {/*        ?*/}
        // {/*        <button onClick={()=>setTimerActive(true)}>*/}
        // {/*            {timerActive ? 'stop' : 'start'}*/}
        // {/*        </button>*/}
        // {/*        :*/}
        // {/*        <button onClick={*/}
        // {/*            ()=>{*/}
        // {/*                setSeconds(20)*/}
        // {/*                // setTimerActive(true)*/}
        // {/*            }*/}
        // {/*        }>ещё раз</button>*/}
        // {/*    }*/}
        // {/*</div>*/}
        // {/*<pre>{JSON.stringify(seconds, null, 2)}</pre>*/}
        // {/*<pre>{JSON.stringify(!!seconds, null, 2)}</pre>*/}
        // {/*<pre>{JSON.stringify(timerActive, null, 2)}</pre>*/}
        // {/*<pre>{JSON.stringify(accRequestStatus, null, 2)}</pre>*/}
        //
        // {MyReq()}
        // {/*<>*/}
        // {/*    <pre>{JSON.stringify(myArray?.length, null, 2)}</pre>*/}
        // {/*    <pre>{JSON.stringify(allAccRequests.length, null, 2)}</pre>*/}
        // {/*</>*/}
        // {/*<pre>{JSON.stringify(allAccRequests, null, 2)}</pre>*/}
