import React, {useEffect, useState} from "react";
import {styled, ThemeProvider} from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
// import BasicTable from "../../components/CompFrontOffice/ListAccReq/BasicTable/BasicTable";
import {Typography} from "@mui/material";
import {useAppDispatch, useAppSelector} from "../../../hook";
import {TGetAccRequestsAT} from "../../../types/typesAccReq";
import {TStateStatus, TUser} from "../../../types/reestrOfAlias";
import {fetchAccRequestsAT, queryAccReqFrontWIPAT} from "../../../store/services/accRequestSlice";
import PinnedSubheaderList
    from "../../../components/CompFrontOffice/CompDepositReq/MyAccReq/PinnedSubheaderList/PinnedSubheaderList";
import RightSideForm from "../../../components/CompFrontOffice/CompDepositReq/MyAccReq/RightSideForm/RightSideForm";
import {queryDepositReqFrontWIPAT} from "../../../store/services/depositRequestSlice";
import {TGetPostDepositRequestAT} from "../../../types/typesDepositRequest";
import {TTotalPickTable} from "./FrontOfficeDepositListReq";
import {filterAndConcatArras} from "../../../Helpers/filterAndConcatArras";
import {Context} from "../../../context";
import {createTheme} from "@material-ui/core/styles";

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const FrontOfficeDepositMyReq: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allAccRequests: TGetAccRequestsAT[] = useAppSelector(state=>state.reducerAccRequests.allAccRequests);
    const allDepositsRequest: TGetPostDepositRequestAT[] = useAppSelector(state=>state.reducerDepositRequest.allDepositsRequest);
    const depositRequestStatus: TStateStatus = useAppSelector(state=>state.reducerDepositRequest.depositRequestStatus);

    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [_idDepositReq, set_idDepositReq] = useState<string>("");
    const [seconds, setSeconds] = useState<number>(20);
    const [timerActive, setTimerActive] = useState<boolean>(false);
    const [myArray, setMyArray] = useState<TTotalPickTable[]>();

    useEffect(()=>{
        const fetchData = async()=>{
            await dispatch(queryDepositReqFrontWIPAT(dataUser?._id!));//FrontWIP
            await dispatch(fetchAccRequestsAT());
        }
        fetchData()
    }, []);

    // const innerRowsArray: TTotalPickTable[] = React.useMemo(()=>{
    //     return filterAndConcatArras(
    //         allDepositsRequest, allAccRequests,
    //         ["_id", "status", "amount", "senderFullName", "senderIIN",
    //             "senderBank", "senderIIK", "senderBIK", "frontManager", "date"],
    //         ["firstName", "lastName", "patronym", "identificationNumber"],
    //         ["Approved"]
    //     )
    //
    // }, [depositRequestStatus])

    useEffect(()=>{
        const innerArray: TTotalPickTable[] = filterAndConcatArras(
            allDepositsRequest, allAccRequests,
            ["_id", "status", "amount", "senderFullName", "senderIIN",
                "senderBank", "senderIIK", "senderBIK", "frontManager", "date", "rejectionReason"],
            ["firstName", "lastName", "patronym", "identificationNumber"],
            ["Approved", "Declined"]//отсеиваем все с: "Approved","Declined"

        )
        setMyArray(innerArray)
        // "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
        const innerIndex = allDepositsRequest.findIndex(item=>item?._id === _idDepositReq)
        if(innerIndex !== -1 && ["Approved", "Declined"].includes(allDepositsRequest[innerIndex]?.status!))
            setTimerActive(true)
    }, [depositRequestStatus]);
    //-----------------------------------------------------------------------------------------
    useEffect(()=>{
        setSeconds(20)
    }, [_idDepositReq]);

    React.useEffect(()=>{
        const fetchData = ()=>{
            if(seconds > 0 && timerActive){
                setTimeout(setSeconds, 100, seconds - 1);
            }else{
                setTimerActive(false);
            }
        }
        fetchData();
    }, [seconds, timerActive]);
    // Context --------------------------------------------------------------------------------
    const set_idDepositReqContext = (_idDepositReq: string)=>{
        set_idDepositReq(_idDepositReq)
    }
    const value = {set_idDepositReqContext, _idDepositReq, timerActive}
    //-----------------------------------------------------------------------------------------

    const MyReq = ()=>{
        return (<>
            <ThemeProvider theme={theme}>
            <Grid
                container spacing={1}
                // sx={{ height: "80vh"}}
                sx={{
                    mt: 0,// чтоб не приклеивалось к toolbar
                    // backgroundColor: '#d6f0ff',
                    // height: '50vh'
                }}
            >
                {!!myArray?.length ?
                    <>
                        <Grid item xs={4} md={3}
                            // sx={{height: "100%"}}
                              sx={{bgcolor: 'background.paper'}}
                        >
                            {!!myArray?.length &&
                            <PinnedSubheaderList
                                // setGetID={set_idDepositReq}
                                // timerActive={timerActive}
                                myArray={
                                    // myArray
                                    (()=>{
                                        if(seconds !== 0){
                                            let innerArray: TTotalPickTable[] = [];
                                            const allDepositsData: TTotalPickTable[] = filterAndConcatArras(
                                                allDepositsRequest, allAccRequests,
                                                ["_id", "status", "amount", "senderFullName", "senderIIN",
                                                    "senderBank", "senderIIK", "senderBIK",
                                                    "frontManager", "date", "rejectionReason"],
                                                ["firstName", "lastName", "patronym", "identificationNumber"],
                                                [""]
                                            )

                                            for(let allDeposit of allDepositsData){
                                                // если нашли заявление со статусом approved = true
                                                // если нашли совпадение по id = false
                                                // false + true
                                                if(
                                                    (allDeposit?.status === "Approved" && allDeposit?._id !== _idDepositReq) ||
                                                    (allDeposit?.status === "Declined" && allDeposit?._id !== _idDepositReq)
                                                ) continue;
                                                innerArray.push(allDeposit)
                                            }
                                            return innerArray;

                                        }else{
                                            return myArray;
                                        }
                                    })()
                                }
                            />}

                        </Grid>
                        <Grid item xs={8} md={9}>
                            {/*{myArray.some(item=>item?._id === getID) &&*/}
                            {(_idDepositReq && !!seconds) &&
                            <RightSideForm
                                seconds={seconds}
                                dataUser={dataUser}
                                userID={(()=>{
                                    // находим клиента по заявке
                                    const innerIndex: number = allDepositsRequest.findIndex(item=>item?._id === _idDepositReq)
                                    if(innerIndex !== -1)
                                        return allDepositsRequest[innerIndex]?.user?._id;
                                    else
                                        return "noUsertID"
                                })()}
                            />}
                        </Grid>
                        {/*<pre>{JSON.stringify(myArray, null, 2)}</pre>*/}
                    </>
                    :
                    <Grid item xs={12} md={12}>
                        <Typography
                            variant="h4"
                            gutterBottom
                            sx={{
                                mt: 10,
                                textAlign: "center",
                                color: "grey"
                            }}
                        >
                            Нет заявлений закрепленных за вами!
                        </Typography>
                    </Grid>
                }
            </Grid>
            </ThemeProvider>
            </>
        )
    }

    return (
        <Context.Provider value={value}>
            {/*<pre>{JSON.stringify(_idDepositReq, null, 2)}</pre>*/}
            {MyReq()}
        </Context.Provider>
    )
}

export default FrontOfficeDepositMyReq;