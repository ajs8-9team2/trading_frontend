import {
    TIdentificationNumber,
    TRejectionReason,
    TSenderFullName,
    TStateStatus,
    TUser
} from "../../../types/reestrOfAlias";
import RightSideForm from "../../../components/CompFrontOffice/CompDepositReq/MyAccReq/RightSideForm/RightSideForm";
import React, {useEffect, useState} from "react";
import {styled, ThemeProvider} from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
// import BasicTable from "../../components/CompFrontOffice/ListAccReq/BasicTable/BasicTable";
import {Divider, Switch, Typography} from "@mui/material";
import {useAppDispatch, useAppSelector} from "../../../hook";
import {TGetAccRequestsAT} from "../../../types/typesAccReq";
import {fetchAccRequestsAT} from "../../../store/services/accRequestSlice";
import Button from "@mui/material/Button";
import DiscreteSlider
    from "../../../components/CompFrontOffice/CompDepositReq/ListAccReq/DiscreteSlider/DiscreteSlider";
import InsetDividers from "../../../components/CompFrontOffice/CompDepositReq/ListAccReq/InsetDividers/InsetDividers";
import {fetchDepositReqFrontAT, fetchDepositRequestAT} from "../../../store/services/depositRequestSlice";
import {TDepositRequest, TGetPostDepositRequestAT} from "../../../types/typesDepositRequest";
import EnhancedTable from "../../../components/CompFrontOffice/CompDepositReq/ListAccReq/EnhancedTable/EnhancedTable";
import HomeIcon from '@mui/icons-material/Home';
import SettingsIcon from '@mui/icons-material/Settings';
import {Context} from "../../../context";
import {createTheme} from "@material-ui/core/styles";
import TransferList from "../../../components/CompFrontOffice/CompDepositReq/TransferList/TransferList";
import TransferList2 from "../../../components/CompFrontOffice/CompDepositReq/TransferList2/TransferList2";
import TransferList3 from "../../../components/CompFrontOffice/CompDepositReq/TransferList3/TransferList3";

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const theme = createTheme({
    palette: {
        primary: {main: "#263238"},
        secondary: {main: '#11cb5f'},
    },
});

type TDeposit =
    Pick<TDepositRequest, "_id" | "status" | "amount" | "senderFullName" | "senderIIN" | "senderBank" | "senderIIK" | "senderBIK" | /*"frontManager" |*/ "date">
// type TAccReq = Pick<TFirstAccReq, "firstName" | "lastName" | "patronym" | "identificationNumber">
// export type TTotalPick = TDeposit & TAccReq;


export type TTotalPickTable = TDeposit & {
    num: string,
    frontManager: string;
    getterFullName: TSenderFullName;
    identificationNumber: TIdentificationNumber;
    rejectionReason?: TRejectionReason;
};

const FrontOfficeDepositListReq: React.FC = ()=>{
    const dispatch = useAppDispatch();
    // const oneDepositRequest: TGetPostDepositRequestAT = useAppSelector(state=>state.reducerDepositRequest.oneDepositRequest);
    const allAccRequests: TGetAccRequestsAT[] = useAppSelector(state=>state.reducerAccRequests.allAccRequests);
    const allDepositsRequest: TGetPostDepositRequestAT[] = useAppSelector(state=>state.reducerDepositRequest.allDepositsRequest);
    const depositsStateStatus: TStateStatus = useAppSelector(state=>state.reducerDepositRequest.depositRequestStatus);
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [_idDepositReq, set_idDepositReq] = useState<string>("");
    // стейт в котором храним один объект полученный в
    // в компоненте EnhancedTable стейта rowsArray
    const [concatOneAccOneDepos, setConcatOneAccOneDepos] = React.useState<TTotalPickTable>();
    const [gridSize, setGridSize] = React.useState<number>(8);
    const [isToggle, setIsToggle] = React.useState<boolean>(false);
    const [isToggle2,setIsToggle2]= React.useState<boolean>(true);

    useEffect(()=>{
        const fetchData = async()=>{
            setIsToggle(true);
            await dispatch(fetchAccRequestsAT());
            await dispatch(fetchDepositRequestAT());
            // await dispatch(fetchDepositReqFrontAT());
        }
        fetchData();
    }, []);
    // свитчер -----------------------------------------------------------------
    const [toggle, setToggle] = React.useState<boolean>(true);
    const [checked, setChecked] = React.useState(false);
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>)=>{
        // setToggle(true)
        setChecked(event.target.checked);
    };
    //--------------------------------------------------------------------------
    // фильтр для сотрудников
    //***************************
    const empArrayFun = ()=>{
        let count = 0;
        const innerObj = allDepositsRequest.reduce((totalObj: { [key: string]: number }, item)=>{
            const innerKey = item?.frontManager?._id;
            if(innerKey){
                totalObj[innerKey] = (totalObj[innerKey] ?? 0) + 1;
            }else{
                count++;
                totalObj["not_assigned"] = count;
            }
            return totalObj;
        }, {});
        return Object.entries(innerObj)
    }
    const [employeesArray, setEmployeesArray] = React.useState<string[]>([]);
    const [employees, setEmployees] = React.useState<string[]>([]);
    const [left3, setLeft3] = React.useState<readonly number[]>([]);
    const [right3, setRight3] = React.useState<readonly number[]>([]);
    // const isToggleRef = React.useRef<boolean>(true);

    useEffect(()=>{
        if(empArrayFun().length && depositsStateStatus === "resolved"){
            const innerArray = empArrayFun()?.map(item=>item[0] + " / " + item[1] + " дел.");
            setEmployeesArray(innerArray)

            if(isToggle || isToggle2){
                const right3Array = [...innerArray.map((item, index)=>index)];//[0,1,2,3,4,5,6]
                setRight3(right3Array);
                //--------------------------------------
                setEmployees(right3Array.map(item=>innerArray[item]));
                //--------------------------------------
                setIsToggle(false);
                setIsToggle2(false);
                // setIsToggle(false)
            }
        }
    }, [depositsStateStatus]);


    //***************************
    //***************************
    // "Pending" , "FrontWIP" , "AccountantWIP" , "Approved" , "Declined" , "Completed"
    const arrayStatusesRequest = ["Pending", "FrontWIP", "AccountantWIP", "Approved", "Declined", "Completed"]
    const [statusReq, setStatusReq] = React.useState<string[]>(arrayStatusesRequest);
    const [left2, setLeft2] = React.useState<readonly number[]>([]);
    const [right2, setRight2] = React.useState<readonly number[]>([...(()=>{
        const lenArray: number[] = [];
        for(let i1 = 0; i1 < arrayStatusesRequest.length; i1++){
            lenArray.push(i1);
        }
        return lenArray;
    })()]);
    //***************************
    //***************************
    // таблица2
    const rowsArrayTableStr = [
        ["_id", "ID заявления"],
        ["status", "Статус рассмотрения"],
        ["date", "дата созд."],
        ["amount", "сумма перевода"],
        ["senderFullName", "ФИО отправителя"],
        ["senderIIN", "ИИН отправителя"],
        ["senderBank", "Банк отправителя"],
        ["senderIIK", "ИИК(IBAN)"],
        ["senderBIK", "БИК"],
        ["frontManager", "ID сотрудника"],
        ["getterFullName", "ФИО получателя"],
        ["identificationNumber", "ИИН получателя"],
    ];
    const [tableRowsName, setTableRowsName] = React.useState<string[][]>(rowsArrayTableStr);
    const [left1, setLeft1] = React.useState<readonly number[]>([]);
    const [right1, setRight1] = React.useState<readonly number[]>([...(()=>{
        const lenArray: number[] = [];
        for(let i1 = 0; i1 < rowsArrayTableStr.length; i1++){
            lenArray.push(i1);
        }
        return lenArray;
    })()]);
    // Context --------------------------------------------------------------------------------
    const set_idDepositReqContext = (_idDepositReq: string)=>{
        set_idDepositReq(_idDepositReq)
    }

    const setConcatOneAccOneDeposContext = (obj: TTotalPickTable)=>{
        setConcatOneAccOneDepos(obj)
    }
    const value = {
        depositsStateStatus,

        //TransferList
        rowsArrayTableStr,
        tableRowsName,
        setTableRowsName,
        left1, setLeft1,
        right1, setRight1,

        //TransferList2
        arrayStatusesRequest,
        setStatusReq,
        statusReq,
        left2, setLeft2,
        right2, setRight2,

        //TransferList3
        employeesArray,
        employees,
        setEmployees,
        left3, setLeft3,
        right3, setRight3,

        _idDepositReq,
        setConcatOneAccOneDeposContext,
        set_idDepositReqContext
    }
    //-----------------------------------------------------------------------------------------

    return (<>
            <ThemeProvider theme={theme}>
                <Context.Provider value={value}>
                    {!!allDepositsRequest.length ?
                        <Grid
                            container spacing={1}
                            // sx={{ height: "80vh"}}
                            sx={{
                                mt: 0,
                                // height: "100%",
                                backgroundColor: 'lightgrey',
                                // height: '50vh'
                            }}
                        >
                            {toggle &&
                            <Grid
                                item xs={12} md={12}
                            >
                                {
                                    // !Object.prototype.toString.call(oneAccRequest).includes("Undefined") &&
                                    <Item>
                                        <Grid
                                            container
                                            direction="row"
                                            justifyContent="space-between"
                                            alignItems="center"
                                        >
                                            {/*<pre>{JSON.stringify(concatOneAccOneDepos, null, 2)}</pre>*/}
                                            {/*<pre>{JSON.stringify(oneDepositRequest, null, 2)}</pre>*/}
                                            <Button
                                                // variant="outlined"
                                                size="small"
                                                onClick={()=>{
                                                    setToggle(!toggle);
                                                }}
                                            >
                                                <SettingsIcon/>

                                            </Button>
                                            <Divider orientation="vertical" flexItem/>
                                            <Typography
                                                variant="h4"
                                                gutterBottom
                                                sx={{
                                                    m: 0,
                                                    p: 0,
                                                    textAlign: "center",
                                                    color: "grey"
                                                }}
                                            >
                                                {"заявление на пополнение счета"}
                                            </Typography>
                                            <Divider orientation="vertical" flexItem/>
                                            <Typography
                                                // variant="h6"
                                                gutterBottom
                                                sx={{
                                                    m: 0,
                                                    p: 0,
                                                    textAlign: "center",
                                                    color: "grey"
                                                }}
                                            >
                                                ваш id: {dataUser?._id?.toUpperCase()}
                                            </Typography>
                                        </Grid>
                                    </Item>
                                }
                            </Grid>}

                            {!toggle &&
                            <Grid
                                item xs={12} md={12}
                            >
                                {
                                    // !Object.prototype.toString.call(oneAccRequest).includes("Undefined") &&
                                    <Item>
                                        <Grid
                                            container
                                            direction="row"
                                            justifyContent="space-between"
                                            alignItems="center"
                                        >
                                            <Button
                                                // variant="outlined"
                                                size="small"
                                                onClick={()=>{
                                                    setToggle(!toggle);
                                                }}
                                                disabled={checked}
                                            >
                                                <HomeIcon/>
                                            </Button>
                                            {/*<Divider orientation="vertical" flexItem/>*/}
                                            <DiscreteSlider
                                                setGridSize={setGridSize}
                                            />
                                            {/*<Divider orientation="vertical" flexItem/>*/}
                                            <Grid>
                                                <Grid
                                                    container
                                                    direction="row"
                                                    justifyContent="space-between"
                                                    alignItems="center"
                                                >
                                                    <Typography sx={{
                                                        fontSize: 16,
                                                        mr: 2
                                                    }} color="text.secondary" gutterBottom>
                                                        {"фильтр"}
                                                    </Typography>

                                                    <Switch
                                                        checked={checked}
                                                        onChange={
                                                            handleChange
                                                        }
                                                        inputProps={{'aria-label': 'controlled'}}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        {/*<pre>{JSON.stringify(oneAccRequest, null, 2)}</pre>*/}
                                        {/*<pre>{*/}
                                        {/*    JSON.stringify(*/}
                                        {/*        Object.prototype.toString.call(oneAccRequest).includes("Undefined"), null, 2)*/}
                                        {/*}</pre>*/}

                                    </Item>
                                }
                            </Grid>}

                            {checked &&
                            <Grid
                                item xs={12} md={12}
                            >
                                {
                                    <Item>
                                        {/*// xs, extra-small: 0px*/}
                                        {/*// sm, small: 600px*/}
                                        {/*// md, medium: 900px*/}
                                        {/*// lg, large: 1200px*/}
                                        {/*// xl, extra-large: 1536px*/}

                                        <Grid container spacing={2}>
                                            <Grid item lg={6} md={12}>
                                                <Item>
                                                    <Typography variant="overline" display="block" gutterBottom>
                                                        фильтрация по столбцам таблицы
                                                    </Typography>
                                                    <TransferList/>
                                                </Item>
                                            </Grid>
                                            <Grid item lg={6} md={12}>
                                                <Item>
                                                    <Typography variant="overline" display="block" gutterBottom>
                                                        фильтрация по статусу заявки
                                                    </Typography>
                                                    <TransferList2/>
                                                </Item>
                                            </Grid>
                                            {employeesArray?.length &&
                                            <Grid item lg={12} md={12}>
                                                <Item>
                                                    <Typography variant="overline" display="block" gutterBottom>
                                                        фильтрация по сотрудникам
                                                    </Typography>
                                                    <TransferList3/>
                                                </Item>
                                            </Grid>
                                            }
                                        </Grid>
                                    </Item>
                                }
                                {/*<pre>{JSON.stringify(employees)}</pre>*/}
                                {/*<pre>{JSON.stringify(employeesArray)}</pre>*/}
                                {/*<pre>{JSON.stringify(right3)}</pre>*/}


                                {/*<pre>{JSON.stringify(depositsStateStatus)}</pre>*/}
                                {/*<pre>{JSON.stringify(tableRowsName,null,0)}</pre>*/}
                                {/*<pre>{JSON.stringify(statusReq,null,0)}</pre>*/}
                            </Grid>}

                            {!!(gridSize) &&
                            <Grid item xs={gridSize} md={gridSize}
                                  sx={{height: "100%"}}
                            >
                                <Item>
                                    {!!allDepositsRequest.length
                                        ?
                                        <EnhancedTable
                                            // setConcatOneAccOneDepos={setConcatOneAccOneDepos}
                                            allAccRequests={allAccRequests}
                                            allDepositsRequest={allDepositsRequest}
                                            // right={right}
                                            // depositsStateStatus={depositsStateStatus}
                                        />
                                        :
                                        <>{"В базе данных нет никаких заявок!"}</>
                                    }
                                </Item>
                            </Grid>
                            }

                            {!!(12 - gridSize) &&
                            <Grid item xs={12 - gridSize} md={12 - gridSize}>
                                <Item>
                                    {
                                        // (Object.prototype.toString.call(oneAccRequest).includes("Undefined") ||
                                        //     Object.prototype.toString.call(oneAccRequest).includes("Null"))
                                        //     ?
                                        //     <>{"???"}</>
                                        //     :
                                        <InsetDividers
                                            concatOneAccOneDepos={concatOneAccOneDepos}
                                            depositsStateStatus={depositsStateStatus}
                                            // setConcatOneAccOneDepos={setConcatOneAccOneDepos}
                                            // oneDepositRequest={oneDepositRequest}
                                            userID={(()=>{
                                                // находим клиента по заявке
                                                const innerIndex: number = allDepositsRequest.findIndex(item=>item?._id === _idDepositReq)
                                                if(innerIndex !== -1)
                                                    return allDepositsRequest[innerIndex]?.user?._id;
                                                else
                                                    return "noUserID"
                                            })()}
                                        />
                                    }

                                    {/*{allAccRequests && <FullInfo {...oneAccRequest!}/>}*/}
                                </Item>
                            </Grid>
                            }

                            {/*<pre>{JSON.stringify(_idDepositReq, null, 2)}</pre>*/}
                        </Grid>
                        :
                        <Grid
                            container spacing={1}
                            // sx={{ height: "80vh"}}
                            sx={{
                                mt: 0,
                                // height: "100%",
                                backgroundColor: 'lightgrey',
                                // height: '50vh'
                            }}
                        >
                            <Grid
                                item xs={12} md={12}
                            >
                                <Typography variant="overline" display="block" gutterBottom>
                                    В базе нет данных для отображения
                                </Typography>
                            </Grid>
                        </Grid>
                    }
                </Context.Provider>
            </ThemeProvider>
        </>
    )
}

export default FrontOfficeDepositListReq;