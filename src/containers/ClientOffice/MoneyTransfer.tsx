import React, {useEffect} from "react";
import {Typography, Snackbar} from "@mui/material";
import MuiAlert, {AlertProps} from '@mui/material/Alert';
import {useNavigate} from 'react-router-dom';
import MoneyTransferForm from '../../components/Forms/MoneyTransferForm/MoneyTransferForm';
import {createDepositRequestAT} from '../../store/services/depositRequestSlice';
import {TDepositRequest,} from '../../types/typesDepositRequest';
import {yupResolver} from '@hookform/resolvers/yup';
import {validationTransfer} from '../../config';
import {useForm} from "react-hook-form";
import {useAppDispatch} from "../../hook";
import {useAppSelector} from '../../hook';
import {TUser} from "../../types/reestrOfAlias";
import {returnNameOfBank} from "../../Helpers/returnNameOfBank";
import {getAccountAT} from "../../store/services/accountSlice";
import {TGetPostAccountAT} from "../../types/typesAccount";
import {SelectChangeEvent} from '@mui/material/Select';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
){
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const patternValidate = (regStr: any, str: string, register: any, nameField: string)=>{
    return {
        ...register(nameField, {
            required: "поле обязательно для заполнения",
            pattern: {
                value: regStr,
                message: str
            }
        })
    }
}

const MoneyTransfer: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const oneAccount: TGetPostAccountAT = useAppSelector(state=>state.reducerAccount.oneAccount);

    useEffect(()=>{
        dispatch(getAccountAT(dataUser?._id!));
    }, [])

    const [openAlert, setOpenAlert] = React.useState(false);

    const handleClick = ()=>{
        setOpenAlert(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string)=>{
        if(reason === 'clickaway'){
            return;
        }
        setOpenAlert(false);
    };

    const [state, setState] = React.useState<TDepositRequest>({
        senderFullName: (()=>{
            if(dataUser)
                return oneAccount?.firstName! + "  " + oneAccount?.lastName! + "  " + (oneAccount?.patronym! ? oneAccount?.patronym! : " ")
            else
                return ""
        })(),
        senderIIN: (()=>{
            if(dataUser)
                return oneAccount?.identificationNumber!
            else
                return ""
        })(),
        senderIIK: "",
        senderBank: "",
        senderBIK: "",
        senderKBe: "",
        amount: "",
    });

    const handleSelect = (e: SelectChangeEvent)=>{
        let {name, value} = e.target;
        let innerStrSender = "";
        let innerStrRec = "";

        //ATYNKZKA
        if(name === "senderBank"){
            innerStrSender = returnNameOfBank(value, "BIC");
            setState(prevState=>{
                return {...prevState, senderBIK: innerStrSender};
            });
        }
        if(name === "recipientBank"){
            innerStrRec = returnNameOfBank(value, "BIC");
            setState(prevState=>{
                return {...prevState, recipientBIK: innerStrRec};
            });
        }

        setState(prevState=>{
            return {...prevState, [name]: value};
        });
    };
    const inputChangeHandler = (e: React.ChangeEvent<HTMLInputElement>)=>{
        let {name, value} = e.target;
        switch(name){

            case "amount":
                let innerTrim = value.replace(/[^0-9]/g, '');
                setState(prevState=>{
                    return {...prevState, [name]: innerTrim.replace(/\B(?=(\d{3})+(?!\d))/g, ' ')};
                });
                break;
            default:
                setState(prevState=>{
                    return {...prevState, [name]: value};
                });
        }
    };

    const {
        register,
        formState: {errors, isValid},
        handleSubmit,
        watch,
        reset
    } = useForm<any>({mode: "onBlur"});
    //mode: onChange | onBlur | onSubmit | onTouched | all = 'onSubmit'
    // console.log(watch("senderFullName"));

    const ObjSenderFullName = {
        ...register("senderFullName", {
            required: "поле обязательно для заполнения",
        })
    }

    const ObjSenderIIN = patternValidate(/^[0-9]{12}$/, "ИИН должен быть числом и состоять из 12 символов", register, "senderIIN");
    const ObjSenderIIK = patternValidate(/^.{20}$/, "ИИК должен состоять из 20 символов", register, "senderIIK");
    const ObjSenderBank = {
        ...register("senderBank"
            //     , {
            //     required: "поле обязательно для заполнения",
            // }
        )
    }
    const ObjSenderBIK = {
        ...register("senderBIK"
            //     , {
            //     required: "поле обязательно для заполнения",
            // }
        )
    }

    const ObjSenderKBe =patternValidate(/^\d{2}$/, "КБе - число состоящее из двух символов", register, "senderKBe");
    const ObjAmount = patternValidate(/^[1-9].*$/, "сумма не может быть отрицательным числом или начинаться с нуля", register, "amount");

//****************************************************************************************
    const submitFormHandler = async(dataState: any)=>{
        const innerObj: {
            senderFullName: string,
            senderIIN: string,
            senderIIK: string,
            senderBIK: string,
            senderBank: string,
            senderKBe: number | string,
            amount: number | string
        } = {...state};

        const newString: any = dataState.amount;
        innerObj.amount = Number((newString as string)
            .replace(/[^0-9]/g, ''));

        // alert(JSON.stringify(innerObj, null, 2))

        // console.log(innerObj.amount)
        await dispatch(createDepositRequestAT(innerObj));
        handleClick();
        setState({
            senderFullName: "",
            senderIIN: "",
            senderIIK: "",
            senderBIK: "",
            senderBank: "",
            senderKBe: "",
            amount: ""
        })
        navigate("/client/client-office");
    }
//###########################################################################################
    return <>
        <div style={{margin: "30px"}}>

            {/*<pre>{JSON.stringify(state, null, 2)}</pre>*/}
            <MoneyTransferForm
                state={state}
                onChange={inputChangeHandler}
                errors={errors}

                ObjSenderFullName={ObjSenderFullName}
                ObjSenderIIN={ObjSenderIIN}
                ObjSenderIIK={ObjSenderIIK}
                ObjSenderBank={ObjSenderBank}
                ObjSenderBIK={ObjSenderBIK}
                ObjSenderKBe={ObjSenderKBe}
                ObjAmount={ObjAmount}

                handleSelect={handleSelect}
                handleSubmit={handleSubmit}
                onSubmit={submitFormHandler}
            />
        </div>

        <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success" sx={{width: '100%'}}>
                Вы отправили заявку на пополнение счета. Ждите одобрение заявки.
            </Alert>
        </Snackbar>
    </>

}

export default MoneyTransfer;