
import {useAppSelector} from "../../hook";
import { Card, CardContent, Grid, Typography} from "@mui/material";
import {TGetAccRequestsAT} from "../../types/typesAccReq";


const ClientAccRequest = ()=>{

    const allAccRequests: TGetAccRequestsAT[] = useAppSelector(state=>state.reducerAccRequests.allAccRequests);

    if(Object.entries(allAccRequests).length)
    return (
        <Card sx={{minWidth: 600, marginBottom: "20px", marginLeft: "70px", marginTop: "20px",  height: "500px",}}>
            <CardContent>
                <Typography variant="h5" component="div">
                    # {allAccRequests[0]?._id}
                </Typography>
                <Typography variant="body2" style={{marginBottom: "40px"}}>
                    Статус: {allAccRequests[0]?.status}
                </Typography>
                <Grid container spacing={2} style={{margin: "5px"}}>
                    <Typography variant="h6" gutterBottom>
                        ФИО:
                    </Typography>
                    <Typography variant="body1" style={{marginTop: "5px", marginLeft: "5px"}}>
                        {`${allAccRequests[0]?.lastName} ${allAccRequests[0]?.firstName} ${allAccRequests[0]?.patronym ? allAccRequests[0]?.patronym : ""}`}
                    </Typography>
                </Grid>
                <Grid container spacing={2} style={{margin: "5px"}}>
                    <Typography variant="h6" gutterBottom>
                        Дата рождения:
                    </Typography>
                    <Typography variant="body1" style={{marginTop: "5px", marginLeft: "5px"}}>
                        {`${allAccRequests[0]?.dateOfBirth} `}
                    </Typography>
                </Grid>
                <Grid container spacing={2} style={{margin: "5px"}}>
                    <Typography variant="h6" gutterBottom>
                        ИИН:
                    </Typography>
                    <Typography variant="body1" style={{marginTop: "5px", marginLeft: "5px"}}>
                        {`${allAccRequests[0]?.identificationNumber} `}
                    </Typography>
                </Grid>
                <Grid container spacing={2} style={{margin: "5px"}}>
                    <Typography variant="h6" gutterBottom>
                        Email:
                    </Typography>
                    <Typography variant="body1" style={{marginTop: "5px", marginLeft: "5px"}}>
                        {`${allAccRequests[0]?.email} `}
                    </Typography>
                </Grid>
                <Grid container spacing={2} style={{margin: "5px"}}>
                    <Typography variant="h6" gutterBottom>
                        Телефон:
                    </Typography>
                    <Typography variant="body1" style={{marginTop: "5px", marginLeft: "5px"}}>
                        {`${allAccRequests[0]?.phone} `}
                    </Typography>
                </Grid>
                <Grid container spacing={2} style={{margin: "5px"}}>
                    <Typography variant="h6" gutterBottom>
                        Адресс:
                    </Typography>
                    <Typography variant="body1" style={{marginTop: "5px", marginLeft: "5px"}}>
                        {`${allAccRequests[0]?.country} ${allAccRequests[0]?.area} ${allAccRequests[0]?.city} ${allAccRequests[0]?.address}`}
                    </Typography>
                </Grid>
                <Grid container spacing={2} style={{margin: "5px"}}>
                    <Typography variant="h6" gutterBottom>
                        Номер пасспорта:
                    </Typography>
                    <Typography variant="body1" style={{marginTop: "5px", marginLeft: "5px"}}>
                        {`${allAccRequests[0]?.idCardNumber} `}
                    </Typography>
                </Grid>
            </CardContent>
        </Card>
    )
    else
    return (
        <Card sx={{minWidth: 600, marginBottom: "20px", marginLeft: "20px", marginTop: "20px",}}>

                <Typography variant="h5" component="div">
                    нет заявления
                </Typography>
        </Card>        
    )
}

export default ClientAccRequest;