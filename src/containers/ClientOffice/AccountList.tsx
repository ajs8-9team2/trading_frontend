import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../hook";
import { getAccountAT } from "../../store/services/accountSlice";
import { TUser } from "../../types/reestrOfAlias";
import { TGetPostAccountAT, } from "../../types/typesAccount";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

const AccountList: React.FC = () => {
    const dispatch = useAppDispatch();
    const oneAccount: TGetPostAccountAT = useAppSelector(state => state.reducerAccount.oneAccount);
    const dataUser: TUser = useAppSelector(state => state.reducerUsers.user);
    useEffect(() => {
        dispatch(getAccountAT(dataUser?._id!));
    }, []);


    return (
        <div >
            <Card sx={{ width: '500px', flexShrink: 0 }}>
                <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                        № счёта:
                    </Typography>
                    <Typography variant="h5" component="div">
                        {oneAccount?.accountNumber}
                    </Typography>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                        Остаток на балансе:
                    </Typography>
                    <Typography variant="body2">
                        <b style={{ color: 'black' }}>{oneAccount?.amount} тг.</b>
                        <br />
                    </Typography>
                    <Typography sx={{ width: "33%", fontWeight: "bold", marginTop: "10px" }}> {oneAccount?.firstName} {oneAccount?.lastName}</Typography>

                </CardContent>
            </Card>
        </div>
    );
}

export default AccountList;
