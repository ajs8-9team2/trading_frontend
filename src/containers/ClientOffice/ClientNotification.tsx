// import {useNavigate} from 'react-router-dom';
// import {Context} from "../../context";
// import {addDishes} from "../../store/services/accountSlice"
import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../hook";
import { T_id } from "../../types/reestrOfAlias";
import { TGetPostNotificationAT, TNoticeObj, TPatchNotificationAT } from "../../types/typesNotification";
import { patchNotificationAT, setFlagRing, setRingState } from "../../store/services/notificationSlice";
import DoneIcon from '@mui/icons-material/Done';
import PriorityHighIcon from '@mui/icons-material/PriorityHigh';
import { Box, Card, CardContent, Container, Divider, Grid, Typography } from "@mui/material";
import { getTime } from "../../Helpers/getTime";
import { getDate } from "../../Helpers/getDate";
import { getTypeOfRequest } from "../../Helpers/getTypeOfRequest";

const ClientNotification = () => {
    // console.clear()
    // const {setCount} = React.useContext(Context)
    const dispatch = useAppDispatch();
    const oneNotification: TGetPostNotificationAT = useAppSelector(state => state.reducerNotification.oneNotification);
    const arrayRef = React.useRef<TNoticeObj[]>([]);
    const getIDNotificationRef = React.useRef<T_id>();

    getIDNotificationRef.current = oneNotification?._id;
    arrayRef.current = (oneNotification?.userNotifications)
        ? [...oneNotification?.userNotifications]
        : [];

    useEffect(() => {
        dispatch(setFlagRing(true))
        dispatch(setRingState(1))
    }, []);

    useEffect(() => {
        return () => {
            dispatch(setFlagRing(false));
            dispatch(setRingState(2))
            if (arrayRef.current) {
                const isNotRead = arrayRef.current?.some(item => !item.isWasRead);
                /** есть ли непрочитанные уведомления, если да, то выполняем */
                if (isNotRead) {
                    const innerObj: TPatchNotificationAT = {
                        _idNotification: getIDNotificationRef.current,
                        userNotifications: (() => {
                            return [...arrayRef.current].map(item => {
                                if (!item.isWasRead)
                                    return { ...item, isWasRead: true }
                                else
                                    return item;
                            })
                        })()
                    }
                    // console.dir(innerObj," +++")
                    dispatch(patchNotificationAT(innerObj));
                }
            }
        }
    }, [])

    // {
    // _id?: T_id,
    // type?: "NEW_MESSAGE" | "MESSAGE_CREATED",
    // date?: string,
    // userID?: string,
    // requestID: string,
    // notice: string,
    // isWasRead: boolean,
    // status: "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed",
    // typeOfRequest: "accRequest" | "depositRequest" | "transferRequest"
    // }

    return (
        <Box component="main"
            sx={{
                flexGrow: 1, py: 2
            }}>
            <Container
                maxWidth="xl"
            >
                {/*<Grid container*/}

                {/*      sx={{*/}
                {/*          backgroundColor:"red"*/}
                {/*      }}*/}
                {/*>*/}
                {/*<Button*/}
                {/*    onClick={handleRequest}*/}
                {/*    variant="contained"*/}
                {/*>*/}
                {/*    {"ОЗНАКОМЛЕН"}*/}
                {/*</Button>*/}
                <Grid item lg={12} md={12} xs={12}
                    sx={{
                        // backgroundColor:"green"
                    }}
                >
                    {oneNotification?.userNotifications &&
                        [...oneNotification?.userNotifications]
                            .reverse()
                            .map((item, index) => (

                                <Card
                                    key={index}
                                    sx={{
                                        // backgroundColor:"green",
                                        mb: 1
                                    }}
                                // sx={{marginY:1}}
                                // sx={{mb: 1}}
                                >
                                    <CardContent>

                                        <Grid
                                            container
                                            direction="row"
                                            justifyContent="space-between"
                                            alignItems="center"
                                        >

                                            <Grid>
                                                <Grid
                                                    container
                                                    direction="row"
                                                    justifyContent="flex-start"
                                                    alignItems="center"
                                                >

                                                    {item.isWasRead ?
                                                        <>
                                                            <DoneIcon />
                                                            <Typography
                                                                sx={{
                                                                    fontSize: 14,
                                                                    marginLeft: 1
                                                                }} color="text.secondary" gutterBottom
                                                            >
                                                                {
                                                                    (() => {
                                                                        if (item.isWasRead)
                                                                            return "прочитано"
                                                                        else
                                                                            return "новое уведомление"
                                                                    })()
                                                                }
                                                            </Typography>
                                                        </>
                                                        :
                                                        <>
                                                            <PriorityHighIcon />
                                                            <Typography
                                                                sx={{
                                                                    fontSize: 14,
                                                                    marginLeft: 1
                                                                }} color="text.secondary" gutterBottom
                                                            >
                                                                {
                                                                    (() => {
                                                                        if (item.isWasRead)
                                                                            return "прочитано"
                                                                        else
                                                                            return "новое уведомление"
                                                                    })()
                                                                }
                                                            </Typography>
                                                        </>
                                                    }
                                                </Grid>
                                            </Grid>

                                            <Grid>
                                                <Grid
                                                    container
                                                    direction="column"
                                                    justifyContent="center"
                                                    alignItems="flex-end"
                                                >
                                                    <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                                                        {getTypeOfRequest(item?.typeOfRequest)}
                                                    </Typography>
                                                    <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                                                        {item?.requestID?.toUpperCase()}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>

                                        <Divider textAlign="center">
                                            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                                {getTime(item?.date!) + " — " + getDate(item?.date!)}
                                            </Typography>
                                        </Divider>
                                        <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                                            {item?.notice}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            ))}
                </Grid>
                {/*</Grid>*/}
            </Container>
            {/*<pre>{JSON.stringify(oneNotification?.userNotifications, null, 2)}</pre>*/}
            {/*<pre>{JSON.stringify(arrayRef.current.reverse(), null, 2)}</pre>*/}
        </Box>
    );
}

export default ClientNotification;