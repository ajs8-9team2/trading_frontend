import React, {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../../hook";
import {Button} from "@mui/material";
import { NavLink } from 'react-router-dom';
import {Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { TGetPostClientStockAT } from '../../types/typesClientStock';
import {getClientStocksAT} from "../../store/services/clientStockSlice";
import { TCompany, TTicker, TStockAmount, T_id, TUser } from "../../types/reestrOfAlias";
import "./ClientStocks.css";

const ClientStocks: React.FC = ()=>{

    const dispatch = useAppDispatch();
    const allClientStocks: TGetPostClientStockAT[] = useAppSelector(state=>state.reducerClientStock.allClientStocks);
    const dataUser:TUser = useAppSelector(state=>state.reducerUsers.user);

    useEffect(() => {
        dispatch(getClientStocksAT(dataUser?._id!));   
    }, [dispatch]);

    interface TGetPostClientStockAT {
        _id?: T_id;
        amount: TStockAmount; 
        company: TCompany;
        ticker: TTicker,                  
    };

    const columns: ColumnsType<TGetPostClientStockAT> = [
        {
            title: "Название",
            dataIndex: "company",
            key: "company"
        },
        {
            title: "Тикер",
            dataIndex: "ticker",
            key: "ticker"
        },
        {
            title: "Количество",
            dataIndex: "amount",
            key: "amount"
        }
    ];

    return (<>
        <div className="ClientStocks" >
            <div  className="client_stocks_title">
                <h1>Мои акции</h1>
                <Button 
                    component={NavLink}
                    variant="contained"
                    to="/client/stock-transactions"
                        style={{marginRight:"20px",backgroundColor: "black", height: "40px"}} 
                    >
                    {"Покупка / Продажа акций"}
                </Button>
            </div>
            <Table columns={columns} dataSource={allClientStocks} rowKey="_id" className="client_stocks" bordered />
        </div>
        </>
    )
};

export default ClientStocks;