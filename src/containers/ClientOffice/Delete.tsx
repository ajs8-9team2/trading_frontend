import {Box, Button, ButtonGroup, Grid, Stack, TextField, Typography} from "@mui/material";
import React from "react";
import {styled} from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import {patchAccRequestAT} from "../../store/services/accRequestSlice";
import {useAppDispatch, useAppSelector} from "../../hook";
import {TPatchAccRequestsAT} from "../../types/typesAccReq";
import {createAccountAT, fetchAccountAT, getAccountAT} from "../../store/services/accountSlice";
import {TUser} from "../../types/reestrOfAlias";
import {TAccount, TGetPostAccountAT} from "../../types/typesAccount";

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const DeleteRightSideForm = ()=>{
    // потом закоменнтировать
    // потом закоменнтировать
    // потом закоменнтировать
    // потом закоменнтировать
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const oneAccount: TGetPostAccountAT = useAppSelector(state=>state.reducerAccount.oneAccount);
    // const allAccount:any/*TfetchGetAccountAT*/ = useAppSelector(state=>state.reducerAccount);
//******************************************
    const dispatch = useAppDispatch();
    const [rejectionReason, setRejectionReason] = React.useState<string>("");

    const inputChangeHandler: React.ChangeEventHandler<HTMLInputElement> = (e)=>{
        const value: string = e.target.value;
        setRejectionReason(value);
    };

    const submitFormHandler: React.FormEventHandler<HTMLFormElement> | undefined
        = async(e)=>{
        e.preventDefault();

        // await dispatch(fetchAccountAT())
        if(dataUser?._id)
        await dispatch(getAccountAT(dataUser?._id!))
    };

    return (
        <>
            <Box sx={{
                marginX: "auto",
                maxWidth: 650,
                padding: 5,
                // paddingTop: 30,
                // paddingBottom: 30,
                boxShadow: "0 18px 30px 0 rgba(0, 0, 0, 0.6)",
                // width: '100%',
                // display: 'flex',
                // alignItems: "center",
                // justifyContent: "center",
            }}>

                <form onSubmit={submitFormHandler}>
                    <Stack spacing={2}>
                        <Item>
                            <ButtonGroup
                                variant="contained"
                                fullWidth
                                aria-label="outlined primary button group"
                            >
                                <Button
                                    type="submit"
                                    // disabled={!!rejectionReason}
                                >
                                    принять
                                </Button>
                            </ButtonGroup>
                        </Item>
                    </Stack>
                </form>
            </Box>
            {/*<pre>{JSON.stringify(allAccount, null, 2)}</pre>*/}
            <pre>{JSON.stringify(oneAccount, null, 2)}</pre>
            {/*<pre>{JSON.stringify(dataUser, null, 2)}</pre>*/}

        </>
    )
}

export default DeleteRightSideForm;