import {
    Button,
    FormControlLabel,
    Grid,
    Radio,
    RadioGroup,
    TextField,
    ThemeProvider,
    Typography
} from "@mui/material";
import { InputLabel, MenuItem, FormControl } from '@mui/material';
import React from "react";
import { createTheme} from '@material-ui/core/styles';
import {TClientAccount} from "../../types/typesClentAccount";
import {useAppDispatch, useAppSelector} from "../../hook";
import {useNavigate} from "react-router-dom";
import {createClientAccountAT} from "../../store/services/clientAccountSlice";
import {TAccount, TGetPostAccountAT} from "../../types/typesAccount";
import Select from '@mui/material/Select';
import { banks, returnNameOfBank } from '../../Helpers/returnNameOfBank';

const AddIban = () => {

    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const account: TGetPostAccountAT = useAppSelector(state=>state.reducerAccount.oneAccount);

    let patronym : string = "";

    if(account?.patronym){
        patronym = account.patronym;
    }

    const userName = `${account?.lastName} ${account?.firstName} ${patronym}`

    const [state, setState] = React.useState<TClientAccount>({
        recipientName: userName,
        BIK: "",
        IIK: "",
        bank: "",
        isMyIIK : true,
    });

    const theme = createTheme({
        palette: {
            primary: { main: "#263238" },
            secondary: { main: '#11cb5f' },
        },
    });

    const inputChangeHandler = (e: any)=>{
        let {name, value} = e.target;
        let innerStr = "";
        //ATYNKZKA
        if(name === "bank" ){
            innerStr = returnNameOfBank(value,"BIC");
            setState(prevState=>{
                return {...prevState, BIK: innerStr};
            });
        }

        setState(prevState=>{
            return {...prevState, [name]: value};
        });
    };



    const handleSubmit = async (e: any) => {
        e.preventDefault();
        console.log(state)
        await dispatch(createClientAccountAT(state));
        navigate("/client/client-iban");
    }

    return <div style={{margin: "40px"}}>
        <ThemeProvider theme={theme}>
        <Typography variant="h5" component="div" style={{marginBottom: "20px"}}>
            Введите данные нового счета:
        </Typography>
        <form onSubmit={handleSubmit}>

            <FormControl style={{marginBottom: "20px"}}>
                <RadioGroup
                    aria-labelledby="demo-controlled-radio-buttons-group"
                    name="isMyIIK"
                    value={state.isMyIIK}
                    onChange={inputChangeHandler}
                >
                    <FormControlLabel onClick={() => setState({...state, recipientName: userName})} value={true} control={<Radio />} label="Мой новый счет" />
                    <FormControlLabel onClick={() => setState({...state, recipientName: ""})} value={false} control={<Radio />} label="Счет третьего лица" />
                </RadioGroup>
            </FormControl>

            <Grid container spacing={2}>

                    <Grid item xs={12}>
                        <TextField
                            id={"recipientName"}
                            name={"recipientName"}
                            label={"ФИО"}
                            variant="standard"
                            type={"text"}
                            value={state.recipientName}
                            required={true}
                            onChange={inputChangeHandler}
                            fullWidth
                            sx={{marginLeft: "12px"}}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <TextField
                            id={"iik"}
                            name={"IIK"}
                            label={"ИИК"}
                            variant={"standard"}
                            type={"text"}
                            value={state.IIK}
                            required={true}
                            onChange={inputChangeHandler}
                            fullWidth
                            sx={{marginLeft: "12px"}}
                        />
                    </Grid>

                <Grid item xs={12} sm={8}>
                    <FormControl sx={{ minWidth: 400, margin: "10px 0 0 0",          border: "1px solid black", borderRadius: 1, padding: "10px"}}>
                        <InputLabel id="bank" sx={{ width: "150px",bgcolor: "#F9FAFC", margin: 0}}>Выберите банк</InputLabel>
                        <Select
                            label= "Банк"
                            id="bank"
                            name="bank"
                            value={state.bank}
                            variant={"standard"}
                            type={"text"}
                            required={true}
                            onChange={inputChangeHandler}
                            fullWidth
                        >
                            {banks.map(bank => {
                                return <MenuItem
                                    value={bank}
                                    key={bank}

                                >{bank}</MenuItem>
                            })}
                        </Select>
                    </FormControl>
                </Grid>

                    <Grid item xs={12}>
                        <TextField
                            id={"bik"}
                            name={"BIK"}
                            label={"БИК/SWIFT"}
                            variant="standard"
                            type={"text"}
                            value={state.BIK}
                            required={true}
                            onChange={inputChangeHandler}
                            fullWidth
                            sx={{marginLeft: "12px"}}
                        />
                    </Grid>

            </Grid>

            <Grid item>
                <Button
                    sx={{mt: "30px"}}
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                >
                    Сохранить счет
                </Button>
            </Grid>
        </form>
        </ThemeProvider>
    </div>
}

export default AddIban;