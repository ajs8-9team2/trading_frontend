import {Box, Card, CardContent, Button, Typography, CardHeader, Grid, ThemeProvider} from '@mui/material';
import {useAppDispatch, useAppSelector} from "../../hook";
import {TUserState} from "../../types/typesUser";
import {useEffect} from "react";
import {useNavigate} from "react-router-dom";
import {getClientAccountAT} from "../../store/services/clientAccountSlice";
import {TClientAccount, TGetPostClientAccountAT} from "../../types/typesClentAccount";
import {createTheme} from "@material-ui/core/styles";

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

export default function ClientIbans(){
    const dispatch = useAppDispatch();
    const dataUser: TUserState = useAppSelector(state=>state.reducerUsers);
    const navigate = useNavigate();
    const allClientAccounts: TGetPostClientAccountAT[] = useAppSelector(state=>state.reducerClientAccount.allClientAccount);

    useEffect(()=>{
        if(dataUser.user?._id){
            dispatch(getClientAccountAT(dataUser.user._id))}
    }, []);

    return <Box>
        <CardHeader title="Сохраненные Банковские Счета"/>
        <Card sx={{minWidth: 600, marginBottom: "20px", marginLeft: "40px"}}>
            <CardContent>
                <Grid style={{marginBottom: "20px"}}>
                    <Typography variant="h5" component="div">
                        Мои Банковские Счета:
                    </Typography>
                </Grid>
                <Grid>

                    {allClientAccounts.map((item, index)=>{
                        if(item?.isMyIIK === true){
                            return (
                                <Card
                                    key={index}
                                    sx={{minWidth: 600, marginBottom: "10px"}}
                                >
                                    <CardContent>
                                        <Typography variant="h6" component="div">
                                            # {item?.IIK}
                                        </Typography>
                                        <Typography variant="body2">
                                            Банк: {item?.bank} БИК: {item?.BIK}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            )
                        }
                    })}

                </Grid>
            </CardContent>
        </Card>

        <Card sx={{minWidth: 600, marginBottom: "20px", marginLeft: "40px"}}>
            <CardContent>
                <Grid style={{marginBottom: "20px"}}>
                    <Typography variant="h5" component="div">
                        Счета Третьих лиц:
                    </Typography>
                </Grid>
                <Grid>

                    {allClientAccounts.map((item, index)=>{
                        if(item?.isMyIIK === false){
                            return (
                                <Card
                                    key={index}
                                    sx={{minWidth: 600, marginBottom: "10px"}}

                                >
                                    <CardContent>
                                        <Typography variant="h6" component="div">
                                            {item?.recipientName}
                                        </Typography>
                                        <Typography variant="h6" component="div">
                                            # {item?.IIK}
                                        </Typography>
                                        <Typography variant="body2">
                                            Банк: {item?.bank} БИК: {item?.BIK}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            )
                        }
                    })}

                </Grid>
            </CardContent>

        </Card>

        <ThemeProvider theme={theme}>
            <Button size="small" onClick={() => navigate("/client/add-iban")} variant="contained" style={{float: "right"}}>
                Добавить новый Счет
            </Button>
        </ThemeProvider>

    </Box>
}
