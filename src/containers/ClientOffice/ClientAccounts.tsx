import AccountList from "./AccountList"

const ClientAccounts = () => {
    return <div style={{margin: "20px"}}>
        <h1>Мои счета:</h1>
            <AccountList/>
        </div>
}

export default ClientAccounts