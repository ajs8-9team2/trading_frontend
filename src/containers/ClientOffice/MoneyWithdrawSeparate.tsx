import * as React from 'react';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hook';
import { TUser } from "../../types/reestrOfAlias";
import { TTransferReq } from "../../types/typesTransferRequest";
import { returnNameOfBank } from "../../Helpers/returnNameOfBank";
import { createTransferReqAT } from "../../store/services/transferRequestSlice";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { validationWithdraw } from '../../config';
import { Grid, TextField, Button } from '@mui/material';
import { NavLink } from "react-router-dom";
import { TGetPostAccountAT } from "../../types/typesAccount";
import { getAccountAT } from "../../store/services/accountSlice";
import { getClientAccountAT } from "../../store/services/clientAccountSlice";
import { MenuItem } from "@material-ui/core";
import { TGetPostClientAccountAT } from "../../types/typesClentAccount";
import { createTheme } from "@material-ui/core/styles";
import MoneyWithdrawSenderForm from '../../components/Forms/MoneyWithdrawSenderForm/MoneyWithdrawSenderForm';
import MoneyWithdrawRecipientForm from '../../components/Forms/MoneyWithdrawRecipientForm/MoneyWithdrawRecipientForm';
import { SelectChangeEvent } from '@mui/material/Select';
import {
    FormControl,
    FormControlLabel,
    InputLabel,
    Radio,
    RadioGroup,
    Select,
    ThemeProvider,
    Typography,
    Snackbar
} from "@mui/material";
import MuiAlert, { AlertProps } from '@mui/material/Alert';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});


const MoneyWithdrawSeparate: React.FC = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const dataUser: TUser = useAppSelector(state => state.reducerUsers.user);
    const oneAccount: TGetPostAccountAT = useAppSelector(state => state.reducerAccount.oneAccount);
    const allClientAccounts: TGetPostClientAccountAT[] = useAppSelector(state => state.reducerClientAccount.allClientAccount);

    useEffect(() => {
        dispatch(getAccountAT(dataUser?._id!));
        dispatch(getClientAccountAT(dataUser?._id!));

    }, [dispatch]);

    const [myIBAN, setMyIBAN] = React.useState<boolean>(true);

    const [dropDownOptions, setDropDownOptions] = React.useState<TGetPostClientAccountAT[]>(allClientAccounts.filter((i) => i?.isMyIIK === true));

    const [state, setState] = React.useState<TTransferReq>({
        senderFullName: (() => {
            if (dataUser)
                return oneAccount?.firstName! + "  " + oneAccount?.lastName! + "  " + (oneAccount?.patronym! ? oneAccount?.patronym! : " ")
            else
                return ""
        })(),
        recipientFullName: "",
        senderIIN: (() => {
            if (dataUser)
                return oneAccount?.identificationNumber!
            else
                return ""
        })(),
        recipientIIN: "",
        senderIIK: "",
        recipientIIK: "",
        senderBIK: "",
        recipientBank: "",
        senderBank: "",
        recipientBIK: "",
        senderKBe: "",
        recipientKBe: "",
        amount: "",

    });

    const [openAlert, setOpenAlert] = React.useState(false);

    const handleClick = () => {
        setOpenAlert(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenAlert(false);
    };


    const handleSelect = (e: SelectChangeEvent) => {
        let { name, value } = e.target;
        let innerStrSender = "";
        let innerStrRec = "";

        //ATYNKZKA
        if (name === "senderBank") {
            innerStrSender = returnNameOfBank(value, "BIC");
            setState(prevState => {
                return { ...prevState, senderBIK: innerStrSender };
            });
        }
        if (name === "recipientBank") {
            innerStrRec = returnNameOfBank(value, "BIC");
            setState(prevState => {
                return { ...prevState, recipientBIK: innerStrRec };
            });
        }

        setState(prevState => {
            return { ...prevState, [name]: value };
        });
    };

    const inputChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        let { name, value } = e.target;

        switch (name) {
            case "amount":
                let innerTrim = value.replace(/[^0-9]/g, '');
                setState(prevState => {
                    return { ...prevState, [name]: innerTrim.replace(/\B(?=(\d{3})+(?!\d))/g, ' ') };
                });
                break;
            default:
                setState(prevState => {
                    return { ...prevState, [name]: value };
                });
        }
    };

    const submitFormHandler = async () => {
        const innerObj: {
            senderFullName: string,
            recipientFullName: string | undefined,
            senderIIN: string,
            recipientIIN: string,
            senderIIK: string,
            recipientIIK: string | undefined,
            senderBIK: string,
            senderBank: string,
            recipientBank: string | undefined,
            recipientBIK: string | undefined,
            senderKBe: number | string,
            recipientKBe: number | string,
            amount: number | string,
        } = { ...state };
        const newString: any = state.amount;
        innerObj.amount = Number((newString as string).replace(/[^0-9]/g, ''));
        console.log(innerObj.amount)
        await dispatch(createTransferReqAT(innerObj));
        handleClick();
        setState({
            senderFullName: "",
            recipientFullName: "",
            senderIIN: "",
            recipientIIN: "",
            senderIIK: "",
            recipientIIK: "",
            senderBIK: "",
            senderBank: "",
            recipientBank: "",
            recipientBIK: "",
            senderKBe: "",
            recipientKBe: "",
            amount: "",
        })
        // navigate("/client/client-office");
    }

    const { register, formState: { errors }, handleSubmit }
        = useForm<TTransferReq>({
            resolver: yupResolver(validationWithdraw)
        });


    const ObjSenderFullName = {
        ...register("senderFullName")
    }
    const ObjRecipientFullName = {
        ...register("recipientFullName")
    }

    const ObjSenderIIN = {
        ...register("senderIIN")
    }
    const ObjRecipientIIN = {
        ...register("recipientIIN")
    }

    const ObjSenderIIK = {
        ...register("senderIIK")
    }
    const ObjRecipientIIK = {
        ...register("recipientIIK")
    }


    const ObjSenderBank = {
        ...register("senderBank")
    }
    const ObjRecipientBank = {
        ...register("recipientBank")
    }

    const ObjSenderBIK = {
        ...register("senderBIK")
    }
    const ObjRecipientBIK = {
        ...register("recipientBIK")
    }

    const ObjSenderKBe = {
        ...register("senderKBe")
    }
    const ObjRecipientKBe = {
        ...register("recipientKBe")
    }

    const ObjAmount = {
        ...register("amount")
    }

    return <>
        <div style={{ margin: "30px" }}>
            <ThemeProvider theme={theme}>
                <Typography variant="h5" component="div" style={{ marginBottom: "20px" }}>
                    Вывод денег:
                </Typography>
                <form onSubmit={submitFormHandler}>

                    <FormControl style={{ marginBottom: "20px" }}>
                        <RadioGroup
                            aria-labelledby="demo-controlled-radio-buttons-group"
                            name="myIBAN"
                            value={myIBAN}
                            onChange={(e) => {
                                setMyIBAN(e.target.value === "true")
                            }}
                        >
                            <FormControlLabel value={true} onClick={() => setDropDownOptions(allClientAccounts.filter((i) => i?.isMyIIK === true))} control={<Radio />} label="На мой личный счет" />
                            <FormControlLabel value={false} onClick={() => setDropDownOptions(allClientAccounts.filter((i) => i?.isMyIIK === false))} control={<Radio />} label="На счет третьего лица" />
                        </RadioGroup>
                    </FormControl>

                    <Typography variant="h6" component="div" style={{ marginBottom: "20px" }}>
                        Сохраненные счета:
                    </Typography>

                    <FormControl fullWidth style={{ marginBottom: "20px" }}>
                        <InputLabel id="demo-simple-select-label">Номер счета</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            variant="outlined"
                            label="Номер счета"
                            defaultValue=""
                        >
                            {dropDownOptions.map((item) => {
                                return <MenuItem value={item?._id} key={item?._id}
                                    onClick={() => {
                                        setState(prevState => {
                                            return {
                                                ...prevState,
                                                recipientFullName: item?.recipientName,
                                                recipientIIK: item?.IIK,
                                                recipientBank: item?.bank,
                                                recipientBIK: item?.BIK
                                            };
                                        });
                                    }
                                    }>{item?.IIK}</MenuItem>
                            })}

                        </Select>
                    </FormControl>

                    <div style={{ display: "flex" }}>
                        <MoneyWithdrawSenderForm
                            state={state}
                            onChange={inputChangeHandler}
                            errors={errors}
                            handleSelect={handleSelect}
                            ObjSenderFullName={ObjSenderFullName}
                            ObjSenderIIN={ObjSenderIIN}
                            ObjSenderIIK={ObjSenderIIK}
                            ObjSenderBank={ObjSenderBank}
                            ObjSenderBIK={ObjSenderBIK}
                            ObjSenderKBe={ObjSenderKBe}
                        />
                        <MoneyWithdrawRecipientForm
                            state={state}
                            onChange={inputChangeHandler}
                            errors={errors}
                            handleSelect={handleSelect}
                            ObjRecipientFullName={ObjRecipientFullName}
                            ObjRecipientIIN={ObjRecipientIIN}
                            ObjRecipientIIK={ObjRecipientIIK}
                            ObjRecipientBank={ObjRecipientBank}
                            ObjRecipientBIK={ObjRecipientBIK}
                            ObjRecipientKBe={ObjRecipientKBe}
                        />
                    </div>
                </form>
            </ThemeProvider>
            <Grid item xs={2} sm={2} style={{ width: "180px", margin: "20px 10px" }}>
                <TextField
                    {...ObjAmount}
                    required
                    id="amount"
                    name="amount"
                    label="Сумма"
                    value={(() => {
                        return state.amount;
                    })()}
                    onChange={inputChangeHandler}
                    fullWidth
                    autoComplete="amount"
                    variant="filled"
                    error={!!errors?.recipientFullName?.message}
                    helperText={errors?.recipientFullName?.message}

                />
            </Grid>

            <div style={{ marginLeft: "10px" }}>
                <Button
                    component={NavLink}
                    variant="contained"
                    color="inherit"
                    to="/client/client-transactions">
                    {"Назад"}
                </Button>

                <Button
                    onClick={handleSubmit(submitFormHandler)}
                    style={{ margin: "30px", backgroundColor: "black", width: "170px" }}
                    variant="contained">
                    Отправить
                </Button>
            </div>
        </div>

        <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                Вы отправили заявку на вывод средств. Ждите одобрение заявки.
            </Alert>
        </Snackbar>
    </>
}

export default MoneyWithdrawSeparate;

