import React from "react";
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from "../../hook";
import { TGetPostAccountAT } from "../../types/typesAccount";
import { getAccountAT } from "../../store/services/accountSlice";
import { Image } from 'antd';
import {
    Avatar,
    Box,
    Button,
    Card,
    CardContent,
    CardHeader,
    Container,
    Grid,
    TextField,
    Typography
} from "@mui/material";
import { uploadsUrl } from "../../axiosApi";
import { TUser } from "../../types/reestrOfAlias";

const AccountProfile = () => {
    let documentFrontSrc;
    let documentBackSrc;
    const dispatch = useAppDispatch();
    // const navigate = useNavigate();
    const dataUser: TUser = useAppSelector(state => state.reducerUsers.user);
    const oneAccount: TGetPostAccountAT = useAppSelector(state => state.reducerAccount.oneAccount);
    const getAccount = async () => {
        await dispatch(getAccountAT(dataUser?._id!));
    }
    React.useEffect(() => {
        getAccount();
    }, [])
    const [values, setValues] = React.useState({
        documentFront: (() => {
            if (dataUser)
                return oneAccount?.documentFront!
            else
                return ""
        })(),
        documentBack: (() => {
            if (dataUser)
                return oneAccount?.documentBack!
            else
                return ""
        })(),
    })
    if (values.documentFront) {
        documentFrontSrc = `${uploadsUrl}/${values.documentFront}`
    }
    if (values.documentBack) {
        documentBackSrc = `${uploadsUrl}/${values.documentBack}`
    }
    return <>
        <Card>
            <CardContent>
                <Box sx={{ alignItems: 'center', display: 'flex', flexDirection: 'column' }}>
                    <Avatar sx={{ height: 64, mb: 2, width: 64 }} />
                    {dataUser ?
                        <Typography color="textPrimary" gutterBottom variant="h5">
                            {oneAccount?.firstName! + "  " + oneAccount?.lastName!}
                        </Typography>
                        :
                        <Typography color="textPrimary" gutterBottom variant="h5">

                        </Typography>
                    }
                </Box>
            </CardContent>

            <CardContent>
                <Grid
                    container
                    direction="column"
                    justifyContent="center"
                    alignItems="center"
                    spacing={2}
                >
                    <Grid item md={6} xs={12}>
                        <Image
                            width={180}
                            src={documentFrontSrc} />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <Image
                            width={180}
                            src={documentBackSrc} />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </>
}
const AccountProfileDetails = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const dataUser: TUser = useAppSelector(state => state.reducerUsers.user);
    const oneAccount: TGetPostAccountAT = useAppSelector(state => state.reducerAccount.oneAccount);

    const getAccount = async () => {
        await dispatch(getAccountAT(dataUser?._id!))
    }
    React.useEffect(() => {
        getAccount();
    }, [])
    const [values, setValues] = React.useState({
        firstName: (() => {
            if (dataUser)
                return oneAccount?.firstName!
            else
                return ""
        })(),
        lastName: (() => {
            if (dataUser)
                return oneAccount?.lastName!
            else
                return ""
        })(),
        patronym: (() => {
            if (dataUser)
                return (oneAccount?.patronym! ? oneAccount?.patronym! : " ")
            else
                return ""
        })(),
        dateOfBirth: (() => {
            if (dataUser)
                return oneAccount?.dateOfBirth!
            else
                return ""
        })(),
        identificationNumber: (() => {
            if (dataUser)
                return oneAccount?.identificationNumber!
            else
                return ""
        })(),
        phone: (() => {
            if (dataUser)
                return oneAccount?.phone!
            else
                return ""
        })(),
        country: (() => {
            if (dataUser)
                return oneAccount?.country!
            else
                return ""
        })(),
        area: (() => {
            if (dataUser)
                return oneAccount?.area!
            else
                return ""
        })(),
        city: (() => {
            if (dataUser)
                return oneAccount?.city!
            else
                return ""
        })(),
        address: (() => {
            if (dataUser)
                return oneAccount?.address!
            else
                return ""
        })(),
    });

    const handleChange = (event: any) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value
        });
    };

    return <>
        <form autoComplete="off" noValidate>
            <Card>
                <CardHeader title="Информация" />
                <CardContent>
                    <Grid container spacing={3}>
                        <Grid item md={6} xs={12}>
                            <TextField
                                fullWidth
                                label="Имя"
                                name="firstName"
                                onChange={handleChange}
                                required
                                value={values.firstName}
                                variant="outlined" />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField
                                fullWidth
                                label="Фамилия"
                                name="lastName"
                                onChange={handleChange}
                                required
                                value={values.lastName}
                                variant="outlined" />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField
                                fullWidth
                                label="Отчество"
                                name="patronym"
                                onChange={handleChange}
                                required
                                value={values.patronym}
                                variant="outlined" />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField
                                fullWidth
                                label="День рождения"
                                name="dateOfBirth"
                                onChange={handleChange}
                                required
                                value={values.dateOfBirth}
                                variant="outlined" />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField
                                fullWidth
                                label="ИИН"
                                name="identificationNumber"
                                onChange={handleChange}
                                required
                                value={values.identificationNumber}
                                variant="outlined" />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField
                                fullWidth
                                label="Номер телефона"
                                name="phone"
                                onChange={handleChange}
                                value={values.phone}
                                variant="outlined" />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField
                                fullWidth
                                label="Страна"
                                name="country"
                                onChange={handleChange}
                                value={values.country}
                                variant="outlined" />
                        </Grid>
                        {/* <Grid item md={6} xs={12}>
                        </Grid> */}
                    </Grid>
                </CardContent>

                <CardHeader title="Адрес" />
                <CardContent>
                    <Grid container spacing={3}>
                        <Grid item md={6} xs={12}>
                            <TextField
                                fullWidth
                                label="Область"
                                name="area"
                                onChange={handleChange}
                                value={values.area}
                                variant="outlined" />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField
                                fullWidth
                                label="Город"
                                name="city"
                                onChange={handleChange}
                                value={values.city}
                                variant="outlined" />
                        </Grid>
                        <Grid item md={12}>
                            <TextField
                                fullWidth
                                label="Адрес"
                                name="address"
                                onChange={handleChange}
                                value={values.address}
                                variant="outlined" />
                        </Grid>
                    </Grid>
                </CardContent>

                {/* <Box sx={{display: 'flex', justifyContent: 'flex-end', p: 2}}>
                    <Button color="primary" variant="contained"                     sx={{ bgcolor: "black", width:"200px" ,
                    "&:hover": {
                        backgroundColor: "#b0a8a8"
                        },
                    }}>
                        Save details
                    </Button>
                </Box> */}
            </Card>
        </form>
    </>
}

const ClientOffice: React.FC = () => {
    return <>
        <Box component="main" sx={{ flexGrow: 1, py: 8 }}>
            <Container maxWidth="lg">

                <Typography sx={{ mb: 3 }} variant="h4">
                    Личный кабинет
                </Typography>
                <Grid container spacing={3}>
                    <Grid item lg={8} md={7} xs={12}>
                        <AccountProfileDetails />
                    </Grid>
                    <Grid item lg={4} md={5} xs={12}>
                        <AccountProfile />
                    </Grid>
                </Grid>
            </Container>
        </Box>
    </>
}

export default ClientOffice;

