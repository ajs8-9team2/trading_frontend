import * as React from 'react';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hook';
import { TUser } from "../../types/reestrOfAlias";
import { TTransferReq } from "../../types/typesTransferRequest";
import { returnNameOfBank } from "../../Helpers/returnNameOfBank";
import { createTransferReqAT } from "../../store/services/transferRequestSlice";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { validationWithdraw } from '../../config';
import MoneyWithdrawForm from '../../components/Forms/MoneyWithdrawForm/MoneyWithdrawForm';
import {
    FormControl,
    FormControlLabel,
    InputLabel,
    Radio,
    RadioGroup,
    Select,
    ThemeProvider,
    Typography
} from "@mui/material";
import {TAccount, TGetPostAccountAT} from "../../types/typesAccount";
import { getAccountAT } from "../../store/services/accountSlice";
import { getClientAccountAT } from "../../store/services/clientAccountSlice";
import { MenuItem } from "@material-ui/core";
import { TGetPostClientAccountAT} from "../../types/typesClentAccount";
import { createTheme } from "@material-ui/core/styles";

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const MoneyWithdrawVTWO: React.FC = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const dataUser: TUser = useAppSelector(state => state.reducerUsers.user);
    const oneAccount: TGetPostAccountAT = useAppSelector(state => state.reducerAccount.oneAccount);
    const allClientAccounts: TGetPostClientAccountAT[] = useAppSelector(state => state.reducerClientAccount.allClientAccount);
    const oneClientAccount: TGetPostClientAccountAT = useAppSelector(state => state.reducerClientAccount.oneClientAccount);

    useEffect(() => {
        dispatch(getAccountAT(dataUser?._id!));
        dispatch(getClientAccountAT(dataUser?._id!));
    }, [dispatch]);

    const [myIBAN, setMyIBAN] = React.useState<boolean>(true);

    const [dropDownOptions, setDropDownOptions] = React.useState<TGetPostClientAccountAT[]>(allClientAccounts.filter((i) => i?.isMyIIK === true));

    const [state, setState] = React.useState<TTransferReq>({
        senderFullName: (() => {
            if (dataUser)
                return oneAccount?.firstName! + "  " + oneAccount?.lastName! + "  " + (oneAccount?.patronym! ? oneAccount?.patronym! : " ")
            else
                return ""
        })(),
        recipientFullName: "",
        senderIIN: (() => {
            if (dataUser)
                return oneAccount?.identificationNumber!
            else
                return ""
        })(),
        recipientIIN: "",
        senderIIK: "",
        recipientIIK: "",
        senderBIK: "",
        recipientBank: "",
        senderBank: "",
        recipientBIK: "",
        senderKBe: "",
        recipientKBe: "",
        amount: "",
    });

    const inputChangeHandler = (e: React.ChangeEvent<HTMLInputElement>)=>{
        let {name, value} = e.target;
        let innerStr = "";
        //ATYNKZKA
        if(name === "senderBank" ){
            innerStr = returnNameOfBank(value,"BIC");
            setState(prevState=>{
                return {...prevState, senderBIK: innerStr};
            });
        }
        if(name === "recipientBank" ){
            innerStr = returnNameOfBank(value,"BIC");
            setState(prevState=>{
                return {...prevState, recipientBIK: innerStr};
            });
        }
        setState(prevState=>{
            return {...prevState, [name]: value};
        });
    };


    const submitFormHandler = async () => {
        const innerObj: {
            senderFullName: string,
            recipientFullName: string | undefined,
            senderIIN: string,
            recipientIIN: string,
            senderIIK: string,
            recipientIIK: string | undefined,
            senderBIK: string,
            senderBank: string,
            recipientBank: string | undefined,
            recipientBIK: string | undefined,
            senderKBe: number | string,
            recipientKBe: number | string,
            amount: number | string,
        } = state;
        console.log(innerObj)
        await dispatch(createTransferReqAT(innerObj));
        navigate("/client/client-office");
    }

    const { register, formState: { errors }, handleSubmit }
        = useForm<TTransferReq>({
            resolver: yupResolver(validationWithdraw)
        });


    const ObjSenderFullName = {
        ...register("senderFullName")
    }
    const ObjRecipientFullName = {
        ...register("recipientFullName")
    }

    const ObjSenderIIN = {
        ...register("senderIIN")
    }
    const ObjRecipientIIN = {
        ...register("recipientIIN")
    }

    const ObjSenderIIK = {
        ...register("senderIIK")
    }
    const ObjRecipientIIK = {
        ...register("recipientIIK")
    }


    const ObjSenderBank = {
        ...register("senderBank")
    }
    const ObjRecipientBank = {
        ...register("recipientBank")
    }

    const ObjSenderBIK = {
        ...register("senderBIK")
    }
    const ObjRecipientBIK = {
        ...register("recipientBIK")
    }

    const ObjSenderKBe = {
        ...register("senderKBe")
    }
    const ObjRecipientKBe = {
        ...register("recipientKBe")
    }

    const ObjAmount = {
        ...register("amount")
    }

    return (
        <div style={{ margin: "30px" }}>

            <ThemeProvider theme={theme}>
                <Typography variant="h5" component="div" style={{ marginBottom: "20px" }}>
                    Вывод денег:
                </Typography>
                <form onSubmit={submitFormHandler}>

                    <FormControl style={{ marginBottom: "20px" }}>
                        <RadioGroup
                            aria-labelledby="demo-controlled-radio-buttons-group"
                            name="myIBAN"
                            value={myIBAN}
                            onChange={(e) => {
                                setMyIBAN(e.target.value === "true")
                            }}
                        >
                            <FormControlLabel value={true} onClick={() => setDropDownOptions(allClientAccounts.filter((i) => i?.isMyIIK === true))} control={<Radio />} label="На мой личный счет" />
                            <FormControlLabel value={false} onClick={() => setDropDownOptions(allClientAccounts.filter((i) => i?.isMyIIK === false))} control={<Radio />} label="На счет третьего лица" />
                        </RadioGroup>
                    </FormControl>

                    <Typography variant="h6" component="div" style={{ marginBottom: "20px" }}>
                        Сохраненные счета:
                    </Typography>

                    <FormControl fullWidth style={{ marginBottom: "20px" }}>
                        <InputLabel id="demo-simple-select-label">Номер счета</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            variant="outlined"
                            label="Номер счета"
                        >
                            {dropDownOptions.map((item) => {
                                return <MenuItem value={item?._id} key={item?._id}
                                    onClick={() => {
                                        setState(prevState => {
                                            return {
                                                ...prevState,
                                                recipientFullName: item?.recipientName,
                                                recipientIIK: item?.IIK,
                                                recipientBank: item?.bank,
                                                recipientBIK: item?.BIK
                                            };
                                        });
                                    }
                                    }>{item?.IIK}</MenuItem>
                            })}
                        </Select>
                    </FormControl>

                    <MoneyWithdrawForm
                        state={state}
                        onChange={inputChangeHandler}
                        onClick={submitFormHandler}
                        errors={errors}
                        ObjSenderFullName={ObjSenderFullName}
                        ObjRecipientFullName={ObjRecipientFullName}
                        ObjSenderIIN={ObjSenderIIN}
                        ObjRecipientIIN={ObjRecipientIIN}
                        ObjSenderIIK={ObjSenderIIK}
                        ObjRecipientIIK={ObjRecipientIIK}
                        ObjSenderBank={ObjSenderBank}
                        ObjRecipientBank={ObjRecipientBank}
                        ObjSenderBIK={ObjSenderBIK}
                        ObjRecipientBIK={ObjRecipientBIK}
                        ObjSenderKBe={ObjSenderKBe}
                        ObjRecipientKBe={ObjRecipientKBe}
                        ObjAmount={ObjAmount}
                        handleSubmit={handleSubmit}
                    />
                </form>
            </ThemeProvider>
        </div>
    )
}

export default MoneyWithdrawVTWO;

