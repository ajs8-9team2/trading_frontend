import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {TAccRequestsState, TGetAccRequestsAT} from "../../types/typesAccReq";
import {useAppDispatch, useAppSelector} from "../../hook";
import {useEffect} from "react";
import {getNotificationAT} from "../../store/services/notificationSlice";
import {TGetPostNotificationAT} from "../../types/typesNotification";
import {TUserState} from "../../types/typesUser";

const PendingAccount = () => {

    const userAccReq : any = useAppSelector(state => state.reducerAccRequests);
    const oneNotification: TGetPostNotificationAT = useAppSelector(state => state.reducerNotification.oneNotification);
    const dispatch = useAppDispatch();
    const dataUser: TUserState = useAppSelector(state => state.reducerUsers);

    useEffect(() => {
        if (dataUser.user?._id) {
            dispatch(getNotificationAT(dataUser.user._id))
            // dispatch(queryAccReqClientAT(dataUser.user._id))
        }
    }, [dispatch]);

    const bull = (
        <Box
            component="span"
            sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
        >
            •
        </Box>
    );

    const card = (
        <React.Fragment>
            <CardContent>
                <Typography variant="h5" component="div" style={{paddingBottom: "25px"}}>
                    Заявка на Открытие счета была отправлена
                </Typography>

                <Typography variant="body2" style={{color : "red"}}>
                    Статус: "На рассмотрении"
                </Typography>
            </CardContent>
        </React.Fragment>
    );

    return  <Box sx={{ minWidth: 320 }} style={{margin : "40px auto"}}>
        <Card variant="outlined" style={{padding: "30px"}}>{card}</Card>
    </Box>

}

export default PendingAccount;