import * as React from 'react';
import {Button} from "@mui/material";
import { NavLink } from 'react-router-dom';
import { Table, Col, Row } from "antd";
import {ColumnsType} from "antd/es/table";
import "./ClientMain.css";

const ClientMain = () => {
    const stocks = [
        {
            company: "Apple",
            ticker: "AAPL",
            amount: 34,
            key: 1
        },
        {
            company: "Microsoft",
            ticker: "MSFT",
            amount: 120,
            key: 2
        },
        {
            company: "Tesla",
            ticker: "TSLA",
            amount: 20,
            key: 3
        },
        {
            company: "Halyk Bank OA",
            ticker: "HSBK",
            amount: 100,
            key: 4
        },
        {
            company: "Netflix",
            ticker: "NFLX",
            amount: 100,
            key: 5
        },
        {
            company: "Chevron",
            ticker: "CVX",
            amount: 100,
            key: 6
        }
    ];

    interface DataType {
        key: number;
        company: string;
        ticker: string;
        price: number;
        change: string;
        volume: string;
    };

    const columns: ColumnsType<DataType> = [
        {
            title: "Название",
            dataIndex: "company",
            key: "company"
        },
        {
            title: "Тикер",
            dataIndex: "ticker",
            key: "ticker"
        },
        {
            title: "Цена",
            dataIndex: "price",
            key: "price"
        },
        {
            title: "Изм.%",
            dataIndex: "change",
            key: "change"
        },
        {
            title: "Объем",
            dataIndex: "volume",
            key: "volume"
        },
    ];

    const data: DataType[] =[ 
    { key: 1, company: "Alibaba ADR", ticker: "BABA", price: 120.57, change: "+0.27%", volume: "10M"}, 
    { key: 2, company: "Amazon", ticker: "AMZN", price: 99.22, change: "+2.10%", volume: "67.87M"},
    { key: 3, company: "AMD", ticker: "AMD", price: 75.17, change: "+0.35%", volume: "48.2M"},
    { key: 4, company: "Apple", ticker: "AAPL", price: 143.26, change: "+0.27%", volume: "53.6M"},
    { key: 6, company: "Halyk Bank AO", ticker: "HSBK", price: 124.39, change: "-0.47%", volume: "16.33K"},
    { key: 7, company: "Intel", ticker: "INTC", price: 30.11, change: "+1.31%", volume: "46.35M"},
    { key: 8, company: "Kaspi.kz AO", ticker: "KSPI", price: 33532, change: "-0.09%", volume: "10.02K"},
    { key: 9, company: "Kcell AO", ticker: "KCEL", price: 1738, change: "+0.27%", volume: "10M"},
    { key: 10, company: "Microsoft", ticker: "MSFT", price: 248.00, change: "+3.27%", volume: "33.31M"},
    { key: 11, company: "Netflix", ticker: "NFLX", price: 369.02, change: "-0.84%", volume: "7.62M"},
    { key: 12, company: "NK KazMunayGaz AO", ticker: "KMGZ", price: 9650, change: "+0.27%", volume: "11.64K"},
    { key: 13, company: "NVIDIA", ticker: "NVDA", price: 198.02, change: "+2.48%", volume: "48.45M"},
    { key: 14, company: "Procter&Gamble", ticker: "PG", price: 120.57, change: "+0.27%", volume: "10M"},
    { key: 14, company: "Tesla", ticker: "TSLA", price: 160.27, change: "+10.97%", volume: "232.88M"}
 ]

    return (
    <div className="ClientMain">
        <h2>Популярные акции</h2>

        <div className="stock_table">
            <Table dataSource={data} columns={columns} bordered />
        </div>
    </div>
    )
}

export default ClientMain;