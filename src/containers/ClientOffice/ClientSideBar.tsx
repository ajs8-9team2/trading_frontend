import * as React from 'react';
import { useNavigate } from "react-router-dom";
import HomeIcon from '@mui/icons-material/Home';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import PeopleIcon from '@mui/icons-material/People';
import ShowChartIcon from '@mui/icons-material/ShowChart';
import type { MenuProps } from 'antd';
import { Layout, Menu, theme } from 'antd';
const { Sider } = Layout;
type MenuItem = Required<MenuProps>['items'][number];

function getItem(label: React.ReactNode, key: React.Key, icon?: React.ReactNode,): MenuItem {
    return { key, icon, label, } as MenuItem;
}

const items: MenuItem[] = [
    getItem('Главная', '/client/', <HomeIcon />),
    getItem('Счета', '/client/client-accounts', <AccountBalanceWalletIcon />),
    getItem('Переводы', '/client/client-transactions', <PeopleIcon />),
    getItem('Акции', '/client/client-stocks', <ShowChartIcon />)
];

function DashboardContent() {
    const navigate = useNavigate();
    const [collapsed, setCollapsed] = React.useState(false);
    const { token: { colorBgContainer } } = theme.useToken();

    return <>
        <Layout style={{ minHeight: '100vh', flex: "none" }}>
            <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                <div style={{ height: 32, margin: 16, background: '#001529' }} />
                <Menu onClick={({ key }) => navigate(key)}
                    theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
            </Sider>
        </Layout>
    </>
}

export default function ClientSideBar() {
    return <DashboardContent />;
}