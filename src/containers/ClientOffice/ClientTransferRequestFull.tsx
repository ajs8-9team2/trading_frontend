import { useParams } from "react-router-dom";
import { Card,CardContent, Grid, Typography } from "@mui/material";
import { TGetPostTransferReqAT } from "../../types/typesTransferRequest";
import { paramsTransferReqAT } from "../../store/services/transferRequestSlice";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../hook";

const ClientTransferRequestFull = () => {
    const dispatch = useAppDispatch();
    const params = useParams();
    const transferRequest: TGetPostTransferReqAT = useAppSelector(state => state.reducerTransferRequest.oneTransferReq);
    useEffect(() => {
        dispatch(paramsTransferReqAT(params.id as string));
    }, []);

    if (transferRequest)
        return (
            <Card sx={{ minWidth: 600, height: "700px", marginBottom: "20px", marginLeft: "70px", marginTop: "20px"}}>
                <CardContent>
                    <Typography variant="h5" component="div">
                        # {transferRequest?._id!}
                    </Typography>
                    <Typography variant="body2" style={{ marginBottom: "40px", marginLeft: "10px" }}>
                        Статус: {transferRequest?.status!}
                    </Typography>
                    <Grid container spacing={2} style={{ margin: "5px", display: "block" }}>
                        <Typography variant="h4" gutterBottom>
                            Данные отправителя:
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            ФИО отправителя: {transferRequest?.senderFullName!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            ИИН отправителя: {transferRequest?.senderIIN!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            ИИК отправителя: {transferRequest?.senderIIK!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            Банк отправителя: {transferRequest?.senderBank!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            Бик отправителя: {transferRequest?.senderBIK!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            КБе отправителя: {transferRequest?.senderKBe!}
                        </Typography>

                        <Typography variant="h4" gutterBottom style={{ marginTop: "15px" }}>
                            Данные получателя:
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            ФИО получателя: {transferRequest?.recipientFullName!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            ИИН получателя: {transferRequest?.recipientIIN!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            ИИК получателя: {transferRequest?.recipientIIK!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            Банк получателя: {transferRequest?.recipientBank!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            Бик получателя: {transferRequest?.recipientBIK!}
                        </Typography>
                        <Typography variant="body1" style={{ marginTop: "5px", marginLeft: "5px" }}>
                            КБе получателя: {transferRequest?.recipientKBe!}
                        </Typography>
                    </Grid>

                    <Typography variant="h6" gutterBottom style={{ marginTop: "15px" }}>
                        Сумма перевода:
                        <b style={{ color: 'black' }}>{transferRequest?.amount!} тг.</b>
                    </Typography>

                </CardContent>
            </Card>
        )
    else
        return (
            <Card sx={{ minWidth: 600, marginBottom: "20px", height: "200px", marginLeft: "70px", marginTop: "20px"}}>
                <Typography variant="h5" component="div" sx={{marginLeft: "70px", marginTop: "20px"}}>
                    Нет заявления
                </Typography>
            </Card>
        )
}

export default ClientTransferRequestFull;