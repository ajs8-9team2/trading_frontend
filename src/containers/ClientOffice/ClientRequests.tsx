import { Box, Card, CardActions, CardContent, Button, Typography, CardHeader, Grid } from '@mui/material';
import { useAppDispatch, useAppSelector } from "../../hook";
import { TUserState } from "../../types/typesUser";
import { useEffect } from "react";
import { queryAccReqClientAT } from "../../store/services/accRequestSlice";
import { useNavigate } from "react-router-dom";
import { TGetAccRequestsAT } from "../../types/typesAccReq";
import { queryTransferReqClientAT } from '../../store/services/transferRequestSlice';
import { queryDepositReqClientAT } from '../../store/services/depositRequestSlice';
import { TGetPostTransferReqAT } from '../../types/typesTransferRequest';
import ClientAccountRequest from '../../components/CopmClient/ClientAccountRequest';
import ClientTransferRequest from '../../components/CopmClient/ClientTranferRequest';
import ClientDepositRequest from '../../components/CopmClient/ClientDepositRequest';

export default function ClientRequests() {
    const dispatch = useAppDispatch();
    const dataUser: TUserState = useAppSelector(state => state.reducerUsers);
    const navigate = useNavigate();
    const allAccRequests: TGetAccRequestsAT[] = useAppSelector(state => state.reducerAccRequests.allAccRequests);
    const transferRequests: TGetPostTransferReqAT[] = useAppSelector(state => state.reducerTransferRequest.allTransferReq);
    const depositRequests: TGetPostTransferReqAT[] = useAppSelector(state => state.reducerDepositRequest.allDepositsRequest);

    useEffect(() => {
        if (dataUser.user?._id) {
            dispatch(queryAccReqClientAT(dataUser.user._id))
        }
    }, []);

    useEffect(() => {
        if (dataUser.user?._id) {
            dispatch(queryTransferReqClientAT(dataUser.user._id))
        }
    }, []);

    useEffect(() => {
        if (dataUser.user?._id) {
            dispatch(queryDepositReqClientAT(dataUser.user._id))
        }
    }, []);


    const viewAccountRequest = (id: string) => {
        navigate(`/client/client-request/accreq/${id}`)
    }

    return <Box>
        <CardHeader title="Мои заявки" style ={{ marginLeft: "70px"}}/>
        <Card sx={{ minWidth: 600, marginBottom: "70px", marginLeft: "40px" }}>
            <CardContent>
                <Grid style={{ marginBottom: "40px" }}>
                    <Typography variant="h5" component="div">
                        Заявки по счетам:
                    </Typography>
                </Grid>
                <Grid>
                    {allAccRequests.map((item) => {
                        return (
                            <ClientAccountRequest
                                item={item}
                                key={item?._id!}
                                viewAccountRequest={viewAccountRequest} />
                        )
                    })
                    }
                </Grid>
            </CardContent>

        </Card>
        <Card sx={{ minWidth: 600, marginLeft: "40px" , marginBottom: "70px"}}>
            <CardContent>
                <Typography variant="h5" component="div">
                    Заявки пополнение счёта:
                </Typography>
                <Grid>
                    {depositRequests.map((item, index) => {
                        return (
                            <ClientTransferRequest
                                item={item}
                                key={item?._id!}
                            />
                        )
                    })
                    }
                </Grid>
            </CardContent>
        </Card>

        <Card sx={{ minWidth: 600, marginLeft: "40px", marginBottom: "70px" }}>
            <CardContent>
                <Typography variant="h5" component="div">
                    Заявки вывод денег:
                </Typography>
                <Grid>
                    {transferRequests.map((item, index) => {
                        return (
                            <ClientDepositRequest
                                item={item}
                                key={item?._id!}
                            />
                        )
                    })
                    }
                </Grid>
            </CardContent>
        </Card>
        <Card sx={{ minWidth: 600, marginBottom: "20px", marginLeft: "40px" }}>
            <CardContent>
                <Typography variant="h5" component="div">
                    Заявки по акциям
                </Typography>
                <Typography variant="body2">
                    В процессе:
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">Перейти</Button>
            </CardActions>
        </Card>
    </Box>
}
