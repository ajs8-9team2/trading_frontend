import React, {useEffect} from "react";
import {useNavigate} from 'react-router-dom';
import {useAppDispatch} from "../../hook";
import {useAppSelector} from '../../hook';
import { Button, Form, Input, InputNumber, Radio, Select, message } from "antd";
import {stocksList} from "../../Helpers/getStockName";
import {TStockReqType, TUser} from "../../types/reestrOfAlias";
import {TStockRequest, TGetPostStockRequestAT} from '../../types/typesStockRequest';
import { TGetPostAccountAT } from "../../types/typesAccount";
import {createStockRequestAT} from "../../store/services/stockRequestSlice";
import { getStockName } from "../../Helpers/getStockName";
import "./StockTransactions.css";

const StockTransaction: React.FC = ()=> {
    const navigate= useNavigate();
    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const oneAccount: TGetPostAccountAT = useAppSelector(state=>state.reducerAccount.oneAccount);

    const [form]  = Form.useForm();
    const [messageApi, contextHolder] = message.useMessage();
    
    useEffect(() => {
        let name;
        if (dataUser) {
            name =  oneAccount?.firstName! + " " + oneAccount?.lastName + " " + (oneAccount?.patronym ? oneAccount?.patronym! : " ");
            form.setFieldsValue({userFullName: name})
        }        
    }, []);


    const layout = {
        labelCol: {span: 8},
        wrapperCol: { span: 14}
    };

    const validateMessages = {
        required: "Заполните поле!"
    };

    const valueChange = (value: any) => {
        const code = getStockName(value, "TICKER");
        form.setFieldsValue({ticker: code})
    };

    const onFinish = async (values: any) => {
        values.price = +values.price;
        await dispatch(createStockRequestAT(values));
        navigate(-1);
    };

    return (
        <div className="StockTransactions">
            <div className="stock_transaction_header">
                <Button 
                    type="default"
                    size="large"
                    onClick={() => navigate(-1)}
                >
                    {"НАЗАД"}
                </Button>

                <h2>Заявка на покупку / продажу акций</h2>
            </div>

            <div>
            <Form {...layout} onFinish={onFinish} validateMessages={validateMessages} className="stock_form" form={form} >
                <Form.Item name="stockReqType" label="Действие" rules={[{ required: true}]}>
                    <Radio.Group>
                        <Radio value="Buy" >Покупка</Radio>
                        <Radio value="Sell" >Продажа</Radio>
                    </Radio.Group>
                </Form.Item>
                <Form.Item name="userFullName" label="ФИО" rules={[{ required: true}]} >
                    <Input />
                </Form.Item>
                <Form.Item name="company" label="Название" rules={[{ required: true}]}>
                    <Select 
                        showSearch
                        placeholder="Выберите или введите название"
                        onChange={valueChange}
                        optionFilterProp="children"
                        filterOption={(input, option) => (option?.label ?? '').toLowerCase().includes(input)}
                        filterSort={(optionA, optionB) => 
                            (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
                        }
                        options={(stocksList || []).map((item) => ({
                            value: item,
                            label: item
                        }))}  
                    />
                </Form.Item>
                <Form.Item name="ticker" label="Тикер" rules={[{ required: true}]} id="ticker">
                    <Input />
                </Form.Item>
                <Form.Item name="price" label="Цена" rules={[{ required: true}]}>
                    <InputNumber<string> placeholder="0.00" min="0" step="0.01" stringMode />
                </Form.Item>
                <Form.Item name="stockAmount" label="Количество" rules={[{ required: true}]}>
                    <InputNumber placeholder="0" min={0} />
                </Form.Item>
                
                <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8}}>
                    {contextHolder}
                    <Button type="primary" htmlType="submit" size="large" >
                        Отправить
                    </Button>
                </Form.Item>
            </Form>
            </div>
        </div>
    )
};

export default StockTransaction;