import { useParams } from "react-router-dom";
import { Card,CardContent, Grid, Typography } from "@mui/material";
import { TGetPostTransferReqAT } from "../../types/typesTransferRequest";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../hook";
import { paramsDepositReqAT } from "../../store/services/depositRequestSlice";

const ClientDepositRequestFull = () => {
    const dispatch = useAppDispatch();
    const params = useParams();

    const transferRequest: TGetPostTransferReqAT = useAppSelector(state => state.reducerDepositRequest.oneDepositRequest);
    console.log(transferRequest);
    
    useEffect(() => {
        dispatch(paramsDepositReqAT(params.id as string));
    }, []);

    if (transferRequest)
        return (
            <Card sx={{ minWidth: 600, height: "700px", marginBottom: "20px", marginLeft: "70px", marginTop: "20px"}}>
                <CardContent>
                    <Typography variant="h5" component="div">
                        # {transferRequest?._id!}
                    </Typography>
                    <Typography variant="body2" style={{ marginBottom: "40px", marginLeft: "10px" }}>
                        Статус: {transferRequest?.status!}
                    </Typography>
                    <Grid container spacing={2} style={{ margin: "5px", display: "block" }}>
                        <Typography variant="h4" gutterBottom>
                            Данные отправителя:
                        </Typography>

                    </Grid>

                    <Typography variant="h6" gutterBottom style={{ marginTop: "15px" }}>
                        Сумма пополнения:
                        <b style={{ color: 'black' }}>{transferRequest?.amount!} тг.</b>
                    </Typography>

                </CardContent>
            </Card>
        )
    else
        return (
            <Card sx={{ minWidth: 600, marginBottom: "20px", height: "200px", marginLeft: "70px", marginTop: "20px"}}>
                <Typography variant="h5" component="div" sx={{marginLeft: "70px", marginTop: "20px"}}>
                    Нет заявления
                </Typography>
            </Card>
        )
}

export default ClientDepositRequestFull;