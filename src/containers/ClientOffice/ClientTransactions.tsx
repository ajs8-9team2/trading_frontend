import * as React from 'react';
import { Typography, Button } from "@mui/material";
import { NavLink } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hook';
import { TGetPostAccountAT } from '../../types/typesAccount';
import { TUser } from '../../types/reestrOfAlias';
import { useEffect } from 'react';
import { getAccountAT } from '../../store/services/accountSlice';

const ClientTransactions: React.FC = () => {
    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state => state.reducerUsers.user);
    const oneAccount: TGetPostAccountAT = useAppSelector(state => state.reducerAccount.oneAccount);
    useEffect(() => {
        dispatch(getAccountAT(dataUser?._id!));
    }, [dispatch]);

    return (
        <div style={{ margin: "30px" }} >
            <Typography paragraph style={{ textAlign: "center" }} >
                {"Мои переводы"}
            </Typography>
            <div style={{ display: "flex" }}>
                <Button
                    component={NavLink}
                    variant="contained"
                    to="/client/money-transfer"
                    style={{ marginRight: "20px", backgroundColor: "black", width: "200px" }}
                >
                    {"Пополнение счёта"}
                </Button>
                {oneAccount?.amount! > 0 ?  <Button
                    component={NavLink}
                    variant="contained"
                    to="/client/withdrawal-money"
                    disabled ={false}
                    style={{ marginRight: "20px", backgroundColor: "black", width: "170px" }}
                >
                    {"Вывод денег"}
                </Button> : <div></div>}
            </div>
        </div>
    )
}

export default ClientTransactions;

