import React, {useState} from "react";
import {Typography } from 'antd';
import TraderMyReqs from "../../components/CompTrader/TraderMyReqs";

const TraderMyList: React.FC = ()=>{

    const { Title } = Typography;

    return (<>
        <div className="TraderList">
            <Title level={3}>Заявки в процессе</Title>
            <TraderMyReqs />
        </div>
        </>
    )
};

export default TraderMyList;