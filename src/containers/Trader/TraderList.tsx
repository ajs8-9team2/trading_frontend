import React, {useState} from "react";
import {Typography } from 'antd';
import TraderReqs from "../../components/CompTrader/TraderReqs";
import "./TraderList.css";

const TraderList: React.FC = ()=>{

    const { Title } = Typography;

    return (<>
        <div className="TraderList" >
            <Title level={3}>Текущие заявки</Title>
            <TraderReqs />
        </div>
        </>
    )
};

export default TraderList;