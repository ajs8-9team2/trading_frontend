import { Button, Container, Grid } from "@mui/material";
import { createAccRequestAT, queryAccReqFrontWIPAT } from "../../store/services/accRequestSlice";
import styled from "@emotion/styled";
import { useAppDispatch, useAppSelector } from "../../hook";


import { TPostAccRequestAT, TGetAccRequestsAT, } from "../../types/typesAccReq";
import { useEffect } from "react";


const StyledContainer = styled(Container)`
    padding-top: 30px;
    padding-bottom: 30px;
    box-shadow: 0 18px 30px 0 rgba(0, 0, 0, 0.6);
`;

const Main = () => {
    const dispatch = useAppDispatch();
    const dataUser: any = useAppSelector(state => state.reducerUsers.user);
    const AccRequests: TGetAccRequestsAT = useAppSelector(state => state.reducerAccRequests.oneAccRequest);

    useEffect(() => {
        if (dataUser)
            dispatch(queryAccReqFrontWIPAT(dataUser?._id));
    }, [])

    const onSubmit = async (e: any) => {
        e.preventDefault()
        const innerObj: TPostAccRequestAT = {
            address: "Nazarbaeva",
            area: "Almaty",
            authority: "ASD",
            city: "Almaty",
            citizenship: "",
            country: "KZ",
            dateOfBirth: "1988-04-04",
            dateOfIssue: "2007-05-12",
            documentBack: "passport1.jpg",
            documentFront: "passport5.jpg",
            email: "maria@tets.com",
            expirationDate: "2027-05-10",
            firstName: "Azza",
            idCardNumber: "66666",
            identificationNumber: "788789651414",
            lastName: "Djunussov",
            patronym: "",
            phone: "+77778892563",
        }
        await dispatch(createAccRequestAT(innerObj));
    }

    return (
        <>
            <StyledContainer maxWidth="xs">
                {!!dataUser ?
                    (<>
                        <pre>{JSON.stringify(dataUser?._id, null, 2)}</pre>
                        <pre>{JSON.stringify(AccRequests, null, 2)}</pre>
                    </>)
                    : (<>{"ввойдите под своим логином"}</>)
                }
                <form onSubmit={onSubmit}>
                    <Grid item>
                        {!!dataUser
                            ?
                            <Button
                                sx={{ mt: "30px" }}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                {"push"}
                            </Button>
                            :
                            <Button
                                sx={{ mt: "30px" }}
                                variant="contained"
                                fullWidth
                                disabled>
                                {"push"}
                            </Button>
                        }
                    </Grid>
                </form>
            </StyledContainer>
        </>
    );


    // return (
    //     <Grid container direction="column" spacing={2}>
    //         <Grid
    //             container
    //             item
    //             direction="row"
    //             justifyContent="space-between"
    //             alignItems="center"
    //         >
    //             <Grid item>
    //                 <Typography variant="h4">
    //                    Home Page
    //                 </Typography>
    //             </Grid>
    //
    //         </Grid>
    //     </Grid>
    // );
};

export default Main;
