import * as React from 'react';
import FirstStage from '../../components/StepByStep/firstStage';
import FourthStage from '../../components/StepByStep/fourthStage';
import SecondStage from '../../components/StepByStep/secondStage';
import ThirdStage from '../../components/StepByStep/thirdStage';
import { createAccRequestAT } from '../../store/services/accRequestSlice';
import { validationFirstStage, validationSecondStage } from '../../config';
import { useAppDispatch, useAppSelector } from '../../hook';
import { TFirstAccReq, TSecondAccReq, TThirdAccReq, TFourthAccReq } from '../../types/typesAccReq';
import { TUser } from '../../types/reestrOfAlias';
import { CssBaseline, Box, Container, Paper, Stepper, Step, StepLabel, Button, Typography, Stack, Snackbar } from '@mui/material';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { useNavigate } from 'react-router-dom';
import MuiAlert, { AlertProps } from '@mui/material/Alert';


const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

// const theme = createTheme();
const steps = ['Этап 1', 'Этап 2', 'Этап 3', 'Этап 4'];

export function getStepContent(
    step: number,
    firstState: TFirstAccReq,
    secondState: TSecondAccReq,
    thirdState: TThirdAccReq,
    fourthState: TFourthAccReq,
    inputFirstChangeHandler: React.ChangeEventHandler<HTMLInputElement>,
    inputSecondChangeHandler: React.ChangeEventHandler<HTMLInputElement>,
    inputThirdChangeHandler: React.ChangeEventHandler<HTMLInputElement>,
    inputFourthChangeHandler: React.ChangeEventHandler<HTMLInputElement>,
    selectFourthChangeHandler: any,
    fileChangeHandler: any,
    ObjFirstName: any,
    ObjLastName: any,
    ObjBirthday: any,
    ObjIdentificationNumber: any,
    ObjDocumentFront: any,
    ObjDocumentBack: any,
    ObjDateOfIssue: any,
    ObjExpirationDate: any,
    ObjIdCardNumber: any,
    ObjCitizenship: any,
    ObjAuthority: any,
    errors: any,
    errorsSecond: any
) {
    switch (step) {
        case 0:
            return <FirstStage
                state={firstState}
                onChange={inputFirstChangeHandler}
                ObjFirstName={ObjFirstName}
                ObjLastName={ObjLastName}
                ObjBirthday={ObjBirthday}
                ObjIdentificationNumber={ObjIdentificationNumber}
                errors={errors} />;
        case 1:
            return <FourthStage
                state={fourthState}
                onChange={inputFourthChangeHandler}
                fileChangeHandler={fileChangeHandler}
                selectFourthChangeHandler={selectFourthChangeHandler}
                ObjDocumentFront={ObjDocumentFront}
                ObjDocumentBack={ObjDocumentBack}
                ObjDateOfIssue={ObjDateOfIssue}
                ObjExpirationDate={ObjExpirationDate}
                ObjIdCardNumber={ObjIdCardNumber}
                ObjCitizenship={ObjCitizenship}
                ObjAuthority={ObjAuthority}
                errorsSecond={errorsSecond}
            />
        case 2:
            return <SecondStage
                state={secondState}
                onChange={inputSecondChangeHandler} />
        case 3:
            return <ThirdStage
                state={thirdState}
                onChange={inputThirdChangeHandler} />
        default:
            throw new Error('Unknown step');
    }
};

const AccountOpening = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const userData: TUser = useAppSelector(state => state.reducerUsers.user!);
    const { register, formState: { errors }, handleSubmit }
        = useForm<TFirstAccReq>({
            resolver: yupResolver(validationFirstStage)
        });

    const { register: registerSecond, formState: { errors: errorsSecond }, handleSubmit: handleSubmitReset }
        = useForm<TFourthAccReq>({
            resolver: yupResolver(validationSecondStage),
        });
    const [open, setOpen] = React.useState(true);
    const [confirmLoading, setConfirmLoading] = React.useState(false);
    const [activeStep, setActiveStep] = React.useState(0);
    const [firstState, setFirstState] = React.useState<TFirstAccReq>({
        firstName: "",
        lastName: "",
        patronym: "",
        dateOfBirth: "",
        identificationNumber: "",
    });

    const [secondState, setSecondState] = React.useState<TSecondAccReq>({
        country: "",
        area: "",
        city: "",
        address: "",
    });

    const [thirdState, setThirdState] = React.useState<TThirdAccReq>({
        email: userData?.email!,
        phone: "",
    });

    const [fourthState, setFourthState] = React.useState<TFourthAccReq>({
        documentFront: "",
        documentBack: "",
        idCardNumber: "",
        citizenship: "",
        authority: "",
        dateOfIssue: "",
        expirationDate: ""
    })

    const [openAlert, setOpenAlert] = React.useState(false);

    const handleClick = () => {
        setOpenAlert(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenAlert(false);
    };

    const inputFirstChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        let { name, value } = e.target;
        setFirstState(prevState => {
            return { ...prevState, [name]: value };
        });
    };

    const inputSecondChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        let { name, value } = e.target;
        setSecondState(prevState => {
            return { ...prevState, [name]: value };
        });
    }

    const inputThirdChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        let { name, value } = e.target;
        setThirdState(prevState => {
            return { ...prevState, [name]: value };
        });
    }

    const inputFourthChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        let { name, value } = e.target;
        setFourthState(prevState => {
            return { ...prevState, [name]: value };
        });
    }

    const selectFourthChangeHandler = (e: any) => {
        let { name, value } = e.target;
        setFourthState(prevState => {
            return { ...prevState, [name]: value };
        });
    };

    const fileChangeHandler = (e: any) => {
        const name = e.target.name;
        const file = e.target.files[0];
        setFourthState(prevState => {
            return {
                ...prevState,
                [name]: file
            }
        });
    };

    const submitFormHandler = (e: any) => {
        handleClick();
        e.preventDefault();
        const formData = new FormData();

        formData.append('firstName', firstState.firstName);
        formData.append('lastName', firstState.lastName);
        formData.append('patronym', firstState.patronym);
        formData.append('dateOfBirth', new Date(firstState.dateOfBirth).toLocaleDateString("ru"));
        formData.append('identificationNumber', firstState.identificationNumber);

        formData.append('country', secondState.country);
        formData.append('area', secondState.area);
        formData.append('city', secondState.city);
        formData.append('address', secondState.address);

        formData.append('email', thirdState.email);
        formData.append('phone', thirdState.phone);

        formData.append('documentFront', fourthState.documentFront!);
        formData.append('documentBack', fourthState.documentBack!);
        formData.append('idCardNumber', fourthState.idCardNumber);
        formData.append('citizenship', fourthState.citizenship);
        formData.append('authority', fourthState.authority);
        formData.append('dateOfIssue', new Date(fourthState.dateOfIssue).toLocaleDateString("ru"));
        formData.append('expirationDate', new Date(fourthState.expirationDate).toLocaleDateString("ru"));
        onSubmit(formData);
    };

    const onSubmit = async (formData: any) => {
        await dispatch(createAccRequestAT(formData));
        navigate("/client/acc-pending");
    };

    const handleNext = (data: TFirstAccReq) => {
        console.log(new Date(firstState.dateOfBirth).toLocaleDateString("ru"))
        setActiveStep(activeStep + 1);
    };

    const handleNextSecond = (data: TFourthAccReq) => {
        console.log(fourthState)
        setActiveStep(activeStep + 1);
    }

    const handleBack = () => {
        setActiveStep(activeStep - 1);
    };

    const handleCancel = () => {
        setOpen(false);
        setActiveStep(activeStep - 1);
    };

    const handleOk = (e: any) => {
        submitFormHandler(e)
        setConfirmLoading(true);
        setTimeout(() => {
            setOpen(false);
            setConfirmLoading(false);
        }, 2000);
    };

    const ObjFirstName = {
        ...register("firstName")
    }

    const ObjLastName = {
        ...register("lastName")
    }

    const ObjBirthday = {
        ...register("dateOfBirth")
    }

    const ObjIdentificationNumber = {
        ...register("identificationNumber")
    }

    const ObjDocumentFront = {
        ...registerSecond("documentFront")
    }

    const ObjDocumentBack = {
        ...registerSecond("documentBack")
    }

    const ObjDateOfIssue = {
        ...registerSecond("dateOfIssue")
    }

    const ObjExpirationDate = {
        ...registerSecond("expirationDate")
    }

    const ObjIdCardNumber = {
        ...registerSecond("idCardNumber")
    }

    const ObjAuthority = {
        ...registerSecond("authority")
    }

    const ObjCitizenship = {
        ...registerSecond("citizenship")
    }

    return <>
        <ThemeProvider theme={theme} >
            <CssBaseline />
            <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
                <Paper variant="outlined" sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}>
                    <Typography component="h1" variant="h4" align="center">
                        Открытие счета
                    </Typography>
                    <Stepper activeStep={activeStep} sx={{ pt: 3, pb: 5 }}>
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel >{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    {activeStep === steps.length ? (

                        <React.Fragment>
                            <Typography variant="h5" gutterBottom align="center">
                                Вы точно хотите отправить заявку без возможности редактировать в будущем?
                            </Typography>
                            <Button onClick={submitFormHandler} sx={{
                                mt: 3, ml: 1, bgcolor: "black", color: "white", width: "200px",
                                "&:hover": {
                                    backgroundColor: "#b0a8a8"
                                }
                            }}>
                                Отправить
                            </Button>
                            <Button onClick={handleBack} sx={{
                                mt: 3, ml: 1, bgcolor: "black", color: "white", width: "200px",
                                "&:hover": {
                                    backgroundColor: "#b0a8a8"
                                }
                            }}>
                                Назад
                            </Button>
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            {getStepContent(
                                activeStep,
                                firstState,
                                secondState,
                                thirdState,
                                fourthState,
                                inputFirstChangeHandler,
                                inputSecondChangeHandler,
                                inputThirdChangeHandler,
                                inputFourthChangeHandler,
                                selectFourthChangeHandler,
                                fileChangeHandler,
                                ObjFirstName,
                                ObjLastName,
                                ObjBirthday,
                                ObjIdentificationNumber,
                                ObjDocumentFront,
                                ObjDocumentBack,
                                ObjDateOfIssue,
                                ObjExpirationDate,
                                ObjIdCardNumber,
                                ObjCitizenship,
                                ObjAuthority,
                                errors,
                                errorsSecond
                            )}
                            <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                                {activeStep !== 0 && (
                                    <Button onClick={handleBack} sx={{
                                        mt: 3, ml: 1, bgcolor: "black", width: "250px", color: "white",
                                        "&:hover": {
                                            backgroundColor: "#b0a8a8"
                                        }
                                    }}>
                                        Назад
                                    </Button>
                                )}

                                {activeStep === 0
                                    ?
                                    <Button
                                        variant="contained"
                                        onClick={handleSubmit(handleNext)}
                                        sx={{
                                            mt: 3, ml: 1, bgcolor: "black", width: "250px",
                                            "&:hover": {
                                                backgroundColor: "#b0a8a8"
                                            }
                                        }}
                                    >
                                        {activeStep === steps.length - 1 ? 'Создать счёт' : 'Следующий шаг'}
                                    </Button>

                                    :
                                    <Button
                                        variant="contained"
                                        onClick={handleSubmitReset(handleNextSecond)}
                                        sx={{
                                            mt: 3, ml: 1, bgcolor: "black", width: "250px",
                                            "&:hover": {
                                                backgroundColor: "#b0a8a8"
                                            }
                                        }}

                                    >
                                        {activeStep === steps.length - 1 ? 'Создать счёт' : 'Следующий шаг'}
                                    </Button>
                                }
                            </Box>
                        </React.Fragment>
                    )}
                </Paper>
            </Container>
        </ThemeProvider>

        <Snackbar open={openAlert} autoHideDuration={3000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                Вы отправили заявку на открытие счета. Ждите одобрение заявки.
            </Alert>
        </Snackbar>

    </>
}

export default AccountOpening;