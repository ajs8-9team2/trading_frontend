import { Route, Routes } from "react-router-dom";
import ProtectedRoute, { ProtectedRouteForClient } from "../components/UI/ProtectedRoute/ProtectedRoute";
import AccountOpening from "../containers/AccountOpening/AccountOpening";
import ClientOffice from "../containers/ClientOffice/ClientOffice";
import Main from "../containers/Main/Main";
import ClientLayout from "../components/UI/Layouts/ClientLayout";
import ClientAccounts from "../containers/ClientOffice/ClientAccounts";
import ClientTransactions from "../containers/ClientOffice/ClientTransactions";
import ClientStocks from "../containers/ClientOffice/ClientStocks";
import { useAppSelector } from "../hook";
import { TUserState } from "../types/typesUser";
import MoneyTransfer from "../containers/ClientOffice/MoneyTransfer";
import ClientRequests from "../containers/ClientOffice/ClientRequests";
import ClientAccRequest from "../containers/ClientOffice/ClientAccRequest";
import ClientIbans from "../containers/ClientOffice/ClientIbans";
import AddIban from "../containers/ClientOffice/AddIban";
import ClientNotification from "../containers/ClientOffice/ClientNotification";
import PendingAccount from "../containers/ClientOffice/PendingAccount";
import MoneyWithdrawSeparate from "../containers/ClientOffice/MoneyWithdrawSeparate";
import ClientTransferRequestFull from "../containers/ClientOffice/ClientTransferRequestFull";
import ClientDepositRequestFull from "../containers/ClientOffice/ClientDepositRequestFull";
import {createTheme} from "@material-ui/core/styles";
import {ThemeProvider} from "@mui/material";
import StockTransactions from "../containers/ClientOffice/StockTransactions";
import ClientMain from "../containers/ClientOffice/ClientMain";

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});


const RoutesClient = () => {
    const dataUser: TUserState = useAppSelector(state => state.reducerUsers);
    return (
        <>
            <ThemeProvider theme={theme}>
            {/*<pre>{JSON.stringify(dataUser?.user?.isHasAccount, null, 2)}</pre>*/}
            <Routes>
                <Route element={<ClientLayout />}>
                    {/*<Route path={"/"} element={<DeleteRightSideForm/>} />*/}
                    <Route path={"/"} element={
                        <ProtectedRoute
                            roles={["client"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientMain />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />
                    <Route path={"/account-opening"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <AccountOpening />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/acc-pending"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>

                            <PendingAccount />

                        </ProtectedRoute>
                    } />

                    <Route path={"/client-request"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientRequests />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/client-iban"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientIbans />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/add-iban"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <AddIban />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/client-request/accreq/:id"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientAccRequest />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/client-request/transferRequests/:id"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientTransferRequestFull />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />
                    <Route path={"/client-request/depositRequests/:id"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientDepositRequestFull />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />


                    <Route path={"/client-accounts"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientAccounts />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />


                    <Route path={"/client-transactions"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientTransactions />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/client-stocks"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientStocks />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/stock-transactions"} element={
                        <ProtectedRoute
                            roles={["client"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <StockTransactions />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/client-office"} element={
                        <ProtectedRoute
                            roles={["client"]}//roles={["client", "admin"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <ClientOffice />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/money-transfer"} element={
                        <ProtectedRoute
                            roles={["client"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <MoneyTransfer />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/withdrawal-money"} element={
                        <ProtectedRoute
                            roles={["client"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            <ProtectedRouteForClient
                                isAllowed={!!dataUser.user?.isHasAccount}
                                redirectUrl={"/client"}>
                                <MoneyWithdrawSeparate />
                            </ProtectedRouteForClient>
                        </ProtectedRoute>
                    } />

                    <Route path={"/client-notice"} element={
                        <ProtectedRoute
                            roles={["client"]}
                            isAllowed={false}
                            redirectUrl={"/sign-in"}>
                            {/*<ProtectedRouteForClient*/}
                            {/*    isAllowed={!!dataUser.user?.isHasAccount}*/}
                            {/*    redirectUrl={"/client"}>*/}
                            <ClientNotification />
                            {/*</ProtectedRouteForClient>*/}
                        </ProtectedRoute>
                    } />

                </Route>
            </Routes>
            </ThemeProvider>
        </>
    )
}
export default RoutesClient;