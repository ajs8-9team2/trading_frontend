import {Route, Routes} from "react-router-dom";
import React from "react";
import {Box, Typography} from "@mui/material";
import FrontOfficeListReq from "../containers/FrontOfficeModern/AccRequest/FrontOfficeListReq";
import FrontOfficeMyReq from "../containers/FrontOfficeModern/AccRequest/FrontOfficeMyReq";
import ProtectedRoute from "../components/UI/ProtectedRoute/ProtectedRoute";
import FrontList from "../containers/FrontOffice/FrontList";
import MyRequests from "../containers/FrontOffice/MyRequests";
import FrontOffice from "../containers/FrontOffice/FrontOffice";
import LabTabsListReq from "../components/CompFrontOffice/CompAccRequest/LabTabs/LabTabsListReq";
import LabTabsMyReq from "../components/CompFrontOffice/CompAccRequest/LabTabs/LabTabsMyReq";
import FrontOfficeDepositListReq from "../containers/FrontOfficeModern/DepositRequest/FrontOfficeDepositListReq";
import FrontOfficeDepositMyReq from "../containers/FrontOfficeModern/DepositRequest/FrontOfficeDepositMyReq";
import FrontOfficeTransferListReq from "../containers/FrontOfficeModern/TransferRequest/FrontOfficeTransferListReq";
import FrontOfficeTransferMyReq from "../containers/FrontOfficeModern/TransferRequest/FrontOfficeTransferMyReq";
import FrontOfficeStockListReq from "../containers/FrontOfficeModern/StockRequest/FrontOfficeStockListReq";
import FrontOfficeStockMyReq from "../containers/FrontOfficeModern/StockRequest/FrontOfficeStockMyReq";

const RoutesFrontOffice = ()=>{
    return (
        <>
            {/*<Typography paragraph>*/}
            {/*    {"front-office/list-requests"}*/}
            {/*</Typography>*/}
            {/*<Typography paragraph>*/}
            {/*    {"front-office/my-requests"}*/}
            {/*</Typography>*/}
            <Routes>
                {/*<Route path="/list-requests" element={*/}
                {/*    <ProtectedRoute*/}
                {/*        roles={["frontOffice"]}*/}
                {/*        isAllowed={false}*/}
                {/*        redirectUrl={"/sign-in"}*/}
                {/*    >*/}

                {/*        /!*<FrontList/>*!/*/}
                {/*        <FrontOffice/>*/}
                {/*    </ProtectedRoute>*/}
                {/*}/>*/}

                {/*<Route path="/my-requests" element={*/}
                {/*    <ProtectedRoute*/}
                {/*        roles={["frontOffice"]}*/}
                {/*        isAllowed={false}*/}
                {/*        redirectUrl={"/sign-in"}*/}
                {/*    >*/}
                {/*         <MyRequests/>*/}
                {/*    </ProtectedRoute>*/}
                {/*}/>*/}

                <Route path="/list-requests" element={
                    <ProtectedRoute
                        roles={["frontOffice"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        {/*<Box sx={{bgcolor: '#cfe8fc', height: '100vh'}}/>*/}
                            <FrontOfficeListReq/>
                        {/*<Box/>*/}

                        {/*<FrontList/>*/}
                        {/*<FrontOffice/>*/}
                    </ProtectedRoute>
                }/>

                <Route path="/my-requests" element={
                    <ProtectedRoute
                        roles={["frontOffice"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <FrontOfficeMyReq/>
                        {/*<MyRequests/>*/}
                    </ProtectedRoute>
                }/>
{/***********************************************************************/}
                <Route path="/list-deposit" element={
                    <ProtectedRoute
                        roles={["frontOffice"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <FrontOfficeDepositListReq/>
                    </ProtectedRoute>
                }/>

                <Route path="/my-deposit" element={
                    <ProtectedRoute
                        roles={["frontOffice"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <FrontOfficeDepositMyReq/>
                    </ProtectedRoute>
                }/>
{/***********************************************************************/}
                <Route path="/list-transfer" element={
                    <ProtectedRoute
                        roles={["frontOffice"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <FrontOfficeTransferListReq/>
                    </ProtectedRoute>
                }/>

                <Route path="/my-transfer" element={
                    <ProtectedRoute
                        roles={["frontOffice"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <FrontOfficeTransferMyReq/>
                    </ProtectedRoute>
                }/>
{/***********************************************************************/}
                <Route path="/list-stock" element={
                    <ProtectedRoute
                        roles={["frontOffice"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <FrontOfficeStockListReq/>
                    </ProtectedRoute>
                }/>

                <Route path="/my-stock" element={
                    <ProtectedRoute
                        roles={["frontOffice"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <FrontOfficeStockMyReq/>
                    </ProtectedRoute>
                }/>
{/***********************************************************************/}
                {/*<Route path="/list-requests3" element={*/}
                {/*    <ProtectedRoute*/}
                {/*        roles={["frontOffice"]}*/}
                {/*        isAllowed={false}*/}
                {/*        redirectUrl={"/sign-in"}*/}
                {/*    >*/}
                {/*        <LabTabsListReq/>*/}
                {/*        /!*<FrontOfficeListReq/>*!/*/}
                {/*    </ProtectedRoute>*/}
                {/*}/>*/}

                {/*<Route path="/my-requests3" element={*/}
                {/*    <ProtectedRoute*/}
                {/*        roles={["frontOffice"]}*/}
                {/*        isAllowed={false}*/}
                {/*        redirectUrl={"/sign-in"}*/}
                {/*    >*/}
                {/*        /!*<FrontOfficeMyReq/>*!/*/}
                {/*        <LabTabsMyReq/>*/}
                {/*    </ProtectedRoute>*/}
                {/*}/>*/}
            </Routes>
        </>
    )
}
export default RoutesFrontOffice;