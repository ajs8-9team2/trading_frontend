import {Route, Routes} from "react-router-dom";
import React from "react";
import AccountantList from "../containers/Accountant/AccountantList";
import AccountantMyList from "../containers/Accountant/AccountantMyList";
import ProtectedRoute from "../components/UI/ProtectedRoute/ProtectedRoute";

const RoutesAccountant = ()=>{
    return (
        <>
            <Routes>
                <Route path="/" element={
                    <ProtectedRoute
                        roles={["accountant"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <AccountantList />
                    </ProtectedRoute>
                }/>

                <Route path="/list-requests" element={
                    <ProtectedRoute
                        roles={["accountant"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <AccountantList />
                    </ProtectedRoute>
                }/>

                <Route path="/my-requests" element={
                    <ProtectedRoute
                        roles={["accountant"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <AccountantMyList />
                    </ProtectedRoute>
                }/>
            </Routes>
        </>
    )
}
export default RoutesAccountant;