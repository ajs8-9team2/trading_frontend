// import {Context} from "../context";
import React, {useState} from "react";
import {Route, Routes} from "react-router-dom";
import AppToolbar from "../components/UI/AppToolbar/AppToolbar";
import {Container, CssBaseline} from "@mui/material";
import Main from "../containers/Main/Main";
import Login from "../containers/Login/Login";
import Register from "../containers/Register/Register";
import RoutesFrontOffice from "./RoutesFrontOffice";
import RoutesClient from "./RoutesClient";
import RoutesAccountant from "./RoutesAccountant";
import RoutesTrader from "./RoutesTrader";
import RestrictedMain from "./RestrictedMain";
import RestrictedPrev from "./RestrictedPrev"
import ProtectedRoute, {ProtectedRouteForClient} from "../components/UI/ProtectedRoute/ProtectedRoute";
import {TUserState} from "../types/typesUser";
import {useAppDispatch, useAppSelector} from "../hook";
import ClientOffice from "../containers/ClientOffice/ClientOffice";
import {TAccount, TGetPostAccountAT} from "../types/typesAccount";
import {getAccountAT} from "../store/services/accountSlice";
import {TStateStatus, TUser} from "../types/reestrOfAlias";

const App = ()=>{
    // const dispatch = useAppDispatch();
    // const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    // const oneAccount: TGetPostAccountAT = useAppSelector(state=>state.reducerAccount.oneAccount);
    // const accountStatus: TStateStatus = useAppSelector(state=>state.reducerAccount.accountStatus);
    // const [countNotice, setCountNotice] = useState<number>(0)
    // React.useEffect(()=>{
    //     const fetchData = async()=>{
    //         if(dataUser?._id)
    //         await dispatch(getAccountAT(dataUser?._id!));
    //     }
    //     fetchData();
    // }, []);

    // React.useEffect(()=>{
    //     if(oneAccount?.userNotifications)
    //         setCountNotice(oneAccount.userNotifications.reduce((sum: number, item: TNoticeObj)=>{
    //             let count = sum;
    //             if(!item.isWasRead)
    //                 count++;
    //             sum = count
    //             return sum;
    //         }, 0))
    // }, []);
    // const setCount = (count: number)=>{
    //     setCountNotice(count)
    // }
    // const value = {setCount, countNotice}

    const userData: TUser = useAppSelector(state=>state.reducerUsers.user!);

    return (
        <>
         {/*<Context.Provider value={value}>*/}
            <CssBaseline/>
            <header>
                {/*<pre>{JSON.stringify(countNotice, null, 2)}</pre>*/}
                <AppToolbar/>
            </header>
            <main>
                {/*md*/}
                {/*<Container*/}
                {/*    // maxWidth='xl'*/}

                {/*    // xs, extra-small: 0px*/}
                {/*    // sm, small: 600px*/}
                {/*    // md, medium: 900px*/}
                {/*    // lg, large: 1200px*/}
                {/*    // xl, extra-large: 1536px*/}

                {/*    disableGutters={true}*/}
                {/*    // fixed*/}
                {/*    // sx={{height: "700px", backgroundColor:"black"}}*/}
                {/*>*/}


                    <Routes>
                        {/* <<< роуты раделены по ролям и распределены на дочерние роуты >>> */}
                        <Route path="/client/*" element={<RoutesClient/>}/>
                        <Route path="/front-office/*" element={<RoutesFrontOffice/>}/>
                        {/*<Route path="/back-office/*" element={<RoutesBackOffice/>}/>*/}
                        <Route path="/trader/*" element={<RoutesTrader/>}/>
                        <Route path="/accountant/*" element={<RoutesAccountant/>}/>

                        <Route path={"/sign-up"} element={
                            <Register/>
                        }/>

                        <Route path={"/sign-in"} element={
                            <Login/>
                        }/>

                        <Route path="/" element={!userData?.email ? (<RestrictedMain />) : (<RestrictedPrev />)}/>

                    </Routes>

                {/*</Container>*/}
            </main>
          {/*</Context.Provider>*/}
    </>
    )
};

export default App;
