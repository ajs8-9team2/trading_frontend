import {useNavigate} from "react-router-dom";
import React, {useEffect} from "react";

const RestrictedPrev = ()=>{
    const navigate = useNavigate();
    useEffect(() => {
        navigate(-1);
    }, []);
    
    return (
        <></>     
    )
}
export default RestrictedPrev;