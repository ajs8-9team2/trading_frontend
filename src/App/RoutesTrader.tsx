import {Route, Routes} from "react-router-dom";
import TraderList from "../containers/Trader/TraderList";
import TraderMyList from "../containers/Trader/TraderMyList";
import ProtectedRoute from "../components/UI/ProtectedRoute/ProtectedRoute";

const RoutesTrader = ()=>{
    return (
        <>
            <Routes>
                <Route path="/" element={
                    <ProtectedRoute
                        roles={["trader"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <TraderList />
                    </ProtectedRoute>
                }/>

                <Route path="/list-requests" element={
                    <ProtectedRoute
                        roles={["trader"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <TraderList />
                    </ProtectedRoute>
                }/>

                <Route path="/my-requests" element={
                    <ProtectedRoute
                        roles={["trader"]}
                        isAllowed={false}
                        redirectUrl={"/sign-in"}
                    >
                        <TraderMyList />
                    </ProtectedRoute>
                }/>
            </Routes>
        </>
    )
}
export default RoutesTrader;