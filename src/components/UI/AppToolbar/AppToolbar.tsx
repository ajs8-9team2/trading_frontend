import React, {useState} from 'react';
import {AppBar, IconButton, Toolbar, Typography} from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import {useAppDispatch, useAppSelector} from "../../../hook";
import {TUser} from "../../../types/reestrOfAlias";
import HasAccess from '../HasAccess/HasAccess';

/**_____________________ ALL MENUS _____________________*/
import AnonymousMenu from "../../Menus/AnonymousMenu/AnonymousMenu";
import ClientMenu from '../../Menus/ClientMenu/ClientMenu';
import FrontOfficeMenu from "../../Menus/FrontOfficeMenu/FrontOfficeMenu";
import AccountantMenu from '../../Menus/AccountantMenu/AccountantMenu';
import BackOfficeMenu from '../../Menus/BackOfficeMenu/BackOfficeMenu';
import TraderMenu from "../../Menus/TraderMenu/TraderMenu";
import {useNavigate} from "react-router-dom";
import {deleteUsersSessionAT} from "../../../store/services/userSlice";

/**_____________________ ALL MENUS _____________________*/

const AppToolbar = ()=>{
    const userData: TUser = useAppSelector(state=>state.reducerUsers.user!);

    return <>
        {/***********************************************/}
        {/*<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>*/}
        {/*    {userData?._id?.toUpperCase()}*/}
        {/*</Typography>*/}

        {/*['client', 'admin', 'frontOffice', 'backOffice', 'trader', 'accountant']*/}

        {/*МЕНЮ ДЛЯ КЛИЕНТА */}
        <HasAccess roles={['client']}>
            <ClientMenu
                email={userData?.email!}
            />
        </HasAccess>

        {/*МЕНЮ ДЛЯ СОТРУДНИКА FRONTOFFICE*/}
        <HasAccess roles={['frontOffice']}>
            <FrontOfficeMenu
                email={userData?.email!}
                fullName={userData?.fullName!}
            />
        </HasAccess>

        {/*МЕНЮ ДЛЯ СОТРУДНИКА ACCOUNTANT*/}
        <HasAccess roles={['accountant']}>
            <AccountantMenu
                email={userData?.email!}
                fullName={userData?.fullName!}
            />
        </HasAccess>

        {/*МЕНЮ ДЛЯ СОТРУДНИКА BACKOFFICE*/}
        <HasAccess roles={['backOffice']}>
            <BackOfficeMenu userData={userData}/>
        </HasAccess>

        <HasAccess roles={['trader']}>
            <TraderMenu 
                email={userData?.email!}
                fullName={userData?.fullName!}/>
        </HasAccess>

        <HasAccess allowed={!userData?.email}>
            {/* roles={[]} */}
            <AnonymousMenu/>
        </HasAccess>
    </>
}

export default AppToolbar;


// {/* <AppBar position="static"> */ }
// {/*<pre>{JSON.stringify(userData, null, 2)}</pre>*/ }
// {/* <Toolbar variant="dense"> */ }
// {/*<IconButton*/ }
// {/*    size="large"*/ }
// {/*    edge="start"*/ }
// {/*    color="inherit"*/ }
// {/*    aria-label="menu"*/ }
// {/*    sx={{mr: 2}}*/ }
// {/*>*/ }
// {/*    <MenuIcon/>*/ }
// {/*</IconButton>*/ }
// {/* <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
//             TRADING
//         </Typography> */}

// {/***********************************************/ }


// {/*<HasAccess*/ }
// {/*    allowed={!!userData?.email}*/ }
// {/*    // roles={['user']}*/ }
// {/*>*/ }
// {/*    <UserMenu*/ }
// {/*        userData={userData}*/ }
// {/*    />*/ }
// {/*</HasAccess>*/ }

// {/*<HasAccess*/ }
// {/*    allowed={!userData?.email}*/ }
// {/*    // roles={[]}*/ }
// {/*>*/ }
// {/*    <AnonymousMenu/>*/ }
// {/*</HasAccess>*/ }

// {/* </Toolbar>
//         </AppBar> */}