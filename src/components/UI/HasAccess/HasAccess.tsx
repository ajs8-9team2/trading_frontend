import { useAppSelector } from "../../../hook";
import { TUser } from "../../../types/reestrOfAlias";
// type HasAccessType = {
//     allowed: boolean,
//     roles?: string,
//     children?: any
// }

const HasAccess = ({ allowed, roles, children }: any) => {
    const userData: TUser = useAppSelector(state => state.reducerUsers.user!);
    return allowed || roles?.includes(userData?.role) ? children : null;
    // return allowed ? children : null;
};
export default HasAccess;

export const HasAccessAccountOpening = ({ allowed, account, children }: any) => {
    return allowed || account ? null : children
}

export const HasAccessClientOffice = ({ allowed, account, children }: any) => {
    return allowed || account ? children : null;
}

// const HasAccess = ({allowed,roles, children}) => {
//     const user = useSelector(state => state.reducerUsers.users);
//     return allowed || roles?.includes(user?.role) ? children : null;
// };