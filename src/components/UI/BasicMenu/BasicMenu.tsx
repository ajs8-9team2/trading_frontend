import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';


// const ProtectedRoute = ({roles, isAllowed, redirectUrl, children}: any)=>{
//     const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user!);
//     if(isAllowed || roles?.includes(dataUser?.role)){
//         return children || <Outlet/>;
//     }
//     return <Navigate to={redirectUrl}/>
// };
// export default ProtectedRoute;
//
// export const ProtectedRouteForClient = ({ isAllowed, redirectUrl, children }: any) => {
//     if (!isAllowed) {
//         return <Navigate to={redirectUrl} />
//     }
//     return children || <Outlet />;
// };



export default function BasicMenu() {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            <Button
                id="basic-button"
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
            >
                {"Dashboard"}
            </Button>
            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                {/*{props.children}*/}
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleClose}>My account</MenuItem>
                <MenuItem onClick={handleClose}>Logout</MenuItem>
            </Menu>
        </div>
    );
}