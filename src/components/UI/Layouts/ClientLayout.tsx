import ClientSideBar from "../../../containers/ClientOffice/ClientSideBar";
import { Outlet } from "react-router-dom";
import { HasAccessClientOffice } from "../HasAccess/HasAccess";
import { useAppDispatch, useAppSelector } from "../../../hook";
import { TUserState } from "../../../types/typesUser";
import { createTheme, CssBaseline } from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';
import { useEffect } from "react";
import { paramsAccRequestAT } from "../../../store/services/accRequestSlice";

const theme = createTheme({
    palette: {
        background: {
            default: '#F9FAFC',
            paper: '#FFFFFF'
        },
    }
})

const ClientLayout = () => {
    const dataUser: TUserState = useAppSelector(state => state.reducerUsers);
    const dispatch = useAppDispatch();

    return <div style={{ display: 'flex', width: "100%" }}>
            <HasAccessClientOffice account={dataUser.user?.isHasAccount}>
                <ClientSideBar />
            </HasAccessClientOffice>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Outlet />
            </ThemeProvider>
        </div>

}

export default ClientLayout;