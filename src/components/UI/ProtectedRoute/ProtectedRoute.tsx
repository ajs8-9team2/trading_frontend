import {TUser} from "../../../types/reestrOfAlias";
import {useAppSelector} from "../../../hook";
import {Navigate, Outlet} from "react-router-dom";
import React from "react";

const ProtectedRoute = ({roles, isAllowed, redirectUrl, children}: any)=>{
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user!);
    if(isAllowed || roles?.includes(dataUser?.role)){
        return children || <Outlet/>;
    }
    return <Navigate to={redirectUrl}/>
};
export default ProtectedRoute;

export const ProtectedRouteForClient = ({ isAllowed, redirectUrl, children }: any) => {
    if (!isAllowed) {
      return <Navigate to={redirectUrl} />
    }
    return children || <Outlet />;
  };
  