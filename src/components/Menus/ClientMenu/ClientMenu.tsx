// import {deleteUsersSessionAT} from "../../../store/services/userSlice";
// import {Context} from "../../../context";
// import {useNavigate} from "react-router-dom";
import { NavLink, useNavigate } from "react-router-dom";
import { AppBar, Badge, Button, IconButton, Menu, MenuItem, Toolbar, Typography } from "@mui/material";
import HomeIcon from '@mui/icons-material/Home';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { red, grey } from '@mui/material/colors';
import { useAppDispatch, useAppSelector } from "../../../hook";
import { HasAccessAccountOpening, HasAccessClientOffice } from "../../UI/HasAccess/HasAccess";
import { TUserState } from "../../../types/typesUser";
import { queryAccReqClientAT } from "../../../store/services/accRequestSlice";
import React, { useEffect, useState } from "react";
import { TGetAccRequestsAT } from "../../../types/typesAccReq";
import { TEmail, TStateStatus } from "../../../types/reestrOfAlias";
import {createUsersSessionAT, deleteUsersSessionAT} from "../../../store/services/userSlice";
import { getNotificationAT, setRingState } from "../../../store/services/notificationSlice";
import { TGetPostNotificationAT, TNoticeObj } from "../../../types/typesNotification";
import { TGetPostAccountAT } from "../../../types/typesAccount";
import { getAccountAT } from "../../../store/services/accountSlice";
import {getOneUserAT} from "../../../store/services/usersDataSlice";

const ClientMenu: React.FC<{ email: TEmail }> = ({ email }) => {
    // const {stateBell} = React.useContext(Context)
    //------------------------------------------------------------
    const stateBell: 0 | 1 | 2 = useAppSelector(state => state.reducerNotification.stateBell);
    const oneNotification: TGetPostNotificationAT = useAppSelector(state => state.reducerNotification.oneNotification);
    const notificationStatus: TStateStatus = useAppSelector(state => state.reducerNotification.notificationStatus);
    const [notificationCount, setNotificationCount] = React.useState<number>(0)
    useEffect(() => {
        let innerCount = 0;
        if (notificationStatus === "resolved")
            innerCount = oneNotification?.userNotifications?.reduce((sum: number, item: TNoticeObj) => {
                let count = sum;
                if (!item.isWasRead)
                    count++;
                sum = count
                return sum;
            }, 0) ?? 0;

        switch (stateBell) {
            case 0:
                setNotificationCount(innerCount)
                break
            case 1:
                setNotificationCount(0)
                break
            case 2:
                dispatch(setRingState(0))
                break
            default:
                console.log("состояние стейта не соотвествует значению стейта!")
        }
    }, [oneNotification?.userNotifications?.length, notificationStatus, stateBell]);
    //------------------------------------------------------------
    const dataUser: TUserState = useAppSelector(state => state.reducerUsers);
    const oneAccRequest: TGetAccRequestsAT = useAppSelector(state => state.reducerAccRequests.oneAccRequest);

    // const allAccRequests: TGetAccRequestsAT[] = useAppSelector(state=>state.reducerAccRequests.allAccRequests);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: any) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const logout = () => {
        dispatch(deleteUsersSessionAT());
        handleClose();
        navigate("/")
    };

    const handleRequest = () => {
        handleClose();
        navigate("/client/client-request");
    }

    const handleMyProfile = () => {
        handleClose();
        navigate("/client/client-office");
    }

    const handleMyIbans = () => {
        handleClose();
        navigate("/client/client-iban");
    }

    const handleMyNotice = () => {
        handleClose();
        navigate("/client/client-notice");
    }

    useEffect(() => {
        if (dataUser.user?._id) {
            dispatch(getNotificationAT(dataUser.user._id))
            // dispatch(queryAccReqClientAT(dataUser.user._id))
        }
    }, []);

    const oneAccount: TGetPostAccountAT = useAppSelector(state=>state.reducerAccount.oneAccount);

    useEffect(()=>{
        dispatch(getAccountAT(dataUser?.user?._id!));
    }, [])



    return <>
        <AppBar sx={{ bgcolor: "#fff" }} position="static">
            <Toolbar variant="regular">

                <Typography variant="h6" component="div" sx={{ flexGrow: 1, color: "#000" }}>
                    TRADING
                    {/*<pre>{JSON.stringify(stateBell, null, 2)}</pre>*/}
                    {/*<pre>{JSON.stringify(notificationCount, null, 2)}</pre>*/}
                    {/*<pre>{JSON.stringify(notificationStatus, null, 2)}</pre>*/}
                </Typography>

                <IconButton
                    // size="large"
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    component={NavLink}
                    // color="inherit"
                    to="/client/client-notice"
                >
                    <Badge color="warning" badgeContent={notificationCount}>
                        {/*<Badge color="warning" badgeContent={accountStatus === "resolved" ? notificationCount : 0}>*/}
                        <NotificationsIcon
                            sx={!notificationCount ? { color: grey[700] } : { color: red[500] }}
                        />
                    </Badge>
                </IconButton>


                <IconButton
                    // size="large"
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    component={NavLink}
                    // color="inherit"
                    to="/client/">
                    <HomeIcon />
                </IconButton>

                <HasAccessAccountOpening account={dataUser.user?.isHasAccount}>
                    {oneNotification?.user?.path === "acc-pending" ? (
                        <p></p>
                    ) : <Button
                        component={NavLink}
                        sx={{ color: "#000" }}
                        // color="inherit"
                        to="/client/account-opening">
                        {"Открыть счет"}
                    </Button>}

                </HasAccessAccountOpening>
                {/* queryAccReqClientAT */}
                <Button
                    sx={{ color: "#000" }}
                    onClick={handleClick}
                    style={{textTransform: "uppercase"}}>
                    {(oneAccount?.firstName! ? oneAccount?.firstName! + "  " + oneAccount?.lastName! : dataUser.user?.email )}
                </Button>

                <Menu
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}>
                    {/*<MenuItem onClick={handleClose}>Profile</MenuItem>*/}
                    <HasAccessClientOffice account={dataUser.user?.isHasAccount}>
                        <MenuItem onClick={handleMyProfile}>Мой профиль</MenuItem>
                    </HasAccessClientOffice>

                    <HasAccessClientOffice account={dataUser.user?.isHasAccount}>
                        <MenuItem onClick={handleMyIbans}>Мои счета</MenuItem>
                    </HasAccessClientOffice>

                    <HasAccessClientOffice account={dataUser.user?.isHasAccount}>
                        <MenuItem onClick={handleRequest}>Мои заявки</MenuItem>
                    </HasAccessClientOffice>

                    {/* <HasAccessClientOffice account={dataUser.user?.isHasAccount}>
                        <MenuItem onClick={handleMyNotice}>Мои уведомления</MenuItem>
                    </HasAccessClientOffice> */}

                    <MenuItem onClick={logout}>Выйти</MenuItem>
                </Menu>
            </Toolbar>
        </AppBar>

    </>
};

export default ClientMenu;

