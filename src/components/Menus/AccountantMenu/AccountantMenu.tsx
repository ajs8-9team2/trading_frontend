import React, { useState } from 'react';
import { NavLink, useNavigate } from "react-router-dom";
import {Button, Menu, MenuItem, AppBar, Toolbar, Typography, ThemeProvider} from "@mui/material";
import { deleteUsersSessionAT } from "../../../store/services/userSlice";
import { useAppDispatch } from "../../../hook";
import { TEmail } from "../../../types/reestrOfAlias";
import { createTheme } from "@material-ui/core/styles";

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const AccountantMenu: React.FC<{ email: TEmail , fullName: string}> = ({ email, fullName }) => {

    const dispatch = useAppDispatch();

    const [anchorEl1, setAnchorEl1] = useState(null);
    const [anchorEl2, setAnchorEl2] = useState(null);
    const [anchorEl3, setAnchorEl3] = useState(null);

    const open1 = Boolean(anchorEl1);
    const open2 = Boolean(anchorEl2);
    const open3 = Boolean(anchorEl3);

    const handleClick1 = (event: any) => { setAnchorEl1(event.currentTarget); };
    const handleClick2 = (event: any) => { setAnchorEl2(event.currentTarget); };
    const handleClick3 = (event: any) => { setAnchorEl3(event.currentTarget); };

    const handleClose1 = () => { setAnchorEl1(null); };
    const handleClose2 = () => { setAnchorEl2(null); };
    const handleClose3 = () => { setAnchorEl3(null); };



    const navigate = useNavigate();
    const logout = () => {
        dispatch(deleteUsersSessionAT());
        handleClose3();
        navigate("/")
    };

    const navigateToMyRequests = () => {
        handleClose1();
        navigate("/accountant/my-requests")
    }

    const navigateToListRequests = () => {
        handleClose2();
        navigate("/accountant/list-requests")
    }

    return <>
    <ThemeProvider theme={theme}>
        <AppBar position="static">

            <Toolbar variant="dense">
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    TRADING
                </Typography>

                <Button
                    component={NavLink}
                    color="inherit"
                    to="/accountant/my-requests">
                    {"Мои заявки"}
                </Button>

                <Button
                    component={NavLink}
                    color="inherit"
                    to="/accountant/list-requests">
                    {"Список заявок"}
                </Button>

                {/*---------------------------------------------*/}

                {/* <Button
                    id="basic-button_1"
                    aria-controls={open1 ? 'basic-menu_1' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open1 ? 'true' : undefined}
                    onClick={handleClick1}
                    color="inherit"
                    style={{ textTransform: "uppercase" }}>
                    {"Мои заявки"}
                </Button>
                <Menu
                    id="basic-menu_1"
                    anchorEl={anchorEl1}
                    open={open1}
                    onClose={handleClose1}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button_1',
                    }}>
                    <MenuItem onClick={navigateToMyRequests}>Пополнение счета</MenuItem>
                    <MenuItem onClick={handleClose1} disabled={true}>Перевод средств</MenuItem>
                    <MenuItem onClick={handleClose1} disabled={true}>Счета клиентов</MenuItem>
                </Menu> */}

                {/*---------------------------------------------*/}

                {/* <Button
                    id="basic-button_2"
                    aria-controls={open2 ? 'basic-menu_2' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open2 ? 'true' : undefined}
                    onClick={handleClick2}
                    color="inherit"
                    style={{ textTransform: "uppercase" }}>
                    {"Список заявок"}
                </Button>
                <Menu
                    id="basic-menu_2"
                    anchorEl={anchorEl2}
                    open={open2}
                    onClose={handleClose2}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button_2',
                    }}>
                    <MenuItem onClick={navigateToListRequests}>Пополнение счета</MenuItem>
                    <MenuItem onClick={handleClose2} disabled={true}>Перевод средств</MenuItem>
                    <MenuItem onClick={handleClose2} disabled={true}>Счета клиентов</MenuItem>
                </Menu> */}

                {/*---------------------------------------------*/}

                <Button
                    id="basic-button_3"
                    aria-controls={open3 ? 'basic-menu_3' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open3 ? 'true' : undefined}
                    onClick={handleClick3}
                    color="inherit"
                    style={{textTransform: "uppercase"}}>
                    {(fullName! ? fullName! : email )}
                </Button>
                <Menu
                    id="basic-menu_3"
                    anchorEl={anchorEl3}
                    open={open3}
                    onClose={handleClose3}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button_3',
                    }}>
                    <MenuItem onClick={logout}>Logout</MenuItem>
                </Menu>
            </Toolbar>

        </AppBar>
    </ThemeProvider>
    </>
};

export default AccountantMenu;

