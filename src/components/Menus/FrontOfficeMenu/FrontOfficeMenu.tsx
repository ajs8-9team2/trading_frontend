import React, {useState} from 'react';
import {NavLink, useNavigate} from "react-router-dom";
import {Button, Menu, MenuItem, AppBar, Toolbar, Typography, ThemeProvider} from "@mui/material";
import {deleteUsersSessionAT} from "../../../store/services/userSlice";
import {useAppDispatch} from "../../../hook";
import {TEmail} from "../../../types/reestrOfAlias";
import {createTheme} from "@material-ui/core/styles";

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const FrontOfficeMenu: React.FC<{ email: TEmail , fullName: string}> = ({email, fullName})=>{
    const dispatch = useAppDispatch();

    const [anchorEl1, setAnchorEl1] = useState(null);
    const [anchorEl2, setAnchorEl2] = useState(null);
    const [anchorEl3, setAnchorEl3] = useState(null);

    const open1 = Boolean(anchorEl1);
    const open2 = Boolean(anchorEl2);
    const open3 = Boolean(anchorEl3);

    const handleClick1 = (event: any)=>{
        setAnchorEl1(event.currentTarget);
    };
    const handleClick2 = (event: any)=>{
        setAnchorEl2(event.currentTarget);
    };
    const handleClick3 = (event: any)=>{
        setAnchorEl3(event.currentTarget);
    };

    const handleClose1 = ()=>{
        setAnchorEl1(null);
    };
    const handleClose2 = ()=>{
        setAnchorEl2(null);
    };
    const handleClose3 = ()=>{
        setAnchorEl3(null);
    };

// handleClose1 /////////////////////////////////////////////////////////////////////////////////
    const navigateToMyRequests = ()=>{
        handleClose1();
        navigate("/front-office/my-requests")
    }

    const navigateToMyDeposit = ()=>{
        handleClose1();
        navigate("/front-office/my-deposit")
    }

    const navigateToMyTransfer = ()=>{
        handleClose1();
        navigate("/front-office/my-transfer")
    }

    const navigateToMyStock = ()=>{
        handleClose1();
        navigate("/front-office/my-stock")
    }

// handleClose2 /////////////////////////////////////////////////////////////////////////////////
    const navigateToListRequests = ()=>{
        handleClose2();
        navigate("/front-office/list-requests")
    }

    const navigateToListDeposit = ()=>{
        handleClose2();
        navigate("/front-office/list-deposit")
    }

    const navigateToListTransfer = ()=>{
        handleClose2();
        navigate("/front-office/list-transfer")
    }

    const navigateToListStock = ()=>{
        handleClose2();
        navigate("/front-office/list-stock")
    }
// handleClose3 /////////////////////////////////////////////////////////////////////////////////
    const navigate = useNavigate();
    const logout = ()=>{
        dispatch(deleteUsersSessionAT());
        handleClose3();
        navigate("/")
    };

    return <>
        <ThemeProvider theme={theme}>
        <AppBar position="static">
            <Toolbar variant="dense">
{/*#################################################################################################*/}
                <Typography variant="h6" component="div" sx={{flexGrow: 1}}>
                    TRADING
                </Typography>
{/*#################################################################################################*/}
{/*#################################################################################################*/}
                <Button
                    id="basic-button_1"
                    aria-controls={open1 ? 'basic-menu_1' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open1 ? 'true' : undefined}
                    onClick={handleClick1}
                    color="inherit"
                    style={{textTransform: "uppercase"}}>
                    {"Мои заявления"}
                </Button>
                <Menu
                    id="basic-menu_1"
                    anchorEl={anchorEl1}
                    open={open1}
                    onClose={handleClose1}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button_1',
                    }}>
{/*<MenuItem onClick={handleClose1} disabled={true}>Все мои заявления</MenuItem>*/}
<MenuItem onClick={navigateToMyRequests} disabled={false}>Мои заявления на открытие счета </MenuItem>
<MenuItem onClick={navigateToMyDeposit} disabled={false}>Мои заявления на пополнение счета</MenuItem>
<MenuItem onClick={navigateToMyTransfer} disabled={false}>Мои заявления на перевод денег</MenuItem>
<MenuItem onClick={navigateToMyStock} disabled={false}>Мои заявления по акциям</MenuItem>
                </Menu>
{/*#################################################################################################*/}
{/*#################################################################################################*/}
                <Button
                    id="basic-button_2"
                    aria-controls={open2 ? 'basic-menu_2' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open2 ? 'true' : undefined}
                    onClick={handleClick2}
                    color="inherit"
                    style={{textTransform: "uppercase"}}>
                    {"Список заявлений"}
                </Button>
                <Menu
                    id="basic-menu_2"
                    anchorEl={anchorEl2}
                    open={open2}
                    onClose={handleClose2}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button_2',
                    }}>
                    {/*{props.children}*/}
{/*<MenuItem onClick={handleClose2} disabled={true}>Все заявления</MenuItem>*/}
<MenuItem onClick={navigateToListRequests} disabled={false}>Заявление на открытие счета</MenuItem>
<MenuItem onClick={navigateToListDeposit} disabled={false}>Заявление на пополнение счета</MenuItem>
<MenuItem onClick={navigateToListTransfer} disabled={false}>Заявление на перевод денег</MenuItem>
<MenuItem onClick={navigateToListStock} disabled={false}>Заявления по акциям</MenuItem>
                </Menu>
{/*#################################################################################################*/}
{/*#################################################################################################*/}
                <Button
                    id="basic-button_3"
                    aria-controls={open3 ? 'basic-menu_3' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open3 ? 'true' : undefined}
                    onClick={handleClick3}
                    color="inherit"
                    style={{textTransform: "uppercase"}}>
                    {(fullName! ? fullName! : email )}
                </Button>
                <Menu
                    id="basic-menu_3"
                    anchorEl={anchorEl3}
                    open={open3}
                    onClose={handleClose3}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button_3',
                    }}>
                    <MenuItem onClick={logout}>Выйти</MenuItem>
                </Menu>
{/*#################################################################################################*/}
{/*#################################################################################################*/}
            </Toolbar>
        </AppBar>
        </ThemeProvider>
    </>
};

export default FrontOfficeMenu;

