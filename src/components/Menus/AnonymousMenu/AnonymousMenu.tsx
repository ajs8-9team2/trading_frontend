import { Button, AppBar, Toolbar, Typography, listItemTextClasses } from "@mui/material";
import { NavLink } from "react-router-dom";
import { useAppDispatch } from "../../../hook";
import { restartAllStates } from "../../../store/services/userSlice";
import Image from '../../../images/main.jpg';
import React, { useEffect, useState } from 'react';
import Footer from "../../Footer/Footer";
import './AnonymousMenu.css';
import icon1 from './img/icons1.png';
import icon2 from './img/icons2.png';
import icon3 from './img/icons3.png';

const AnonymousMenu: React.FC = () => {
    const dispatch = useAppDispatch();
    const restartStates = () => {
        dispatch(restartAllStates());
    }
    let [date, setDate] = useState(new Date());
    let days = [
        'Воскресенье',
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота'
    ];
    let n = date.getDay();

    useEffect(() => {
        let timer = setInterval(() => setDate(new Date()), 1000)
        return function cleanup() {
            clearInterval(timer)
        }

    });

    return (
        <>
            <AppBar sx={{ bgcolor: "#fff" }} position="static">
                <Toolbar variant="dense">
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1, color: "#000", fontSize: "15px", fontFamily: 'Roboto, sans-serif' }}>
                        TRADING
                    </Typography>
                    <p style={{ color: "#797777", marginRight: "20px", fontSize: "15px", fontFamily: 'Roboto, sans-serif' }}>{days[n]}, {date.toLocaleString('ru',
                        {
                            day: 'numeric',
                            month: 'long',
                        })}  {date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}
                    </p>
                    <Button
                        component={NavLink}
                        sx={{ color: "#000", textTransform: "capitalize", marginRight: "20px", fontSize: "15px", fontFamily: 'Roboto, sans-serif' }}
                        to="/"
                    >
                        Главная
                    </Button>
                    <Button
                        onClick={restartStates}
                        component={NavLink}
                        sx={{ color: "#000", textTransform: "capitalize", marginRight: "20px", fontSize: "15px", fontFamily: 'Roboto, sans-serif' }}
                        to="/sign-up">
                        Регистрация
                    </Button>
                    <Button
                        onClick={restartStates}
                        component={NavLink}
                        sx={{ color: "#000", textTransform: "capitalize", fontSize: "15px", fontFamily: 'Roboto, sans-serif' }}
                        to="/sign-in">
                        Войти
                    </Button>
                </Toolbar>
            </AppBar>
            <div style={{
                position: `relative`,
                height: `650px`,
                backgroundImage: `url(${Image})`,
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                width: `100%`,
                marginTop: '2px'
            }}
            >
                <div className="about_company">
                    <h1 className="about_company__title">
                        ИНВЕСТИРУЙТЕ, СОХРАНЯЙТЕ, ЗАРАБАТЫВАЙТЕ
                    </h1>
                    <div className="about_company__text">
                        Полнофункциональный брокерский счёт.
                        Доступ на международные рынки ценных бумаг.
                        Полная консультационная поддержка.
                    </div>
                    <button className="service_btn_company" >Перейти на страницу услуг</button>
                </div>
            </div>

            <section className="services">
                <div className="container">
                    <div className="services__top">
                        <div className="services__title-box">
                            <div className="news__title ">
                                Наши Услуги
                            </div>
                            <div className="services__text">
                                Мы создали собственные технологичные продукты, которые позволят Вам подбирать лучшие инвестиционные продукты, совершать сделки в любой точке мира и в любое время.

                            </div>

                        </div>
                        <div className="services__btn">
                            <a href="#">
                                Показать все услуги
                            </a>
                        </div>
                    </div>
                    <div className="services__items">
                        <div className="services__item">
                            <img src={icon1} width="50" height="50" />
                            <div className="services__item-title">
                                Брокерские услуги на рынке ценных бумаг Казахстана
                            </div>
                            <div className="services__item-text">
                                Осуществляйте сделки с ценными бумагами в Казахстане легко и удобно по конкурентным комиссиям.
                            </div>

                        </div>
                        <div className="services__item">
                            <img src={icon2} width="50" height="50" />
                            <div className="services__item-title">
                                Брокерские услуги на международном финансовом рынке
                            </div>
                            <div className="services__item-text">
                                Партнерами компании являются крупнейшие инвестиционные дома и онлайн-брокеры, что позволяет рассчитывать клиентам компании на лучшие условия обслуживания на международном финансовом рынке.
                            </div>

                        </div>
                        <div className="services__item">
                            <img src={icon3} width="50" height="50" />
                            <div className="services__item-title">
                                Организация корпоративных размещений
                            </div>
                            <div className="services__item-text">
                                Благодаря налаженной инфраструктуре, компания поможет эмитентам на всех этапах выпуска ценных бумаг – с момента регистрации соответствующих проспектов и прохождения биржевого листинга до фактического размещения среди инвесторов.
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section className="about">
                <div className="container">
                    <div className="about__inner">
                        <div className="about__title">
                            О компании
                        </div>
                        <div className="about__text">
                            Компания работает на казахстанском рынке ценных бумаг более 20 лет, имеет все необходимые лицензии и в настоящее время располагает существенными накопленными ресурсами для предоставления полного спектра инвестиционных услуг.
                        </div>
                        <button className="service_btn" >Узнать больше</button>
                    </div>
                </div>
            </section>


            <section className="form">
                <div className="container">
                    <div className="form__inner">
                        <div className="form__content">
                            <div className="form__title-box">
                                <div className="form__title">
                                    Получить Консультацию
                                </div>
                                <div className="form__text">
                                    Не сомневайтесь и свяжитесь с нами для получения подробной консультации. После отправки Ваших контактных данных наши специалисты свяжутся с Вами в кратчайший срок.
                                </div>
                            </div>
                            <div className="form__box">
                                <form>
                                    <div className="form__box-inner">
                                        <div className="form__box-left">
                                            <label>
                                                E-mail
                                                <input type="text" />
                                            </label>
                                            <label>
                                                Контактный телефон
                                                <input type="text" />
                                            </label>
                                            <label>
                                                ФИО
                                                <input type="text" />
                                            </label>

                                        </div>
                                        <div className="form__box-right">
                                            <label>
                                                Сообщение
                                                <textarea></textarea>
                                            </label>
                                            <button className="service_btn" type="submit">Получить консультацию</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </>

    );
};


export default AnonymousMenu;
