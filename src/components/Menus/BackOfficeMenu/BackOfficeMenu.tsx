import { useState } from 'react';
import { NavLink } from "react-router-dom";
import { Button, Menu, MenuItem, AppBar, Toolbar, Typography } from "@mui/material";
import { deleteUsersSessionAT } from "../../../store/services/userSlice";
import { useAppDispatch } from "../../../hook";
import { useNavigate } from "react-router-dom";

const BackOfficeOffice = ({ userData }: any) => {
    const dispatch = useAppDispatch();
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: any) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const navigate = useNavigate();
    const logout = () => {
        dispatch(deleteUsersSessionAT());
        handleClose();
        navigate("/")
    };

    return <>
        <AppBar position="static">
            <Toolbar variant="dense">
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    TRADING
                </Typography>

                <Button
                    component={NavLink}
                    color="inherit"
                    to="back-office/*">
                    {"****"}
                </Button>

                <Button
                    color="inherit"
                    onClick={handleClick}
                    style={{ textTransform: "lowercase" }}>
                    {userData?.email}
                </Button>

                <Menu
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}>
                    {/*<MenuItem onClick={handleClose}>Profile</MenuItem>*/}
                    {/*<MenuItem onClick={handleClose}>My account</MenuItem>*/}
                    <MenuItem onClick={logout}>Logout</MenuItem>
                </Menu>
            </Toolbar>
        </AppBar>
    </>
};

export default BackOfficeOffice;