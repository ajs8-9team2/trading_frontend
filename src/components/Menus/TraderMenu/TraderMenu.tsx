import { useState } from 'react';
import { NavLink, useNavigate } from "react-router-dom";
import { Button, Menu, MenuItem, AppBar, Toolbar, Typography, ThemeProvider } from "@mui/material";
import { deleteUsersSessionAT } from "../../../store/services/userSlice";
import { useAppDispatch } from "../../../hook";
import { TEmail } from "../../../types/reestrOfAlias";
import { createTheme } from "@material-ui/core/styles";

const theme = createTheme({
    palette: {
        primary: { main: "#263238" },
        secondary: { main: '#11cb5f' },
    },
});

const TraderMenu: React.FC<{ email: TEmail , fullName: string}> = ({ email, fullName }) => {
    const dispatch = useAppDispatch();

    const [anchorEl, setAnchorEl] = useState(null);

    const open = Boolean(anchorEl);

    const handleClick = (event: any) => { setAnchorEl(event.currentTarget); };

    const handleClose = () => { setAnchorEl(null); };

    const navigate = useNavigate();
    const logout = () => {
        dispatch(deleteUsersSessionAT());
        handleClose();
        navigate("/")
    };


    return <>
    <ThemeProvider theme={theme}>
        <AppBar position="static">

            <Toolbar variant="dense">
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    TRADING
                </Typography>

                <Button
                    component={NavLink}
                    color="inherit"
                    to="/trader/my-requests">
                    {"Мои заявки"}
                </Button>

                <Button
                    component={NavLink}
                    color="inherit"
                    to="/trader/list-requests">
                    {"Список заявок"}
                </Button>

                <Button
                    id="basic-button"
                    aria-controls={open ? 'basic-menu' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open ? 'true' : undefined}
                    onClick={handleClick}
                    color="inherit"
                    style={{textTransform: "uppercase"}}>
                    {(fullName! ? fullName! : email )}
                </Button>
                <Menu
                    id="basic-menu_3"
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}>
                    <MenuItem onClick={logout}>Logout</MenuItem>
                </Menu>
            </Toolbar>

        </AppBar>
    </ThemeProvider>
    </>
};

export default TraderMenu;