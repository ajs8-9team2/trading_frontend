import * as React from 'react';
import { Button, Card, CardContent, Typography,CardActions } from '@mui/material';
import { TGetAccRequestsAT } from '../../types/typesAccReq';


type TPropsClientAccountRequest = {
    item: TGetAccRequestsAT,
    viewAccountRequest: any,
}

export default function ClientAccountRequest({
    item,
    viewAccountRequest,
}: TPropsClientAccountRequest) {
    
    return <>
        <Card
            key={item?._id!}
            sx={{ minWidth: 600, marginBottom: "20px" }}
            onClick={() => viewAccountRequest(item?._id!)}
        >
            <CardContent>
                <Typography variant="h5" component="div">
                    # {item?._id!}
                </Typography>
                <Typography variant="body2">
                    Статус: {item?.status!}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" onClick={() => viewAccountRequest(item?._id!)}>Посмотреть
                    заявку</Button>
            </CardActions>
        </Card>
    </>
}