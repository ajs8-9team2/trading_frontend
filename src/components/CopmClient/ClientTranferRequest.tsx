import * as React from 'react';
import { Button, Card, CardContent, Typography, CardActions } from '@mui/material';
import { TGetPostTransferReqAT,  } from '../../types/typesTransferRequest';
import { Link } from "react-router-dom";

type TPropsClientTransferRequest = {
    item: TGetPostTransferReqAT,

}

export default function ClientTranferRequest({
    item,

}: TPropsClientTransferRequest) {
    console.log(item);
    const id = item?._id!;

    return <>
        <Card
            key={item?._id!}
            sx={{ minWidth: 600, marginBottom: "20px" }}
        >
            <CardContent>
                <Typography variant="h5" component="div">
                    # {item?._id!}
                </Typography>
                <Typography variant="body2">
                    Статус: {item?.status!}
                </Typography>
            </CardContent>

            <CardActions>
                <Button component={Link} to={`/client/client-request/transferRequests/${id}`} variant="text" >Посмотреть
                    заявку</Button>
            </CardActions>
        </Card>
    </>
}