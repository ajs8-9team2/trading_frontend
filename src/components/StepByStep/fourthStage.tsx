import * as React from 'react';
import {Grid, Typography, TextField, Button, InputLabel, MenuItem, FormControl, ThemeProvider} from '@mui/material';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { TRegisterPropsFourthStage } from '../../types/typesAccReq';
import "./styles.css";
import {createTheme} from "@material-ui/core/styles";
import { cutLength } from '../../Helpers/cutLength';

const theme = createTheme({
    palette: {
        primary: { main: "#263238", contrastText: "white"},
    },
});

export default function FourthStage({
    state,
    onChange,
    fileChangeHandler,
    selectFourthChangeHandler,
    ObjDocumentFront,
    ObjDocumentBack,
    ObjDateOfIssue,
    ObjExpirationDate,
    ObjIdCardNumber,
    ObjCitizenship,
    ObjAuthority,
    errorsSecond }: TRegisterPropsFourthStage) {
    const [filename, setFilename] = React.useState("");
    const [filename2, setFilename2] = React.useState("");
    const [firstSelect, setFirstSelect] = React.useState('');
    const [secondSelect, setSecondSelect] = React.useState('');

    const onFileChange = (e: any) => {
        const file = e.target.files[0];
        if (file) {
            setFilename(file.name);
        } else {
            setFilename("");
        }
        fileChangeHandler(e);
    };

    const onFileChange2 = (e: any) => {
        const file = e.target.files[0];
        if (file) {
            setFilename2(file.name);
        } else {
            setFilename2("");
        }
        fileChangeHandler(e);
    };

    const handleChangeFirstSelect = (e: SelectChangeEvent) => {
        setFirstSelect(e.target.value);
        selectFourthChangeHandler(e);
    };

    const handleChangeSecondSelect = (e: SelectChangeEvent) => {
        setSecondSelect(e.target.value);
        selectFourthChangeHandler(e);
    }

    const clearAuthority = () => {
        state.authority = "";
    }

    return <>
        <React.Fragment>

            <Typography variant="h6" gutterBottom>
                Удостоверение личности
            </Typography>
            <Grid container spacing={3}>

                <Grid item xs={12} sm={6}>
                    <div className="upload">
                        <ThemeProvider theme={theme}>
                        <Button type="button" className="btn-warning" variant="contained">
                            {filename ? cutLength(filename, 20) : "Передняя сторона"}
                            <input
                                type="file"
                                name="documentFront"
                                {...ObjDocumentFront}
                                onChange={onFileChange}
                                accept=".png, .jpg, .jpeg" />
                        </Button>
                        </ThemeProvider>
                    </div>
                    {errorsSecond.documentFront && <div className="error">{errorsSecond?.documentFront?.message}</div>}
                </Grid>
                <Grid item xs={12} sm={6}>
                    <div className="upload" >
                        <ThemeProvider theme={theme}>
                        <Button type="button" className="btn-warning" variant="contained">
                            {filename2 ? cutLength(filename2, 20) : "Задняя сторона"}
                            <input
                                type="file"
                                name="documentBack"
                                {...ObjDocumentBack}
                                onChange={onFileChange2}
                                accept=".png, .jpg, .jpeg" />
                        </Button>
                        </ThemeProvider>
                    </div>
                    {errorsSecond.documentBack && <div className="error">{errorsSecond?.documentBack?.message}</div>}
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl sx={{ minWidth: 220 }}>
                        <InputLabel id="demo-simple-select-helper-label" sx={{ width: "120px",bgcolor: "#F9FAFC"}}>Гражданство</InputLabel>
                        <Select
                            {...ObjCitizenship}
                            labelId="demo-simple-select-helper-label"
                            id="demo-simple-select-helper"
                            name="citizenship"
                            value={firstSelect}
                            onChange={handleChangeFirstSelect}
                            error={!!errorsSecond?.citizenship?.message}
                            helpertext={errorsSecond?.citizenship?.message}>
                            <MenuItem onClick={clearAuthority} value={"resident"}>Гражданин РК</MenuItem>
                            <MenuItem onClick={clearAuthority} value={"not-resident"}>Иностранный Гражданин</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                {firstSelect ?
                    firstSelect === "resident"
                        ?
                        <Grid item xs={12} sm={6}>
                            <FormControl sx={{ minWidth: 220 }}>
                                <InputLabel id="demo-simple-select-helper-label" sx={{ width: "120px",bgcolor: "#F9FAFC"}}>Кем выдано</InputLabel>
                                <Select
                                    {...ObjAuthority}
                                    labelId="demo-simple-select-helper-label"
                                    id="demo-simple-select-helper"
                                    name="authority"
                                    value={secondSelect}
                                    onChange={handleChangeSecondSelect}
                                    error={!!errorsSecond?.authority?.message}
                                    helpertext={errorsSecond?.authority?.message}>
                                    <MenuItem value={"МЮ РК"}>МЮ РК</MenuItem>
                                    <MenuItem value={"МВД РК"}>МВД РК</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        :
                        <Grid item xs={12} sm={6}>
                            <TextField
                                {...ObjAuthority}
                                required
                                id="authority"
                                name="authority"
                                label="Кем выдано"
                                value={state.authority}
                                onChange={onChange}
                                fullWidth
                                autoComplete="given-name"
                                variant="standard"
                                error={!!errorsSecond?.authority?.message}
                                helperText={errorsSecond?.authority?.message}
                            />
                        </Grid>
                    :
                    <Grid item xs={12} sm={6}>
                        <FormControl sx={{ minWidth: 220 }}>
                            <InputLabel id="demo-simple-select-helper-label" sx={{ width: "220px",bgcolor: "#F9FAFC"}}>Кем выдано</InputLabel>
                            <Select
                                {...ObjAuthority}
                                labelId="demo-simple-select-helper-label"
                                id="demo-simple-select-helper"
                                name="authority"
                                value={secondSelect}
                                onChange={handleChangeSecondSelect}
                                error={!!errorsSecond?.authority?.message}
                                helpertext={errorsSecond?.authority?.message}
                                disabled>
                                <MenuItem value={"МЮ РК"}>МЮ РК</MenuItem>
                                <MenuItem value={"МВД РК"}>МВД РК</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                }

                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjDateOfIssue}
                        required
                        id="dateOfIssue"
                        name="dateOfIssue"
                        label="Дата выдачи"
                        type="date"
                        value={state.dateOfIssue}
                        onChange={onChange}
                        fullWidth
                        variant="standard"
                        error={!!errorsSecond?.dateOfIssue?.message}
                        helperText={errorsSecond?.dateOfIssue?.message}
                        sx={{ width: 220 }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid>
                
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjExpirationDate}
                        required
                        id="expirationDate"
                        name="expirationDate"
                        label="Срок выдачи"
                        type="date"
                        value={state.expirationDate}
                        onChange={onChange}
                        fullWidth
                        variant="standard"
                        error={!!errorsSecond?.expirationDate?.message}
                        helperText={errorsSecond?.expirationDate?.message}
                        sx={{ width: 220 }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjIdCardNumber}
                        required
                        id="idCardNumber"
                        name="idCardNumber"
                        label="Номер уд.личности"
                        type="tel"
                        value={state.idCardNumber}
                        onChange={onChange}
                        fullWidth
                        variant="standard"
                        error={!!errorsSecond?.idCardNumber?.message}
                        helperText={errorsSecond?.idCardNumber?.message}
                        inputProps={{ maxLength: 9 }}
                    />
                </Grid>
            </Grid>

        </React.Fragment>
    </>
}