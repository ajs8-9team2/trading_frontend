import * as React from 'react';
import { Typography, Grid, TextField } from '@mui/material';
import { TRegisterPropsSecondStage } from '../../types/typesAccReq';

export default function SecondStage({ state, onChange }: TRegisterPropsSecondStage) {
    return <>
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Адрес проживания
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="country"
                        name="country"
                        label="Страна"
                        value={state.country}
                        onChange={onChange}
                        fullWidth
                        variant="standard"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="area"
                        name="area"
                        label="Область"
                        value={state.area}
                        onChange={onChange}
                        fullWidth
                        variant="standard"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="city"
                        name="city"
                        label="Город"
                        value={state.city}
                        onChange={onChange}
                        fullWidth
                        variant="standard"
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        id="address"
                        name="address"
                        label="Адрес"
                        value={state.address}
                        onChange={onChange}
                        placeholder="Улица/Дом/Квартира"
                        fullWidth
                        variant="standard"
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    </>
}