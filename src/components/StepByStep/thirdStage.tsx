import * as React from 'react';
import { Typography, Grid, TextField, Input, InputLabel } from '@mui/material';
import { TRegisterPropsThirdStage } from '../../types/typesAccReq';
import { IMaskInput } from 'react-imask';

interface CustomProps {
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
}

const TextMaskCustom = React.forwardRef<HTMLElement, CustomProps>(
    function TextMaskCustom(props, ref) {
        const inputRef = React.useRef(null);
        const { onChange, ...other } = props;
        return (
            <IMaskInput
                {...other}
                mask="+7 (#00) 000-0000"
                definitions={{
                    '#': /[1-9]/,
                }}
                inputRef={inputRef}
                onAccept={(value: any) => onChange({ target: { name: props.name, value } })}
                overwrite
            />
        );
    },
);

export default function ThirdStage({ state, onChange }: TRegisterPropsThirdStage) {

    return <>
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Контактные данные
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        id="email"
                        name="email"
                        label="Email"
                        value={state.email}
                        onChange={onChange}
                        fullWidth
                        autoComplete="cc-name"
                        variant="standard"
                    />
                </Grid>
                <Grid item xs={12}>
                    <InputLabel htmlFor="phone">Номер телефона</InputLabel>
                    <Input
                        id="phone"
                        name="phone"
                        value={state.phone}
                        onChange={onChange}
                        inputComponent={TextMaskCustom as any}
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    </>
}