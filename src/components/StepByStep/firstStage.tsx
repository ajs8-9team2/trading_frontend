import * as React from 'react';
import { Grid, Typography, TextField } from '@mui/material';
import { TRegisterPropsFirstStage } from '../../types/typesAccReq';

export default function FirstStage({
    state,
    onChange,
    ObjFirstName,
    ObjLastName,
    ObjBirthday,
    ObjIdentificationNumber,
    errors
}: TRegisterPropsFirstStage) {

    return <>
        <React.Fragment >
            <Typography variant="h6" gutterBottom>
                Данные о себе
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjFirstName}
                        required
                        id="firstName"
                        name="firstName"
                        label="Имя"
                        value={state.firstName}
                        onChange={onChange}
                        fullWidth
                        autoComplete="given-name"
                        variant="standard"
                        error={!!errors?.firstName?.message}
                        helperText={errors?.firstName?.message}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjLastName}
                        required
                        id="lastName"
                        name="lastName"
                        label="Фамилия"
                        value={state.lastName}
                        onChange={onChange}
                        fullWidth
                        autoComplete="family-name"
                        variant="standard"
                        error={!!errors?.lastName?.message}
                        helperText={errors?.lastName?.message}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="patronym"
                        name="patronym"
                        label="Отчество"
                        value={state.patronym}
                        onChange={onChange}
                        fullWidth
                        variant="standard"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjBirthday}
                        required
                        id="dateOfBirth"
                        name="dateOfBirth"
                        label="День рождения"
                        type="date"
                        value={state.dateOfBirth}
                        onChange={onChange}
                        fullWidth
                        variant="standard"
                        error={!!errors?.dateOfBirth?.message}
                        helperText={errors?.dateOfBirth?.message}
                        sx={{ width: 220 }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjIdentificationNumber}
                        required
                        id="identificationNumber"
                        name="identificationNumber"
                        label="ИИН"
                        type="tel"
                        value={state.identificationNumber}
                        onChange={onChange}
                        fullWidth
                        variant="standard"
                        inputProps={{ maxLength: 12 }}
                        error={!!errors?.identificationNumber?.message}
                        helperText={errors?.identificationNumber?.message}
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    </>
}