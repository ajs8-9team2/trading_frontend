import * as React from 'react';
import { Grid,  TextField} from '@mui/material';
import { TTransferReq } from '../../../types/typesTransferRequest';
import { InputLabel, MenuItem, FormControl } from '@mui/material';
import Select from '@mui/material/Select';
import { banks } from '../../../Helpers/returnNameOfBank';

type TFormPropsMoneyWithdrawRecipient = {
    state: TTransferReq,
    onChange: React.ChangeEventHandler<HTMLInputElement>,
    handleSelect:any,
    errors?: any,
    ObjRecipientFullName?: any,
    ObjRecipientIIN?: any,
    ObjRecipientIIK?: any,
    ObjRecipientBank?: any,
    ObjRecipientBIK?: any,
    ObjRecipientKBe?: any,
}

export default function MoneyWithdrawRecipientForm({
    state,
    onChange,
    errors,
    handleSelect,
    ObjRecipientFullName,
    ObjRecipientIIN,
    ObjRecipientIIK,
    ObjRecipientBank,
    ObjRecipientBIK,
    ObjRecipientKBe,
}: TFormPropsMoneyWithdrawRecipient) {

    return <>
        <React.Fragment  >
        <Grid container spacing={2}>

                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjRecipientFullName}                   
                        required
                        id="recipientFullName"
                        name="recipientFullName"
                        label="ФИО получателя "
                        value={state.recipientFullName}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientFullName"
                        variant="standard"
                        error={!!errors?.recipientFullName?.message}
                        helperText={errors?.recipientFullName?.message}
                        sx={{marginLeft: "10px"}}
                       />
                </Grid>

                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjRecipientIIN}
                        required
                        id="recipientIIN"
                        name="recipientIIN"
                        label="ИИН получателя"
                        value={state.recipientIIN}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientIIN"
                        variant="standard"
                        error={!!errors?.recipientIIN?.message}
                        helperText={errors?.recipientIIN?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>
        
                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjRecipientIIK}
                        required
                        id="recipientIIK"
                        name="recipientIIK"
                        label="ИИК получателя"
                        value={state.recipientIIK}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientIIK"
                        variant="standard"
                        error={!!errors?.recipientIIK?.message}
                        helperText={errors?.recipientIIK?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>

                <Grid item xs={12} sm={8}>
                    <FormControl sx={{ minWidth: 400, margin: "10px 0 0 0" , padding: 0}}>
                        <InputLabel id="recipientBank" sx={{ width: "300px",bgcolor: "#F9FAFC", margin: 0}}>Выберите банк бенефициара (получателя)</InputLabel>
                        <Select
                            {...ObjRecipientBank}
                            label="Банк бенефициара (получателя)"
                            id="recipientBank"
                            name="recipientBank"
                            fullWidth
                            defaultValue = ""
                            value={state.recipientBank}
                            onChange={handleSelect}
                            error={!!errors?.recipientBank?.message}
                            required={true}
                        >
                            {banks.map(bank => {
                                return <MenuItem
                                    value={bank}
                                    key={bank}
                                >{bank}</MenuItem>
                            })}
                        </Select>
                    </FormControl>
                </Grid>
 
                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjRecipientBIK}
                        required
                        id="recipientBIK"
                        name="recipientBIK"
                        label="БИК/SWIFT бенефициара (получателя)"
                        value={state.recipientBIK}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientBIK"
                        variant="standard"
                        error={!!errors?.recipientBIK?.message}
                        helperText={errors?.recipientBIK?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>

                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjRecipientKBe}
                        required
                        id="recipientKBe"
                        name="recipientKBe"
                        label="КБе бенефициара (получателя)"
                        value={state.recipientKBe}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientKBe"
                        variant="standard"
                        error={!!errors?.recipientKBe?.message}
                        helperText={errors?.recipientKBe?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>  

            </Grid>

        </React.Fragment>
    </>
}