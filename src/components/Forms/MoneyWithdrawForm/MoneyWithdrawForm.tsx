import * as React from 'react';
import { Grid,  TextField, Button} from '@mui/material';
import { NavLink } from "react-router-dom";
import { TTransferReq } from '../../../types/typesTransferRequest';
import { InputLabel, MenuItem, FormControl } from '@mui/material';
import Select from '@mui/material/Select';
import { banks } from '../../../Helpers/returnNameOfBank';

type TFormPropsMoneyWithdraw = {
    state: TTransferReq,
    onChange: React.ChangeEventHandler<HTMLInputElement>,
    onClick: React.MouseEventHandler<HTMLButtonElement> | undefined,
    errors?: any,
    ObjSenderFullName?: any,
    ObjRecipientFullName?: any,
    ObjSenderIIN?: any,
    ObjRecipientIIN?: any,
    ObjSenderIIK?: any,
    ObjRecipientIIK?: any,
    ObjSenderBank?: any,
    ObjRecipientBank?: any,
    ObjSenderBIK?: any,
    ObjRecipientBIK?: any,
    ObjSenderKBe?: any,
    ObjRecipientKBe?: any,
    ObjAmount?: any,
    handleSubmit?: any,
}

export default function MoneyWithdrawForm({
    state,
    onChange,
    onClick,
    errors,
    ObjSenderFullName,
    ObjRecipientFullName,
    ObjSenderIIN,
    ObjRecipientIIN,
    ObjSenderIIK,
    ObjRecipientIIK,
    ObjSenderBank,
    ObjRecipientBank,
    ObjSenderBIK,
    ObjRecipientBIK,
    ObjSenderKBe,
    ObjRecipientKBe,
    ObjAmount,
    handleSubmit,
}: TFormPropsMoneyWithdraw) {

    return <>
        <React.Fragment  >
        <Grid container spacing={2}>

                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjSenderFullName}
                        required
                        id="senderFullName"
                        name="senderFullName"
                        label="ФИО отправителя "
                        value={state.senderFullName}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderFullName"
                        variant="standard"
                        error={!!errors?.senderFullName?.message}
                        helperText={errors?.senderFullName?.message}
                        sx={{marginLeft: "10px"}}
                       />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjRecipientFullName}                   
                        required
                        id="recipientFullName"
                        name="recipientFullName"
                        label="ФИО получателя "
                        value={state.recipientFullName}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientFullName"
                        variant="standard"
                        error={!!errors?.recipientFullName?.message}
                        helpertext={errors?.recipientFullName?.message}
                        sx={{marginLeft: "10px"}}
                       />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjSenderIIN}
                        required
                        id="senderIIN"
                        name="senderIIN"
                        label="ИИН отправителя"
                        value={state.senderIIN}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderIIN"
                        variant="standard"
                        error={!!errors?.senderIIN?.message}
                        helpertext={errors?.senderIIN?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjRecipientIIN}
                        required
                        id="recipientIIN"
                        name="recipientIIN"
                        label="ИИН получателя"
                        value={state.recipientIIN}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientIIN"
                        variant="standard"
                        error={!!errors?.recipientIIN?.message}
                        helperText={errors?.recipientIIN?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjSenderIIK}
                        required
                        id="senderIIK"
                        name="senderIIK"
                        label="ИИК отправителя"
                        value={state.senderIIK}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderIIK"
                        variant="standard"
                        error={!!errors?.senderIIK?.message}
                        helperText={errors?.senderIIK?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>
        
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjRecipientIIK}
                        required
                        id="recipientIIK"
                        name="recipientIIK"
                        label="ИИК получателя"
                        value={state.recipientIIK}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientIIK"
                        variant="standard"
                        error={!!errors?.recipientIIK?.message}
                        helperText={errors?.recipientIIK?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl sx={{ minWidth: 400, margin: "10px 0 0 0" }}>
                        <InputLabel id="senderBank" sx={{ width: "220px",bgcolor: "#F9FAFC"}}>Выберите банк отправителя</InputLabel>
                        <Select
                            {...ObjSenderBank}
                            label="Банк отправителя"
                            id="senderBank"
                            name="senderBank"
                            fullWidth
                            value={state.senderBank}
                            onChange={onChange}
                            error={!!errors?.senderBank?.message}
                            helperText={errors?.senderBank?.message}
                            required={true}
                        >
                            {banks.map(bank => {
                                return <MenuItem
                                    value={bank}
                                    key={bank}

                                >{bank}</MenuItem>
                            })}
                        </Select>
                    </FormControl>
                </Grid>


                <Grid item xs={12} sm={6}>
                    <FormControl sx={{ minWidth: 400, margin: "10px 0 0 0" , padding: 0}}>
                        <InputLabel id="recipientBank" sx={{ width: "300px",bgcolor: "#F9FAFC", margin: 0}}>Выберите бенефициара (получателя)</InputLabel>
                        <Select
                            {...ObjRecipientBank}
                            label="Банк бенефициара (получателя)"
                            id="recipientBank"
                            name="recipientBank"
                            fullWidth
                            value={state.recipientBank}
                            onChange={onChange}
                            error={!!errors?.recipientBank?.message}
                            helperText={errors?.recipientBank?.message}
                            required={true}
                        >
                            {banks.map(bank => {
                                return <MenuItem
                                    value={bank}
                                    key={bank}

                                >{bank}</MenuItem>
                            })}
                        </Select>
                    </FormControl>
                </Grid>
                
{/* 
                <Grid item xs={12} sm={6}>
                        <TextField
                            {...ObjSenderBank}
                            required
                            id="senderBank"
                            name="senderBank"
                            label="Банк отправителя"
                            value={state.senderBank}
                            onChange={onChange}
                            fullWidth
                            autoComplete="senderBank"
                            variant="standard"
                            error={!!errors?.senderBank?.message}
                            helperText={errors?.senderBank?.message}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            {...ObjRecipientBank}
                            required
                            id="recipientBank"
                            name="recipientBank"
                            label="Банк бенефициара (получателя)"
                            value={state.recipientBank}
                            onChange={onChange}
                            fullWidth
                            autoComplete="recipientBank"
                            variant="standard"
                            error={!!errors?.recipientBank?.message}
                            helperText={errors?.recipientBank?.message}
                        />
                    </Grid> */}
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjSenderBIK}
                        required
                        id="senderBIK"
                        name="senderBIK"
                        label="БИК/SWIFT отправителя"
                        value={state.senderBIK}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderBIK"
                        variant="standard"
                        error={!!errors?.senderBIK?.message}
                        helperText={errors?.senderBIK?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid> 
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjRecipientBIK}
                        required
                        id="recipientBIK"
                        name="recipientBIK"
                        label="БИК/SWIFT бенефициара (получателя)"
                        value={state.recipientBIK}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientBIK"
                        variant="standard"
                        error={!!errors?.recipientBIK?.message}
                        helperText={errors?.recipientBIK?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>
              
                
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjSenderKBe}
                        required
                        id="senderKBe"
                        name="senderKBe"
                        label="КБе отправителя"
                        value={state.senderKBe}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderKBe"
                        variant="standard"
                        error={!!errors?.senderKBe?.message}
                        helperText={errors?.senderKBe?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>
                
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjRecipientKBe}
                        required
                        id="recipientKBe"
                        name="recipientKBe"
                        label="КБе бенефициара (получателя)"
                        value={state.recipientKBe}
                        onChange={onChange}
                        fullWidth
                        autoComplete="recipientKBe"
                        variant="standard"
                        error={!!errors?.recipientKBe?.message}
                        helperText={errors?.recipientKBe?.message}
                        sx={{marginLeft: "10px"}}
                    />
                </Grid>  
                <Grid item xs={12} sm={6}>
                    <TextField
                        {...ObjAmount}
                        required
                        id="amount"
                        name="amount"
                        label="Сумма"
                        type = "number"
                        value={state.amount}
                        onChange={onChange}
                        fullWidth
                        autoComplete="amount"
                        variant="filled" 
                        error={!!errors?.amount?.message}
                        helperText={errors?.amount?.message}
                    />
                </Grid>
            </Grid>
            <div style={{marginLeft: "10px"}}>
            <Button
                    component={NavLink}
                    variant="contained"
                    color="inherit"
                    to="/client/client-transactions">
                    {"Назад"}
            </Button>

            <Button onClick={handleSubmit(onClick)}
                style={{ margin: "30px" }}
                variant="contained">
                Отправить
            </Button>

            </div>

    
        </React.Fragment>
    </>
}