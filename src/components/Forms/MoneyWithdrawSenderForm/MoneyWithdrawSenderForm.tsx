import * as React from 'react';
import { Grid, TextField} from '@mui/material';
import { TTransferReq } from '../../../types/typesTransferRequest';
import { InputLabel, MenuItem, FormControl } from '@mui/material';
import Select from '@mui/material/Select';
import { banks } from '../../../Helpers/returnNameOfBank';

type TFormPropsMoneyWithdrawSender = {
    state: TTransferReq,
    onChange: React.ChangeEventHandler<HTMLInputElement>,
    handleSelect:any,
    errors?: any,
    ObjSenderFullName?: any,
    ObjSenderIIN?: any,
    ObjSenderIIK?: any,
    ObjSenderBank?: any,
    ObjSenderBIK?: any,
    ObjSenderKBe?: any,
}

export default function MoneyWithdrawSenderForm({
    state,
    onChange,
    errors,
    handleSelect,
    ObjSenderFullName,
    ObjSenderIIN,
    ObjSenderIIK,
    ObjSenderBank,
    ObjSenderBIK,
    ObjSenderKBe,

}: TFormPropsMoneyWithdrawSender) {

    return <>
        <React.Fragment  >
            <Grid container spacing={2}>

                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjSenderFullName}
                        required
                        id="senderFullName"
                        name="senderFullName"
                        label="ФИО отправителя "
                        value={state.senderFullName}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderFullName"
                        variant="standard"
                        error={!!errors?.senderFullName?.message}
                        helperText={errors?.senderFullName?.message}
                        sx={{ marginLeft: "10px" }}
                    />
                </Grid>

                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjSenderIIN}
                        required
                        id="senderIIN"
                        name="senderIIN"
                        label="ИИН отправителя"
                        value={state.senderIIN}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderIIN"
                        variant="standard"
                        error={!!errors?.senderIIN?.message}
                        helperText={errors?.senderIIN?.message}
                        sx={{ marginLeft: "10px" }}
                    />
                </Grid>

                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjSenderIIK}
                        required
                        id="senderIIK"
                        name="senderIIK"
                        label="ИИК отправителя"
                        value={state.senderIIK}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderIIK"
                        variant="standard"
                        error={!!errors?.senderIIK?.message}
                        helperText={errors?.senderIIK?.message}
                        sx={{ marginLeft: "10px" }}
                    />
                </Grid>

                <Grid item xs={12} sm={8}>
                    <FormControl sx={{ minWidth: 400, margin: "10px 0 0 0" }}>
                        <InputLabel id="senderBank" sx={{ width: "220px", bgcolor: "#F9FAFC" }}>Выберите банк отправителя</InputLabel >
                        <Select
                            {...ObjSenderBank}
                            label="Банк отправителя"
                            id="senderBank"
                            name="senderBank"
                            fullWidth
                            defaultValue = " "
                            value={state.senderBank}
                            onChange={handleSelect}
                            error={!!errors?.senderBank?.message}
                            required={true}
                        >
                            {banks.map(bank => {
                                return <MenuItem
                                    value={bank}
                                    key={bank}
                                >{bank}</MenuItem>
                            })}
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjSenderBIK}
                        required
                        id="senderBIK"
                        name="senderBIK"
                        label="БИК/SWIFT отправителя"
                        value={state.senderBIK}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderBIK"
                        variant="standard"
                        error={!!errors?.senderBIK?.message}
                        helperText={errors?.senderBIK?.message}
                        sx={{ marginLeft: "10px" }}
                    />
                </Grid>


                <Grid item xs={12} sm={8}>
                    <TextField
                        {...ObjSenderKBe}
                        required
                        id="senderKBe"
                        name="senderKBe"
                        label="КБе отправителя"
                        value={state.senderKBe}
                        onChange={onChange}
                        fullWidth
                        autoComplete="senderKBe"
                        variant="standard"
                        error={!!errors?.senderKBe?.message}
                        helperText={errors?.senderKBe?.message}
                        sx={{ marginLeft: "10px" }}
                    />
                </Grid>

            </Grid>

        </React.Fragment>
    </>
}