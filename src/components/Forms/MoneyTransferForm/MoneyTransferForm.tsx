import * as React from 'react';
import {Grid, TextField, Button, Typography, ButtonGroup} from '@mui/material';
import {NavLink} from "react-router-dom";
import {TDepositRequest} from '../../../types/typesDepositRequest';
import {InputLabel, MenuItem, FormControl} from '@mui/material';
import Select from '@mui/material/Select';
import {banks} from '../../../Helpers/returnNameOfBank';

import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import {styled} from '@mui/material/styles';

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'left',
    color: theme.palette.text.secondary,
}));

type TFormPropsMoneyTransfer = {
    state: TDepositRequest,
    onChange: React.ChangeEventHandler<HTMLInputElement>,
    onSubmit: React.FormEventHandler<HTMLFormElement>,// React.MouseEventHandler<HTMLButtonElement> | undefined,
    errors?: any,
    ObjSenderFullName?: any,
    ObjSenderIIN?: any,
    ObjSenderIIK?: any,
    ObjSenderBank?: any,
    ObjSenderBIK?: any,
    ObjSenderKBe?: any,
    ObjAmount?: any,
    handleSubmit?: any,
    handleSelect?: any,
}

export default function MoneyTransferForm({
                                              state,
                                              onChange,
                                              onSubmit,
                                              errors,
                                              handleSelect,
                                              ObjSenderFullName,
                                              ObjSenderIIN,
                                              ObjSenderIIK,
                                              ObjSenderBank,
                                              // ObjSenderBIK,
                                              ObjSenderKBe,
                                              ObjAmount,
                                              handleSubmit
                                          }: TFormPropsMoneyTransfer){

    return <>
        <form onSubmit={handleSubmit(onSubmit)}>
            <Box sx={{width: '100%'}}>
                <Grid
                    // container
                    // direction="column"
                    // justifyContent="center"
                    // alignItems="center"
                >
                    <Stack spacing={2}>

                        <Item sx={{textAlign: 'center'}}>
                            <Typography
                                    sx={{fontSize: "22px"}}
                                variant="subtitle2"
                                gutterBottom>
                                {"Пополнение счёта"}
                            </Typography>
                        </Item>

                        <Item>
                            {/*    // xs, extra-small: 0px*/}
                            {/*    // sm, small: 600px*/}
                            {/*    // md, medium: 900px*/}
                            {/*    // lg, large: 1200px*/}
                            {/*    // xl, extra-large: 1536px*/}


                            {/*<Grid*/}
                            {/*    container*/}
                            {/*    spacing={2}*/}
                            {/*    direction="column"*/}
                            {/*    justifyContent="center"*/}
                            {/*    alignItems="stretch"*/}
                            {/*>*/}


                            <Grid
                                container
                                spacing={2}
                                sx={{margin: "20px auto"}}
                                // direction="column"
                                // justifyContent="center"
                                // alignItems="stretch"
                                item xs={10}
                            >
                                <Grid item xs={12}>
                                    <TextField
                                        {...ObjSenderFullName}
                                        required
                                        id="senderFullName"
                                        name="senderFullName"
                                        label="ФИО отправителя "
                                        value={state.senderFullName}
                                        onChange={onChange}
                                        fullWidth
                                        autoComplete="senderFullName"
                                        variant="outlined"
                                        error={!!errors?.senderFullName?.message}
                                        helperText={errors?.senderFullName?.message}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        {...ObjSenderIIN}
                                        required
                                        id="senderIIN"
                                        name="senderIIN"
                                        label="ИИН отправителя"
                                        value={state.senderIIN}
                                        onChange={onChange}
                                        fullWidth
                                        autoComplete="senderIIN"
                                        variant="outlined"
                                        error={!!errors?.senderIIN?.message}
                                        helperText={errors?.senderIIN?.message}
                                    />
                                </Grid>
                                {/*{errors.senderFullName && <span>This field is required</span>}*/}
                                <Grid item xs={12}>
                                    <TextField
                                        {...ObjSenderIIK}
                                        required
                                        id="senderIIK"
                                        name="senderIIK"
                                        label="ИИК отправителя"
                                        value={state.senderIIK}
                                        onChange={onChange}
                                        fullWidth
                                        autoComplete="senderIIK"
                                        variant="outlined"
                                        error={!!errors?.senderIIK?.message}
                                        helperText={errors?.senderIIK?.message}
                                    />
                                </Grid>
                                {/*****************************************************************/}
                                <Grid item xs={12}>
                                    {/*<FormControl sx={{minWidth: 800, margin: "10px 0 0 0"}}>*/}
                                    <FormControl sx={{width: "100%"}}>
                                        {/*<FormControl>*/}

                                        <InputLabel id="senderBank" sx={{width: "220px", bgcolor: "#F9FAFC"}}>Выберите
                                            банк отправителя
                                        </InputLabel>

                                        <Select
                                            {...ObjSenderBank}
                                            label="Банк отправителя"
                                            id="senderBank"
                                            name="senderBank"
                                            fullWidth
                                            value={state.senderBank}
                                            onChange={handleSelect}
                                            // error={!!errors?.senderBank?.message}
                                            // helpertext={errors?.senderBank?.message}
                                            // required={true}
                                        >
                                            {banks.map(bank=>{
                                                return <MenuItem
                                                    value={bank}
                                                    key={bank}

                                                >{bank}</MenuItem>
                                            })}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        // required
                                        id="senderBIK"
                                        name="senderBIK"
                                        label="БИК/SWIFT отправителя"
                                        value={state.senderBIK}
                                        onChange={onChange}
                                        fullWidth
                                        autoComplete="senderBIK"
                                        variant="outlined"
                                        // error={!!errors?.senderBIK?.message}
                                        // helperText={errors?.senderBIK?.message}
                                    />
                                </Grid>
                                {/*****************************************************************/}
                                <Grid item xs={12}>
                                    <TextField
                                        {...ObjSenderKBe}
                                        required
                                        id="senderKBe"
                                        name="senderKBe"
                                        label="КБе отправителя"
                                        value={state.senderKBe}
                                        onChange={onChange}
                                        fullWidth
                                        autoComplete="senderKBe"
                                        variant="outlined"
                                        error={!!errors?.senderKBe?.message}
                                        helperText={errors?.senderKBe?.message}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        {...ObjAmount}
                                        required
                                        id="amount"
                                        name="amount"
                                        label="Сумма"
                                        value={state.amount.toLocaleString('ru')}
                                        onChange={onChange}
                                        fullWidth
                                        autoComplete="amount"
                                        variant="outlined"
                                        error={!!errors?.amount?.message}
                                        helperText={errors?.amount?.message}
                                    />
                                </Grid>
                            </Grid>

                        </Item>

                        {/*<Item sx={{textAlign: 'center'}}>*/}
                        <Item>

                            <Box
                                sx={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    '& > *': {
                                        m: 1,
                                    },
                                }}
                            >
                                <ButtonGroup
                                    variant="outlined"
                                    size="large"
                                    // aria-label="outlined button group"
                                    aria-label="large button group"
                                >

                                    <Button
                                        sx={{width:"150px"}}
                                        component={NavLink}
                                        // variant="contained"
                                        color="inherit"
                                        to="/client/client-transactions">
                                        {"Назад"}
                                    </Button>
                                    <Button
                                        sx={{width:"150px"}}
                                        // onClick={handleSubmit(onClick)}
                                        // style={{margin: "30px"}}
                                        // variant="contained"
                                        color="inherit"
                                        type="submit"
                                    >
                                        {"Отправить"}
                                    </Button>

                                </ButtonGroup>
                            </Box>
                        </Item>

                    </Stack>

                </Grid>
            </Box>


            {/*<Button onClick={handleSubmit(onClick)}*/}
            {/*    style={{ margin: "30px" }}*/}
            {/*    variant="contained">*/}
            {/*    Отправить*/}
            {/*</Button>*/}
        </form>

    </>

}