import {Button, Grid, TextField, Typography} from "@mui/material";
import React from 'react';
import {TRegisterLoginProps} from "../../../types/typesUser";

const RegisterForm = ({
                          errors,handleSubmit,passwordError,
                          ObjEmail,ObjPassword1,ObjPassword2,
                          onSubmit,dataUser, buttonText}: TRegisterLoginProps)=>{

    return (
        // <form onSubmit={onSubmit}>
        <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextField
                        {...ObjEmail}
                        id={"email"}
                        name={"email"}
                        label={"Email"}
                        variant="outlined"
                        type={"text"}
                        // value={state.username}
                        required={true}
                        // onChange={onChange}
                        fullWidth

                        error={!!dataUser?.error?.message || !!errors?.email?.message}
                        helperText={dataUser?.error?.message || errors?.email?.message}
                    />
                </Grid>

                <Grid item xs={12}>
                    <TextField
                        {...ObjPassword1}
                        id={"password1"}
                        name={"password1"}
                        label={"Password"}
                        variant="outlined"
                        type={"password"}
                        // value={state.password}
                        required={true}
                        // onChange={onChange}
                        fullWidth

                        error={!!errors?.password1?.message}
                        helperText={errors?.password1?.message}
                    />
                </Grid>

                <Grid item xs={12}>
                    <TextField
                        {...ObjPassword2}
                        id={"password2"}
                        name={"password2"}
                        label={"Password"}
                        variant="outlined"
                        type={"password"}
                        // value={state.password}
                        required={true}
                        // onChange={onChange}
                        fullWidth

                        error={!!errors?.password2?.message}
                        helperText={errors?.password2?.message}
                    />
                </Grid>
            </Grid>
            <Grid item>
                <Typography style={{color:"red"}}>
                    {passwordError}
                </Typography>
            </Grid>
            <Grid item sx={{mt: "30px "}} textAlign='center'>
                <Button
                    sx={{ bgcolor: "black", width:"250px" ,
                        "&:hover": {
                            backgroundColor: "#b0a8a8"
                          },
                      }}
                    type="submit"
                    variant="contained"
                >
                    {buttonText}
                </Button>
            </Grid>
        </form>
    )
}

export default RegisterForm