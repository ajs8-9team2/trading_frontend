import {Button, Grid, TextField} from "@mui/material";
import {TRegisterLoginProps} from "../../../types/typesUser";

const LoginForm = ({
                       errors, handleSubmit,
                       ObjEmail, ObjPassword1,
                       onSubmit, dataUser, buttonText
                   }: TRegisterLoginProps)=>{

    return (
        <form  onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextField
                        {...ObjEmail}
                        id={"email"}
                        name={"email"}
                        label={"Email"}
                        variant="outlined"
                        type={"text"}
                        // value={state.username}
                        required={true}
                        // onChange={onChange}
                        fullWidth

                        error={!!dataUser?.error?.message || !!errors?.email?.message}
                        helperText={dataUser?.error?.message || errors?.email?.message}
                    />
                </Grid>

                <Grid item xs={12}>
                    <TextField
                        {...ObjPassword1}
                        id={"password1"}
                        name={"password1"}
                        label={"Password"}
                        variant="outlined"
                        type={"password"}
                        // value={state.password}
                        required={true}
                        // onChange={onChange}
                        fullWidth

                        error={!!errors?.password1?.message}
                        helperText={errors?.password1?.message}
                    />
                </Grid>
            </Grid>
            <Grid item sx={{mt: "30px "}} textAlign='center'>
                <Button
                    sx={{ bgcolor: "black", width:"250px" ,
                    "&:hover": {
                        backgroundColor: "#b0a8a8"
                        },
                    }}
                    type="submit"
                    variant="contained"
                >
                    {buttonText}
                </Button>
            </Grid>
        </form>   
    )
}

export default LoginForm;