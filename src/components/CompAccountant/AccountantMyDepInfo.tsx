import React from "react";
import {Button, Space, message, Divider} from 'antd';
import {TextField} from "@mui/material";
import {TGetPostDepositRequestAT} from "../../types/typesDepositRequest";
import {patchDepositRequestAT} from "../../store/services/depositRequestSlice";
import {getDate} from "../../Helpers/getDate";
import {notificationObject} from "../../Helpers/notificationObject";
import {getStatus} from "../../Helpers/getStatus";
import {TUser} from "../../types/reestrOfAlias";
import {useAppDispatch, useAppSelector} from "../../hook";
import "./AccountantMyDepInfo.css";

const AccountantMyDepInfo = (propsDepRequest: TGetPostDepositRequestAT)=>{
    const {
        _id, date, senderFullName, status, amount, senderIIK, senderIIN, senderBank, senderBIK, senderKBe, accountant, user
    } = propsDepRequest!

    const oneDepositsRequest: TGetPostDepositRequestAT = useAppSelector(state=>state.reducerDepositRequest.oneDepositRequest);

    const dispatch = useAppDispatch();

    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [messageApi, contextHolder] = message.useMessage();

    const [rejectionReason, setRejectionReason] = React.useState<string>("");
    const inputChangeHandler: React.ChangeEventHandler<HTMLInputElement> = (e)=>{
        const value: string = e.target.value;
        setRejectionReason(value);
    };

    const approveReq = (id: string | undefined) => {
        dispatch(patchDepositRequestAT({
            status: "Completed",
            _idDepositRequest: id,
        }));
        dispatch(notificationObject(
            _id!,
            user?._id!,
            "Completed",
            "depositRequest",
            getStatus("Completed")
        ));
        messageApi.open({
            type: "success",
            content: "Заявка одобрена"
        });
    };

    const declineReq = (id: string | undefined) => {
        dispatch(patchDepositRequestAT({
            status: "Declined",
            rejectionReason: rejectionReason,
            _idDepositRequest: id,
        }));
        dispatch(notificationObject(
            _id!,
            user?._id!,
            "Declined",
            "depositRequest",
            getStatus("Declined") + " по причине: " + rejectionReason
        ));
        messageApi.open({
            type: "warning",
            content: "Заявка отклонена"
        });
    };

    return (
        <div className="AccountantReqInfo">
            <Divider orientation="left" plain>Данные заявки</Divider>
            <p>Номер заявки: <strong>{_id}</strong></p>
            <p>Дата: <strong>{getDate(date as string)}</strong></p>
            <Divider orientation="left" plain>Данные заказа</Divider>
            <p>ФИО: <strong>{senderFullName}</strong></p>
            <p>ИИН отправителя: <strong>{senderIIN}</strong></p>
            <p>ИИК отправителя: <strong>{senderIIK}</strong></p>
            <p>Банк отправителя: <strong>{senderBank}</strong></p>
            <p>БИК отправителя: <strong>{senderBIK}</strong></p>
            <p>КБе отправителя: <strong>{senderKBe}</strong></p>
            <p>Сумма: <strong>{amount}</strong></p>

            <Divider orientation="left" plain>Обработка заявки</Divider>
            <h3>Причина отказа: </h3>
            <div className="accountant_rejection">
                <TextField 
                    multiline
                    rows={4} 
                    placeholder="Введите причину отказа"
                    id={"reason"}
                    name={"reason"}
                    value={rejectionReason}
                    onChange={inputChangeHandler}
                />

                <div className="info_buttons">
                    <Space>
                        {contextHolder}
                        <Button type="primary" onClick={()=>{approveReq(_id)}}>Одобрить</Button>
                        <Button onClick={()=>{declineReq(_id)}} disabled={!rejectionReason}>Отклонить</Button>
                    </Space>
                </div>

            </div>
        </div>
    )
};

export default AccountantMyDepInfo;
