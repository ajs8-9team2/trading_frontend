import React, {useEffect, useState} from "react";
import {Table, Button, Drawer, Space } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import {useAppDispatch, useAppSelector} from "../../hook";
import {fetchAccountAT, fetchOneAccountAT} from "../../store/services/accountSlice";
import {TGetPostAccountAT} from "../../types/typesAccount";
import { T_id, TAccountNumber, TFirstName, TLastName, TDateOfBirth, TIdentificationNumber, TEmail, TPhone, TAuthority, TDateOfIssue, TExpirationDate, TIdCardNumber, TAmount, TAccountType, TPatronym, TCountry, TCity, TArea, TAddress } from "../../types/reestrOfAlias";
import AccountsListedInfo from "./AccountsListedInfo";


const AccountsListed: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allAccount: TGetPostAccountAT[] = useAppSelector(state=>state.reducerAccount.allAccount);
    const oneAccount: TGetPostAccountAT = useAppSelector(state=>state.reducerAccount.oneAccount);

    useEffect(() => {
        dispatch(fetchAccountAT());   
    }, []);

    const [openDrawer, setOpenDrawer] = useState(false);

    const selectAcc = (record: any) => {
        dispatch(fetchOneAccountAT(record._id));
        setOpenDrawer(true);
    };

    const onCloseDrawer = () => {
        setOpenDrawer(false);
    };

    interface TGetPostAccountAT {
        _id?: T_id;                                 
        accountNumber: TAccountNumber;                
        firstName: TFirstName ;                       
        lastName: TLastName ;                         
        dateOfBirth: TDateOfBirth ;                  
        identificationNumber: TIdentificationNumber;  
        email: TEmail;                               
        phone: TPhone;                               
        authority: TAuthority;                        
        dateOfIssue: TDateOfIssue;                    
        expirationDate: TExpirationDate;              
        idCardNumber: TIdCardNumber;                 
        amount: TAmount                               
        accountType?: TAccountType;
        patronym?: TPatronym,
        country?: TCountry;
        city?: TCity;
        area?: TArea;
        address?: TAddress;
    };
      
    const columns: ColumnsType<TGetPostAccountAT> = [
        {
            title: 'Номер счета',
            dataIndex: 'accountNumber',
            key: 'accountNumber',
        },
        {
            title: 'Фамилия',
            dataIndex: 'lastName',
            key: 'lastName',
        },
        {
            title: 'Имя',
            dataIndex: 'firstName',
            key: 'firstName',
        },
        {
            title: 'Номер телефона',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'ИИН',
            dataIndex: 'identificationNumber',
            key: 'identificationNumber',
        },
        {
            title: 'Номер удостоверения',
            dataIndex: 'idCardNumber',
            key: 'idCardNumber',
        },
        {
            title: 'Остаток на счете',
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: 'Действие',
            key: 'action',
            render: (_, record) => (
                <Button onClick={()=>selectAcc(record)}>Посмотреть</Button>
            ),
        },
    ];

    return (
        <>
            <Table columns={columns} dataSource={allAccount} rowKey="_id" />

            <Drawer 
                title="Информация о счете клиента"
                width={720}
                onClose={onCloseDrawer}
                open={openDrawer}
                // extra={
                //     <Space>
                //         <Button onClick={onCloseDrawer}>Отмена</Button>
                //     </Space>
                // }
            >
                <AccountsListedInfo user={undefined} {...oneAccount!} />
            </Drawer>
        </>
    )
}

export default AccountsListed;