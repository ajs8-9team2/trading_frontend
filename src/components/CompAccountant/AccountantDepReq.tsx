import React, {useEffect, useState} from "react";
import {Table, Tag, Button, Drawer, Space } from 'antd';
import { SearchOutlined } from "@ant-design/icons";
import type { ColumnsType } from 'antd/es/table';
import {useAppDispatch, useAppSelector} from "../../hook";
import {fetchDepositReqAccountantAT, paramsDepositReqAT} from "../../store/services/depositRequestSlice";
import {TGetPostDepositRequestAT} from "../../types/typesDepositRequest";
import {getDate} from "../../Helpers/getDate";
import { getStatusColor } from "../../Helpers/getStatusColor";
import { getStatusTrader } from "../../Helpers/getStatusTrader";
import { filterAntTable } from "../../Helpers/filterAntTable";
import AccountantDepReqInfo from "../../components/CompAccountant/AccountantDepReqInfo"
import { TAccountant, TAmount, TDate, TSenderBank, TSenderBIK, TSenderFullName, TSenderIIK, TSenderIIN, TSenderKBe, TStatusTransferDeposit, T_id } from "../../types/reestrOfAlias";


const AccountantDepReq: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allDepositsRequest: TGetPostDepositRequestAT[] = useAppSelector(state=>state.reducerDepositRequest.allDepositsRequest);
    const oneDepositsRequest: TGetPostDepositRequestAT = useAppSelector(state=>state.reducerDepositRequest.oneDepositRequest);

    useEffect(() => {
        dispatch(fetchDepositReqAccountantAT());   
    }, [dispatch]);

    const [openDrawer, setOpenDrawer] = useState(false);

    const selectDepReq = (record: any) => {
        dispatch(paramsDepositReqAT(record._id));
        setOpenDrawer(true);
    };

    const onCloseDrawer = () => {
        setOpenDrawer(false);
    };

    interface TGetPostDepositRequestAT {
        _id?: T_id;
        status?: TStatusTransferDeposit;
        senderIIN: TSenderIIN;             
        senderIIK: TSenderIIK;           
        senderBIK: TSenderBIK;              
        senderBank: TSenderBank;            
        senderKBe: TSenderKBe;              
        senderFullName: TSenderFullName;    
        amount: TAmount;                   
        accountant?: TAccountant;
        date?: TDate;
    };
      
    const columns: ColumnsType<TGetPostDepositRequestAT> = [
        {
            title: 'Номер заявки',
            dataIndex: '_id',
            key: '_id',
        },
        {
            title: 'ФИО',
            dataIndex: 'senderFullName',
            key: 'senderFullName',
            ...filterAntTable<TGetPostDepositRequestAT>(),
            onFilter: (value, record) => {
                return record.senderFullName
                    .toString()
                    .toLowerCase()
                    .includes(value.toString().toLowerCase())
            }
        },
        {
            title: 'Дата',
            dataIndex: 'date',
            key: 'date',
            render: (date) => (
                getDate(date)
            )
        },
        {
            title: 'ИИН отправителя',
            dataIndex: 'senderIIN',
            key: 'senderIIN',
            ...filterAntTable<TGetPostDepositRequestAT>(),
            onFilter: (value, record) => {
                return record.senderIIN
                    .toString()
                    .toLowerCase()
                    .includes(value.toString().toLowerCase())
            }
        },
        {
            title: 'Сумма',
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: 'Статус',
            key: 'status',
            dataIndex: 'status',
            filters: [ {text: "В ожидании", value: "Approved"}, {text: "В процессе", value: "AccountantWIP"}],
            filterIcon: () => {return <SearchOutlined />},
            onFilter: (value: string | number | boolean, record) => record.status!.indexOf(value as string) === 0,
            render: (status) => (
                <>  
                    <Tag key={status} color={getStatusColor(status)}>
                    {getStatusTrader(status)}
                    </Tag> 
                </>
            ),
        },
        {
            title: 'Действие',
            key: 'action',
            render: (_, record) => (
                <Button onClick={()=>selectDepReq(record)}>Посмотреть</Button>
            ),
        },
    ];

    return (
        <>
            <Table columns={columns} dataSource={allDepositsRequest} rowKey="_id" />

            <Drawer 
                title="Полная информация по заявке"
                width={720}
                onClose={onCloseDrawer}
                open={openDrawer}
                extra={
                    <Button onClick={onCloseDrawer}>Отмена</Button>
                }
            >
                <AccountantDepReqInfo {...oneDepositsRequest!}/>
            </Drawer>
        </>
    )
}

export default AccountantDepReq;