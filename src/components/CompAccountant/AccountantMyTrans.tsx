import React, {useEffect, useState} from "react";
import {Table, Tag, Button, Drawer, Space } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import {useAppDispatch, useAppSelector} from "../../hook";
import {getDate} from "../../Helpers/getDate";
import { getStatusColor } from "../../Helpers/getStatusColor";
import { getStatusTrader } from "../../Helpers/getStatusTrader";
import {queryTransferReqAccountantWIPAT, paramsTransferReqAT} from "../../store/services/transferRequestSlice";
import {TGetPostTransferReqAT} from "../../types/typesTransferRequest";
import { T_id, TAccountant, TStatusTransferDeposit, TSenderIIN, TRecipientIIN, TSenderIIK, TRecipientIIK, TSenderBIK, TRecipientBIK, TSenderBank, TRecipientBank, TSenderKBe, TRecipientKBe, TSenderFullName, TRecipientFullName, TAmount, TDate, TUser } from "../../types/reestrOfAlias";
import AccountantMyTransInfo from "./AccountantMyTransInfo";


const AccountantMyTrans: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allTransferReq: TGetPostTransferReqAT[] = useAppSelector(state=>state.reducerTransferRequest.allTransferReq);
    const oneTransferReq: TGetPostTransferReqAT = useAppSelector(state=>state.reducerTransferRequest.oneTransferReq);

    const dataUser:TUser = useAppSelector(state=>state.reducerUsers.user);

    useEffect(() => {
        dispatch(queryTransferReqAccountantWIPAT(dataUser?._id!));  
    }, [dispatch]);

    const [openDrawer, setOpenDrawer] = useState(false);

    const selectDepReq = (record: any) => {
        dispatch(paramsTransferReqAT(record._id));
        setOpenDrawer(true);
    };

    const onCloseDrawer = () => {
        setOpenDrawer(false);
    };

    const assignDepReq = () => {
        console.log(oneTransferReq);
    };

    interface TGetPostTransferReqAT {
        _id?: T_id;
        accountant?: TAccountant;
        status?: TStatusTransferDeposit; 
        senderIIN: TSenderIIN;                    
        recipientIIN: TRecipientIIN;              
        senderIIK: TSenderIIK;                    
        recipientIIK: TRecipientIIK;              
        senderBIK: TSenderBIK;                  
        recipientBIK: TRecipientBIK;          
        senderBank: TSenderBank;               
        recipientBank: TRecipientBank;           
        senderKBe: TSenderKBe;                 
        recipientKBe: TRecipientKBe;            
        senderFullName: TSenderFullName;        
        recipientFullName: TRecipientFullName;   
        amount: TAmount;                        
        date?: TDate;
    };
      
    const columns: ColumnsType<TGetPostTransferReqAT> = [
        {
            title: 'Номер заявки',
            dataIndex: '_id',
            key: '_id',
        },
        {
            title: 'ФИО',
            dataIndex: 'senderFullName',
            key: 'senderFullName',
        },
        {
            title: 'Дата',
            dataIndex: 'date',
            key: 'date',
            render: (date) => (
                getDate(date)
            )
        },
        {
            title: 'ИИН отправителя',
            dataIndex: 'senderIIN',
            key: 'senderIIN',
        },
        {
            title: 'Сумма',
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: 'Статус',
            key: 'status',
            dataIndex: 'status',
            render: (status) => (
                <>  
                    <Tag key={status} color={getStatusColor(status)}>
                    {getStatusTrader(status)}
                    </Tag> 
                </>
            ),
        },
        {
            title: 'Действие',
            key: 'action',
            render: (_, record) => (
                <Button onClick={()=>selectDepReq(record)}>Посмотреть</Button>
            ),
        },
    ];

    return (
        <>
            <Table columns={columns} dataSource={allTransferReq} rowKey="_id" />

            <Drawer 
                title="Полная информация по заявке"
                width={720}
                onClose={onCloseDrawer}
                open={openDrawer}
                // extra={
                //     <Space>
                //         <Button onClick={onCloseDrawer}>Отмена</Button>
                //         <Button onClick={assignDepReq} type="primary" >Взять в работу</Button>
                //     </Space>
                // }
            >
                <AccountantMyTransInfo {...oneTransferReq!} />
            </Drawer>
        </>
    )
}

export default AccountantMyTrans;