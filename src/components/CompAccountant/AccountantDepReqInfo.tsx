import {Button, message, Divider} from 'antd';
import {TGetPostDepositRequestAT} from "../../types/typesDepositRequest";
import {patchDepositRequestAT} from "../../store/services/depositRequestSlice";
import {notificationObject} from "../../Helpers/notificationObject";
import {getDate} from "../../Helpers/getDate";
import {getStatus} from "../../Helpers/getStatus";
import { getStatusColor } from "../../Helpers/getStatusColor";
import { getStatusTrader } from "../../Helpers/getStatusTrader";
import {TUser} from "../../types/reestrOfAlias";
import {useAppDispatch, useAppSelector} from "../../hook";

const AccountantDepReqInfo = (propsDepRequest: TGetPostDepositRequestAT)=>{
    const {
        _id, date, senderFullName, status, amount, senderIIK, senderIIN, senderBank, senderBIK, senderKBe, accountant, user
    } = propsDepRequest!

    // const oneDepositsRequest: TGetPostDepositRequestAT = useAppSelector(state=>state.reducerDepositRequest.oneDepositRequest);

    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [messageApi, contextHolder] = message.useMessage();

    const acceptReq = (id: string | undefined) => {
        dispatch(patchDepositRequestAT({
            status: "AccountantWIP",
            _idDepositRequest: id,
            accountant: dataUser?._id!
        }));
        dispatch(notificationObject(
            _id!,
            user?._id!,
            "AccountantWIP",
            "depositRequest",
            getStatus("AccountantWIP")
        ));
        messageApi.open({
            type: "success",
            content: "Заявка закреплена за вами"
        });
    };

    return (
        <div className="AccountantReqInfo">
            <Divider orientation="left" plain>Данные заявки</Divider>
            <p>Номер заявки: <strong>{_id}</strong></p>
            <p>Дата: <strong>{getDate(date as string)}</strong></p>
            <p>Статус: <strong>{getStatusTrader(status)}</strong></p>
            {!!accountant ? (
                <p>Закреплена за: <strong>{accountant.fullName}</strong></p>
            ) : <p></p>}
            <Divider orientation="left" plain>Данные заказа</Divider>
            <p>ФИО: <strong>{senderFullName}</strong></p>
            <p>ИИН отправителя: <strong>{senderIIN}</strong></p>
            <p>ИИК отправителя: <strong>{senderIIK}</strong></p>
            <p>Банк отправителя: <strong>{senderBank}</strong></p>
            <p>БИК отправителя: <strong>{senderBIK}</strong></p>
            <p>КБе отправителя: <strong>{senderKBe}</strong></p>
            <p>Сумма: <strong>{amount}</strong></p>
            
            <Divider orientation="left" plain>Обработка заявки</Divider>
            {contextHolder}
            <Button type="primary" onClick={()=>{acceptReq(_id)}} disabled={status === "AccountantWIP"} > Начать работу над заявкой</Button>
        </div>
    )
};

export default AccountantDepReqInfo;
