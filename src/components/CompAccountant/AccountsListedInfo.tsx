import {TGetPostAccountAT} from "../../types/typesAccount";
import {useAppDispatch} from "../../hook";

const AccountsListedInfo = (propsAccInfo: TGetPostAccountAT)=>{
    const {
        accountNumber, firstName, lastName, dateOfBirth, identificationNumber, email, phone, authority, dateOfIssue, expirationDate, idCardNumber, amount, accountType, patronym, country, city, area, address,
    } = propsAccInfo!

    return (
        <div className="AccountantReqInfo">
            <p>Номер счета: <strong>{accountNumber}</strong></p>
            <p>Тип счета: <strong>{accountType}</strong></p>
            <p>Фамилия: <strong>{lastName}</strong></p>
            <p>Имя: <strong>{firstName}</strong></p>
            <p>Отчество: <strong>{patronym}</strong></p>
            <p>Дата рождения: <strong>{dateOfBirth}</strong></p>
            <p>Электронная почта: <strong>{email}</strong></p>
            <p>Телефон: <strong>{phone}</strong></p>
            <p>ИИН: <strong>{identificationNumber}</strong></p>
            <p>Номер удостоверения: <strong>{idCardNumber}</strong></p>
            <p>Выдано: <strong>{authority}</strong></p>
            <p>Дата выдачи: <strong>{dateOfIssue}</strong></p>
            <p>Годно до: <strong>{expirationDate}</strong></p>
            <p>Страна: <strong>{country}</strong></p>
            <p>Область: <strong>{area}</strong></p>
            <p>Город: <strong>{city}</strong></p>
            <p>Адрес: <strong>{address}</strong></p>
            <p>Сумма: <strong>{amount}</strong></p>
        </div>
    )
};

export default AccountsListedInfo;
