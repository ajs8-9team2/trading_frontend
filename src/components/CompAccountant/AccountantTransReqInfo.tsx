import {Button, message, Divider} from 'antd';
import {TGetPostTransferReqAT} from "../../types/typesTransferRequest";
import {patchTransferReqAT} from "../../store/services/transferRequestSlice";
import {notificationObject} from "../../Helpers/notificationObject";
import {getDate} from "../../Helpers/getDate";
import {getStatus} from "../../Helpers/getStatus";
import {TUser} from "../../types/reestrOfAlias";
import {useAppDispatch, useAppSelector} from "../../hook";

const AccountantTransReqInfo = (propsTransRequest: TGetPostTransferReqAT)=>{
    const {
        _id, date, senderFullName, status, amount, senderIIK, senderIIN, senderBank, senderBIK, senderKBe, accountant, recipientIIK, recipientIIN, recipientBIK, recipientBank, recipientFullName, recipientKBe, user
    } = propsTransRequest!

    const oneTransferReq: TGetPostTransferReqAT = useAppSelector(state=>state.reducerTransferRequest.oneTransferReq);

    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [messageApi, contextHolder] = message.useMessage();

    const acceptReq = (id: string | undefined) => {
        dispatch(patchTransferReqAT({
            status: "AccountantWIP",
            _idTransferReq: id,
            accountant: dataUser?._id!
        }));
        dispatch(notificationObject(
            _id!,
            user?._id!,
            "AccountantWIP",
            "transferRequest",
            getStatus("AccountantWIP")
        ));
        messageApi.open({
            type: "success",
            content: "Заявка закреплена за вами"
        });
    };

    return (
        <div className="AccountantReqInfo">
            <p>Номер заявки: <strong>{_id}</strong></p>
            <p>Дата: <strong>{getDate(date as string)}</strong></p>
            <p>Статус: <strong>{status}</strong></p>
            <p>ФИО отправителя: <strong>{senderFullName}</strong></p>
            <p>ИИН отправителя: <strong>{senderIIN}</strong></p>
            <p>ИИК отправителя: <strong>{senderIIK}</strong></p>
            <p>Банк отправителя: <strong>{senderBank}</strong></p>
            <p>БИК отправителя: <strong>{senderBIK}</strong></p>
            <p>КБе отправителя: <strong>{senderKBe}</strong></p>
            <p>ФИО получателя: <strong>{recipientFullName}</strong></p>
            <p>ИИН получателя: <strong>{recipientIIN}</strong></p>
            <p>ИИК получателя: <strong>{recipientIIK}</strong></p>
            <p>Банк получателя: <strong>{recipientBank}</strong></p>
            <p>БИК получателя: <strong>{recipientBIK}</strong></p>
            <p>КБе получателя: <strong>{recipientKBe}</strong></p>
            <p>Сумма: <strong>{amount}</strong></p>
            {!!accountant ? (
                <p>Закреплена за: <strong>{accountant?._id}</strong></p>
            ) : <p></p>}
            <Button type="primary" onClick={()=>{acceptReq(_id)}} disabled={status === "AccountantWIP"} > Начать работу над заявкой</Button>
        </div>
    )
};

export default AccountantTransReqInfo;
