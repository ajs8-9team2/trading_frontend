import React from "react";
import {Button, Input, Space, message, Divider} from 'antd';
import {TextField} from "@mui/material";
import {TGetPostTransferReqAT} from "../../types/typesTransferRequest";
import {patchTransferReqAT} from "../../store/services/transferRequestSlice";
import {notificationObject} from "../../Helpers/notificationObject";
import {getDate} from "../../Helpers/getDate";
import {getStatus} from "../../Helpers/getStatus";
import {TUser} from "../../types/reestrOfAlias";
import {useAppDispatch, useAppSelector} from "../../hook";

const AccountantMyTransInfo = (propsTransRequest: TGetPostTransferReqAT)=>{
    const {
        _id, date, senderFullName, status, amount, senderIIK, senderIIN, senderBank, senderBIK, senderKBe, accountant, recipientIIK, recipientIIN, recipientBIK, recipientBank, recipientFullName, recipientKBe, user
    } = propsTransRequest!

    const oneTransferReq: TGetPostTransferReqAT = useAppSelector(state=>state.reducerTransferRequest.oneTransferReq);

    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [messageApi, contextHolder] = message.useMessage();

    const [rejectionReason, setRejectionReason] = React.useState<string>("");
    const inputChangeHandler: React.ChangeEventHandler<HTMLInputElement> = (e)=>{
        const value: string = e.target.value;
        setRejectionReason(value);
    };

    const approveReq = (id: string | undefined) => {
        dispatch(patchTransferReqAT({
            status: "Completed",
            _idTransferReq: id,
        }));
        dispatch(notificationObject(
            _id!,
            user?._id!,
            "Completed",
            "transferRequest",
            getStatus("Completed")
        ));
        messageApi.open({
            type: "success",
            content: "Заявка одобрена"
        });
    };

    const declineReq = (id: string | undefined) => {
        dispatch(patchTransferReqAT({
            status: "Declined",
            rejectionReason: rejectionReason,
            _idTransferReq: id,
        }));
        dispatch(notificationObject(
            _id!,
            user?._id!,
            "Declined",
            "transferRequest",
            getStatus("Declined") + " по причине: " + rejectionReason
        ));
        messageApi.open({
            type: "warning",
            content: "Заявка отклонена"
        });
    };

        return (
            <div className="AccountantReqInfo">
                <Divider orientation="left" plain>Данные заявки</Divider>
                <p>Номер заявки: <strong>{_id}</strong></p>
                <p>Дата: <strong>{getDate(date as string)}</strong></p>
                <Divider orientation="left" plain>Данные заказа</Divider>
                <p>ФИО отправителя: <strong>{senderFullName}</strong></p>
                <p>ИИН отправителя: <strong>{senderIIN}</strong></p>
                <p>ИИК отправителя: <strong>{senderIIK}</strong></p>
                <p>Банк отправителя: <strong>{senderBank}</strong></p>
                <p>БИК отправителя: <strong>{senderBIK}</strong></p>
                <p>КБе отправителя: <strong>{senderKBe}</strong></p>
                <p>ФИО получателя: <strong>{recipientFullName}</strong></p>
                <p>ИИН получателя: <strong>{recipientIIN}</strong></p>
                <p>ИИК получателя: <strong>{recipientIIK}</strong></p>
                <p>Банк получателя: <strong>{recipientBank}</strong></p>
                <p>БИК получателя: <strong>{recipientBIK}</strong></p>
                <p>КБе получателя: <strong>{recipientKBe}</strong></p>
                <p>Сумма: <strong>{amount}</strong></p>

                <Divider orientation="left" plain>Обработка заявки</Divider>
                <h3>Причина отказа: </h3>
                <div className="accountant_rejection">
                <TextField 
                    multiline
                    rows={4} 
                    placeholder="Введите причину отказа"
                    id={"reason"}
                    name={"reason"}
                    value={rejectionReason}
                    onChange={inputChangeHandler}
                />
                <div className="info_buttons">
                    <Space>
                        {contextHolder}
                        <Button type="primary" onClick={()=>{approveReq(_id)}} >Одобрить</Button>
                        <Button onClick={()=>{declineReq(_id)}} disabled={!rejectionReason}>Отклонить</Button>
                    </Space>
                </div>
                
            </div>
            </div>
        )
}

export default AccountantMyTransInfo;
