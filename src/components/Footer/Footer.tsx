import React from "react";
import "./Footer.css";
import phone from './img/phone.png';
import mail from './img/sms.png';
import marker from './img/marker.png';

function Footer() {
  return (
    <footer className="footer">
      <div className="footer__content">
        <div className="container">
          <div className="footer__inner">
            <div className="footer__info">
              <div className="footer__list">
                <div><img src={phone} width="20" height="20" /><a className="footer__phone" href="">+7 (727) 344 29 00</a></div>
                <div><img src={mail} width="20" height="20" /><a className="footer__mail" href="#">info@fhs.kz</a></div>
                <div><img src={marker} width="20" height="20" /><a className="footer__adress" href="#">Алматы, пр.Назарбаева, 242</a></div>
              </div>
            </div>
            <div className="footer__map">
              {/* //карта 
         */}

            </div>
          </div>
        </div>
      </div>
      <div className="footer__copy">
        <div className="container">
          <div className="copy__text">
            ©{new Date().getFullYear()} Лицензия № 3.1.1.224 от 27.03.18. Все права защищены.
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
