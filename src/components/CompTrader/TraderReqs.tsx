import React, {useEffect, useState} from "react";
import {Table, Tag, Button, Drawer, Space } from 'antd';
import { SearchOutlined } from "@ant-design/icons";
import type { ColumnsType } from 'antd/es/table';
import {useAppDispatch, useAppSelector} from "../../hook";
import {queryStockReqTraderAT, paramsStockReqAT} from "../../store/services/stockRequestSlice";
import {TGetPostStockRequestAT} from "../../types/typesStockRequest";
import {getDate} from "../../Helpers/getDate";
import { getStatusColor } from "../../Helpers/getStatusColor";
import { getStatusTrader } from "../../Helpers/getStatusTrader";
import { getStockReqType } from "../../Helpers/getStockReqType";
import { filterAntTable } from "../../Helpers/filterAntTable";
import TraderReqInfo from "./TraderReqInfo";
import { TStatusStock, TTrader, TCompany, TTicker, TStockAmount, TPrice, TUserFullName, TStockReqType, TDate,  T_id } from "../../types/reestrOfAlias";


const TraderReqs: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allStocksRequest: TGetPostStockRequestAT[] = useAppSelector(state=>state.reducerStock.allStocksRequest);
    const oneStockRequest: TGetPostStockRequestAT = useAppSelector(state=>state.reducerStock.oneStockRequest);

    useEffect(() => {
        dispatch(queryStockReqTraderAT());  
    }, [dispatch]);

    const [openDrawer, setOpenDrawer] = useState(false);

    const selectStockReq = (record: any) => {
        dispatch(paramsStockReqAT(record._id));
        setOpenDrawer(true);
    };

    const onCloseDrawer = () => {
        setOpenDrawer(false);
    };


    interface TGetPostStockRequestAT {
        _id: T_id;
        status: TStatusStock;
        stockReqType: TStockReqType;              
        stockAmount: TStockAmount; 
        price: TPrice;
        ticker: TTicker,                  
        date: TDate;
    };
      
    const columns: ColumnsType<TGetPostStockRequestAT> = [
        {
            title: 'Номер заявки',
            dataIndex: '_id',
            key: '_id',
            ...filterAntTable<TGetPostStockRequestAT>(),
            onFilter: (value, record) => {
                return record._id
                    .toString()
                    .toLowerCase()
                    .includes(value.toString().toLowerCase())
            }
        },
        {
            title: 'Дата',
            dataIndex: 'date',
            key: 'date',
            render: (date) => (
                getDate(date)
            )
        },
        {
            title: 'Тип заявки',
            dataIndex: 'stockReqType',
            key: 'stockReqType',
            filters: [ {text: "Покупка", value: "Buy"}, {text: "Продажа", value: "Sell"}],
            filterIcon: () => {return <SearchOutlined />},
            onFilter: (value: string | number | boolean, record) => record.stockReqType!.indexOf(value as string) === 0,
            render: (stockReqType) => (
                getStockReqType(stockReqType)
            )
        },
        {
            title: 'Тикер',
            dataIndex: 'ticker',
            key: 'ticker',
            ...filterAntTable<TGetPostStockRequestAT>(),
            onFilter: (value, record) => {
                return record.ticker
                    .toString()
                    .toLowerCase()
                    .includes(value.toString().toLowerCase())
            }
        },
        {
            title: 'Статус',
            key: 'status',
            dataIndex: 'status',
            filters: [ {text: "Approved", value: "Approved"}, {text: "TraderWIP", value: "TraderWIP"}],
            filterIcon: () => {return <SearchOutlined />},
            onFilter: (value: string | number | boolean, record) => record.status!.indexOf(value as string) === 0,
            render: (status) => (
                <>  
                    <Tag key={status} color={getStatusColor(status)}>
                    {getStatusTrader(status)}
                    </Tag> 
                </>
            ),
        },
        {
            title: 'Действие',
            key: 'action',
            render: (_, record) => (
                <Button onClick={()=>selectStockReq(record)}>Посмотреть</Button>
            ),
        },
    ];

    return (
        <div className="trader_table">
            <Table columns={columns} dataSource={allStocksRequest} rowKey="_id" />

            <Drawer 
                title="Полная информация по заявке"
                width={720}
                onClose={onCloseDrawer}
                open={openDrawer}
                extra={
                    <Button onClick={onCloseDrawer}>Отмена</Button>
                }
            >
                <TraderReqInfo {...oneStockRequest!}/>
            </Drawer>
        </div>
    )
}

export default TraderReqs;