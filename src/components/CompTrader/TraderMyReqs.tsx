import React, {useEffect, useState} from "react";
import {Table, Button, Drawer, Space, Divider, message, Form, InputNumber } from 'antd';
import {TextField} from "@mui/material";
import { SearchOutlined } from "@ant-design/icons";
import type { ColumnsType } from 'antd/es/table';
import {useAppDispatch, useAppSelector} from "../../hook";
import {queryStockReqTraderWIPAT, paramsStockReqAT, patchStockRequestAT} from "../../store/services/stockRequestSlice";
import {TGetPostStockRequestAT} from "../../types/typesStockRequest";
import {createClientStockAT } from "../../store/services/clientStockSlice";
import {notificationObject} from "../../Helpers/notificationObject";
import {getStatus} from "../../Helpers/getStatus";
import {getDate} from "../../Helpers/getDate";
import { getStockReqType } from "../../Helpers/getStockReqType";
import TraderReqInfo from "./TraderReqInfo";
import { filterAntTable } from "../../Helpers/filterAntTable";
import { TStatusStock, TTrader, TCompany, TTicker, TStockAmount, TPrice, TUserFullName, TStockReqType, TDate,  T_id, TUser, TRejectionReason } from "../../types/reestrOfAlias";
import { gridDensityValueSelector } from "@mui/x-data-grid";


const TraderMyReqs: React.FC = ()=>{
    const dispatch = useAppDispatch();
    const allStocksRequest: TGetPostStockRequestAT[] = useAppSelector(state=>state.reducerStock.allStocksRequest);
    const oneStockRequest: TGetPostStockRequestAT | any = useAppSelector(state=>state.reducerStock.oneStockRequest);
    const dataUser:TUser = useAppSelector(state=>state.reducerUsers.user);

    useEffect(() => {
        dispatch(queryStockReqTraderWIPAT(dataUser?._id!));   
    }, [dispatch]);

    const [openDrawer, setOpenDrawer] = useState(false);
    const [openChild, setOpenChild] = useState(false);

    const [messageApi, contextHolder] = message.useMessage();

    const [reason, setReason] = React.useState<string>("");
    const inputChangeHandler: React.ChangeEventHandler<HTMLInputElement> = (e)=>{
        const value: string = e.target.value;
        setReason(value);
    };

    const onCloseDrawer = () => {
        setOpenDrawer(false);
    };

    const onCloseChild = () => {
        setOpenChild(false);
    };

    const selectStockReq = (record: any) => {
        dispatch(paramsStockReqAT(record._id));
        setOpenDrawer(true);
    };

    const acceptReq = (id: string | undefined) => {
        setOpenChild(true);
    };

    const [form] = Form.useForm();
    const onReset = () => {
        form.resetFields();
    };

    const declineReq = (id: string | undefined) => {
        dispatch(patchStockRequestAT({
            status: "Declined",
            rejectionReason: reason,
            _idStockRequest: id,
        }));
        dispatch(notificationObject(
            oneStockRequest._id!,
            oneStockRequest.user?._id!,
            "Declined",
            "stockRequest",
            getStatus("Declined") + " по причине: " + reason
        ));
        messageApi.open({
            type: "warning",
            content: "Заявка отклонена"
        });
        console.log("Declined " + id)
    };

    
    const onFinish = (values: any) => { 
        let tradeResult = "";
        if (oneStockRequest.stockReqType === "Buy") {
            tradeResult = ". Куплено: " + values.stockResult + ". Не удалось купить: " + values.stockFail+ ". Итоговая цена: " + values.priceResult; 
        }  else {
            tradeResult = ". Продано: " + values.stockResult + ". Не удалось продать: " + values.stockFail+ ". Итоговая цена: " + values.priceResult;
        }  

        dispatch(patchStockRequestAT({
            status: "PartiallyCompleted",
            _idStockRequest: oneStockRequest._id
        }));
        dispatch(notificationObject(
            oneStockRequest._id!,
            oneStockRequest.user?._id!,
            "PartiallyCompleted",
            "stockRequest",
            getStatus("PartiallyCompleted") + tradeResult,
        ));
        const responseData = {
            user: oneStockRequest.user._id,
            company: oneStockRequest.company,
            ticker: oneStockRequest.ticker,
            stockReqType: oneStockRequest.stockReqType,
            amount: values.stockResult
        }
        dispatch(createClientStockAT(responseData));
        messageApi.open({
            type: "success",
            content: "Заявка одобрена"
        });
        onCloseChild();
        onCloseDrawer();
        onReset();
    };

    interface TGetPostStockRequestAT {
        company: TCompany;
        _id: T_id;
        status: TStatusStock | undefined;
        stockReqType: TStockReqType;              
        stockAmount: TStockAmount; 
        price: TPrice;
        ticker: TTicker,                  
        date: TDate;
        rejectionReason?: TRejectionReason
    };
      
    const columns: ColumnsType<TGetPostStockRequestAT> = [
        {
            title: 'Номер заявки',
            dataIndex: '_id',
            key: '_id',
            ...filterAntTable<TGetPostStockRequestAT>(),
            onFilter: (value, record) => {
                return record._id
                    .toString()
                    .toLowerCase()
                    .includes(value.toString().toLowerCase())
            }
        },
        {
            title: 'Дата',
            dataIndex: 'date',
            key: 'date',
            render: (date) => (
                getDate(date)
            )
        },
        {
            title: 'Тип заявки',
            dataIndex: 'stockReqType',
            key: 'stockReqType',
            filters: [ {text: "Покупка", value: "Buy"}, {text: "Продажа", value: "Sell"}],
            filterIcon: () => {return <SearchOutlined />},
            onFilter: (value: string | number | boolean, record) => record.stockReqType!.indexOf(value as string) === 0,
            render: (stockReqType) => (
                getStockReqType(stockReqType)
            )
        },
        {
            title: 'Тикер',
            dataIndex: 'ticker',
            key: 'ticker',
            ...filterAntTable<TGetPostStockRequestAT>(),
            onFilter: (value, record) => {
                return record.ticker
                    .toString()
                    .toLowerCase()
                    .includes(value.toString().toLowerCase())
            }
        },
        {
            title: 'Действие',
            key: 'action',
            render: (_, record) => (
                <Button onClick={()=>selectStockReq(record)}>Посмотреть</Button>
            ),
        },
    ];

    return (
        <div className="trader_table">
            <Table columns={columns} dataSource={allStocksRequest} rowKey="_id" />

            {!!oneStockRequest ? (<Drawer 
                title="Полная информация по заявке"
                width={720}
                onClose={onCloseDrawer}
                open={openDrawer}
                extra={
                    <Button onClick={onCloseDrawer}>Отмена</Button>
                }
                >
                    <Divider orientation="left" plain>Данные заявки</Divider>
                <p>Номер заявки: <strong>{oneStockRequest._id}</strong></p>
                <p>Тип заявки: <strong>{getStockReqType(oneStockRequest.stockReqType)}</strong></p>
                <p>Дата: <strong>{getDate(oneStockRequest.date as string)}</strong></p>

                <Divider orientation="left" plain>Данные заказа</Divider>
                <p>Название: <strong>{oneStockRequest.company}</strong></p>
                <p>Тикер: <strong>{oneStockRequest.ticker}</strong></p>
                <p>Цена: <strong>{oneStockRequest.price}</strong></p>
                <p>Количество: <strong>{oneStockRequest.stockAmount}</strong></p>
                
                <Divider orientation="left" plain>Обработка заявки</Divider>
                <h3>Причина отказа: </h3>
                <div className="accountant_rejection">
                    <TextField 
                        multiline
                        rows={4} 
                        placeholder="Введите причину отказа"
                        id={"reason"}
                        name={"reason"}
                        value={reason}
                        onChange={inputChangeHandler}
                    />
                    <div className="info_buttons">
                        <Space>
                            {contextHolder}
                            <Button type="primary" onClick={()=>{acceptReq(oneStockRequest._id)}}>Принять</Button>
                            <Button onClick={()=>{declineReq(oneStockRequest._id)}} disabled={!reason}>Отклонить</Button>
                        </Space>
                    </div>
                    
                </div>

                <Drawer
                    title="Покупка / Продажа акций по заявке клиента"
                    width={720}
                    onClose={onCloseChild}
                    open={openChild}
                    extra={
                        <Button onClick={onCloseChild}>Отмена</Button>
                    }
                >
                    {!!(oneStockRequest.stockReqType === "Buy") ? (
                        <Form
                            form={form}
                            name="request_form"
                            labelCol = {{ span: 6 }}
                            wrapperCol= {{ span: 16}}
                            onFinish = {onFinish}
                    >
                        <Form.Item label="Куплено акций" name="stockResult">
                            <InputNumber min={0}/>
                        </Form.Item>
                        <Form.Item label="Не удалось купить" name="stockFail">
                            <InputNumber min={0}/>
                        </Form.Item>
                        <Form.Item label="Итоговая цена" name="priceResult">
                            <InputNumber min={0}/>
                        </Form.Item>

                        <Button type="primary" htmlType="submit">Завершить заявку</Button>
                    </Form> 
                    ) : (
                        <Form
                            form={form}
                            name="request_form"
                            labelCol = {{ span: 6 }}
                            wrapperCol= {{ span: 16}}
                            onFinish = {onFinish}
                    >
                        <Form.Item label="Продано акций" name="stockResult">
                            <InputNumber min={0}/>
                        </Form.Item>
                        <Form.Item label="Не удалось продать" name="stockFail">
                            <InputNumber min={0}/>
                        </Form.Item>
                        <Form.Item label="Итоговая цена" name="priceResult">
                            <InputNumber min={0}/>
                        </Form.Item>

                        <Button type="primary" htmlType="submit">Завершить заявку</Button>
                    </Form> 
                    )}
                </Drawer>
            
            </Drawer>) : <div></div>}
        </div>
    )
}

export default TraderMyReqs;