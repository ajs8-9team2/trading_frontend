import React, {useState} from "react";
import {Button, message, Divider, Tag} from 'antd';
import {patchStockRequestAT} from "../../store/services/stockRequestSlice";
import {notificationObject} from "../../Helpers/notificationObject";
import {getDate} from "../../Helpers/getDate";
import { getStatusColor } from "../../Helpers/getStatusColor";
import { getStatusTrader } from "../../Helpers/getStatusTrader";
import { getStockReqType } from "../../Helpers/getStockReqType";
import {TGetPostStockRequestAT} from "../../types/typesStockRequest";
import {getStatus} from "../../Helpers/getStatus";
import {TUser} from "../../types/reestrOfAlias";
import {useAppDispatch, useAppSelector} from "../../hook";

const TraderReqInfo = (propsStockRequest: any)=>{
    const {
        _id, date, status, trader, stockAmount, price, company, ticker, stockReqType, user
    } = propsStockRequest!

    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);
    const [messageApi, contextHolder] = message.useMessage();

    const acceptReq = (id: string | undefined) => {
        dispatch(patchStockRequestAT({
            status: "TraderWIP",
            _idStockRequest: id,
            trader: dataUser?._id!
        }));
        dispatch(notificationObject(
            _id!,
            user?._id!,
            "TraderWIP",
            "stockRequest",
            getStatus("TraderWIP")
        ));
        messageApi.open({
            type: "success",
            content: "Заявка закреплена за вами"
        });
    };

    return (
        <div className="TraderReqInfo">
            <Divider orientation="left" plain>Данные заявки</Divider>
            <p>Номер заявки: <strong>{_id}</strong></p>
            <p>Статус: <strong>{getStatusTrader(status)}</strong></p>
            <p>Тип заявки: <strong>{getStockReqType(stockReqType)}</strong></p>
            <p>Дата: <strong>{getDate(date as string)}</strong></p>
            {!!trader ? (
                <p>Закреплена за: <strong>{trader.fullName}</strong></p>
            ) : <p></p>}

            <Divider orientation="left" plain>Данные заказа</Divider>
            <p>Название: <strong>{company}</strong></p>
            <p>Тикер: <strong>{ticker}</strong></p>
            <p>Цена: <strong>{price}</strong></p>
            <p>Количество: <strong>{stockAmount}</strong></p>
            
            <Divider orientation="left" plain>Обработка заявки</Divider>
            {contextHolder}
            <Button type="primary" onClick={()=>{acceptReq(_id)}} disabled={status === "TraderWIP"} > Начать работу над заявкой</Button>
        </div>
    )
};

export default TraderReqInfo;
