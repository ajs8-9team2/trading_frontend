import * as React from "react";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableSortLabel from "@mui/material/TableSortLabel";
import Box from "@mui/material/Box";
import {visuallyHidden} from "@mui/utils";
import {Order} from "../EnhancedTable";
import {Divider} from "@mui/material";
import {TTotalPickTable} from "../../../../../../containers/FrontOfficeModern/DepositRequest/FrontOfficeDepositListReq";
import {Context} from "../../../../../../context";


interface HeadCell{
    disablePadding: boolean;
    id: keyof TTotalPickTable;
    label: string;
    numeric: boolean;
}

// {
//     _id: string,
//         firstName: string,
//     lastName: string,
//     patronym: string,
//     dateOfBirth: string,
//     identificationNumber: string,
// }

// const headCells: readonly HeadCell[] = [
//     {
//         id: '_id',
//         numeric: false,
//         disablePadding: false,
//         label: 'ID заявления',
//     },
//     {
//         id: 'status',
//         numeric: false,
//         disablePadding: false,
//         label: 'Статус рассмотрения:',
//     },
//     {
//         id: 'date',
//         numeric: false,
//         disablePadding: false,
//         label: 'дата создания:',
//     },
//     {
//         id: 'amount',
//         numeric: false,
//         disablePadding: false,
//         label: 'сумма перевода:',
//     },
//     {
//         id: 'senderFullName',
//         numeric: false,
//         disablePadding: false,
//         label: 'ФИО отправителя:',
//     },
//     {
//         id: 'senderIIN',
//         numeric: false,
//         disablePadding: false,
//         label: 'ИИН отправителя:',
//     },
//     {
//         id: 'senderBank',
//         numeric: false,
//         disablePadding: false,
//         label: 'Банк отправителя:',
//     },
//     {
//         id: 'senderIIK',
//         numeric: false,
//         disablePadding: false,
//         label: 'ИИК(IBAN):',
//     },
//     {
//         id: 'senderBIK',
//         numeric: false,
//         disablePadding: false,
//         label: 'БИК:',
//     },
//     {
//         id: 'frontManager',
//         numeric: false,
//         disablePadding: false,
//         label: 'ID сотрудника:',
//     },
//     {
//         id: 'getterFullName',
//         numeric: false,
//         disablePadding: false,
//         label: 'ФИО получателя:',
//     },
//     {
//         id: 'identificationNumber',
//         numeric: false,
//         disablePadding: false,
//         label: 'ИИН получателя:',
//     },
//
//     // {
//     //     id: 'firstName',
//     //     numeric: false,
//     //     disablePadding: false,
//     //     label: 'Имя',
//     // },
//     // {
//     //     id: 'lastName',
//     //     numeric: false,
//     //     disablePadding: false,
//     //     label: 'Фамилия',
//     // },
//     // {
//     //     id: 'patronym',
//     //     numeric: false,
//     //     disablePadding: false,
//     //     label: 'Отчество',
//     // },
//     // {
//     //     id: 'dateOfBirth',
//     //     numeric: false,
//     //     disablePadding: false,
//     //     label: 'Д/Р',
//     // },
//     // {
//     //     id: 'identificationNumber',
//     //     numeric: false,
//     //     disablePadding: false,
//     //     label: 'ИИН',
//     // },
// ];

interface EnhancedTableProps{
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof TTotalPickTable)=>void;
    order: Order;
    orderBy: string;
    rowCount: number;
    // right: readonly number[];
}

function EnhancedTableHead(props: EnhancedTableProps){
    // const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
    const {tableRowsName} = React.useContext(Context)
    const {order, orderBy, onRequestSort} = props;
    const createSortHandler =
        (property: keyof TTotalPickTable)=>(event: React.MouseEvent<unknown>)=>{
            onRequestSort(event, property);
        };

    return (
        <TableHead>
            <TableRow>
                {/*<TableCell padding="checkbox">*/}
                    {/*<Checkbox*/}
                    {/*    color="primary"*/}
                    {/*    indeterminate={numSelected > 0 && numSelected < rowCount}*/}
                    {/*    checked={rowCount > 0 && numSelected === rowCount}*/}
                    {/*    onChange={onSelectAllClick}*/}
                    {/*    inputProps={{*/}
                    {/*        'aria-label': 'select all desserts',*/}
                    {/*    }}*/}
                    {/*/>*/}
                {/*</TableCell>*/}

                <TableCell
                    // key={item.id}
                    // align={item.numeric ? 'right' : 'left'}
                    align={'left'}
                    // padding={item.disablePadding ? 'none' : 'normal'}
                    padding={'normal'}
                    sortDirection={orderBy === "num" ? order : false}
                >
                    <TableSortLabel
                        active={orderBy === "num"}
                        direction={orderBy === "num" ? order : 'asc'}
                        onClick={createSortHandler("num")}
                    >
                        {"#"}
                        {orderBy === "num" ? (
                            <Box component="span" sx={visuallyHidden}>
                                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                            </Box>
                        ) : null}
                    </TableSortLabel>
                </TableCell>

                {(()=>{
                    return tableRowsName.map((item: string[])=>{
                        const headCells: {
                            [key: string]: any
                        } = {}
                        headCells.id = item[0];
                        headCells.numeric = false;
                        headCells.disablePadding = false;
                        headCells.label = item[1];

                        return (
                            <TableCell
                                key={headCells.id}
                                // align={headCells.numeric ? 'right' : 'left'}
                                align={'left'}
                                padding={headCells.disablePadding ? 'none' : 'normal'}
                                sortDirection={orderBy === headCells.id ? order : false}
                            >
                                <TableSortLabel
                                    active={orderBy === headCells.id}
                                    direction={orderBy === headCells.id ? order : 'asc'}
                                    onClick={createSortHandler(headCells.id)}
                                >
                                    {headCells.label}
                                    {orderBy === headCells.id ? (
                                        <Box component="span" sx={visuallyHidden}>
                                            {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                        </Box>
                                    ) : null}
                                </TableSortLabel>
                            </TableCell>
                        )
                    })
                })()}

                {/*{headCells.map((item, index)=>{*/}
                {/*    return (*/}
                {/*        <TableCell*/}
                {/*            key={item.id}*/}
                {/*            // align={item.numeric ? 'right' : 'left'}*/}
                {/*            align={'center'}*/}
                {/*            padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*            sortDirection={orderBy === item.id ? order : false}*/}
                {/*        >*/}
                {/*            <TableSortLabel*/}
                {/*                active={orderBy === item.id}*/}
                {/*                direction={orderBy === item.id ? order : 'asc'}*/}
                {/*                onClick={createSortHandler(item.id)}*/}
                {/*            >*/}
                {/*                {item.label}*/}
                {/*                {orderBy === item.id ? (*/}
                {/*                    <Box component="span" sx={visuallyHidden}>*/}
                {/*                        {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*                    </Box>*/}
                {/*                ) : null}*/}
                {/*            </TableSortLabel>*/}
                {/*            <Divider orientation="vertical" flexItem />*/}
                {/*        </TableCell>*/}
                {/*    )*/}
                {/*})}*/}


                {/*{right.includes(0) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "_id" ? order : false}*/}
                {/*>*/}

                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "_id"}*/}
                {/*        direction={orderBy === "_id" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("_id")}*/}
                {/*    >*/}
                {/*        {"ID заявления"}*/}
                {/*        {orderBy === "_id" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}



                {/*{right.includes(1) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "date" ? order : false}*/}
                {/*>*/}
                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "date"}*/}
                {/*        direction={orderBy === "date" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("date")}*/}
                {/*    >*/}
                {/*        {"дата созд."}*/}
                {/*        {orderBy === "date" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}



                {/*{right.includes(2) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "status" ? order : false}*/}
                {/*>*/}
                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "status"}*/}
                {/*        direction={orderBy === "status" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("status")}*/}
                {/*    >*/}
                {/*        {"статус"}*/}
                {/*        {orderBy === "status" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}



                {/*{right.includes(3) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "frontManager" ? order : false}*/}
                {/*>*/}
                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "frontManager"}*/}
                {/*        direction={orderBy === "frontManager" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("frontManager")}*/}
                {/*    >*/}
                {/*        {"ID менеджера"}*/}
                {/*        {orderBy === "frontManager" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}



                {/*{right.includes(4) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "rejectionReason" ? order : false}*/}
                {/*>*/}
                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "rejectionReason"}*/}
                {/*        direction={orderBy === "rejectionReason" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("rejectionReason")}*/}
                {/*    >*/}
                {/*        {"причина отклонения"}*/}
                {/*        {orderBy === "rejectionReason" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}


                {/*{right.includes(4) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "firstName" ? order : false}*/}
                {/*>*/}

                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "firstName"}*/}
                {/*        direction={orderBy === "firstName" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("firstName")}*/}
                {/*    >*/}
                {/*        {"Имя"}*/}
                {/*        {orderBy === "firstName" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}


                {/*{right.includes(5) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "lastName" ? order : false}*/}
                {/*>*/}

                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "lastName"}*/}
                {/*        direction={orderBy === "lastName" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("lastName")}*/}
                {/*    >*/}
                {/*        {"Фамилия"}*/}
                {/*        {orderBy === "lastName" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}


                {/*{right.includes(6) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "patronym" ? order : false}*/}
                {/*>*/}

                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "patronym"}*/}
                {/*        direction={orderBy === "patronym" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("patronym")}*/}
                {/*    >*/}
                {/*        {"Отчество"}*/}
                {/*        {orderBy === "patronym" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}

                {/*{right.includes(7) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "dateOfBirth" ? order : false}*/}
                {/*>*/}

                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "dateOfBirth"}*/}
                {/*        direction={orderBy === "dateOfBirth" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("dateOfBirth")}*/}
                {/*    >*/}
                {/*        {"Д/Р"}*/}
                {/*        {orderBy === "dateOfBirth" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}

                {/*{right.includes(8) &&*/}
                {/*<TableCell*/}
                {/*    // key={item.id}*/}
                {/*    // align={item.numeric ? 'right' : 'left'}*/}
                {/*    align={'right'}*/}
                {/*    // padding={item.disablePadding ? 'none' : 'normal'}*/}
                {/*    padding={'normal'}*/}
                {/*    sortDirection={orderBy === "identificationNumber" ? order : false}*/}
                {/*>*/}

                {/*    <TableSortLabel*/}
                {/*        active={orderBy === "identificationNumber"}*/}
                {/*        direction={orderBy === "identificationNumber" ? order : 'asc'}*/}
                {/*        onClick={createSortHandler("identificationNumber")}*/}
                {/*    >*/}
                {/*        {"ИИН"}*/}
                {/*        {orderBy === "identificationNumber" ? (*/}
                {/*            <Box component="span" sx={visuallyHidden}>*/}
                {/*                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}*/}
                {/*            </Box>*/}
                {/*        ) : null}*/}
                {/*    </TableSortLabel>*/}
                {/*</TableCell>}*/}

            </TableRow>
        </TableHead>
    );
}

export default EnhancedTableHead;