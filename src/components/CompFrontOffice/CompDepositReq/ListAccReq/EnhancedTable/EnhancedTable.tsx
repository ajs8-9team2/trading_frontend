import * as React from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import {TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import {useAppDispatch} from "../../../../../hook";
import EnhancedTableHead from "./EnhancedTableHead/EnhancedTableHead";
import {getDate} from "../../../../../Helpers/getDate";
import {Divider, Typography} from "@mui/material";
import {TDepositRequestState, TGetPostDepositRequestAT} from "../../../../../types/typesDepositRequest";
import {TTotalPickTable} from "../../../../../containers/FrontOfficeModern/DepositRequest/FrontOfficeDepositListReq";
import {getStatus} from "../../../../../Helpers/getStatus";
import {TStateStatus, TStatusTransferDeposit} from "../../../../../types/reestrOfAlias";
import {filterAndConcatArras} from "../../../../../Helpers/filterAndConcatArras";
import {genericFilter} from "../../../../../Helpers/genericFilter";
import {Context} from "../../../../../context";
import {getValueOfKey} from "../../../../../Helpers/getValueOfKey";
import {THeaderTable} from "../../../CompAccRequest/ListAccReq/EnhancedTable/EnhancedTable";

export type Order = 'asc' | 'desc';


function descendingComparator<T>(a: T, b: T, orderBy: keyof T){
    if(b[orderBy] < a[orderBy]){
        return -1;
    }
    if(b[orderBy] > a[orderBy]){
        return 1;
    }
    return 0;
}

function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
): (
    a: { [key in Key]: number | string },
    b: { [key in Key]: number | string },
)=>number{
    return order === 'desc'
        ? (a, b)=>descendingComparator(a, b, orderBy)
        : (a, b)=>-descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort<T>(array: readonly T[], comparator: (a: T, b: T)=>number){
    const stabilizedThis = array.map((el, index)=>[el, index] as [T, number]);
    stabilizedThis.sort((a, b)=>{
        const order = comparator(a[0], b[0]);
        if(order !== 0){
            return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el)=>el[0]);
}

export default function EnhancedTable({
                                          // setConcatOneAccOneDepos,
                                          allDepositsRequest,
                                          allAccRequests,
                                          // right,/*,setGetID*/
                                          // depositsStateStatus
                                      }: {
    // setConcatOneAccOneDepos:React.Dispatch<React.SetStateAction<TTotalPickTable | undefined>>;
    allDepositsRequest: TGetPostDepositRequestAT[];
    allAccRequests: TGetAccRequestsAT[];
    // right: readonly number[];
    // depositsStateStatus: TStateStatus
}){

    const {
        statusReq,
        tableRowsName,
        depositsStateStatus,
        setConcatOneAccOneDeposContext,
        set_idDepositReqContext,
        employees
    } = React.useContext(Context)

    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof TTotalPickTable>('num');
    const [page, setPage] = React.useState(0);
    const [dense, setDense] = React.useState(false);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [rowsArray, setRowsArray] = React.useState<any>([]);

    // в данном use пересобирается таблица,
    // если произошло изменение в стейте
    React.useEffect(()=>{
        const innerRowsArray: TTotalPickTable[] = filterAndConcatArras(
            allDepositsRequest, allAccRequests,
            ["_id", "status", "amount", "senderFullName", "senderIIN",
                "senderBank", "senderIIK", "senderBIK", "frontManager", "date"],
            ["firstName", "lastName", "patronym", "identificationNumber"],
            [""]
        )

// ["_id", "ID заявления"],
// ["status", "Статус рассмотрения"],
// ["date", "дата созд."],
// ["amount", "сумма перевода"],
// ["senderFullName", "ФИО отправителя"],
// ["senderIIN", "ИИН отправителя"],
// ["senderBank", "Банк отправителя"],
// ["senderIIK", "ИИК(IBAN)"],
// ["senderBIK", "БИК"],
// ["frontManager", "ID сотрудника"],
// ["getterFullName", "ФИО получателя"],
// ["identificationNumber", "ИИН получателя"],
        //---------------------------------------------------------------------------------
        console.clear()
        const innerObj: {
            [key: string]: any
        } = {}

        tableRowsName.forEach((item: string[])=>{
            innerObj[item[0]] = undefined;
        })
        //------------------------------------
        let innerCount = 0;
        // let innerRowsArray = new Array();
        let newInnerRowsArray: Partial<TTotalPickTable>[] = [];

        for(const innerRowsArrayElement of innerRowsArray){
            // console.log(JSON.stringify(innerRowsArrayElement, null, 2))
            const hasAssignedTo: any = {...innerRowsArrayElement}
            if(!hasAssignedTo.hasOwnProperty("frontManager"))
                hasAssignedTo["frontManager"] = "not_assigned"
            //**********
            if(!statusReq.includes(innerRowsArrayElement?.status)) continue
            const isFlag = employees?.some((item: (string | undefined)[])=>item.includes(innerRowsArrayElement?.frontManager));
            const isFlag2 = employees?.some((item: (string | undefined)[])=>item.includes(hasAssignedTo.frontManager));
            const isFlag3 = isFlag || isFlag2;
            if(!isFlag3) continue;

            // const itemObj: any = innerRowsArrayElement;
            // innerCount++;
            // const myObj = {...innerObj}
            // myObj.num = innerCount;//index + 1;
            //
            // for(const itemKey in itemObj){
            //
            //     // console.log(Object.keys(myObj), " <<<<<<<<")
            //     // if(!Object.keys(myObj).includes(itemKey)) continue;
            //     // if(myObj.hasOwnProperty(itemKey))
            //     // if(statusReq.includes[itemObj.status])
            //     if(typeof itemObj[itemKey] === "object"){
            //         if(itemKey === "frontManager")
            //             myObj[itemKey] = itemObj[itemKey]?._id ?? "none";
            //     }else
            //         myObj[itemKey] = itemObj[itemKey];
            // }
            //
            // /** там где значение ключа undefined добавляем none */
            // for(const myObjKey in myObj){
            //     if(myObj[myObjKey] === undefined)
            //         myObj[myObjKey] = "none";
            // }
            // console.log(myObj," <<<")
            // newInnerRowsArray.push(myObj)
            innerCount++;
            newInnerRowsArray.push({...innerRowsArrayElement, num: innerCount + ""})
        }

        // console.log(  JSON.stringify(newInnerRowsArray, null, 2))
        // {
        //     "_id": "63bcfbc3a072d68817b9cf7b",
        //     "status": "FrontWIP",
        //     "amount": 55245,
        //     "senderFullName": "Джакоб Вернеров Кэйсерович",
        //     "senderIIN": "870318123456",
        //     "senderBank": "АО «Евразийский Банк»",
        //     "senderIIK": "KZ687624324899292281",
        //     "senderBIK": "EURIKZKA",
        //     "date": "2023-01-10T05:46:25.598Z",
        //     "frontManager": "63bcfbb1a072d68817b9cb4f",
        //     "getterFullName": "Аделхард Амалрихов Еремиасович",
        //     "identificationNumber": "950824123456"
        // },

        setRowsArray(newInnerRowsArray);
        // setRowsArray(innerRowsArray);

        if(0)//fake data
            setRowsArray([{
                "_id": "63bcfbc3a072d68817b9cf7b",
                "status": "FrontWIP",
                "amount": 55245,
                "senderFullName": "Джакоб Вернеров Кэйсерович",
                "senderIIN": "870318123456",
                "senderBank": "АО «Евразийский Банк»",
                "senderIIK": "KZ687624324899292281",
                "senderBIK": "EURIKZKA",
                "date": "2023-01-10T05:46:25.598Z",
                "frontManager": "63bcfbb1a072d68817b9cb4f",
                "getterFullName": "Аделхард Амалрихов Еремиасович",
                "identificationNumber": "950824123456",
            }]);
    }, [depositsStateStatus/*, tableRowsName*/, statusReq, employees])
//#########################################################################

    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof TTotalPickTable,
    )=>{
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };


    // const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const handleClick = (deposit_id: string, rowsArray: TTotalPickTable[])=>{
        const innerArray = rowsArray.filter(item=>item._id === deposit_id)
        setConcatOneAccOneDeposContext(innerArray[0])
        set_idDepositReqContext(deposit_id)// получаем id заявки
        // alert(JSON.stringify(innerArray[0]))
        // setConcatOneAccOneDepos(innerArray[0])
        // dispatch(paramsDepositReqAT(name))
        // setGetID(name)
    };

    const handleChangePage = (event: unknown, newPage: number)=>{
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>)=>{
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>)=>{
        setDense(event.target.checked);
    };
    // Avoid a layout jump when reaching the last page with empty rowsArray.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rowsArray.length) : 0;


    //##################################################################################
    // allDepositsRequest,
    //     allAccRequests,
    if(0)
        return (
            <>
                {/*<pre>{JSON.stringify(allAccRequests[0], null, 2)}</pre>*/}

                <pre>{JSON.stringify(rowsArray, null, 2)}</pre>
            </>
        )
    //##################################################################################
    if(tableRowsName.length && rowsArray.length && employees.length)
        return (
            <Box sx={{
                width: '100%',
                // height: '100%'// метка, потом удалить или пересмотреть
            }}>
                {/*<pre>{JSON.stringify(right, null, 2)}</pre>*/}
                <Paper sx={{width: '100%', mb: 2}}>
                    {/*<EnhancedTableToolbar numSelected={selected.length} />*/}
                    <TableContainer>
                        {/*<pre>{JSON.stringify(depositsData.depositRequestStatus, null, 2)}</pre>*/}

                        <Table
                            // sx={{minWidth: 750}}
                            sx={{minWidth: 50}}
                            aria-labelledby="tableTitle"
                            size={dense ? 'small' : 'medium'}
                        >
                            <EnhancedTableHead
                                // right={right}
                                order={order}
                                orderBy={orderBy}
                                onRequestSort={handleRequestSort}
                                rowCount={rowsArray.length}
                            />
                            <TableBody>
                                {/* if you don't need to support IE11, you can replace the `stableSort` call with:
              rowsArray.sort(getComparator(order, orderBy)).slice() */}
                                {/*---------------------------------------------------------------------------------------*/}
                                {/*---------------------------------------------------------------------------------------*/}
                                {/*---------------------------------------------------------------------------------------*/}
                                {/*{stableSort(rowsArray, getComparator(order, orderBy))*/}
                                {stableSort(rowsArray, getComparator(order, orderBy))
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((item, index)=>{

                                        // "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
                                        // if(item.status === "FrontWIP" || item.status === "Pending"){
                                        const labelId = `enhanced-table-checkbox-${index}`;
                                        return (
                                            <TableRow
                                                hover
                                                // onClick={(event) => handleClick(event, item?._id!)}
                                                onClick={()=>handleClick(item._id as string, rowsArray)}
                                                // role="checkbox"
                                                // tabIndex={-1}
                                                key={item._id}
                                            >
                                                {/*<TableCell padding="checkbox">*/}
                                                {/*<Checkbox*/}
                                                {/*    color="primary"*/}
                                                {/*    checked={isItemSelected}*/}
                                                {/*    inputProps={{*/}
                                                {/*        'aria-labelledby': labelId,*/}
                                                {/*    }}*/}
                                                {/*/>*/}
                                                {/*</TableCell>*/}


                                                {/*<TableCell>*/}
                                                {/*    <pre>{JSON.stringify(item.num, null, 2)}</pre>*/}
                                                {/*    /!*<pre>{item.status}</pre>*!/*/}
                                                {/*</TableCell>*/}

                                                <TableCell
                                                    padding="checkbox"
                                                    // padding="none"
                                                    component="th"
                                                    // id={labelId}
                                                    scope="row"
                                                    align="left"
                                                >
                                                    {item.num}
                                                </TableCell>


                                                {(()=>{
                                                    const itemObj: any = item;
                                                    const innerObj: {
                                                        [key: string]: any
                                                    } = {}

                                                    tableRowsName.forEach((item2: string[])=>{
                                                        // const innerValue =
                                                        innerObj[item2[0]] = getValueOfKey(item2[0], itemObj) ?? "none"; //itemObj[item2[0]] ?? "none";
                                                    })

                                                    return Object.values(innerObj).map((item3, index3)=>{
                                                        return (
                                                            <TableCell
                                                                key={index + index3}
                                                                // component="th"
                                                                align="left"
                                                            >
                                                                {/*{<pre>{JSON.stringify(item3, null, 2)}</pre>}*/}
                                                                {item3}
                                                            </TableCell>
                                                        )
                                                    })

                                                    // {
                                                    //     "_id": "63bcfbc3a072d68817b9cf7b",
                                                    //     "status": "FrontWIP",
                                                    //     "date": "2023-01-10T05:46:25.598Z",
                                                    //     "frontManager": "63bcfbb1a072d68817b9cb4f",
                                                    //     "getterFullName": "Аделхард Амалрихов Еремиасович",
                                                    //     "amount": 55245,
                                                    //     "senderFullName": "Джакоб Вернеров Кэйсерович",
                                                    //     "senderIIN": "870318123456",
                                                    //     "senderBank": "АО «Евразийский Банк»",
                                                    //     "senderIIK": "KZ687624324899292281",
                                                    //     "senderBIK": "EURIKZKA",
                                                    //     "identificationNumber": "950824123456",
                                                    //     "num": 1
                                                    // }

                                                    // return [
                                                    //     <TableCell
                                                    //         key={index}
                                                    //         // component="th"
                                                    //         align="right"
                                                    //     >
                                                    //         {<pre>{JSON.stringify(item, null, 2)}</pre>}
                                                    //         {/*{item}*/}
                                                    //     </TableCell>
                                                    // ]
                                                })()}
                                            </TableRow>
                                        );
                                    })}
                                {emptyRows > 0 && (
                                    <TableRow
                                        style={{
                                            height: (dense ? 33 : 53) * emptyRows,
                                        }}
                                    >
                                        <TableCell colSpan={6}/>
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        // count={rowsArray.length}
                        count={rowsArray.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                </Paper>
                <FormControlLabel
                    control={<Switch checked={dense} onChange={handleChangeDense}/>}
                    label="Высота строки"
                />
                {/*<pre>{JSON.stringify(allAccRequests, null, 2)}</pre>*/}
            </Box>
        )
    else
        return (
            <Box sx={{
                width: '100%',
                // height: '100%'// метка, потом удалить или пересмотреть
            }}>
                <Typography variant="subtitle1" gutterBottom>
                    нет данных для отображения. Настройте фильтры.
                </Typography>
            </Box>
        )

}