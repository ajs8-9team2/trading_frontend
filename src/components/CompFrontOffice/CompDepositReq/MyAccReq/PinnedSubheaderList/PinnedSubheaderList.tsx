import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import {TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import BasicModal from "../BasicModal/BasicModal";
import {getDate} from '../../../../../Helpers/getDate';
import {Typography} from "@mui/material";
import {TTotalPickTable} from "../../../../../containers/FrontOfficeModern/DepositRequest/FrontOfficeDepositListReq";
import {Context} from "../../../../../context";

type TPinnedSubheaderList = {
    myArray: TTotalPickTable[]; //TGetAccRequestsAT[];
    // set_idDepositReq: React.Dispatch<React.SetStateAction<string>>;
    // timerActive: boolean;
    // getID: string,
}

export default function PinnedSubheaderList(
    {myArray}: TPinnedSubheaderList
){
    const {timerActive, set_idDepositReqContext} = React.useContext(Context)

    return (
        <List
            sx={{
                width: '100%',
                maxWidth: 360,
                bgcolor: 'background.paper',
                position: 'relative',
                overflow: 'auto',
                maxHeight: 600,
                '& ul': {padding: 0},

            }}
            subheader={<li/>}
        >
            <Typography
                variant="h6"
                gutterBottom
                sx={{
                    // mt: 10,
                    textAlign: "center",
                    color: "grey"
                }}
            >
                всего заявлений: {myArray.length}
            </Typography>

            {
                // myArray.map((sectionId)=>{
                myArray.map((sectionId)=>{
                    // if(sectionId?.status !== "Approved")
                    return (
                        <li
                            key={`section-${sectionId?._id!}`}
                        >
                            {/* блокируем кнопку, если секунды не равны 0 или 20 */}
                            <Button
                                fullWidth
                                onClick={()=>{
                                    set_idDepositReqContext(sectionId?._id!);
                                }}
                                disabled={timerActive}
                            >
                                {<ul>
                                    <Divider/>
                                    <ListSubheader>
                                        {/*<ListItem>*/}
                                        <ListItemText primary={`ID ${sectionId?._id!}`}/>
                                        {/*</ListItem>*/}
                                        {/*<Divider/>*/}
                                    </ListSubheader>
                                    {[
                                        getDate(sectionId?.date!),
                                        sectionId?.status,

                                        sectionId?.senderIIN,
                                        sectionId?.senderFullName,
                                        sectionId?.senderBank,
                                        sectionId?.senderBIK,
                                        sectionId?.senderIIK,
                                        sectionId?.amount,

                                        sectionId?.getterFullName,
                                        sectionId?.identificationNumber
                                    ].map((item, index)=>(

                                        <ListItem
                                            key={`item-${index}-${item}`}
                                            dense={true}
                                        >
                                            <ListItemText
                                                sx={{borderBottom: "1px solid grey", marginY: 0}}

                                                primary={
                                                    (()=>{
                                                        switch(index){
                                                            case 0:
                                                                return `дата заявления: `
                                                            case 1:
                                                                return `Статус рассмотрения:`
                                                            case 2:
                                                                return `ИИН отправителя:`
                                                            case 3:
                                                                return `ФИО отправителя:`
                                                            case 4:
                                                                return `Банк отправителя:`
                                                            case 5:
                                                                return `БИК отправителя:`
                                                            case 6:
                                                                return `ИИК(IBAN):`
                                                            case 7:
                                                                return `сумма перевода:`
                                                            case 8:
                                                                return `ФИО получателя:`
                                                            case 9:
                                                                return `ИИН получателя:`
                                                            // case 12:
                                                            //     return `причина отказа: `
                                                        }
                                                    })() as string
                                                }
                                                secondary={item}
                                            />
                                            {/*</Grid>*/}

                                            {/*<Grid>*/}
                                            {/*    <ListItemText secondary={item}/>*/}
                                            {/*    </Grid>*/}

                                            {/*</Grid>*/}
                                        </ListItem>
                                    ))}

                                    {/*{"Open photo"}*/}
                                </ul>}
                            </Button>
                        </li>
                    )
                })
            }
        </List>
    );
}


// <Demo>
//     <List dense={dense}>
//         {ingredients.map((item)=> (
//             <ListItem key={item._id}>
//                 <ListItemText
//                     sx={{borderBottom: "1px solid grey",marginY:0}}
//                     primary={item.title}
//                     secondary={secondary ? item.amount+" "+item.unit : null}
//                 />
//             </ListItem>
//         ))}
//     </List>
// </Demo>
