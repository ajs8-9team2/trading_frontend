import {Box, Button, ButtonGroup, Snackbar, Stack, TextField, Typography} from "@mui/material";
import React, {useEffect} from "react";
import {styled} from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import {paramsAccRequestAT, patchAccRequestAT} from "../../../../../store/services/accRequestSlice";
import {useAppDispatch, useAppSelector} from "../../../../../hook";
import {TGetAccRequestsAT, TPatchAccRequestsAT} from "../../../../../types/typesAccReq";
import {createAccountAT} from "../../../../../store/services/accountSlice";
import MuiAlert, {AlertProps} from '@mui/material/Alert';
import {TPatchDepositRequestAT} from "../../../../../types/typesDepositRequest";
import {TRejectionReason, TStatusTransferDeposit, TUser} from "../../../../../types/reestrOfAlias";
import {patchDepositRequestAT} from "../../../../../store/services/depositRequestSlice";
import LinearProgress, { LinearProgressProps } from '@mui/material/LinearProgress';
import {notificationObject} from "../../../../../Helpers/notificationObject";
import {getStatus} from "../../../../../Helpers/getStatus";
import {Context} from "../../../../../context";

function LinearProgressWithLabel(props: LinearProgressProps & { value: number }) {
    return (
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Box sx={{ width: '100%', mr: 1 }}>
                <LinearProgress color="success" variant="determinate" {...props} />
            </Box>
            <Box sx={{ minWidth: 35 }}>
                <Typography variant="body2" color="text.secondary">{`${Math.round(
                    props.value,
                )}%`}</Typography>
            </Box>
        </Box>
    );
}

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
){
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const patchDepositRequest = (
    status: "Declined" | "Approved",
    _idDepositRequest: string,
    rejectionReason = "none"
)=>{
    return {
        status,
        _idDepositRequest,
        rejectionReason,
    }
}

type TPropsRightSideForm = {
    seconds:number;
    dataUser: TUser;
    userID: string | undefined;
}
//############################################################################################
const RightSideForm = ({
                           seconds,
                           userID,
                        }: TPropsRightSideForm)=>{

    const {_idDepositReq, timerActive} = React.useContext(Context)

    const dispatch = useAppDispatch();
    // const oneAccRequest: TGetAccRequestsAT = useAppSelector(state=>state.reducerAccRequests.oneAccRequest);
    const [rejectionReason, setRejectionReason] = React.useState<string>("");
    const [progress, setProgress] = React.useState(0);

    React.useEffect(() => {
        const gradationArray = [
            100,95,90,85,80,75,70,65,60,55,50,45,40,35,30,25,20,15,10,5,0
        ]
        setProgress(gradationArray[seconds])
    }, [seconds]);
    //******************************************
    const [open, setOpen] = React.useState(false);
    const handleClick = ()=>{
        setOpen(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string)=>{
        if(reason === 'clickaway'){
            return;
        }
        setOpen(false);
    };
    //******************************************
    const inputChangeHandler: React.ChangeEventHandler<HTMLInputElement> = (e)=>{
        const value: string = e.target.value;
        setRejectionReason(value);
    };

    const submitFormHandler: React.FormEventHandler<HTMLFormElement> | undefined
        = (e)=>{
        e.preventDefault();
        // для Snackbar
        handleClick()

        // id сотрудника в объект передавать не надо, так как
        // на странице где отображается список заявок
        // он уже когда-то закрепил заявку за собой
        const innerObj: TPatchDepositRequestAT = (!!rejectionReason)
            ? patchDepositRequest("Declined", _idDepositReq, rejectionReason)
            : patchDepositRequest("Approved", _idDepositReq);

            // ?
            // {
            //     status: "Declined",//
            //     _idDepositRequest: _idDepositReq,// _id заявки
            //     rejectionReason: rejectionReason,
            // }
            // :
            // {
            //     status: "Approved",
            //     _idDepositRequest: _idDepositReq,// _id заявки
            //     rejectionReason: "none",
            // }
        dispatch(patchDepositRequestAT(innerObj));
        const innerUserID = userID ?? "";

        if(!rejectionReason){
            dispatch(notificationObject(
                _idDepositReq,
                innerUserID,
                "Approved",
                "depositRequest",
                getStatus("Approved")
            ))
        }else{
            dispatch(notificationObject(
                _idDepositReq,
                innerUserID,
                "Declined",
                "depositRequest",
                getStatus("Declined") + ": " + rejectionReason
            ))
        }
    };
//############################################################################################
    return (
        <>
            {/*<pre>{"потом удалить id клиента"}</pre>*/}
            {/*<pre>{JSON.stringify(userID, null, 2)}</pre>*/}
            <Typography
                variant="h5"
                gutterBottom
                sx={{
                    m: 0,
                    p: 0,
                    textAlign: "center",
                    color: "grey"
                }}
            >
                {"мои заявления на пополнение счета"}
            </Typography>
            <Typography
                variant="h5"
                gutterBottom
                sx={{textAlign: "center", color: "grey"}}
            >
                {"ID заявления на пополнение счета:"}
            </Typography>
            <Typography
                variant="h6"
                gutterBottom
                sx={{textAlign: "center", color: "grey"}}
            >
                {_idDepositReq?.toUpperCase()}
            </Typography>
            <Box sx={{
                marginX: "auto",
                maxWidth: 650,
                padding: 5,
                // paddingTop: 30,
                // paddingBottom: 30,
                boxShadow: "0 18px 30px 0 rgba(0, 0, 0, 0.6)",
                // width: '100%',
                // display: 'flex',
                // alignItems: "center",
                // justifyContent: "center",
            }}>
                <form onSubmit={submitFormHandler}>

                    <Stack spacing={2}>
                        <Item>
                            <TextField
                                multiline
                                rows={4}
                                id={"reason"}
                                name={"reason"}
                                label={"указать причину отклонения при отказе в подтверждении заявки."}
                                variant="outlined"
                                type={"text"}
                                value={rejectionReason}
                                // required={true}
                                onChange={inputChangeHandler}
                                fullWidth
                            />
                        </Item>
                        <Item>
                            <ButtonGroup
                                variant="contained"
                                fullWidth
                                aria-label="outlined primary button group"
                            >
                                <Button
                                    type="submit"
                                    disabled={!!rejectionReason}
                                >
                                    принять
                                </Button>
                                <Button
                                    type="submit"
                                    disabled={!rejectionReason}
                                >
                                    отклонить
                                </Button>

                            </ButtonGroup>
                        </Item>
                    </Stack>
                </form>
                {timerActive
                && <LinearProgressWithLabel value={progress}/>}
            </Box>

            {/*<Box sx={{ width: '100%' }}>*/}
            {/*    <LinearProgressWithLabel value={progress} />*/}
            {/*</Box>*/}
            {/*<pre>{JSON.stringify(oneAccount, null, 2)}</pre>*/}
            {/*<pre>{JSON.stringify(dataUser, null, 2)}</pre>*/}

            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={!rejectionReason ? "success" : "error"} sx={{width: '100%'}}>
                    {!rejectionReason ? "Вы одобрили заявление клиента!" : "Вы отклонили заявление клиента"}
                </Alert>
            </Snackbar>

        </>
    )
}

export default RightSideForm;