import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import {TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import {Button, Grid, Snackbar, Typography} from "@mui/material";
import {patchAccRequestAT} from "../../../../../store/services/accRequestSlice";
import {useAppDispatch, useAppSelector} from "../../../../../hook";
import {TUser} from "../../../../../types/reestrOfAlias";
import {getDate} from "../../../../../Helpers/getDate";
import MuiAlert, {AlertProps} from "@mui/material/Alert";
import {notificationObject} from "../../../../../Helpers/notificationObject";
import {getStatus} from "../../../../../Helpers/getStatus";

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
){
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});


export default function InsetDividers(oneAccRequest: TGetAccRequestsAT){
    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);

    const {
        _id, date, lastName, firstName, patronym, dateOfBirth,
        identificationNumber, idCardNumber, authority, dateOfIssue,
        expirationDate, email, phone, country, area, city, address,
        status, /*workStatus,*/ assignedTo, user,rejectionReason
    } = oneAccRequest!


    //******************************************
    const [open, setOpen] = React.useState(false);
    const handleClick = ()=>{
        setOpen(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string)=>{
        if(reason === 'clickaway'){
            return;
        }
        setOpen(false);
    };
    //******************************************
    return (
        <Grid
            container
            direction="column"
            justifyContent="space-between"
            // alignItems="stretch"
            sx={{
                bgcolor: '#cfe8fc',
                height: '100%',
                // height: '77vh',
            }}
        >
            {/*<pre>{JSON.stringify(user?._id, null, 2)}</pre>*/}
            {Object.entries(oneAccRequest as object).length ? <>
                    <Grid item xs>
                        <List
                            sx={{
                                width: '100%',
                                bgcolor: 'background.paper',
                                overflow: 'auto',
                                // maxHeight: 400,
                                // height: '100%',
                                height: '70vh',
                            }}
                        >
                            <ListItem>
                                <ListItemText primary="ID заявления:" secondary={_id?.toUpperCase()}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="Дата заявки:" secondary={getDate(date)}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="ФИО:" secondary={`${firstName} ${lastName} ${patronym}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="Дата рождения:" secondary={`${dateOfBirth}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="ИИН:" secondary={`${identificationNumber}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="# удостоверения:" secondary={`${idCardNumber}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="Орган выдачи:" secondary={`${authority}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="Дата выдачи - Срок действия"
                                              secondary={`${dateOfIssue} - ${expirationDate}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="e-mail:" secondary={`${email}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="Номер телефона:" secondary={`${phone}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="Адрес:" secondary={`${country} ${area} ${city} ${address}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="Статус:" secondary={`${status}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="Причина отклоения:" secondary={`${rejectionReason}`}/>
                            </ListItem><Divider/>
                            <ListItem>
                                <ListItemText primary="Закреплено за сотрудником:"
                                              secondary={`${assignedTo?._id?.toUpperCase()}`}/>
                            </ListItem><Divider/>
                        </List>
                    </Grid>
                    <Grid item xs>
                        {
                            // (gridSize>11 ) &&
                            <Button
                                sx={{height: '100%'}}
                                onClick={()=>{
                                    dispatch(patchAccRequestAT({
                                        // rejectionReason: "Ой все!!!",
                                        // _id: "6375ea703e24be311d3fb216",
                                        status: "WorkInProgress",// status: "Pending" | "WorkInProgress" | "Approved" | "Declined",
                                        _idAccReq: _id,// _id заявки
                                        assignedTo: dataUser?._id!// _id пользователя
                                    }))
                                    // <<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>
                                    dispatch(notificationObject(
                                        _id!,
                                        user?._id!,
                                        "FrontWIP",
                                        "accRequest",
                                        getStatus("FrontWIP")
                                    ));
                                    // <<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>
                                    // для Snackbar
                                    handleClick()
                                }}
                                // type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                disabled={status === "Approved" || status === "Declined" || status ===  "WorkInProgress"}
                            >
                                {"Взять в работу"}
                            </Button>}
                    </Grid>
                </>
                :
                <Grid item xs>
                    <Typography
                        variant="h6"
                        gutterBottom
                        sx={{
                            // mt: 10,
                            textAlign: "center",
                            color: "grey"
                        }}
                    >
                        {"Выберите заявление для работы"}
                    </Typography>
                </Grid>

            }

            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={"success"} sx={{width: '100%'}}>
                    {"Заявление клиента закреплено за вами!"}
                </Alert>
            </Snackbar>
        </Grid>

    )
}
