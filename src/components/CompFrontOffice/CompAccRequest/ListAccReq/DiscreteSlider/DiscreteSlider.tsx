import * as React from 'react';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Slider from '@mui/material/Slider';
import {useEffect} from "react";
import {fetchAccRequestsAT} from "../../../../../store/services/accRequestSlice";

const marks = [
    {value: 0, label: '',},
    {value: 3, label: '',},
    {value: 4, label: '',},
    {value: 5, label: '',},
    {value: 6, label: '',},
    {value: 7, label: '',},
    {value: 8, label: '',},
    {value: 9, label: '',},
    {value: 12, label: '',},

    // {value: 0},
    // {value: 3},
    // {value: 4},
    // {value: 5},
    // {value: 6},
    // {value: 7},
    // {value: 8},
    // {value: 9},
    // {value: 12},
];

function valueLabelFormat(value: number){
    return marks.findIndex((mark)=>mark.value === value) + 1;
}

type TpiscreteSliderProps = {
    setGridSize: React.Dispatch<React.SetStateAction<number>>
}
export default function DiscreteSlider({setGridSize}: TpiscreteSliderProps){

    const handleChange = (event: Event, newValue: number | number[])=>{
        setGridSize(newValue as number);
        // setValue(newValue as number);
    };

    return (
        <Box
            sx={{
                display: "flex",
                justifyContent: "center",
            }}>

            <Box sx={{
                width: 500,
            }}>
                <Slider
                    aria-label="Restricted values"
                    defaultValue={9}
                    valueLabelFormat={valueLabelFormat}
                    step={null}
                    // valueLabelDisplay="auto"
                    marks={marks}
                    onChange={handleChange}
                    min={0}
                    max={12}
                />
            </Box>
        </Box>
    );

}


// import * as React from 'react';
// import Box from '@mui/material/Box';
// import Stack from '@mui/material/Stack';
// import Slider from '@mui/material/Slider';
// import {useEffect} from "react";
// import {fetchAccRequestsAT} from "../../../../store/services/AccRequestSlice";
//
//
// type TpiscreteSliderProps = {
//     // valuetext: (value: number, index: number)=>string
//     setGridSize: React.Dispatch<React.SetStateAction<number>>
// }
//
// function valuetext(value: number){
//     return `${value}°C`;
// }
//
// // export default function DiscreteSlider({valuetext}: TpiscreteSliderProps){
// //const setGridSize: (value: (((prevState: 0) => 0) | 0)) => void
// export default function DiscreteSlider({setGridSize}: TpiscreteSliderProps){
//     // const [value, setValue] = React.useState<number>(8);
//
//     const handleChange = (event: Event, newValue: number | number[])=>{
//         setGridSize(newValue as number);
//         // setValue(newValue as number);
//     };
//
//     return (
//         <Box
//             sx={{
//                 display: "flex",
//                 justifyContent: "center",
//                 // alignItems: "center"
//             }}>
//
//             <Box sx={{
//                 width: 500,
//                 // display: "flex",
//                 // justifyContent:"center",
//                 // alignItems:"center"
//             }}>
//                 {/*<pre>{JSON.stringify(value, null, 2)}</pre>*/}
//                 {/*<Stack spacing={2} direction="row" sx={{mb: 1}} alignItems="center">*/}
//                 {/*<VolumeDown />*/}
//                 {/*<Slider*/}
//                 {/*    aria-label="Volume"*/}
//                 {/*    value={value}*/}
//                 {/*    onChange={handleChange}*/}
//                 {/*/>*/}
//                 {/*<VolumeUp />*/}
//                 <Slider
//                     onChange={handleChange}
//                     aria-label="Temperature"
//                     defaultValue={8}
//                     getAriaValueText={valuetext}
//                     valueLabelDisplay="auto"
//                     step={1}
//                     marks
//                     min={0}
//                     max={12}
//                 />
//                 {/*</Stack>*/}
//                 {/*<Slider disabled defaultValue={30} aria-label="Disabled slider" />*/}
//             </Box>
//         </Box>
//     );
//
// }