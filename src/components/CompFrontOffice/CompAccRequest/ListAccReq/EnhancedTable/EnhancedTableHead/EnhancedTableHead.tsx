import * as React from "react";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableSortLabel from "@mui/material/TableSortLabel";
import Box from "@mui/material/Box";
import {visuallyHidden} from "@mui/utils";
import {Order, THeaderTable} from "../EnhancedTable";
import {Divider} from "@mui/material";
import {Context} from "../../../../../../context";


// interface HeadCell{
//     disablePadding: boolean;
//     id: keyof THeaderTable;
//     label: string;
//     numeric: boolean;
// }

// {
//     _id: string,
//         firstName: string,
//     lastName: string,
//     patronym: string,
//     dateOfBirth: string,
//     identificationNumber: string,
// }

// const headCells: readonly HeadCell[] = [
//     {
//         id: 'num',
//         numeric: false,
//         disablePadding: false,
//         label: 'num',
//     },
//     {
//         id: '_id',
//         numeric: false,
//         disablePadding: false,
//         label: '_id',
//     },
//     {
//         id: 'date',
//         numeric: false,
//         disablePadding: false,
//         label: 'дата создания',
//     },
//     {
//         id: 'assignedTo',
//         numeric: false,
//         disablePadding: false,
//         label: 'Закреплено за:',
//     },
//     {
//         id: 'status',
//         numeric: false,
//         disablePadding: false,
//         label: 'Статус рассмотрения:',
//     },
//     {
//         id: 'firstName',
//         numeric: false,
//         disablePadding: false,
//         label: 'Имя',
//     },
//     {
//         id: 'lastName',
//         numeric: false,
//         disablePadding: false,
//         label: 'Фамилия',
//     },
//     {
//         id: 'patronym',
//         numeric: false,
//         disablePadding: false,
//         label: 'Отчество',
//     },
//     {
//         id: 'dateOfBirth',
//         numeric: false,
//         disablePadding: false,
//         label: 'Д/Р',
//     },
//     {
//         id: 'identificationNumber',
//         numeric: false,
//         disablePadding: false,
//         label: 'ИИН',
//     },
//     // {
//     //     id: 'rejectionReason',
//     //     numeric: false,
//     //     disablePadding: false,
//     //     label: 'прич. отклон.',
//     // },
// ];

interface EnhancedTableProps{
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof THeaderTable)=>void;
    order: Order;
    orderBy: string;
    rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps){

    const {tableRowsName} = React.useContext(Context)
    const {order, orderBy, onRequestSort} = props;
    const createSortHandler =
        (property: keyof THeaderTable)=>(event: React.MouseEvent<unknown>)=>{
            onRequestSort(event, property);
        };

    return (
        <TableHead>
            <TableRow>
                {/*<TableCell padding="checkbox">*/}
                {/*    <Checkbox*/}
                {/*        color="primary"*/}
                {/*        indeterminate={numSelected > 0 && numSelected < rowCount}*/}
                {/*        checked={rowCount > 0 && numSelected === rowCount}*/}
                {/*        onChange={onSelectAllClick}*/}
                {/*        inputProps={{*/}
                {/*            'aria-label': 'select all desserts',*/}
                {/*        }}*/}
                {/*    />*/}
                {/*</TableCell>*/}
                <TableCell
                    // key={item.id}
                    // align={item.numeric ? 'right' : 'left'}
                    align={'left'}
                    // padding={item.disablePadding ? 'none' : 'normal'}
                    padding={'normal'}
                    sortDirection={orderBy === "num" ? order : false}
                >
                    <TableSortLabel
                        active={orderBy === "num"}
                        direction={orderBy === "num" ? order : 'asc'}
                        onClick={createSortHandler("num")}
                    >
                        {"#"}
                        {orderBy === "num" ? (
                            <Box component="span" sx={visuallyHidden}>
                                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                            </Box>
                        ) : null}
                    </TableSortLabel>
                </TableCell>

                {(()=>{
                    return tableRowsName.map((item: string[])=>{
                        const headCells: {
                            [key: string]: any
                        } = {}
                        headCells.id = item[0];
                        headCells.numeric = false;
                        headCells.disablePadding = false;
                        headCells.label = item[1];

                        return (
                            <TableCell
                                key={headCells.id}
                                // align={headCells.numeric ? 'right' : 'left'}
                                align={'left'}
                                padding={headCells.disablePadding ? 'none' : 'normal'}
                                sortDirection={orderBy === headCells.id ? order : false}
                            >
                                <TableSortLabel
                                    active={orderBy === headCells.id}
                                    direction={orderBy === headCells.id ? order : 'asc'}
                                    onClick={createSortHandler(headCells.id)}
                                >
                                    {headCells.label}
                                    {orderBy === headCells.id ? (
                                        <Box component="span" sx={visuallyHidden}>
                                            {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                        </Box>
                                    ) : null}
                                </TableSortLabel>
                            </TableCell>
                        )
                    })
                })()}
            </TableRow>
        </TableHead>
    );
}

export default EnhancedTableHead;