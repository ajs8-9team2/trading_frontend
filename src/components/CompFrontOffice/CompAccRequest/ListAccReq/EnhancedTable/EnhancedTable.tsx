// import {alpha} from '@mui/material/styles';
// import TableHead from '@mui/material/TableHead';
// import TableSortLabel from '@mui/material/TableSortLabel';
// import Toolbar from '@mui/material/Toolbar';
// import Typography from '@mui/material/Typography';
// import Checkbox from '@mui/material/Checkbox';
// import IconButton from '@mui/material/IconButton';
// import Tooltip from '@mui/material/Tooltip';
// import DeleteIcon from '@mui/icons-material/Delete';
// import FilterListIcon from '@mui/icons-material/FilterList';
// import {visuallyHidden} from '@mui/utils';
import * as React from 'react';
import {useEffect} from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import {TFirstAccReq, TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import {useAppDispatch} from "../../../../../hook";
import {paramsAccRequestAT} from "../../../../../store/services/accRequestSlice";
import {TUser} from "../../../../../types/reestrOfAlias";
import {getDate} from "../../../../../Helpers/getDate";
import {Typography} from "@mui/material";
import {getStatus} from "../../../../../Helpers/getStatus";
import {Context} from "../../../../../context";
import {getValueOfKey} from "../../../../../Helpers/getValueOfKey";
import EnhancedTableHead from "./EnhancedTableHead/EnhancedTableHead";
import {TTotalPickTableTransferReq} from "../../../CompTransferReq/ListAccReq/EnhancedTable/EnhancedTable";

export type Order = 'asc' | 'desc';
export type THeaderTable = Partial<TFirstAccReq> &
    Partial<{
        num: string,
        _id: string,
        date: string,
        status: "Pending" | "WorkInProgress" | "Approved" | "Declined",
        assignedTo: TUser | string | undefined,
        idCardNumber: string,
        // rejectionReason: string,
    }>


function descendingComparator<T>(a: T, b: T, orderBy: keyof T){
    if(b[orderBy] < a[orderBy]){
        return -1;
    }
    if(b[orderBy] > a[orderBy]){
        return 1;
    }
    return 0;
}


function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
): (
    a: { [key in Key]: number | string },
    b: { [key in Key]: number | string },
)=>number{
    return order === 'desc'
        ? (a, b)=>descendingComparator(a, b, orderBy)
        : (a, b)=>-descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort<T>(array: readonly T[], comparator: (a: T, b: T)=>number){
    const stabilizedThis = array.map((item, index)=>[item, index] as [T, number]);
    stabilizedThis.sort((a, b)=>{
        const order = comparator(a[0], b[0]);
        if(order !== 0){
            return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el)=>el[0]);
}

export default function EnhancedTable({
                                          allAccRequests
                                          /*, right,setGetID*/
                                      }: {
    allAccRequests: TGetAccRequestsAT[],
    // right: readonly number[]
    // setGetID: React.Dispatch<React.SetStateAction<string>>,
}){

    const {
        statusReq,
        tableRowsName,
        accRequestStatus,
        employees
    } = React.useContext(Context)

    const dispatch = useAppDispatch();

    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof THeaderTable>('num');
    // const [selected, setSelected] = React.useState<readonly string[]>([]);
    // const [selectedOne, setOneSelected] = React.useState("");
    const [page, setPage] = React.useState(0);
    const [dense, setDense] = React.useState(false);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [rowsArray, setRowsArray] = React.useState<any>([]);


    useEffect(()=>{
        console.clear()

        const innerObj: {
            [key: string]: any
        } = {}

        tableRowsName.forEach((item: string[])=>{
            innerObj[item[0]] = undefined;
        })
        //------------------------------------
        let innerCount = 0;
        // let innerRowsArray = new Array();
        let innerRowsArray: THeaderTable[] = [];

        for(const allAccReqElement of allAccRequests){
            const hasAssignedTo: any = {...allAccReqElement}
            if(!hasAssignedTo.hasOwnProperty("assignedTo"))
                hasAssignedTo["assignedTo"] = "not_assigned"

            if(!statusReq.includes(allAccReqElement?.status)) continue;
            const isFlag = employees?.some((item: (string | undefined)[])=>item.includes(allAccReqElement?.assignedTo?._id));
            const isFlag2 = employees?.some((item: (string | undefined)[])=>item.includes(hasAssignedTo.assignedTo));
            const isFlag3 = isFlag || isFlag2;
            if(!isFlag3) continue;

            // console.log(isFlag2,"\t\t",employees,"\t\t",hasAssignedTo?.assignedTo)
            // if(!employees?.some((item: (string | undefined)[]) => item.includes("not_assigned"))) continue;

            const itemObj: any = allAccReqElement;
            innerCount++;
            const myObj = {...innerObj}
            myObj.num = innerCount;//index + 1;

            for(const itemKey in itemObj){
                // console.log(Object.keys(myObj), " <<<<<<<<")
                // if(!Object.keys(myObj).includes(itemKey)) continue;
                // if(myObj.hasOwnProperty(itemKey))
                // if(statusReq.includes[itemObj.status])
                if(typeof itemObj[itemKey] === "object"){
                    if(itemKey === "assignedTo")
                        myObj[itemKey] = itemObj[itemKey]?._id ?? "none";
                }else
                    myObj[itemKey] = itemObj[itemKey];
            }

            /** там где значение ключа undefined добавляем none */
            for(const myObjKey in myObj){
                if(myObj[myObjKey] === undefined)
                    myObj[myObjKey] = "none";
            }
            // console.log(myObj," <<<")
            innerRowsArray.push(myObj)
        }

        setRowsArray(innerRowsArray);

        if(0)// fake data
            setRowsArray([{
                "_id": "63bcfbbfa072d68817b9cd40",
                "status": "Approved",
                "firstName": "Джебберт",
                "lastName": "Адалвалфов",
                "patronym": "Йоханович",
                "dateOfBirth": "11.08.1971",
                "identificationNumber": "710811123456",
                "assignedTo": "63bcfbb1a072d68817b9cb4e",
                //     {
                //     "_id": "63bcfbb1a072d68817b9cb4e",
                //     "email": "front0@mail.com",
                //     "role": "frontOffice",
                //     "token": "bKXDZQPMOatwewgQTo4rF",
                //     "path": "account-opening",
                //     "isHasAccount": false,
                //     "__v": 0
                // },
                "idCardNumber": "735148278",
                "date": "2023-01-10T05:46:25.435Z",
            }]);
    }, [accRequestStatus,/*tableRowsName,*/ statusReq, employees]);
//#########################################################################

    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof THeaderTable,
    )=>{
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleClick = (name: string)=>{
        dispatch(paramsAccRequestAT(name))
        // setGetID(name)
    };

    const handleChangePage = (event: unknown, newPage: number)=>{
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>)=>{
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>)=>{
        setDense(event.target.checked);
    };
    // Avoid a layout jump when reaching the last page with empty rowsArray.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rowsArray.length) : 0;


    //##################################################################################
    if(0)
        return (
            <pre>{JSON.stringify(rowsArray, null, 2)}</pre>
        )
    //##################################################################################
    if(tableRowsName.length && rowsArray.length /*&& employees.length*/)
        return (
            <Box sx={{
                width: '100%',
                // height: '100%'// метка, потом удалить или пересмотреть
            }}>
                {/*<pre>{JSON.stringify(right, null, 2)}</pre>*/}
                <Paper sx={{width: '100%', mb: 2}}>
                    {/*<EnhancedTableToolbar numSelected={selected.length} />*/}
                    <TableContainer>
                        <Table
                            // sx={{minWidth: 750}}
                            sx={{minWidth: 50}}
                            aria-labelledby="tableTitle"
                            size={dense ? 'small' : 'medium'}
                        >
                            <EnhancedTableHead
                                order={order}
                                orderBy={orderBy}
                                onRequestSort={handleRequestSort}
                                rowCount={rowsArray.length}
                            />
                            <TableBody>
                                {/* if you don't need to support IE11, you can replace the `stableSort` call with:
              rowsArray.sort(getComparator(order, orderBy)).slice() */}
                                {/*---------------------------------------------------------------------------------------*/}
                                {/*---------------------------------------------------------------------------------------*/}
                                {/*---------------------------------------------------------------------------------------*/}
                                {stableSort(rowsArray, getComparator(order, orderBy))
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((item, index)=>{
                                        // const labelId = `enhanced-table-checkbox-${index}`;

                                        return (
                                            <TableRow
                                                hover
                                                // onClick={(event) => handleClick(event, item?._id!)}
                                                onClick={()=>handleClick(item._id as string)}
                                                // role="checkbox"
                                                // tabIndex={-1}
                                                key={item._id}
                                            >
                                                {/*<TableCell padding="checkbox">*/}
                                                {/*<Checkbox*/}
                                                {/*    color="primary"*/}
                                                {/*    checked={isItemSelected}*/}
                                                {/*    inputProps={{*/}
                                                {/*        'aria-labelledby': labelId,*/}
                                                {/*    }}*/}
                                                {/*/>*/}
                                                {/*</TableCell>*/}


                                                <TableCell
                                                    padding="checkbox"
                                                    // padding="none"
                                                    component="th"
                                                    // id={labelId}
                                                    scope="row"
                                                    align="left"
                                                >
                                                    {item.num}
                                                </TableCell>


                                                {(()=>{
                                                    const itemObj: any = item;
                                                    const innerObj: {
                                                        [key: string]: any
                                                    } = {}

                                                    tableRowsName.forEach((item2: string[])=>{
                                                        // const innerValue =
                                                        innerObj[item2[0]] = getValueOfKey(item2[0], itemObj) ?? "none"; //itemObj[item2[0]] ?? "none";
                                                    })

                                                    return Object.values(innerObj).map((item3, index3)=>{
                                                        return (
                                                            <TableCell
                                                                key={index + index3}
                                                                // component="th"
                                                                align="left"
                                                            >
                                                                {/*{<pre>{JSON.stringify(item3, null, 2)}</pre>}*/}
                                                                {item3}
                                                            </TableCell>
                                                        )
                                                    })

                                                    // return [
                                                    //     <TableCell
                                                    //         key={index}
                                                    //         // component="th"
                                                    //         align="left"
                                                    //     >
                                                    //         {<pre>{JSON.stringify(item, null, 2)}</pre>}
                                                    //         {/*{item}*/}
                                                    //     </TableCell>
                                                    // ]

                                                })()}
                                            </TableRow>
                                        );
                                    })}
                                {emptyRows > 0 && (
                                    <TableRow
                                        style={{
                                            height: (dense ? 33 : 53) * emptyRows,
                                        }}
                                    >
                                        <TableCell colSpan={6}/>
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={rowsArray.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                </Paper>
                <FormControlLabel
                    control={<Switch checked={dense} onChange={handleChangeDense}/>}
                    label="Высота строки"
                />
                {/*<pre>{JSON.stringify(allAccRequests, null, 2)}</pre>*/}
            </Box>
        )
    else
        return (
            <Box sx={{
                width: '100%',
                // height: '100%'// метка, потом удалить или пересмотреть
            }}>
                <Typography variant="subtitle1" gutterBottom>
                    нет данных для отображения. Настройте фильтры.
                </Typography>
            </Box>
        )

}











