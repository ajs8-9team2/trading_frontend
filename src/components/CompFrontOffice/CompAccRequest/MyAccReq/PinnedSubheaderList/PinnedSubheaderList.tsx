import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import {TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import BasicModal from "../BasicModal/BasicModal";
import {getDate} from '../../../../../Helpers/getDate';
import {Typography} from "@mui/material";
import {getStatus} from "../../../../../Helpers/getStatus";
import {Context} from "../../../../../context";


type TPinnedSubheaderList = {
    myArray: TGetAccRequestsAT[];
    // timerActive: boolean;
}

export default function PinnedSubheaderList(
    {myArray/*, timerActive*/}: TPinnedSubheaderList
){
    const {timerActive, set_idAccReqContext} = React.useContext(Context)

    return (
        <List
            sx={{
                width: '100%',
                maxWidth: 360,
                bgcolor: 'background.paper',
                position: 'relative',
                overflow: 'auto',
                maxHeight: 600,
                '& ul': {padding: 0},

            }}
            subheader={<li/>}
        >
            <Typography
                variant="h6"
                gutterBottom
                sx={{
                    // mt: 10,
                    textAlign: "center",
                    color: "grey"
                }}
            >
                всего заявлений: {myArray.length}
            </Typography>

            {
                // myArray.map((sectionId)=>{
                myArray.map((sectionId)=>{
                    // if(sectionId?.status !== "Approved")
                    return (
                        <li
                            key={`section-${sectionId?._id!}`}
                        >
                            {/* блокируем кнопку, если секунды не равны 0 или 20 */}
                            <Button
                                fullWidth
                                onClick={()=>{
                                    // set_idAccReq(sectionId?._id!);
                                    set_idAccReqContext(sectionId?._id!)
                                }}
                                disabled={timerActive}
                            >
                                {<ul>
                                    <Divider/>
                                    <ListSubheader>
                                        {/*<ListItem>*/}
                                        <ListItemText primary={`ID ${sectionId?._id!}`}/>
                                        {/*</ListItem>*/}
                                        {/*<Divider/>*/}
                                    </ListSubheader>
                                    {[
                                        getDate(sectionId?.date!),
                                        sectionId?.firstName!,
                                        sectionId?.lastName!,
                                        sectionId?.patronym!,
                                        sectionId?.identificationNumber?.toUpperCase(),
                                        sectionId?.idCardNumber!,
                                        sectionId?.dateOfIssue!,
                                        sectionId?.expirationDate!,
                                        sectionId?.authority!,
                                        sectionId?.email!,
                                        sectionId?.phone!,
                                        getStatus(sectionId?.status!),
                                        sectionId?.rejectionReason!,
                                        sectionId?.assignedTo?._id!,
                                    ].map((item, index)=>(

                                        <ListItem
                                            key={`item-${index}-${item}`}
                                            dense={true}
                                        >
                                            {/*<ListItemText primary={`AZA ${item} ${index}`} />*/}
                                            {/*<Grid*/}
                                            {/*    container*/}
                                            {/*    direction="row"*/}
                                            {/*    justifyContent="space-between"*/}
                                            {/*    // alignItems="center"*/}
                                            {/*    // sx={{textAlign:"left"}}*/}
                                            {/*>*/}
                                            {/*    <Grid>*/}
                                            <ListItemText
                                                sx={{borderBottom: "1px solid grey", marginY: 0}}

                                                primary={
                                                    (()=>{
                                                        switch(index){
                                                            case 0:
                                                                return `дата заявки: `
                                                            case 1:
                                                                return `имя: `
                                                            case 2:
                                                                return `фамилия: `
                                                            case 3:
                                                                return `отчество: `
                                                            case 4:
                                                                return `ИИН: `
                                                            case 5:
                                                                return `# удостоверения : `
                                                            case 6:
                                                                return `Дата выдачи : `
                                                            case 7:
                                                                return `Срок действия : `
                                                            case 8:
                                                                return `Орган выдачи: `
                                                            case 9:
                                                                return `e-mail: `
                                                            case 10:
                                                                return `Номер телефона: `
                                                            case 11:
                                                                return `статус рассмотрения: `
                                                            case 12:
                                                                return `причина отказа: `
                                                            case 13:
                                                                return `id сотрудника: `
                                                        }
                                                    })() as string
                                                }
                                                secondary={item}
                                            />
                                            {/*</Grid>*/}

                                            {/*<Grid>*/}
                                            {/*    <ListItemText secondary={item}/>*/}
                                            {/*    </Grid>*/}

                                            {/*</Grid>*/}
                                        </ListItem>
                                    ))}

                                    {/*{"Open photo"}*/}
                                </ul>}
                            </Button>
                            <BasicModal
                                documentFront={sectionId?.documentFront!}
                                documentBack={sectionId?.documentBack!}
                            />
                        </li>
                    )

                })
            }
        </List>
    );
}


// <Demo>
//     <List dense={dense}>
//         {ingredients.map((item)=> (
//             <ListItem key={item._id}>
//                 <ListItemText
//                     sx={{borderBottom: "1px solid grey",marginY:0}}
//                     primary={item.title}
//                     secondary={secondary ? item.amount+" "+item.unit : null}
//                 />
//             </ListItem>
//         ))}
//     </List>
// </Demo>
