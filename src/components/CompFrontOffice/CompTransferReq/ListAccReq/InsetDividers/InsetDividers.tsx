// import * as React from 'react';
// import List from '@mui/material/List';
// import ListItem from '@mui/material/ListItem';
// import ListItemText from '@mui/material/ListItemText';
// import Divider from '@mui/material/Divider';
// import {TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import {Button, Divider, Grid, List, ListItemText, Snackbar, Typography} from "@mui/material";
import {patchAccRequestAT} from "../../../../../store/services/accRequestSlice";
import {useAppDispatch, useAppSelector} from "../../../../../hook";
import {TStateStatus, TUser} from "../../../../../types/reestrOfAlias";
import {getDate} from "../../../../../Helpers/getDate";
import MuiAlert, {AlertProps} from "@mui/material/Alert";
import {TTotalPickTable} from "../../../../../containers/FrontOfficeModern/DepositRequest/FrontOfficeDepositListReq";
import {TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import {ListItem} from "@material-ui/core";
import React from "react";
import {getStatus} from "../../../../../Helpers/getStatus";
import {patchDepositRequestAT} from "../../../../../store/services/depositRequestSlice";
import {TGetPostDepositRequestAT} from "../../../../../types/typesDepositRequest";
import {TGetPostTransferReqAT} from "../../../../../types/typesTransferRequest";
import {patchTransferReqAT} from "../../../../../store/services/transferRequestSlice";
import {notificationObject} from "../../../../../Helpers/notificationObject";

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
){
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function InsetDividers(oneTransferReq: TGetPostTransferReqAT){
    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);

    const {
        _id, date, rejectionReason, status, amount, senderIIN, recipientIIN, senderIIK, recipientIIK,
        senderBIK, recipientBIK, senderBank, recipientBank, senderKBe, recipientKBe, senderFullName,
        recipientFullName,user
    } = oneTransferReq!

    //******************************************
    const [open, setOpen] = React.useState(false);
    const handleClick = ()=>{
        setOpen(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string)=>{
        if(reason === 'clickaway'){
            return;
        }
        setOpen(false);
    };
    //******************************************
    return (
        <Grid
            container
            direction="column"
            justifyContent="space-between"
            // alignItems="stretch"
            sx={{
                bgcolor: '#cfe8fc',
                height: '100%',
                // height: '77vh',
            }}
        >
            {/*<pre>{JSON.stringify(user?._id, null, 2)}</pre>*/}
            {/*<pre>{JSON.stringify(concatOneAccOneDepos, null, 2)}</pre>*/}
            {/*{Object.entries(concatOneAccOneDepos as object).length ? <>*/}
            {!!oneTransferReq ? <>
                    <Grid item xs>
                        <List
                            sx={{
                                width: '100%',
                                bgcolor: 'background.paper',
                                overflow: 'auto',
                                // maxHeight: 400,
                                // height: '100%',
                                height: '70vh',
                            }}
                        >
                            <ListItem>
                                <ListItemText primary='ID заявления:' secondary={_id?.toUpperCase()}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='дата создания:' secondary={getDate(date!)}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='Причина отклонения:' secondary={rejectionReason}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='Статус рассмотрения:' secondary={getStatus(status!)}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='сумма перевода:' secondary={amount}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='ИИН отправителя:' secondary={senderIIN}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='ИИН получателя:' secondary={recipientIIN}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='ИИК(IBAN) отправителя:' secondary={senderIIK}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='ИИК(IBAN) получателя:' secondary={recipientIIK}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='БИК отправителя:' secondary={senderBIK}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='БИК получателя:' secondary={recipientBIK}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='Банк отправителя:' secondary={senderBank}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='Банк получателя:' secondary={recipientBank}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='КБе отправителя:' secondary={senderKBe}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='КБе получателя:' secondary={recipientKBe}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='ФИО отправителя:' secondary={senderFullName}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='ФИО получателя:' secondary={recipientFullName}/>
                            </ListItem>





                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='дата создания:'*/}
                            {/*                  secondary={getDate(concatOneAccOneDepos?.date as string)}/>*/}
                            {/*</ListItem>*/}

                            {/*<Divider/>*/}

                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='ID сотрудника:'*/}
                            {/*                  secondary={concatOneAccOneDepos.frontManager?.toUpperCase()}/>*/}
                            {/*</ListItem>*/}
                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='Статус рассмотрения:'*/}
                            {/*                  secondary={getStatus(concatOneAccOneDepos.status!)}/>*/}
                            {/*</ListItem>*/}

                            {/*<Divider/>*/}

                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='ИИН отправителя:' secondary={concatOneAccOneDepos.senderIIN}/>*/}
                            {/*</ListItem>*/}
                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='ФИО отправителя:' secondary={concatOneAccOneDepos.senderFullName}/>*/}
                            {/*</ListItem>*/}
                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='Банк отправителя:' secondary={concatOneAccOneDepos.senderBank}/>*/}
                            {/*</ListItem>*/}
                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='БИК отправителя:' secondary={concatOneAccOneDepos.senderBIK}/>*/}
                            {/*</ListItem>*/}
                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='ИИК(IBAN):' secondary={concatOneAccOneDepos.senderIIK}/>*/}
                            {/*</ListItem>*/}
                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='сумма перевода:' secondary={concatOneAccOneDepos.amount}/>*/}
                            {/*</ListItem>*/}

                            {/*<Divider/>*/}

                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='ФИО получателя:' secondary={concatOneAccOneDepos.getterFullName}/>*/}
                            {/*</ListItem>*/}
                            {/*<ListItem>*/}
                            {/*    <ListItemText primary='ИИН получателя:'*/}
                            {/*                  secondary={concatOneAccOneDepos.identificationNumber}/>*/}
                            {/*</ListItem>*/}


                        </List>
                    </Grid>
                    <Grid item xs>
                        {
                            // (gridSize>11 ) &&
                            <Button
                                sx={{height: '100%'}}
                                onClick={()=>{
                                    dispatch(patchTransferReqAT({
                                        // rejectionReason: "Ой все!!!",
                                        // _id: "6375ea703e24be311d3fb216",
                                        status: "FrontWIP",// status: "Pending" | "WorkInProgress" | "Approved" | "Declined",
                                        _idTransferReq: _id,// _id заявки
                                        frontManager: dataUser?._id!// _id пользователя
                                    }))
                                    // <<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>
                                    dispatch(notificationObject(
                                        _id!,
                                        user?._id!,
                                        "FrontWIP",
                                        "transferRequest",
                                        getStatus("FrontWIP")
                                    ));
                                    // <<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>

                                    // для Snackbar
                                    handleClick()
                                }}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                disabled={
            // "Pending" | "FrontWIP" | "AccountantWIP" | "Approved" | "Declined" | "Completed"
                                    status === "FrontWIP" ||
                                    status === "AccountantWIP" ||
                                    status === "Approved" ||
                                    status === "Declined" ||
                                    status === "Completed"
                                }
                            >
                                {"Взять в работу"}
                            </Button>}
                    </Grid>
                </>
                :
                <Grid item xs>
                    <Typography
                        variant="h6"
                        gutterBottom
                        sx={{
                            // mt: 10,
                            textAlign: "center",
                            color: "grey"
                        }}
                    >
                        {"Выберите заявление для работы"}
                    </Typography>
                </Grid>

            }

            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={"success"} sx={{width: '100%'}}>
                    {"Заявление клиента закреплено за вами!"}
                </Alert>
            </Snackbar>
        </Grid>

    )
}
