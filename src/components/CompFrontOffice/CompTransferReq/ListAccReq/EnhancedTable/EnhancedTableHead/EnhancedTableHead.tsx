import * as React from "react";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableSortLabel from "@mui/material/TableSortLabel";
import Box from "@mui/material/Box";
import {visuallyHidden} from "@mui/utils";
import {Order, TTotalPickTableTransferReq} from "../EnhancedTable";
import {Divider} from "@mui/material";
import {Context} from "../../../../../../context";

interface HeadCell{
    disablePadding: boolean;
    id: keyof TTotalPickTableTransferReq;
    label: string;
    numeric: boolean;
}

// {
//     _id: string,
//         firstName: string,
//     lastName: string,
//     patronym: string,
//     dateOfBirth: string,
//     identificationNumber: string,
// }

const headCells: readonly HeadCell[] = [
    {
        id: '_id',
        numeric: false,
        disablePadding: false,
        label: 'ID заявления',
    },
    {
        id: 'date',
        numeric: false,
        disablePadding: false,
        label: 'дата создания:',
    },
    {
        id: 'rejectionReason',
        numeric: false,
        disablePadding: false,
        label: 'Причина отклонения:',
    },
    {
        id: 'status',
        numeric: false,
        disablePadding: false,
        label: 'Статус рассмотрения:',
    },
    {
        id: 'amount',
        numeric: false,
        disablePadding: false,
        label: 'сумма перевода:',
    },
    {
        id: 'senderIIN',
        numeric: false,
        disablePadding: false,
        label: 'ИИН отправителя:',
    },
    {
        id: 'recipientIIN',
        numeric: false,
        disablePadding: false,
        label: 'ИИН получателя:',
    },
    {
        id: 'senderIIK',
        numeric: false,
        disablePadding: false,
        label: 'ИИК(IBAN) отправителя:',
    },
    {
        id: 'recipientIIK',
        numeric: false,
        disablePadding: false,
        label: 'ИИК(IBAN) получателя:',
    },
    {
        id: 'senderBIK',
        numeric: false,
        disablePadding: false,
        label: 'БИК отправителя:',
    },
    {
        id: 'recipientBIK',
        numeric: false,
        disablePadding: false,
        label: 'БИК получателя:',
    },
    {
        id: 'senderBank',
        numeric: false,
        disablePadding: false,
        label: 'Банк отправителя:',
    },
    {
        id: 'recipientBank',
        numeric: false,
        disablePadding: false,
        label: 'Банк получателя:',
    },
    {
        id: 'senderKBe',
        numeric: false,
        disablePadding: false,
        label: 'КБе отправителя:',
    },
    {
        id: 'recipientKBe',
        numeric: false,
        disablePadding: false,
        label: 'КБе получателя:',
    },
    {
        id: 'senderFullName',
        numeric: false,
        disablePadding: false,
        label: 'ФИО отправителя:',
    },
    {
        id: 'recipientFullName',
        numeric: false,
        disablePadding: false,
        label: 'ФИО получателя:',
    },
];

interface EnhancedTableProps{
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof TTotalPickTableTransferReq)=>void;
    order: Order;
    orderBy: string;
    rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps){

    const {tableRowsName} = React.useContext(Context)
    const {order, orderBy, onRequestSort} = props;
    const createSortHandler =
        (property: keyof TTotalPickTableTransferReq)=>(event: React.MouseEvent<unknown>)=>{
            onRequestSort(event, property);
        };

    return (
        <TableHead>
            <TableRow>
                {/*<TableCell padding="checkbox">*/}
                    {/*<Checkbox*/}
                    {/*    color="primary"*/}
                    {/*    indeterminate={numSelected > 0 && numSelected < rowCount}*/}
                    {/*    checked={rowCount > 0 && numSelected === rowCount}*/}
                    {/*    onChange={onSelectAllClick}*/}
                    {/*    inputProps={{*/}
                    {/*        'aria-label': 'select all desserts',*/}
                    {/*    }}*/}
                    {/*/>*/}
                {/*</TableCell>*/}

                <TableCell
                    // key={item.id}
                    // align={item.numeric ? 'right' : 'left'}
                    align={'left'}
                    // padding={item.disablePadding ? 'none' : 'normal'}
                    padding={'normal'}
                    sortDirection={orderBy === "num" ? order : false}
                >
                    <TableSortLabel
                        active={orderBy === "num"}
                        direction={orderBy === "num" ? order : 'asc'}
                        onClick={createSortHandler("num")}
                    >
                        {"#"}
                        {orderBy === "num" ? (
                            <Box component="span" sx={visuallyHidden}>
                                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                            </Box>
                        ) : null}
                    </TableSortLabel>
                </TableCell>

                {(()=>{
                    return tableRowsName.map((item: string[])=>{
                        const headCells: {
                            [key: string]: any
                        } = {}
                        headCells.id = item[0];
                        headCells.numeric = false;
                        headCells.disablePadding = false;
                        headCells.label = item[1];

                        return (
                            <TableCell
                                key={headCells.id}
                                // align={headCells.numeric ? 'right' : 'left'}
                                align={'left'}
                                padding={headCells.disablePadding ? 'none' : 'normal'}
                                sortDirection={orderBy === headCells.id ? order : false}
                            >
                                <TableSortLabel
                                    active={orderBy === headCells.id}
                                    direction={orderBy === headCells.id ? order : 'asc'}
                                    onClick={createSortHandler(headCells.id)}
                                >
                                    {headCells.label}
                                    {orderBy === headCells.id ? (
                                        <Box component="span" sx={visuallyHidden}>
                                            {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                        </Box>
                                    ) : null}
                                </TableSortLabel>
                            </TableCell>
                        )
                    })
                })()}
            </TableRow>
        </TableHead>
    );
}

export default EnhancedTableHead;