import * as React from 'react';
import {useEffect} from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import EnhancedTableHead from "./EnhancedTableHead/EnhancedTableHead";
import {getDate} from "../../../../../Helpers/getDate";
import {Typography, List, ListItem, ListItemText} from "@mui/material";
import Grid from '@mui/material/Grid';
import {getStatus} from "../../../../../Helpers/getStatus";
import {TStatusTransferDeposit} from "../../../../../types/reestrOfAlias";
import {TGetPostTransferReqAT, TTransferReq} from "../../../../../types/typesTransferRequest";
import {paramsTransferReqAT} from "../../../../../store/services/transferRequestSlice";
import {useAppDispatch} from "../../../../../hook";
import {Context} from "../../../../../context";
import {getValueOfKey} from "../../../../../Helpers/getValueOfKey";

export type Order = 'asc' | 'desc';


function descendingComparator<T>(a: T, b: T, orderBy: keyof T){
    if(b[orderBy] < a[orderBy]){
        return -1;
    }
    if(b[orderBy] > a[orderBy]){
        return 1;
    }
    return 0;
}

function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
): (
    a: { [key in Key]: number | string },
    b: { [key in Key]: number | string },
)=>number{
    return order === 'desc'
        ? (a, b)=>descendingComparator(a, b, orderBy)
        : (a, b)=>-descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort<T>(array: readonly T[], comparator: (a: T, b: T)=>number){
    const stabilizedThis = array.map((el, index)=>[el, index] as [T, number]);
    stabilizedThis.sort((a, b)=>{
        const order = comparator(a[0], b[0]);
        if(order !== 0){
            return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el)=>el[0]);
}

type TTablePick = Pick<TTransferReq,
    "_id" | "status" | "amount" |
    "senderIIN" | "recipientIIN" | "senderIIK" | "recipientIIK" | "senderBIK" | "recipientBIK" |
    "senderBank" | "recipientBank" | "senderKBe" | "recipientKBe" | "senderFullName" | "recipientFullName" |
    "rejectionReason" | "date">
export type TTotalPickTableTransferReq = Partial<TTablePick> & Partial<{ num: string }>

export default function EnhancedTable({
                                          allTransferReq
                                          // right,
                                      }: {
    allTransferReq: TGetPostTransferReqAT[];
    // right: readonly number[];
}){
    const {
        statusReq,
        tableRowsName,
        transferReqStatus,
        employees
    } = React.useContext(Context)

    const dispatch = useAppDispatch();

    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof TTotalPickTableTransferReq>('num');
    const [page, setPage] = React.useState(0);
    const [dense, setDense] = React.useState(false);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [rowsArray, setRowsArray] = React.useState<any>([]);

    useEffect(()=>{
        console.clear()

        const innerObj: {
            [key: string]: any
        } = {}

        tableRowsName.forEach((item: string[])=>{
            innerObj[item[0]] = undefined;
        })
        //------------------------------------

        let innerCount = 0;
        // let innerRowsArray = new Array();
        let innerRowsArray:TTotalPickTableTransferReq[] = [];

        for(const allTransferReqElement of allTransferReq){
            if(!statusReq.includes(allTransferReqElement?.status)) continue
            console.log(JSON.stringify(allTransferReqElement, null, 2))
            const hasAssignedTo: any = {...allTransferReqElement}
            if(!hasAssignedTo.hasOwnProperty("frontManager"))
                hasAssignedTo["frontManager"] = "not_assigned"
            //**********
            if(!statusReq.includes(allTransferReqElement?.status)) continue
            const isFlag = employees?.some((item: (string | undefined)[])=>item.includes(allTransferReqElement?.frontManager?._id));
            const isFlag2 = employees?.some((item: (string | undefined)[])=>item.includes(hasAssignedTo.frontManager));
            const isFlag3 = isFlag || isFlag2;
            if(!isFlag3) continue;

            const itemObj: any = allTransferReqElement;
            innerCount++;
            const myObj = {...innerObj}
            myObj.num = innerCount;//index + 1;

            for(const itemKey in itemObj){
                // console.log(Object.keys(myObj), " <<<<<<<<")
                // if(!Object.keys(myObj).includes(itemKey)) continue;
                // if(myObj.hasOwnProperty(itemKey))
                // if(statusReq.includes[itemObj.status])
                if(typeof itemObj[itemKey] === "object"){
                    if(itemKey === "frontManager")
                        myObj[itemKey] = itemObj[itemKey]?._id ?? "none";
                }else
                    myObj[itemKey] = itemObj[itemKey];
            }

            /** там где значение ключа undefined добавляем none */
            for(const myObjKey in myObj){
                if(myObj[myObjKey] === undefined)
                    myObj[myObjKey] = "none";
            }
            // console.log(myObj)
            innerRowsArray.push(myObj)
        }
        // const innerRowsArray: any = allTransferReq.map((item, index)=>{
        //     // console.log("-----------------------------")
        //     // console.log(statusReq)
        //     // console.log(itemObj.status)// AccountantWIP Approved FrontWIP Pending
        //     // console.log(statusReq.includes(itemObj.status))
        //     const itemObj: any = item
        //     const myObj = {...innerObj}
        //     if(statusReq.includes(item?.status)){
        //         innerCount++;
        //         myObj.num = innerCount;//index + 1;
        //
        //         for(const itemKey in itemObj){
        //             // console.log(Object.keys(myObj), " <<<<<<<<")
        //             // if(!Object.keys(myObj).includes(itemKey)) continue;
        //             // if(myObj.hasOwnProperty(itemKey))
        //             // if(statusReq.includes[itemObj.status])
        //             if(typeof itemObj[itemKey] === "object"){
        //                 if(itemKey === "frontManager")
        //                     myObj[itemKey] = itemObj[itemKey]?._id ?? "none";
        //             }else
        //                 myObj[itemKey] = itemObj[itemKey];
        //         }
        //
        //         /** там где значение ключа undefined добавляем none */
        //         for(const myObjKey in myObj){
        //             if(myObj[myObjKey] === undefined)
        //                 myObj[myObjKey] = "none";
        //         }
        //         // console.log(JSON.stringify(myObj, null, 2))
        //         return myObj;
        //     }
        // })

        setRowsArray(innerRowsArray);

        if(0)
            setRowsArray([{
                "_id": "63bcfbc4a072d68817b9d03d",
                "senderFullName": "Бернд Гантрамов Волфович",
                "recipientFullName": "Готтолд Дитричов Арманович",
                "date": "2023-01-10T05:46:25.642Z",
                "rejectionReason": "none",
                "status": "Pending",
                "amount": 35154,
                "senderIIN": "981106123456",
                "recipientIIN": "850415123456",
                "senderIIK": "KZ565117876957546098",
                "recipientIIK": "KZ815735603727760131",
                "senderBIK": "VTBAKZKZ",
                "recipientBIK": "CITIKZKA",
                "senderBank": "ДО АО Банк ВТБ (Казахстан)",
                "recipientBank": "АО «Ситибанк Казахстан»",
                "senderKBe": "17",
                "recipientKBe": "17",
                "num": 1
            }]);
    }, [transferReqStatus,/* tableRowsName,*/ statusReq, employees]);

    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof TTotalPickTableTransferReq,
    )=>{
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleClick = (name: string)=>{
        dispatch(paramsTransferReqAT(name))
    };

    const handleChangePage = (event: unknown, newPage: number)=>{
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>)=>{
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>)=>{
        setDense(event.target.checked);
    };
    // Avoid a layout jump when reaching the last page with empty rowsAZAZA.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rowsArray.length) : 0;


    //##################################################################################
    // allDepositsRequest,
    //     allAccRequests,
    if(0)
        return (
            <>
                {/*<pre>{JSON.stringify(allAccRequests[0], null, 2)}</pre>*/}

                <pre>{JSON.stringify(rowsArray, null, 2)}</pre>
            </>
        )
    //##################################################################################
    if(tableRowsName.length && rowsArray.length  && employees.length)
        return (
            <Box sx={{
                width: '100%',
                // height: '100%'// метка, потом удалить или пересмотреть
            }}>
                {/*<pre>{JSON.stringify(right, null, 2)}</pre>*/}
                <Paper sx={{width: '100%', mb: 2}}>
                    {/*<EnhancedTableToolbar numSelected={selected.length} />*/}

                    <TableContainer>
                        {/*<pre>{JSON.stringify(depositsData.depositRequestStatus, null, 2)}</pre>*/}

                        <Table
                            // sx={{minWidth: 750}}
                            sx={{minWidth: 50}}
                            aria-labelledby="tableTitle"
                            size={dense ? 'small' : 'medium'}
                        >
                            <EnhancedTableHead
                                order={order}
                                orderBy={orderBy}
                                onRequestSort={handleRequestSort}
                                rowCount={rowsArray.length}
                            />
                            <TableBody>
                                {/* if you don't need to support IE11, you can replace the `stableSort` call with:
              rowsArray.sort(getComparator(order, orderBy)).slice() */}
                                {/*---------------------------------------------------------------------------------------*/}
                                {/*---------------------------------------------------------------------------------------*/}
                                {/*---------------------------------------------------------------------------------------*/}
                                {/*{stableSort(rowsArray, getComparator(order, orderBy))*/}
                                {stableSort(rowsArray, getComparator(order, orderBy))
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((item, index)=>{

                                        const labelId = `enhanced-table-checkbox-${index}`;
                                        return (
                                            <TableRow
                                                hover
                                                // onClick={(event) => handleClick(event, item?._id!)}
                                                onClick={()=>handleClick(item._id as string)}
                                                // role="checkbox"
                                                // tabIndex={-1}
                                                key={item._id}
                                            >
                                                {/*<TableCell padding="checkbox">*/}
                                                {/*<Checkbox*/}
                                                {/*    color="primary"*/}
                                                {/*    checked={isItemSelected}*/}
                                                {/*    inputProps={{*/}
                                                {/*        'aria-labelledby': labelId,*/}
                                                {/*    }}*/}
                                                {/*/>*/}
                                                {/*</TableCell>*/}

                                                <TableCell
                                                    padding="checkbox"
                                                    // padding="none"
                                                    component="th"
                                                    // id={labelId}
                                                    scope="row"
                                                    align="left"
                                                >
                                                    {item.num}
                                                </TableCell>


                                                {true ?
                                                    (()=>{
                                                        const itemObj: any = item;
                                                        const innerObj: {
                                                            [key: string]: any
                                                        } = {}

                                                        tableRowsName.forEach((item2: string[])=>{
                                                            // const innerValue =
                                                            innerObj[item2[0]] = getValueOfKey(item2[0], itemObj) ?? "none"; //itemObj[item2[0]] ?? "none";
                                                        })

                                                        return Object.values(innerObj).map((item3, index3)=>{
                                                            return (
                                                                <TableCell
                                                                    key={index + index3}
                                                                    // component="th"
                                                                    align="left"
                                                                >
                                                                    {/*{<pre>{JSON.stringify(item3, null, 2)}</pre>}*/}
                                                                    {item3}
                                                                </TableCell>
                                                            )
                                                        })

                                                        // return [
                                                        //     <TableCell
                                                        //         key={index}
                                                        //         // component="th"
                                                        //         align="left"
                                                        //     >
                                                        //         {<pre>{JSON.stringify(item, null, 2)}</pre>}
                                                        //         {/*{item}*/}
                                                        //     </TableCell>
                                                        // ]

                                                    })()
                                                    :
                                                    (()=>{

                                                        return (
                                                            <TableCell
                                                                key={index}
                                                                // component="th"
                                                                align="left"
                                                            >
                                                                {Object.entries(item).map(item=>(
                                                                    <Grid container spacing={2}>
                                                                        <Grid item xs={6} md={4}>
                                                                            <Typography variant="h6" gutterBottom>
                                                                                {item[0]}
                                                                            </Typography>
                                                                        </Grid>

                                                                        <Grid item xs={6} md={8}>
                                                                            <Typography variant="body2" gutterBottom>
                                                                                {item[1]}
                                                                            </Typography>
                                                                        </Grid>
                                                                    </Grid>
                                                                ))}

                                                                {/*<List dense={true}>*/}
                                                                {/*    {Object.entries(item).map(item=>(*/}
                                                                {/*        <ListItem>*/}
                                                                {/*            <ListItemText*/}
                                                                {/*                primary={item[0]}*/}
                                                                {/*            />*/}
                                                                {/*            <ListItemText*/}
                                                                {/*                secondary={item[1]}*/}
                                                                {/*            />*/}
                                                                {/*        </ListItem>*/}
                                                                {/*    ))}*/}
                                                                {/*</List>*/}
                                                            </TableCell>
                                                        )
                                                    })()
                                                }

                                            </TableRow>
                                        );
                                    })}
                                {emptyRows > 0 && (
                                    <TableRow
                                        style={{
                                            height: (dense ? 33 : 53) * emptyRows,
                                        }}
                                    >
                                        <TableCell colSpan={6}/>
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>

                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        // count={rowsArray.length}
                        count={rowsArray.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                </Paper>
                <FormControlLabel
                    control={<Switch checked={dense} onChange={handleChangeDense}/>}
                    label="Высота строки"
                />
                {/*<pre>{JSON.stringify(allAccRequests, null, 2)}</pre>*/}
            </Box>
        )
    else
        return (
            <Box sx={{
                width: '100%',
                // height: '100%'// метка, потом удалить или пересмотреть
            }}>
                <Typography variant="subtitle1" gutterBottom>
                    нет данных для отображения. Настройте фильтры.
                </Typography>
            </Box>
        )

}


/*
-------------------------------------------------------------------------------
{
     "_id": "63a152f97bea12d8dde2e33a",               ***
     "user": {
         "_id": "63a152f37bea12d8dde2e271",
         "email": "client38@mail.com",
         "role": "client",
         "token": "OvYr_UhoP0zCJwlswJ0uU",
         "path": "account-opening",
         "isHasAccount": true,
         "__v": 0
     },
     "status": "FrontWIP",                            ***
     "amount": 26556,
     "senderFullName": "Берндт Джерфридов Джокемович",
     "senderIIN": "721115123456",
     "senderBank": "АО ДБ «Банк Китая в Казахстане»",
     "senderIIK": "KZ517782351831804445",
     "senderBIK": "BKCHKZKA",
     "senderKBe": "17",
     "frontManager": {
         "_id": "63a152f37bea12d8dde2e27f",
         "email": "front2@mail.com",
         "role": "frontOffice",
         "token": "WRQBsEM4fwmnF84f1-qe5",
         "path": "account-opening",
         "isHasAccount": false,
         "__v": 0
     },
     "rejectionReason": "none",                       ***
     "requestType": "Deposit request",
     "date": "2022-12-20T06:15:15.470Z",              ***
     "__v": 0
},
-------------------------------------------------------------------------------
{
    "_id": "63a152f87bea12d8dde2e2c9",
    "user": {
      "_id": "63a152f37bea12d8dde2e24b",
      "email": "client0@mail.com",
      "role": "client",
      "token": "qubUo_csgFgqijeWSR8BA",
      "path": "account-opening",
      "isHasAccount": false,
      "__v": 0
    },
    "status": "Pending",
    "firstName": "Агидиус",
    "lastName": "Горстов",
    "patronym": "Годафридович",
    "dateOfBirth": "16.03.1985",
    "identificationNumber": "850316123456",
    "email": "client0@mail.com",
    "phone": "+7(777)4483255",
    "country": "Республика Казахстан",
    "area": "Байконыр область",
    "city": "Байконыр город",
    "address": "Кенесары переулок, 839",
    "authority": "Министерство Юстиции РК",
    "dateOfIssue": "21.04.2020",
    "expirationDate": "21.04.2030",
    "documentFront": "passport2.jpg",
    "documentBack": "passport4.jpg",
    "requestType": "Account Request",
    "rejectionReason": "none",
    "idCardNumber": "540325177",
    "date": "2022-12-20T06:15:15.283Z",
    "citizenship": "not-resident",
    "__v": 0
  },
-------------------------------------------------------------------------------
*/