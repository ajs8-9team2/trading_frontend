import * as React from 'react';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import {Context} from "../../../../context";
import {useEffect} from "react";
import {cutLength} from "../../../../Helpers/cutLength";
import {getStatus} from "../../../../Helpers/getStatus";

function not(a: readonly number[], b: readonly number[]){
    return a.filter((value)=>b.indexOf(value) === -1);
}

function intersection(a: readonly number[], b: readonly number[]){
    return a.filter((value)=>b.indexOf(value) !== -1);
}

type propsTransferList = {
    // setRight: React.Dispatch<React.SetStateAction<readonly number[]>>;
    // setLeft: React.Dispatch<React.SetStateAction<readonly number[]>>;
    // right: readonly number[];
    // left: readonly number[];
}

export default function TransferList2({
                                          // setRight,
                                          // setLeft,
                                          // right,
                                          // left
                                      }: propsTransferList){
    const {
        arrayStatusesRequest,
        setStatusReq,
        transferReqStatus,
        left2, setLeft2,
        right2, setRight2,
    } = React.useContext(Context)


    const [checked, setChecked] = React.useState<readonly number[]>([]);
    // const [left2, setLeft2] = React.useState<readonly number[]>([]);
    // const [right2, setRight2] = React.useState<readonly number[]>([...(()=>{
    //     const lenArray: number[] = [];
    //     for(let i1 = 0; i1 < arrayStatusesRequest.length; i1++){
    //         lenArray.push(i1);
    //     }
    //     return lenArray;
    // })()]);

    useEffect(()=>{
        const innerArray: string[] = [];
        for(const number of right2){
            innerArray.push(arrayStatusesRequest[number]);
        }
        // alert(JSON.stringify(innerArray, null, 2))
        setStatusReq(innerArray);
    }, [right2.length, transferReqStatus]);


    const leftChecked = intersection(checked, left2);
    const rightChecked = intersection(checked, right2);

    const handleToggle = (value: number)=>()=>{
        // if(value !== 0){
            const currentIndex = checked.indexOf(value);
            const newChecked = [...checked];

            if(currentIndex === -1){
                newChecked.push(value);
            }else{
                newChecked.splice(currentIndex, 1);
            }

            setChecked(newChecked);
        // }
    }

    const handleAllRight = ()=>{
        setRight2(right2.concat(left2));
        setLeft2([]);
    };

    const handleCheckedRight = ()=>{
        setRight2(right2.concat(leftChecked));
        setLeft2(not(left2, leftChecked));
        setChecked(not(checked, leftChecked));
    };

    const handleCheckedLeft = ()=>{
        setLeft2(left2.concat(rightChecked));
        setRight2(not(right2, rightChecked));
        setChecked(not(checked, rightChecked));
    };

    const handleAllLeft = ()=>{
        // const innerArray = [...right]
        //
        // if(innerArray.includes(0)){
        //     innerArray.shift()
        // }
        //
        // setLeft(left.concat(innerArray));
        // setRight([0]);

        setLeft2(left2.concat(right2));
        setRight2([]);
    };

    const customList = (items: readonly number[])=>(
        <Paper sx={{width: 230, height: 230, overflow: 'auto'}}>
            {/*<pre>{JSON.stringify(right, null, 2)}</pre>*/}
            <List dense component="div" role="list">
                {items.map((value: number)=>{
                    const labelId = `transfer-list-item-${value}-label`;

                    return (
                        <ListItem
                            key={value}
                            role="listitem"
                            button
                            onClick={handleToggle(value)}
                        >
                            <ListItemIcon>

                                {/*{value === 0 ?*/}
                                {/*    <Checkbox*/}
                                {/*        // checked*/}
                                {/*        // tabIndex={-1}*/}
                                {/*        disabled*/}
                                {/*        // disableRipple*/}
                                {/*        inputProps={{*/}
                                {/*            'aria-labelledby': labelId,*/}
                                {/*        }}*/}
                                {/*    />*/}
                                {/*    :*/}
                                    <Checkbox
                                        checked={checked.indexOf(value) !== -1}
                                        tabIndex={-1}
                                        // disabled
                                        disableRipple
                                        inputProps={{
                                            'aria-labelledby': labelId,
                                        }}
                                    />
                                {/*}*/}

                            </ListItemIcon>
                            <ListItemText id={labelId} primary={
                                (()=>{
                                    // return arrayStatusesRequest[value]
                                    return cutLength(getStatus(arrayStatusesRequest[value]), 29)
                                })()

                                // `List item ${value + 1}`
                            }/>
                        </ListItem>
                    );
                })}
                <ListItem/>
            </List>
        </Paper>
    );

    return (
        <Grid container spacing={2} justifyContent="center" alignItems="center">
            <Grid item>{customList(left2)}</Grid>
            <Grid item>
                <Grid container direction="column" alignItems="center">
                    <Button
                        sx={{my: 0.5}}
                        variant="outlined"
                        size="small"
                        onClick={handleAllRight}
                        disabled={left2.length === 0}
                        aria-label="move all right"
                    >
                        ≫
                    </Button>
                    <Button
                        sx={{my: 0.5}}
                        variant="outlined"
                        size="small"
                        onClick={handleCheckedRight}
                        disabled={leftChecked.length === 0}
                        aria-label="move selected right"
                    >
                        &gt;
                    </Button>
                    <Button
                        sx={{my: 0.5}}
                        variant="outlined"
                        size="small"
                        onClick={handleCheckedLeft}
                        disabled={rightChecked.length === 0}
                        aria-label="move selected left"
                    >
                        &lt;
                    </Button>
                    <Button
                        sx={{my: 0.5}}
                        variant="outlined"
                        size="small"
                        onClick={handleAllLeft}
                        disabled={right2.length === 0}
                        aria-label="move all left"
                    >
                        ≪
                    </Button>
                </Grid>
            </Grid>
            <Grid item>{customList(right2)}</Grid>
        </Grid>
    );
}