import * as React from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import FrontOfficeMyReq from "../../../../containers/FrontOfficeModern/AccRequest/FrontOfficeMyReq";
import FrontOfficeListReq from "../../../../containers/FrontOfficeModern/AccRequest/FrontOfficeListReq";

export default function LabTabsListReq() {
    const [value, setValue] = React.useState('1');

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };

    return (
        <Box sx={{ width: '100%', typography: 'body1' }}>
            <TabContext value={value}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <TabList onChange={handleChange} aria-label="lab API tabs example">
                        <Tab label="Список клиентских заявлений" value="1" />
                        <Tab label="Item Two" value="2" />
                        <Tab label="Item Three" value="3" />
                    </TabList>
                </Box>
                <TabPanel value="1">
                    {"List account req / Version 1 / page 1"}
                    <FrontOfficeListReq/>
                    {/*<FrontOfficeMyReq/>*/}
                </TabPanel>
                <TabPanel value="2"> {"List account req / Version 1 / page 2"}</TabPanel>
                <TabPanel value="3"> {"List account req / Version 1 / page 3"}</TabPanel>
            </TabContext>
        </Box>
    );
}