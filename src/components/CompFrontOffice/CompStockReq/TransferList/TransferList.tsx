import * as React from 'react';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import {Context} from "../../../../context";
import {useEffect} from "react";

function not(a: readonly number[], b: readonly number[]){
    return a.filter((value)=>b.indexOf(value) === -1);
}

function intersection(a: readonly number[], b: readonly number[]){
    return a.filter((value)=>b.indexOf(value) !== -1);
}

export default function TransferList(){
    const {
        rowsArrayTableStr,
        transferReqStatus,
        setTableRowsName,
        left1, setLeft1,
        right1, setRight1,
    } = React.useContext(Context)

    const [checked, setChecked] = React.useState<readonly number[]>([]);
    // const [left1, setLeft1] = React.useState<readonly number[]>([]);
    // const [right1, setRight1] = React.useState<readonly number[]>([...(()=>{
    //     const lenArray: number[] = [];
    //     for(let i1 = 0; i1 < rowsArrayTableStr.length; i1++){
    //         lenArray.push(i1);
    //     }
    //     return lenArray;
    // })()]);

    useEffect(()=>{
        const innerArray: string[][] = [];
        for(const number of right1){
            innerArray.push(rowsArrayTableStr[number]);
        }
        setTableRowsName(innerArray);
    }, [right1.length, transferReqStatus]);

    const leftChecked = intersection(checked, left1);
    const rightChecked = intersection(checked, right1);

    const handleToggle = (value: number)=>()=>{
        if(value !== 0){
            const currentIndex = checked.indexOf(value);
            const newChecked = [...checked];

            if(currentIndex === -1){
                newChecked.push(value);
            }else{
                newChecked.splice(currentIndex, 1);
            }

            setChecked(newChecked);
        }
    }

    const handleAllRight = ()=>{
        setRight1(right1.concat(left1));
        setLeft1([]);
    };

    const handleCheckedRight = ()=>{
        setRight1(right1.concat(leftChecked));
        setLeft1(not(left1, leftChecked));
        setChecked(not(checked, leftChecked));
    };

    const handleCheckedLeft = ()=>{
        setLeft1(left1.concat(rightChecked));
        setRight1(not(right1, rightChecked));
        setChecked(not(checked, rightChecked));
    };

    const handleAllLeft = ()=>{
        const innerArray = [...right1]

        if(innerArray.includes(0)){
            innerArray.shift()
        }

        setLeft1(left1.concat(innerArray));
        setRight1([0]);
    };

    const customList = (items: readonly number[])=>(
        <Paper sx={{width: 230, height: 230, overflow: 'auto'}}>
            {/*<pre>{JSON.stringify(right, null, 2)}</pre>*/}
            <List dense component="div" role="list">
                {items.map((value: number)=>{
                    const labelId = `transfer-list-item-${value}-label`;

                    return (
                        <ListItem
                            key={value}
                            role="listitem"
                            button
                            onClick={handleToggle(value)}
                        >
                            <ListItemIcon>

                                {value === 0 ?
                                    <Checkbox
                                        // checked
                                        // tabIndex={-1}
                                        disabled
                                        // disableRipple
                                        inputProps={{
                                            'aria-labelledby': labelId,
                                        }}
                                    />
                                    :
                                    <Checkbox
                                        checked={checked.indexOf(value) !== -1}
                                        tabIndex={-1}
                                        // disabled
                                        disableRipple
                                        inputProps={{
                                            'aria-labelledby': labelId,
                                        }}
                                    />
                                }

                            </ListItemIcon>
                            <ListItemText id={labelId} primary={
                                (()=>{
                                    return rowsArrayTableStr[value][1]
                                })()
                            }/>
                        </ListItem>
                    );
                })}
                <ListItem/>
            </List>
        </Paper>
    );

    return (
        <Grid container spacing={2} justifyContent="center" alignItems="center">
            <Grid item>{customList(left1)}</Grid>
            <Grid item>
                <Grid container direction="column" alignItems="center">
                    <Button
                        sx={{my: 0.5}}
                        variant="outlined"
                        size="small"
                        onClick={handleAllRight}
                        disabled={left1.length === 0}
                        aria-label="move all right"
                    >
                        ≫
                    </Button>
                    <Button
                        sx={{my: 0.5}}
                        variant="outlined"
                        size="small"
                        onClick={handleCheckedRight}
                        disabled={leftChecked.length === 0}
                        aria-label="move selected right"
                    >
                        &gt;
                    </Button>
                    <Button
                        sx={{my: 0.5}}
                        variant="outlined"
                        size="small"
                        onClick={handleCheckedLeft}
                        disabled={rightChecked.length === 0}
                        aria-label="move selected left"
                    >
                        &lt;
                    </Button>
                    <Button
                        sx={{my: 0.5}}
                        variant="outlined"
                        size="small"
                        onClick={handleAllLeft}
                        disabled={right1.length === 0}
                        aria-label="move all left"
                    >
                        ≪
                    </Button>
                </Grid>
            </Grid>
            <Grid item>{customList(right1)}</Grid>
        </Grid>
    );
}