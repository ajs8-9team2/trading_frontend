import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {Card, CardContent, CardMedia, Grid, Pagination, Stack} from "@mui/material";
import {uploadsUrl as apiURL} from "../../../../../axiosApi";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

type TImagePhoto = {
    documentFront:string,
    documentBack:string,
}

export default function BasicModal({documentFront,documentBack}:TImagePhoto){
    // открытие и закрытие модального окна
    const [open, setOpen] = React.useState<boolean>(false);
    const handleOpen = ()=>setOpen(true);
    const handleClose = ()=>setOpen(false);
    //------------------------------------------------------------
    // пагинация
    const [page, setPage] = React.useState<number>(1);
    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
    };
    //------------------------------------------------------------

    return (
        <div>
            <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                // sx={{textAlign:"left"}}
            >
                <Button
                    onClick={handleOpen}
                    fullWidth
                    // variant="contained"
                    color="primary"
                >Open photo</Button>
            </Grid>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Card
                        sx={{display: 'flex', justifyContent: 'flex-start'}}
                    >
                        <CardContent
                            sx={{
                                marginX: "auto"
                            }}
                        >
                            {/*documentFront && documentBack если не будет хотя бы одного фото
                            то фото отображаться не будет*/}
                            {!!(documentFront && documentBack)?
                                <CardMedia
                                    component="img"
                                    sx={{
                                        maxHeight:300,
                                        // width: 400,
                                        border: "1px solid grey"
                                    }}
                                    // image={`${apiURL}/uploads/${documentFront}`}
                                    image={`${apiURL}/${
                                        (()=>{
                                            if(page === 1)
                                                return documentFront;
                                            else
                                                return documentBack;
                                        })() as string
                                    }`}
                                    alt={"фото не загружено"}
                                />
                                :
                                <Box sx={{
                                    minHeight:200,
                                    width: 200,
                                    height: "100%",
                                    display: 'flex',
                                    alignItems: "center",
                                    justifyContent: "center",
                                    // border: "1px solid grey",
                                    // backgroundColor: "black",
                                    color: "black"
                                }}>
                                    <Typography
                                        // sx={{backgroundColor: "green",}}
                                        variant={"h4"}
                                    >
                                        no image
                                    </Typography>
                                </Box>
                            }
                        </CardContent>
                    </Card>
                    <Box sx={{
                        pt:2,
                        display: 'flex',
                        alignItems: "center",
                        justifyContent: "center",
                    }}>
                        <Stack spacing={2}>
                            {/*<Typography>Photo: {page}</Typography>*/}
                            <Pagination count={2} page={page} onChange={handleChange} />
                        </Stack>
                    </Box>
                    {/*<Typography id="modal-modal-title" variant="h6" component="h2">*/}
                    {/*    Text in a modal*/}
                    {/*</Typography>*/}
                    {/*<Typography id="modal-modal-description" sx={{mt: 2}}>*/}
                    {/*    Duis mollis, est non commodo luctus, nisi erat porttitor ligula.*/}
                    {/*</Typography>*/}
                </Box>
            </Modal>
        </div>
    );
}