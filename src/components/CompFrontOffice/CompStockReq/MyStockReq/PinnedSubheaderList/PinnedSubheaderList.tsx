import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import {TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import BasicModal from "../BasicModal/BasicModal";
import {getDate} from '../../../../../Helpers/getDate';
import {Typography} from "@mui/material";
import {getStatus} from "../../../../../Helpers/getStatus";
import {Context} from "../../../../../context";
import {TGetPostStockRequestAT} from "../../../../../types/typesStockRequest";

type TPinnedSubheaderList = {
    myArray: TGetPostStockRequestAT[];
}

export default function PinnedSubheaderList(
    {myArray}: TPinnedSubheaderList
){
    const {timerActive, set_idStockReqContext} = React.useContext(Context)

    return (
        <List
            sx={{
                width: '100%',
                maxWidth: 360,
                bgcolor: 'background.paper',
                position: 'relative',
                overflow: 'auto',
                maxHeight: 600,
                '& ul': {padding: 0},

            }}
            subheader={<li/>}
        >
            <Typography
                variant="h6"
                gutterBottom
                sx={{
                    // mt: 10,
                    textAlign: "center",
                    color: "grey"
                }}
            >
                всего заявлений: {myArray.length}
            </Typography>

            {
                // myArray.map((sectionId)=>{
                myArray.map((sectionId)=>{
                    // if(sectionId?.status !== "Approved")
                    return (
                        <li
                            key={`section-${sectionId?._id!}`}
                        >
                            {/* блокируем кнопку, если секунды не равны 0 или 20 */}
                            <Button
                                fullWidth
                                onClick={()=>{
                                    set_idStockReqContext(sectionId?._id!);
                                }}
                                disabled={timerActive}
                            >
                                {<ul>
                                    <Divider/>
                                    <ListSubheader>
                                        {/*<ListItem>*/}
                                        <ListItemText primary={`ID ${sectionId?._id!}`}/>
                                        {/*</ListItem>*/}
                                        {/*<Divider/>*/}
                                    </ListSubheader>
                                    {[
                                        getDate(sectionId?.date!),//0
                                        sectionId?.rejectionReason!,//1
                                        getStatus(sectionId?.status!),//2
                                        sectionId?.frontManager?._id?.toUpperCase(),//3
                                        sectionId?.userFullName,//4
                                        sectionId?.company,//5
                                        sectionId?.ticker,//6
                                        sectionId?.stockAmount,//7
                                        sectionId?.price,//8
                                        sectionId?.stockReqType,//9
                                    ].map((item, index)=>(

                                        <ListItem
                                            key={`item-${index}-${item}`}
                                            dense={true}
                                        >
                                            <ListItemText
                                                sx={{borderBottom: "1px solid grey", marginY: 0}}

                                                primary={
                                                    (()=>{
                                                        switch(index){
                                                            case 0:return "дата создания:"
                                                            case 1:return "Причина отклонения:"
                                                            case 2:return "Статус рассмотрения:"
                                                            case 3:return "ID сотрудника:"
                                                            case 4:return "ФИО клиента: "
                                                            case 5:return "компания: "
                                                            case 6:return "тиккер: "
                                                            case 7:return "Количество акций: "
                                                            case 8:return "Цена, предлагаемая клиентом: "
                                                            case 9:return "Buy / Sell: "
                                                        }
                                                    })() as string
                                                }
                                                secondary={item}
                                            />
                                            {/*</Grid>*/}

                                            {/*<Grid>*/}
                                            {/*    <ListItemText secondary={item}/>*/}
                                            {/*    </Grid>*/}

                                            {/*</Grid>*/}
                                        </ListItem>
                                    ))}

                                    {/*{"Open photo"}*/}
                                </ul>}
                            </Button>
                        </li>
                    )
                })
            }
        </List>
    );
}


// <Demo>
//     <List dense={dense}>
//         {ingredients.map((item)=> (
//             <ListItem key={item._id}>
//                 <ListItemText
//                     sx={{borderBottom: "1px solid grey",marginY:0}}
//                     primary={item.title}
//                     secondary={secondary ? item.amount+" "+item.unit : null}
//                 />
//             </ListItem>
//         ))}
//     </List>
// </Demo>
