import {Box, Button, ButtonGroup, Snackbar, Stack, TextField, Typography} from "@mui/material";
import React from "react";
import {styled} from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import {useAppDispatch} from "../../../../../hook";
import MuiAlert, {AlertProps} from '@mui/material/Alert';
import {TUser} from "../../../../../types/reestrOfAlias";
import LinearProgress, {LinearProgressProps} from '@mui/material/LinearProgress';
import {Context} from "../../../../../context";
import {notificationObject} from "../../../../../Helpers/notificationObject";
import {getStatus} from "../../../../../Helpers/getStatus";
import {patchStockRequestAT} from "../../../../../store/services/stockRequestSlice";
import {TPatchStockRequestAT} from "../../../../../types/typesStockRequest";

function LinearProgressWithLabel(props: LinearProgressProps & { value: number }) {
    return (
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Box sx={{ width: '100%', mr: 1 }}>
                <LinearProgress color="success" variant="determinate" {...props} />
            </Box>
            <Box sx={{ minWidth: 35 }}>
                <Typography variant="body2" color="text.secondary">{`${Math.round(
                    props.value,
                )}%`}</Typography>
            </Box>
        </Box>
    );
}

const Item = styled(Paper)(({theme})=>({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
){
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const patchStockRequests = (
    status: "Declined" | "Approved",
    _idStockRequest: string,
    rejectionReason = "none"
)=>{
    const patchObj:TPatchStockRequestAT = {
        status,
        _idStockRequest,
        rejectionReason,
    }
    return patchObj;
}

type TPropsRightSideForm = {
    seconds:number;
    userID: string | undefined;
    dataUser: TUser;
    // _idDepositReq: string;
    // timerActive:boolean;
}
//############################################################################################
const RightSideForm = ({
                           seconds,
                           userID,
                        }: TPropsRightSideForm)=>{

    const {_idStockReq, timerActive} = React.useContext(Context)

    const dispatch = useAppDispatch();
    // const oneAccRequest: TGetAccRequestsAT = useAppSelector(state=>state.reducerAccRequests.oneAccRequest);
    const [rejectionReason, setRejectionReason] = React.useState<string>("");
    const [progress, setProgress] = React.useState(0);

    React.useEffect(() => {
        const gradationArray = [
            100,95,90,85,80,75,70,65,60,55,50,45,40,35,30,25,20,15,10,5,0
        ]
        setProgress(gradationArray[seconds])
    }, [seconds]);
    //******************************************
    const [open, setOpen] = React.useState(false);
    const handleClick = ()=>{
        setOpen(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string)=>{
        if(reason === 'clickaway'){
            return;
        }
        setOpen(false);
    };
    //******************************************
    const inputChangeHandler: React.ChangeEventHandler<HTMLInputElement> = (e)=>{
        const value: string = e.target.value;
        setRejectionReason(value);
    };

    const submitFormHandler: React.FormEventHandler<HTMLFormElement> | undefined
        = (e)=>{
        e.preventDefault();
        // для Snackbar
        handleClick();

        // id сотрудника в объект передавать не надо, так как
        // на странице где отображается список заявок
        // он уже когда-то закрепил заявку за собой
        const innerObj: TPatchStockRequestAT = (!!rejectionReason)
            ? patchStockRequests("Declined", _idStockReq, rejectionReason)
            : patchStockRequests("Approved", _idStockReq);
            // {
            //     status: "Declined",//
            //     _idStockReq: _idStockReq,// _id заявки
            //     rejectionReason: rejectionReason,
            // }
            // :
            // {
            //     status: "Approved",
            //     _idStockReq: _idStockReq,// _id заявки
            //     rejectionReason: "none",
            // }
            // alert(JSON.stringify(innerObj, null, 2))
        dispatch(patchStockRequestAT(innerObj));

        const innerUserID = userID ?? "";

        if(!rejectionReason){
            // alert("Approved")
            dispatch(notificationObject(
                _idStockReq,
                innerUserID,//"63bba340a110a2541a878136",
                "Approved",
                "stockRequest",
                getStatus("Approved")
            ));
        }else{
            // alert("Declined")
            dispatch(notificationObject(
                _idStockReq,
                innerUserID,//"63bba340a110a2541a878136",
                "Declined",
                "stockRequest",
                getStatus("Declined") + ": " + rejectionReason
            ));
        }
    };
//############################################################################################
    return (
        <>
            {/*<pre>{"потом удалить id клиента"}</pre>*/}
            {/*<pre>{JSON.stringify(userID, null, 2)}</pre>*/}

            <Typography
                variant="h5"
                gutterBottom
                sx={{
                    m: 0,
                    p: 0,
                    textAlign: "center",
                    color: "grey"
                }}
            >
                {"мои заявления по акциям"}
            </Typography>
            <Typography
                variant="h5"
                gutterBottom
                sx={{textAlign: "center", color: "grey"}}
            >
                {"ID заявления на перевод:"}
            </Typography>
            <Typography
                variant="h6"
                gutterBottom
                sx={{textAlign: "center", color: "grey"}}
            >
                {_idStockReq?.toUpperCase()}
            </Typography>
            <Box sx={{
                marginX: "auto",
                maxWidth: 650,
                padding: 5,
                // paddingTop: 30,
                // paddingBottom: 30,
                boxShadow: "0 18px 30px 0 rgba(0, 0, 0, 0.6)",
                // width: '100%',
                // display: 'flex',
                // alignItems: "center",
                // justifyContent: "center",
            }}>

                <form onSubmit={submitFormHandler}>
                    <Stack spacing={2}>
                        <Item>
                            <TextField
                                multiline
                                rows={4}
                                id={"reason"}
                                name={"reason"}
                                label={"указать причину отклонения при отказе в подтверждении заявки."}
                                variant="outlined"
                                type={"text"}
                                value={rejectionReason}
                                // required={true}
                                onChange={inputChangeHandler}
                                fullWidth
                            />
                        </Item>
                        <Item>
                            <ButtonGroup
                                variant="contained"
                                fullWidth
                                aria-label="outlined primary button group"
                            >
                                <Button
                                    type="submit"
                                    disabled={!!rejectionReason}
                                >
                                    принять
                                </Button>
                                <Button
                                    type="submit"
                                    disabled={!rejectionReason}
                                >
                                    отклонить
                                </Button>

                            </ButtonGroup>
                        </Item>
                    </Stack>
                </form>
                {timerActive
                && <LinearProgressWithLabel value={progress}/>}
            </Box>

            {/*<Box sx={{ width: '100%' }}>*/}
            {/*    <LinearProgressWithLabel value={progress} />*/}
            {/*</Box>*/}
            {/*<pre>{JSON.stringify(oneAccount, null, 2)}</pre>*/}
            {/*<pre>{JSON.stringify(dataUser, null, 2)}</pre>*/}

            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={!rejectionReason ? "success" : "error"} sx={{width: '100%'}}>
                    {!rejectionReason ? "Вы одобрили заявление клиента!" : "Вы отклонили заявление клиента"}
                </Alert>
            </Snackbar>

        </>
    )
}

export default RightSideForm;