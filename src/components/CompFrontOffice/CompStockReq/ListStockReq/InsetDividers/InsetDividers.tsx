// import * as React from 'react';
// import List from '@mui/material/List';
// import ListItem from '@mui/material/ListItem';
// import ListItemText from '@mui/material/ListItemText';
// import Divider from '@mui/material/Divider';
// import {TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import {Button, Divider, Grid, List, ListItemText, Snackbar, Typography} from "@mui/material";
import {patchAccRequestAT} from "../../../../../store/services/accRequestSlice";
import {useAppDispatch, useAppSelector} from "../../../../../hook";
import {TStateStatus, TUser} from "../../../../../types/reestrOfAlias";
import {getDate} from "../../../../../Helpers/getDate";
import MuiAlert, {AlertProps} from "@mui/material/Alert";
import {TTotalPickTable} from "../../../../../containers/FrontOfficeModern/DepositRequest/FrontOfficeDepositListReq";
import {TGetAccRequestsAT} from "../../../../../types/typesAccReq";
import {ListItem} from "@material-ui/core";
import React from "react";
import {getStatus} from "../../../../../Helpers/getStatus";
import {patchDepositRequestAT} from "../../../../../store/services/depositRequestSlice";
import {TGetPostDepositRequestAT} from "../../../../../types/typesDepositRequest";
import {TGetPostTransferReqAT} from "../../../../../types/typesTransferRequest";
import {patchTransferReqAT} from "../../../../../store/services/transferRequestSlice";
import {notificationObject} from "../../../../../Helpers/notificationObject";
import {TGetPostStockRequestAT} from "../../../../../types/typesStockRequest";
import { patchStockRequestAT } from "../../../../../store/services/stockRequestSlice";

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
){
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function InsetDividers(oneStockRequest: TGetPostStockRequestAT){
    const dispatch = useAppDispatch();
    const dataUser: TUser = useAppSelector(state=>state.reducerUsers.user);

    const {
        // _id, date, rejectionReason, status, amount, senderIIN, recipientIIN, senderIIK, recipientIIK,
        // senderBIK, recipientBIK, senderBank, recipientBank, senderKBe, recipientKBe, senderFullName,
        // recipientFullName,user
        _id, user, status, frontManager, rejectionReason, date, userFullName,
        company, ticker, stockAmount, price, stockReqType, stockResult, stockFail, priceResult,
    } = oneStockRequest!

    //******************************************
    const [open, setOpen] = React.useState(false);
    const handleClick = ()=>{
        setOpen(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string)=>{
        if(reason === 'clickaway'){
            return;
        }
        setOpen(false);
    };
    //******************************************
    return (
        <Grid
            container
            direction="column"
            justifyContent="space-between"
            // alignItems="stretch"
            sx={{
                bgcolor: '#cfe8fc',
                height: '100%',
                // height: '77vh',
            }}
        >
            {/*<pre>{JSON.stringify(user?._id, null, 2)}</pre>*/}
            {/*<pre>{JSON.stringify(concatOneAccOneDepos, null, 2)}</pre>*/}
            {/*{Object.entries(concatOneAccOneDepos as object).length ? <>*/}
            {!!oneStockRequest ? <>
                    <Grid item xs>
                        <List
                            sx={{
                                width: '100%',
                                bgcolor: 'background.paper',
                                overflow: 'auto',
                                // maxHeight: 400,
                                // height: '100%',
                                height: '70vh',
                            }}
                        >

                            {/*["_id", "ID заявления"],*/}
                            {/*["date", "дата созд."],*/}
                            {/*["rejectionReason", "Причина откл."],*/}
                            {/*["status", "Статус рассмотрения"],*/}
                            {/*["frontManager", "ID сотрудника"],*/}

                            {/*["userFullName", "ФИО клиента"],*/}
                            {/*["company", "компания"],*/}
                            {/*["ticker", "тиккер"],*/}
                            {/*["stockAmount", "Количество акций"],*/}
                            {/*["price", "Цена, предлагаемая клиентом"],*/}
                            {/*["stockReqType", "Buy / Sell"],*/}


                            <ListItem>
                                <ListItemText primary='ID заявления:' secondary={_id?.toUpperCase()}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='ID сотрудника:' secondary={frontManager?._id?.toUpperCase()}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='Статус рассмотрения:' secondary={getStatus(status!)}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='дата создания:' secondary={getDate(date!)}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary='Причина отклонения:' secondary={rejectionReason}/>
                            </ListItem>

                            <ListItem>
                                <ListItemText primary="ФИО клиента: " secondary={userFullName}/>
                            </ListItem>
                            <ListItem>
                                <ListItemText primary="компания: " secondary={company}/>
                            </ListItem>
                            <ListItem>
                                <ListItemText primary="тиккер: " secondary={ticker}/>
                            </ListItem>
                            <ListItem>
                                <ListItemText primary="Количество акций: " secondary={stockAmount}/>
                            </ListItem>
                            <ListItem>
                                <ListItemText primary="Цена, предлагаемая клиентом: " secondary={price}/>
                            </ListItem>
                            <ListItem>
                                <ListItemText primary="Buy / Sell: " secondary={stockReqType}/>
                            </ListItem>


                        </List>
                    </Grid>
                    <Grid item xs>
                        {
                            // (gridSize>11 ) &&
                            <Button
                                sx={{height: '100%'}}
                                onClick={()=>{
                                    dispatch(patchStockRequestAT({
                                        status: "FrontWIP",// status: "Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted"
                                        _idStockRequest: _id,// _id заявки
                                        frontManager: dataUser?._id!// _id пользователя
                                    }))
                                    // <<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>
                                    // уведомления по трейдингу
                                    dispatch(notificationObject(
                                        _id!,
                                        user?._id!,
                                        "FrontWIP",
                                        "stockRequest",
                                        getStatus("FrontWIP")
                                    ));
                                    // <<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>

                                    // для Snackbar
                                    handleClick()
                                }}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                disabled={
                                    // "Pending" | "FrontWIP" | "TraderWIP" | "Approved" | "Declined" | "Completed" | "PartiallyCompleted"
                                    status === "FrontWIP" ||
                                    status === "TraderWIP" ||
                                    status === "Approved" ||
                                    status === "Declined" ||
                                    status === "Completed" ||
                                    status === "PartiallyCompleted"
                                }
                            >
                                {"Взять в работу"}
                            </Button>}
                    </Grid>
                </>
                :
                <Grid item xs>
                    <Typography
                        variant="h6"
                        gutterBottom
                        sx={{
                            // mt: 10,
                            textAlign: "center",
                            color: "grey"
                        }}
                    >
                        {"Выберите заявление для работы"}
                    </Typography>
                </Grid>

            }

            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={"success"} sx={{width: '100%'}}>
                    {"Заявление клиента закреплено за вами!"}
                </Alert>
            </Snackbar>
        </Grid>

    )
}
